require 'yaml'
require_relative 'errors'

# Config
APP_CONFIG = YAML.load_file('config/config.yml')
DB_CONFIG = YAML.load_file('config/database.yml')
APP_INFO = YAML.load_file('config/info.yml')

# Setup environment dengan default adalah development
ENV['RACK_ENV'] ||= 'development'

class Hash

  def deep_symbolize_keys
    dup.deep_symbolize_keys!
  end unless method_defined? :deep_symbolize_keys

  def deep_symbolize_keys!
    keys.each do |key|
      value = delete(key)
      # symbolize each hash
      value.deep_symbolize_keys! if value.is_a? Hash
      # symbolize each hash inside an array
      value.each{ |v| v.deep_symbolize_keys! if v.is_a? Hash } if value.is_a? Array
      self[(key.to_sym rescue key) || key] = value
    end
    self
  end unless method_defined? :deep_symbolize_keys!

end
