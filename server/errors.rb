#
# Class untuk error otentikasi (belum login).
#
class Unauthenticated < Exception
  def initialize(msg = 'Anda harus login terlebih dahulu')
    super msg
  end
end

#
# Class untuk error otorisasi (tidak diijinkan).
#
class Unauthorized < Exception
  def initialize(msg = 'Anda tidak punya otoritas untuk mengakses data ini')
    super msg
  end
end
