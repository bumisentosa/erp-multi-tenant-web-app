require './server/app'

class App < Route
  get '/help' do
    content_type :html
    erb :help
  end

  get '/ketentuan' do
    content_type :html
    erb :ketentuan
  end

  get '/register' do
    content_type :html
    erb :register
  end
end
