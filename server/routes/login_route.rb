require './server/app'
require './server/models/auth'

class App < Route
  get '/login' do
    @title = 'Login'
    @stylesheets = ['/css/bootstrap.css', '/css/login.css']
    content_type :html
    erb :login
  end

  post '/login' do
    token = Auth.login(params['username'], params['password'])
    unless token
      flash[:username] = params['username']
      flash[:error] = 'Nama pengguna dan atau kata sandi salah!'
      return redirect back
    end

    if params['remember']
      # Remember user yang login
      max_age = APP_CONFIG['login_remember_max_age']
      response.set_cookie 'token', value: token, max_age: max_age.to_s
    else
      # Set session cookie
      response.set_cookie 'token', token
    end

    if params['next']
      redirect params['next']
    else
      redirect to('/')
    end
  end

  get '/reset-password' do
    content_type :html
    erb :reset_password
  end

  get '/logout' do
    Auth.logout current_token
    response.set_cookie 'token', ''
    flash[:message] = 'Berhasil logout'
    redirect to('/login')
  end
end
