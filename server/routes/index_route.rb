require './server/app'

class App < Route
  get '/' do
    @stylesheets = ['/css/bootstrap.css', '/css/home.css']
    content_type :html
    erb :index
  end
end
