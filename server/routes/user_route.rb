require './server/app'
require './server/models/user_profile'

class App < Route
  get '/users/:username/profile' do |username|
    username = normalize_username(username)
    raise Unauthorized unless Auth.allowed?(username, :users, :me, :profile)

    json UserProfile.get(username)
  end
end
