require 'cgi'
require 'sinatra/base'
require 'sinatra/contrib'
require 'rack-flash'
require 'json'

require './server/helpers/html_helper'
require './server/models/auth'

#
# Base route. Class ini ditujukan untuk diwariskan ke class lain.
#
class Route < Sinatra::Base
  include HtmlHelper
  register Sinatra::Contrib
  use Rack::Flash
  enable :sessions

  set :json_encoder, :to_json
  set :root, File.expand_path('../', File.dirname(__FILE__))
  set :public_folder, File.expand_path(settings.production? ? '../dist' : '../client', settings.root)
  set :show_exceptions, false

  configure :production do
    set :dump_errors, false
  end

  #
  # Set default content_type dan variable untuk view (jika diperlukan).
  #
  before do
    content_type :json
    @description = APP_INFO['product']['description']
  end

  #
  # Cek otentikasi.
  #
  before do
    return if public_path?(request.path_info) # abaikan jika public path
    token = request.cookies['token']
    raise Unauthenticated unless Auth.logged_in?(token)
  end

  #
  # Halaman tidak ditemukan.
  #
  not_found do
    content_type :text
    'yah...!, halaman yang anda minta tidak ada! :('
  end

  #
  # Terdapat error.
  #
  error do |error|
    content_type :text
    if error.is_a? Sequel::DatabaseError
      error.message
    else
      'yah...!, terjadi kesalahan pada server! :('
    end
  end

  #
  # Error otentikasi (belum login).
  #
  error Unauthenticated do |error|
    # Jika ini adalah halaman maka redirect ke login, jika tidak maka
    # kirim header 401
    if page_path?(request.path_info)
      redirect_to_login
    else
      halt 401, error.message
    end
  end

  #
  # Error otorisasi (tidak diijinkan).
  #
  error Unauthorized do |error|
    halt 403, error.message
  end

  #
  # Redirect ke halaman login.
  #
  def redirect_to_login(message = nil)
    message = 'Anda harus masuk terlebih dahulu!' unless message
    flash[:error] = message unless message == ''
    next_url = "?next=#{CGI.escape(request.fullpath)}" if request.fullpath != '/'
    redirect "/login#{next_url}"
  end

  #
  # Return true jika path ini ditentukan sebagai public.
  #
  def public_path?(path_info)
    false
  end

  #
  # Return true jika path ini ditentukan sebagai halaman (bukan api).
  # Ini dibedakan karena behavior untuk halaman dan api sedikit berbeda.
  #
  def page_path?(path_info)
    false
  end

  #
  # Normalkan nama pengguna, jika nama pengguna adalah "me" maka nama pengguna
  # ini akan diambil dari token user yang sudah login.
  #
  def normalize_username(username)
    if username == 'me'
      token = current_token
      username = Auth.extract_token(token)[:username]
    end
    username
  end

  #
  # Token saat ini.
  #
  def current_token()
    request.cookies['token']
  end

  #
  # User yang login saat ini.
  #
  def current_user()
    Auth.extract_token(current_token)[:username]
  end

  #
  # Get tenant dari user.
  #
  def get_tenant(username)
    user = User.get(username)
    return Tenant.get(user[:tenant_id]) if user
  end

end
