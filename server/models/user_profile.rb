require './server/models/base_model'

class UserProfile < Model
  view_name :v_user_profiles

  columns do |c|
    c.string :username, primary_key: true
    c.string :display_name, presence: true, label: 'Nama'
    c.string :photo_url
  end
end
