require './server/models/base_model'

class UserRule < Model
  columns do |c|
    c.integer :rule_id, primary_key: true
    c.integer :role_id
    c.string  :rule, presence: true, label: 'Rule'
  end
end
