require './server/models/base_model'
require './server/models/auth'

class Tenant < Model
  columns do |c|
    c.integer :tenant_id, primary_key: true
    c.string  :name, presence: true, label: 'Nama'
    c.string  :address, presence: true, label: 'Alamat'
    c.string  :city
    c.string  :region
    c.string  :country
    c.string  :phone_number, presence: true, label: 'No. Telepon'
    c.string  :email, presence: true, email: true, label: 'Email'
    c.string  :contact_name

    c.string  :db_name, presence: true
    c.string  :db_host, presence: true
    c.string  :db_port, presence: true
    c.string  :db_user, presence: true
    c.string  :db_password, presence: true, max_length: 16
  end

  def self.justify_data(id, options)
    data = super(id, options)
    if data && data[:db_password]
      data[:db_password] = Auth.encrypt(data[:db_password])
    end
    data
  end
end
