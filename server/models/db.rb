require 'sequel'
require './server/helpers/encryption_helper'

# Setup sequel
Sequel.extension :core_extensions
Sequel.extension :inflector

# Class untuk membuka database.
#
# Setiap user berada pada tenant tertentu, dan setiap tenant memiliki database
# tersendiri. Disini akan berusaha mendapatkan database yang sesuai dengan
# tenant dari user itu (user didapat melalui token).
# Jika token ditentukan maka akan digunakan database default.
class DB
  extend EncryptionHelper

  # Buka database.
  def self.open_database(*args, &block)
    options = args[0] || {};

    # Jika melewatkan database langsung digunakan
    if options[:db]
      return options[:db] unless block_given?
      return block.call options[:db]
    end

    db_config = nil
    if options[:token]
      # Jika ini adalah token, maka database dipilih sesuai dengan di tenant
      # mana user(token) itu berada
      db_config = db_config_specified(options[:token])
    else
      # Gunakan default database
      db_config = db_config_main
    end
    raise Sequel::DatabaseConnectionError unless db_config

    if block_given?
      # Open database dengan blok
      Sequel.mysql2(db_config) do |db|
        block_result = block.call(db)
        db.disconnect # disconnect setelah block dipanggil
        block_result
      end
    else
      Sequel.mysql2(db_config)
    end
  end

  # Konfigurase database utama.
  def self.db_config_main
    DB_CONFIG[ENV['RACK_ENV']]
  end

  # Konfigurasi database menyesuaikan dengan di tenant mana user ini berada.
  def self.db_config_specified(token)
    tenant = get_tenant(token)
    if tenant
      { database: tenant[:db_name],
        user:     tenant[:db_user],
        password: decrypt(tenant[:db_password]),
        host:     tenant[:db_host],
        port:     tenant[:db_port] }
    end
  end

  # Get data tenant berdasarkan token.
  def self.get_tenant(token)
    tenant = nil
    Sequel.mysql2 db_config_main do |db|
      username = extract_token(token)[:username]
      db_tenants = db[:tenants].
                   select(:db_name, :db_host, :db_port, :db_user, :db_password).
                   join(:users, :users__tenant_id).
                   where(username: username)
      tenant = db_tenants.first
    end
    tenant
  end

  # Generate dataset.
  #
  # Options:
  #   :page      - Nomor halaman, nil untuk mengambil semua data
  #   :limit     - (default: 20) Banyaknya row per halaman
  #   :columns   - Kolom/field mana saja yang di-select
  #   :filter    - Filter pencarian data
  #   :order     - Pengurutan
  def self.get_dataset(db, name, options = {})
    dataset = db[name]
    dataset = dataset.select(*options[:columns]) if options[:columns]
    dataset = dataset.where(options[:filter]) if options[:filter]
    dataset = dataset.order(*options[:order]) if options[:order]

    if options[:page] || options[:limit]
      options = { page: 1, limit: 20 }.merge(options)
      dataset = dataset.extension :pagination
      dataset = dataset.paginate(options[:page], options[:limit])
    end

    dataset
  end
end
