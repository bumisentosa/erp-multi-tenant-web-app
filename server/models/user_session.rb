require './server/models/base_model'

class UserSession < Model
  columns do |c|
    c.string :username, primary_key: true
    c.string :token
  end
end
