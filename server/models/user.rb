require './server/models/base_model'
require './server/models/auth'

class User < Model
  columns do |c|
    c.string  :username, primary_key: true, label: 'Nama Pengguna'
    c.string  :password, presence: true, password: true, min_length: 6, max_length: 16, label: 'Kata Sandi'
    c.integer :role_id
    c.integer :tenant_id
  end

  def self.justify_data(id, options)
    data = super(id, options)
    if data && data[:password]
      data[:password] = Auth.create_password_hash(data[:username], data[:password])
    end
    data
  end
end
