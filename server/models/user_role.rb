require './server/models/base_model'

class UserRole < Model
  columns do |c|
    c.integer :role_id, primary_key: true
    c.string  :role, presence: true, label: 'Role'
  end
end
