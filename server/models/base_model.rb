require 'sequel'
require 'event_emitter'

require './server/helpers/validation_helper'
require './server/models/db'

#
# Base model untuk komunikasi data dengan database. Class ini ditujukan untuk
# diwariskan ke class lain.
#
class Model
  extend ValidationHelper

  #
  # Method untuk men-set nama field/kolom.
  #
  def self.column(column, options = {})
    @columns = {} unless @columns
    @columns[column] = options
    @primary_key = column if options[:primary_key]
  end

  #
  # Block columns.
  #
  def self.columns
    yield self
  end

  #
  # Method missing.
  # string(), text(), integer(), decimal(), date(), datetime(), boolean()
  #
  def self.method_missing(method_name, *args)
    if [:string, :text, :integer, :decimal, :date, :datetime, :boolean].include? method_name
      options = { type: method_name }
      options = options.merge(args[1]) if args.length >= 2
      self.column args[0], options
    end
  end

  #
  # Get tabel data.
  # Return data berupa array jika yang diambil semua data,
  # atau hash berupa {:data, :page} jika diambil per halaman.
  #
  # Options:
  #   :page      - Nomor halaman, nil untuk mengambil semua data
  #   :limit     - (default: 20) Banyaknya row per halaman
  #   :columns   - Kolom/field mana saja yang di-select
  #   :filter    - Filter pencarian data
  #   :order     - Pengurutan
  #   :db        - Sequel::Database
  #   :token     - Token user yang login
  #
  def self.select(options = {})
    is_paged = options[:page]

    data, page = nil, nil
    DB.open_database(options) do |db|
      view_name = @view_name || @table_name
      dataset = DB.get_dataset(db, view_name, options)
      data = dataset.all
      data.map! do |item|
        item.each do |column, value|
          # BigDecimal tidak di-print dengan bagus jika di convert ke json
          item[column] = value.to_f if value.is_a? BigDecimal
        end
        item
      end
      page = {
        count:   dataset.page_count,
        current: dataset.current_page,
        next:    dataset.next_page,
        prev:    dataset.prev_page
      } if is_paged
    end

    self.emit :select, data, page if data
    is_paged ? { data: data, page: page } : data
  end

  #
  # Syntatic-sugar untuk select data dengan filter.
  #
  def self.filter(filter, options = {})
    options[:filter] = filter
    select options
  end

  #
  # Get data berdasarkan id tabel.
  #
  # Options:
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.get(id, options = {})
    data = nil
    DB.open_database(options) do |db|
      view_name = @view_name || @table_name
      dataset = DB.get_dataset(db, view_name, options)
      data = dataset.where(@primary_key => id).first
      data.each do |column, value|
        data[column] = value.to_f if value.is_a? BigDecimal
      end
    end

    self.emit :get, data if data
    data
  end

  #
  # Insert data baru.
  #
  # Options:
  #   :data  - (required) Data yang di-insert
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.insert(options = {})
    # Validasi sebelum disimpan
    error = get_error(@columns, options[:data])
    raise Sequel::ConstraintViolation, error[:message] if error

    result = nil
    DB.open_database(options) do |db|
      data = justify_data(nil, options)
      dataset = DB.get_dataset(db, @table_name, options)
      result = dataset.insert(data)
    end

    self.emit :insert, result if result
    result
  end

  #
  # Update data.
  #
  # Options:
  #   :data  - (required) Data yang di-update
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.update(id, options = {})
    # Validasi sebelum disimpan
    error = get_error(@column, options[:data], partial_column: true)
    raise Sequel::ConstraintViolation, error.message if error

    result = nil
    DB.open_database(options) do |db|
      data = justify_data(id, options)
      dataset = DB.get_dataset(db, @table_name, options)
      result = dataset.where(@primary_key => id).update(data)
    end

    self.emit :update, result if result >= 1
    result
  end

  #
  # Update data per kolom.
  #
  # Options:
  #   :data  - (required) Data yang di-update
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.update_per(column, column_id, options = {})
    data = options[:data].kind_of?(Array) ? options[:data] : [options[:data]]

    # validasi
    data.each do |item|
      error = get_error(@columns, item, options)
      raise Sequel::ConstraintViolation, error[:message] if error
    end

    result = []
    ids = data.map{ |item| item[@primary_key] }.compact
    DB.open_database(options) do |db|
      db.transaction do
        dataset = db[@table_name]
        # Hapus data yang tidak ada
        dataset.where(column => column_id).exclude(@primary_key => ids).delete

        # Insert/update data
        result = data.inject([]) do |items, item|
          item_options = options
          item_options[:data] = item
          item = justify_data(nil, item_options)
          if item[@primary_key]
            row_count = dataset.where(@primary_key => item[@primary_key]).update(item)
            dataset.insert(item) if row_count === 0
          else
            item[@primary_key] = dataset.insert(item)
          end
          items << item
        end
      end
    end
    result
  end

  #
  # Hapus data.
  #
  # Options:
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.delete(id, options = {})
    result = nil
    DB.open_database(options) do |db|
      dataset = DB.get_dataset(db, @table_name, options)
      result = dataset.where(@primary_key => id).delete
    end

    self.emit :delete, result if result
    result
  end

  #
  # Hapus semua data.
  #
  # Options:
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  def self.delete_all!(options = {})
    result = nil
    DB.open_database(options) do |db|
      dataset = DB.get_dataset(db, @table_name, options)
      result = dataset.delete
    end

    self.emit :delete, result if result
    result
  end

  #
  # Truncate tabel.
  #
  # Options:
  #   :db    - Sequel::Database
  #   :token - Token user yang login
  #
  # Warning: Pada MySQL 5.5, tabel tidak dapat di-truncate jika mempunyai
  # relasi dengan tabel lain.
  #
  def self.truncate!(options = {})
    result = nil
    DB.open_database(options) do |db|
      dataset = DB.get_dataset(db, @table_name, options)
      result = dataset.truncate
    end

    self.emit :truncate, result if result
    result
  end

  #
  # Saat diwariskan ke class lain.
  #
  def self.inherited(subclass)
    className = subclass.to_s.split('::').last
    tableized = className.tableize.to_sym

    # Set default nama primary key dan table_name
    subclass.primary_key "#{className.underscore}_id".to_sym
    subclass.table_name tableized

    # Apply juga event emitter untuk subclass
    EventEmitter.apply subclass
  end

  #
  # Edit data sebelum disimpan.
  # Ini berguna untuk menyingkirkan column yang tidak ada pada database dan
  # juga meng-encrypt data sebelum disimpan misalnya.
  #
  def self.justify_data(id, options)
    # Bersihkan column yang tidak ditentukan
    data = options[:data]
    data.each do |key, value|
      if @columns.has_key? key
        column = @columns[key]
        data[key] = BigDecimal(value.to_s) if column[:type] === :decimal
        data[key] = nil if value === '' && (column[:type] === :date || column[:type] === :datetime)
        data[key] = nil if value === '' && !column[:presence]
      else
        data.delete key
      end
    end
    data
  end

  protected

  #
  # Set nama primary key.
  #
  def self.primary_key(value = nil)
    @primary_key = value if value
    @primary_key
  end

  #
  # Set nama tabel.
  #
  def self.table_name(value = nil)
    @table_name = value if value
    @table_name
  end

  #
  # Set nama view.
  #
  def self.view_name(value = nil)
    @view_name = value if value
    @view_name
  end

end
