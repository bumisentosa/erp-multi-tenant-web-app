require './server/helpers/encryption_helper'
require './server/models/db'

# Class untuk otentikasi dan otorisasi.
class Auth
  extend EncryptionHelper

  # Login berdasarkan username dan password. Jika berhasil login, maka akan
  # memberikan nilai kembalian berupa token.
  def self.login(username, password)
    token = nil

    DB.open_database do |db|
      # Select tabel user
      password = create_password_hash(username, password)
      db_users = db[:users].where(username: username, password: password)

      if db_users.count == 1
        # Simpan token ke tabel user_session
        token = create_token(username, password)
        db_user_sessions = db[:user_sessions].where(username: username)
        if db_user_sessions.count >= 1
          db_user_sessions.update(token: token)
        else
          db_user_sessions.insert(username: username, token: token)
        end
      end
    end

    token
  end

  # Logout.
  def self.logout(token)
    DB.open_database do |db|
      # Hapus dari user_session
      db[:user_sessions].where(token: token).delete
    end
  end

  # Cek apakah token masih valid/tidak.
  def self.logged_in?(token)
    return false if !token || token.empty?

    logged_in = false
    username = extract_token(token)[:username]
    DB.open_database do |db|
      db_user_sessions = db[:user_sessions].where(username: username, token: token)
      logged_in = db_user_sessions.count == 1
    end
    logged_in
  end

  # Cek apakah user dapat mengakses resource yang diminta.
  def self.allowed?(username, *resources)
    resource = resources.join('.')
    rules = get_rules(username)

    rules.each do |rule|
      if rule == 'all'
        return true
      elsif rule.end_with? '.all'
        return true if resource.start_with? rule.chomp('.all')
      else
        return true if rule == resource
      end
    end
    return false
  end

  # Get daftar rules dari user ini.
  def self.get_rules(username)
    rules = []
    DB.open_database do |db|
      db_rules = db[:users].select(:rule).
                 join(:user_roles, :users__role_id).
                 join(:user_rules, :user_rules__role_id).
                 where(users__username: username)
      items = db_rules.all
      items.each { |item| rules << item[:rule] }
    end
    rules
  end
end
