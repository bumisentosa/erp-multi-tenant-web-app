# ============================================================================
#
# ENTERPRISE RESOURCE PLANNING
#
# ============================================================================

require './server/routes/base_route'

class App < Route
  set :root, File.dirname(__FILE__)

  def public_path?(path_info)
    path = ['/login', '/reset-password', '/register', '/help', '/ketentuan']
    path.include? path_info
  end

  def page_path?(path_info)
    path_info == '/'
  end
end
