#
# Helper untuk view html, seperti menambahkan title, description, stylesheets,
# dan script.
#
module HtmlHelper
  attr_accessor :title, :stylesheets, :scripts

  #
  # Get title untuk tag title html.
  #
  def title
    product_name = APP_INFO['product']['name']
    @title ? "#{@title} - #{product_name}" : product_name
  end

  #
  # Get description untuk tag meta.
  #
  def description
    @description = APP_INFO['product']['description'] unless @description
    @description
  end

  #
  # Generate tag html stylesheet.
  #   <link rel="stylesheet" href="style1.css">
  #   <link rel="stylesheet" href="style2.css">
  #   ...
  #
  def stylesheets
    html = ''
    @stylesheets && @stylesheets.each do |stylesheet|
      rel = stylesheet.end_with?('.less') ? 'stylesheet/less' : 'stylesheet'
      html += '<link rel="' + rel + '" href="' + stylesheet + '">'
    end
    html
  end

  #
  # Generate tag html script.
  #   <script type="text/javascript" src="script1.js"></script>
  #   <script type="text/javascript" src="script2.js"></script>
  #   ...
  #
  def scripts
    html = ''
    @scripts && @scripts.each do |script|
      html += '<script type="text/javascript" src="' + script + '"></script>'
    end
    html
  end

  #
  # Nama-nama folder didalam path yang ditentukan.
  #
  def folder_name(path, options)
    entries = []
    excepts = options[:excepts] || []
    excepts = [excepts] unless excepts.kind_of? Array
    excepts << '.'
    excepts << '..'

    Dir.entries(path).select do |entry|
      if !excepts.include?(entry) && File.directory?(File.join(path, entry))
          entries << entry
      end
    end
    entries
  end

  #
  # Daftar dari file pada direktori yang ditentukan.
  #
  def files_in(root_dir, search_path)
    result = []
    files = Dir.glob("#{root_dir}#{search_path}")

    # Urutkan file
    files = files.map { |file| [file.count('/'), file] }
    files = files.sort.map { |file| file[1] }

    # Tambahkan file
    files.each do |file|
      file.slice! root_dir
      result << file
    end

    result
  end

end
