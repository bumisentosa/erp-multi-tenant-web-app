require 'date'
require 'encryptor' # Enkripsi untuk token
require 'pbkdf2'    # Enkripsi 1 arah untuk password

#
# Setup default options untuk enkripsi
#
Encryptor.default_options.merge!(
  algorithm: 'aes-256-cbc',
  key: APP_CONFIG['secret_key']
)

#
# Helper untuk encrypt dan decrypt data, encrypt password satu arah, serta
# membuat dan meng-extract token.
#
module EncryptionHelper

  #
  # Encrypt data.
  #
  def encrypt(value, options = {})
    options = { hex: true }.merge(options)

    result = Encryptor.encrypt(value, options)
    result = bin_to_hex(result) if options[:hex]
    result
  end

  #
  # Encrypt menggunakan PBKDF2 (Password-Based Key Derivation Function 2).
  #
  def pbkdf2(value, options = {})
    options = { hex: true }.merge(options)

    secret = PBKDF2.new do |p|
      p.password = value
      p.salt = APP_CONFIG['secret_key']
      p.iterations = 10000
    end
    options[:hex] ? secret.hex_string : secret.bin_string
  end

  #
  # Decrypt data.
  #
  def decrypt(value, options = {})
    options = { hex: true }.merge(options)

    value = hex_to_bin(value) if options[:hex]
    Encryptor.decrypt(value, options)
  end

  #
  # Buat token komunikasi data dengan client.
  #
  def create_token(username, password_hash)
    password_bin = hex_to_bin(password_hash)
    date = Date.today.strftime('%y%m%d')
    encrypt "#{username}::#{password_bin}::#{date}"
  end

  #
  # Extract token dan return username, password, dan date.
  #
  def extract_token(token)
    result = { username: nil, password: nil, date: nil }
    return result unless token

    decrypted_vaue = decrypt(token).split('::')
    return result unless decrypted_vaue.length == 3

    result[:username] = decrypted_vaue[0]
    result[:password] = bin_to_hex(decrypted_vaue[1])
    result[:date]     = Date.parse(decrypted_vaue[2])

    result
  end

  #
  # Buat password hash untuk disimpan di tabel user.
  #
  def create_password_hash(username, password)
    pbkdf2 "#{username}.#{password}"
  end

  #
  # Convert karakter binary ke hexadecimal.
  #
  def bin_to_hex(value)
    value.unpack('H*').first
  end

  #
  # Convert karakter hexadecimal ke binary.
  #
  def hex_to_bin(value)
    value.scan(/../).map { |x| x.hex }.pack('c*')
  end

end
