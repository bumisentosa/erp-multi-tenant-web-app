root_dir = File.dirname(__FILE__)

require_relative 'global'

# Require semua file pada folder listeners
Dir["#{root_dir}/listeners/**/*.rb"].each do |file|
  require file
end

# Require routes
require_relative 'routes/login_route'
require_relative 'routes/index_route'
require_relative 'routes/help_route'
require_relative 'routes/user_route'

# Require modules
dirname = "#{root_dir}/modules"
Dir.entries(dirname).select do |entry|
  if entry != '.' && entry != '..' && File.directory?(File.join(dirname, entry))
    require_relative "modules/#{entry}/init"
  end
end
