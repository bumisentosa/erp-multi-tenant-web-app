require './server/modules/frm/app'

module FRM
  class App < Route
    get '/' do
      content_type :html
      erb :index
    end
  end
end
