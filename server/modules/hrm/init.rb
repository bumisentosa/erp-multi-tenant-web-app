relative_dir = File.dirname(__FILE__)

# Require semua file pada folder listeners
Dir["#{relative_dir}/listeners/**/*.rb"].each do |file|
  require file
end

# Require routes
require_relative 'routes/index_route'

require_relative 'routes/bank_route'
require_relative 'routes/location_route'
require_relative 'routes/position_route'

require_relative 'routes/project_group_route'
require_relative 'routes/project_type_route'
require_relative 'routes/project_route'

require_relative 'routes/letter_type_route'
require_relative 'routes/letter_route'

require_relative 'routes/driver_license_route'
require_relative 'routes/religion_route'
require_relative 'routes/education_route'
require_relative 'routes/competence_route'
require_relative 'routes/social_security_route'
require_relative 'routes/employee_route'
