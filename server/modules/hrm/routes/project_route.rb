require './server/modules/hrm/app'
require './server/modules/hrm/models/project'

module HRM
  class App < Route

    #
    # Get data proyek dari database beserta addendumnya.
    #
    def get_project(id, db)
      project = HRM::Project.get(id, db: db)
      options = { filter: { project_id: id }, db: db }
      project[:locations] = HRM::ProjectLocation.select(options.merge({ order: :location_sort_asc }))
      project[:addendums] = HRM::ProjectAddendum.select(options.merge({ order: :addendum_date }))
      project
    end

    #
    # Filter data request child items sehingga mengembalikan data valid saja.
    #
    def filter_req_project_items(project_id, primary_fields, data)
      return nil unless data
      data.inject([]) do |items, item|
        item.symbolize_keys!
        valid = true
        primary_fields.each do |field|
          if !item[field]
            valid = false
            break
          end
        end
        if valid
          item[:project_id] = project_id
          items << item
        end
        items
      end
    end

    #
    # GET /projects/:id
    #
    get '/projects/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :read)

      DB.open_database(token: current_token) do |db|
        if id
          json get_project(id, db)
        else
          query_params = { order: [:group_name, :sort_asc], db: db }
          if params['page']
            query_params[:page] = params['page'].to_i
            params.delete 'page'
          end
          query_params[:filter] = params.symbolize_keys
          json HRM::Project.select(query_params)
        end
      end
    end

    #
    # POST, PUT /projects/:id
    #
    route :post, :put, '/projects/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, method)

      DB.open_database(token: current_token) do |db|
        db.transaction do
          # data
          data = JSON.parse(request.body.read).deep_symbolize_keys

          # child items
          locations = data[:locations]
          addendums = data[:addendums]

          if id
            HRM::Project.update(id, data: data, db: db)
          else
            id = HRM::Project.insert(data: data, db: db)
            status 201 # Created
          end

          # filter items
          locations = filter_req_project_items(id, [:location_id], locations)
          addendums = filter_req_project_items(id, [:addendum_name, :addendum_no], addendums)

          # update items
          HRM::ProjectLocation.update_per :project_id, id, data: locations, db: db
          HRM::ProjectAddendum.update_per :project_id, id, data: addendums, db: db
        end

        json get_project(id, db)
      end
    end

    #
    # DELETE /projects/:id
    #
    delete '/projects/:id' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :delete)
      id = id.split(',');
      json HRM::Project.delete(id, token: current_token)
    end

  end
end
