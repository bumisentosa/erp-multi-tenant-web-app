require './server/modules/hrm/app'
require './server/modules/hrm/models/project_type'

module HRM
  class App < Route
    #
    # GET /projects/types/:id
    #
    get '/projects/types/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :types, :read)
      if id
        json HRM::ProjectType.get(id, token: current_token)
      else
        query_params = { order: :project_type, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::ProjectType.select(query_params)
      end
    end

    #
    # POST, PUT /projects/types/:id
    #
    route :post, :put, '/projects/types/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :types, method)

      data = JSON.parse(request.body.read).symbolize_keys

      if id
        HRM::ProjectType.update(id, data: data, token: current_token)
      else
        data[:type_id] = HRM::ProjectType.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /projects/types/:id
    #
    delete '/projects/types/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :types, :delete)
      json HRM::ProjectType.delete(id, token: current_token)
    end
  end
end
