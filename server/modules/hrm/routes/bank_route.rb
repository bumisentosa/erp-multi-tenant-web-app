require './server/modules/hrm/app'
require './server/modules/hrm/models/bank'

module HRM
  class App < Route
    #
    # GET /banks/:id
    #
    get '/banks/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :banks, :read)
      if id
        json HRM::Bank.get(id, token: current_token)
      else
        query_params = { order: :bank_name, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Bank.select(query_params)
      end
    end

    #
    # POST, PUT /banks/:id
    #
    route :post, :put, '/banks/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :banks, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::Bank.update(id, data: data, token: current_token)
      else
        data[:bank_id] = HRM::Bank.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /banks/:id
    #
    delete '/banks/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :banks, :delete)
      json HRM::Bank.delete(id, token: current_token)
    end
  end
end
