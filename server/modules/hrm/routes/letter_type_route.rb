require './server/modules/hrm/app'
require './server/modules/hrm/models/letter_type'

module HRM
  class App < Route
    #
    # GET /letters/types/:id
    #
    get '/letters/types/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, :types, :read)
      if id
        json HRM::LetterType.get(id, token: current_token)
      else
        query_params = { order: :letter_code, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::LetterType.select(query_params)
      end
    end

    #
    # POST, PUT /letters/types/:id
    #
    route :post, :put, '/letters/types/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, :types, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::LetterType.update(id, data: data, token: current_token)
      else
        data[:type_id] = HRM::LetterType.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /letters/types/:id
    #
    delete '/letters/types/:id' do |id|
      id = id.split(',')
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, :types, :delete)
      json HRM::LetterType.delete(id, token: current_token)
    end
  end
end
