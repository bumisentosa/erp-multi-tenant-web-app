require './server/modules/hrm/app'
require './server/modules/hrm/models/competence'

module HRM
  class App < Route
    #
    # GET /competences/:id
    #
    get '/competences/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :competences, :read)
      if id
        json HRM::Competence.get(id, token: current_token)
      else
        query_params = { order: :competence_code, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Competence.select(query_params)
      end
    end

    #
    # POST, PUT /competences/:id
    #
    route :post, :put, '/competences/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :competences, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::Competence.update(id, data: data, token: current_token)
      else
        data[:competence_id] = HRM::Competence.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /competences/:id
    #
    delete '/competences/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :competences, :delete)
      json HRM::Competence.delete(id, token: current_token)
    end
  end
end
