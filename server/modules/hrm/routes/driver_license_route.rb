require './server/modules/hrm/app'
require './server/modules/hrm/models/driver_license'

module HRM
  class App < Route
    #
    # GET /driver-licenses/:id
    #
    get '/driver-licenses/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :driver_licenses, :read)
      if id
        json HRM::DriverLicense.get(id, token: current_token)
      else
        query_params = { order: :license_code, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::DriverLicense.select(query_params)
      end
    end

    #
    # POST, PUT /driver-licenses/:id
    #
    route :post, :put, '/driver-licenses/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :driver_licenses, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::DriverLicense.update(id, data: data, token: current_token)
      else
        HRM::DriverLicense.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /driver-licenses/:id
    #
    delete '/driver-licenses/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :driver_licenses, :delete)
      json HRM::DriverLicense.delete(id, token: current_token)
    end
  end
end
