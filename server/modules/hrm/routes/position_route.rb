require './server/modules/hrm/app'
require './server/modules/hrm/models/position'

module HRM
  class App < Route
    #
    # GET /positions/:id
    #
    get '/positions/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :positions, :read)
      if id
        json HRM::Position.get(id, token: current_token)
      else
        query_params = { order: :sort_asc, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Position.select(query_params)
      end
    end

    #
    # POST, PUT /positions/:id
    #
    route :post, :put, '/positions/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :positions, method)

      data = JSON.parse(request.body.read).symbolize_keys
      data[:parent] = nil if data[:parent] == ''

      if id
        HRM::Position.update(id, data: data, token: current_token)
      else
        id = HRM::Position.insert(data: data, token: current_token)
        status 201 # Created
      end
      json HRM::Position.get(id, token: current_token);
    end

    #
    # DELETE /positions/:id
    #
    delete '/positions/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :positions, :delete)
      json HRM::Position.delete(id, token: current_token)
    end
  end
end
