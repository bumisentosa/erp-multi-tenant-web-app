require './server/modules/hrm/app'
require './server/modules/hrm/models/project_group'

module HRM
  class App < Route
    #
    # GET /projects/groups/:id
    #
    get '/projects/groups/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :groups, :read)
      if id
        json HRM::ProjectGroup.get(id, token: current_token)
      else
        query_params = { order: :group_name, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::ProjectGroup.select(query_params)
      end
    end

    #
    # POST, PUT /projects/groups/:id
    #
    route :post, :put, '/projects/groups/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :groups, method)

      data = JSON.parse(request.body.read).symbolize_keys

      if id
        HRM::ProjectGroup.update(id, data: data, token: current_token)
      else
        data[:group_id] = HRM::ProjectGroup.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /projects/groups/:id
    #
    delete '/projects/groups/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :projects, :groups, :delete)
      json HRM::ProjectGroup.delete(id, token: current_token)
    end
  end
end
