require './server/modules/hrm/app'
require './server/modules/hrm/models/education'

module HRM
  class App < Route
    #
    # GET /educations/levels/:id
    #
    get '/educations/levels/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :educations, :levels, :read)
      if id
        json HRM::EducationLevel.get(id, token: current_token)
      else
        query_params = { order: :level_id, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::EducationLevel.select(query_params)
      end
    end

    #
    # POST, PUT /educations/levels/:id
    #
    route :post, :put, '/educations/levels/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :educations, :levels, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::EducationLevel.update(id, data: data, token: current_token)
      else
        HRM::EducationLevel.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /educations/levels/:id
    #
    delete '/educations/levels/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :educations, :levels, :delete)
      json HRM::EducationLevel.delete(id, token: current_token)
    end
  end
end
