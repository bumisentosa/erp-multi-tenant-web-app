require './server/modules/hrm/app'
require './server/modules/hrm/models/letter'

module HRM
  class App < Route
    #
    # GET /letters/:id
    #
    get '/letters/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, :read)
      if id
        json HRM::Letter.get(id, token: current_token)
      else
        query_params = { order: :letter_date.desc, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Letter.select(query_params)
      end
    end

    #
    # POST, PUT /letters/:id
    #
    route :post, :put, '/letters/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::Letter.update(id, data: data, token: current_token)
      else
        data[:letter_id] = HRM::Letter.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /letters/:id
    #
    delete '/letters/:id' do |id|
      id = id.split(',')
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :letters, :delete)
      json HRM::Letter.delete(id, token: current_token)
    end
  end
end
