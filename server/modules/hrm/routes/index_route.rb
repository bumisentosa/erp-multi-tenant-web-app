require './server/models/user'
require './server/models/tenant'
require './server/modules/hrm/app'

module HRM
  class App < Route
    get '/' do
      public_folder = settings.public_folder
      @angular_app = 'hrm'

      @stylesheets = ['/css/bootstrap.css', '/css/app.css']

      if settings.production?
        @scripts = [
          '/lib/jquery/jquery.min.js',
          '/lib/underscore/underscore-min.js',
          '/lib/angular/angular.min.js',
          '/lib/angular-animate/angular-animate.min.js',
          '/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
          '/lib/angular-ui-router/release/angular-ui-router.min.js',
          '/lib/angular-touch/angular-touch.min.js',
          '/lib/angular-resource/dist/resource.min.js',
          '/lib/ng-file-upload/angular-file-upload-shim.min.js',
          '/lib/ng-file-upload/angular-file-upload.min.js',
          '/app/hrm.min.js',
          '/app/hrm-tpls.min.js'
        ]
      else
        @scripts = [
          '/lib/jquery/jquery.js',
          '/lib/underscore/underscore.js',
          '/lib/angular/angular.js',
          '/lib/angular-animate/angular-animate.js',
          '/lib/angular-bootstrap/ui-bootstrap-tpls.js',
          '/lib/angular-ui-router/release/angular-ui-router.js',
          '/lib/angular-touch/angular-touch.js',
          '/lib/angular-resource/src/resource.js',
          '/lib/ng-file-upload/angular-file-upload-shim.js',
          '/lib/ng-file-upload/angular-file-upload.js',
          '/app/app.js',
          '/app/layout.js'
        ]

        # Module spesific scripts
        @scripts.push *files_in(public_folder, '/app/modules/hrm/**/*.js')

        # App scripts (tanpa module)
        app_folders = folder_name("#{public_folder}/app", excepts: 'modules').join(',')
        @scripts.push *files_in(public_folder, "/app/{#{app_folders}}/**/*.js")
      end

      @tenant = get_tenant(current_user)

      content_type :html
      erb :index
    end

  end
end
