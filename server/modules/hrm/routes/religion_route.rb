require './server/modules/hrm/app'
require './server/modules/hrm/models/religion'

module HRM
  class App < Route
    #
    # GET /religions/:id
    #
    get '/religions/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :religions, :read)
      if id
        json HRM::Religion.get(id, token: current_token)
      else
        query_params = { order: :religion_id, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Religion.select(query_params)
      end
    end

    #
    # POST, PUT /religions/:id
    #
    route :post, :put, '/religions/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :religions, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::Religion.update(id, data: data, token: current_token)
      else
        HRM::Religion.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /religions/:id
    #
    delete '/religions/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :religions, :delete)
      json HRM::Religion.delete(id, token: current_token)
    end
  end
end
