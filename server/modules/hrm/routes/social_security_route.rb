require './server/modules/hrm/app'
require './server/modules/hrm/models/social_security'

module HRM
  class App < Route
    #
    # GET /social-security/:id
    #
    get '/social-security/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :social_security, :read)
      if id
        json HRM::SocialSecurity.get(id, token: current_token)
      else
        query_params = { order: :ss_code, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::SocialSecurity.select(query_params)
      end
    end

    #
    # POST, PUT /social-security/:id
    #
    route :post, :put, '/social-security/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :social_security, method)

      data = JSON.parse(request.body.read).symbolize_keys
      if id
        HRM::SocialSecurity.update(id, data: data, token: current_token)
      else
        HRM::SocialSecurity.insert(data: data, token: current_token)
        status 201 # Created
      end
      json data
    end

    #
    # DELETE /social-security/:id
    #
    delete '/social-security/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :social_security, :delete)
      json HRM::SocialSecurity.delete(id, token: current_token)
    end
  end
end
