require './server/modules/hrm/app'
require './server/modules/hrm/models/location'

module HRM
  class App < Route
    #
    # GET /locations/:id
    #
    get '/locations/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :locations, :read)
      if id
        json HRM::Location.get(id, token: current_token)
      else
        query_params = { order: :sort_asc, token: current_token }
        if params['page']
          query_params[:page] = params['page'].to_i
          params.delete 'page'
        end
        query_params[:filter] = params.symbolize_keys
        json HRM::Location.select(query_params)
      end
    end

    #
    # POST, PUT /locations/:id
    #
    route :post, :put, '/locations/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :locations, method)

      data = JSON.parse(request.body.read).symbolize_keys
      data[:parent] = nil if data[:parent] == ''

      if id
        HRM::Location.update(id, data: data, token: current_token)
      else
        id = HRM::Location.insert(data: data, token: current_token)
        status 201 # Created
      end
      json HRM::Location.get(id, token: current_token);
    end

    #
    # DELETE /locations/:id
    #
    delete '/locations/:id' do |id|
      id = id.split(',');
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :locations, :delete)
      json HRM::Location.delete(id, token: current_token)
    end
  end
end
