require 'securerandom'
require './server/modules/hrm/app'
require './server/modules/hrm/models/employee'
require './server/modules/hrm/models/letter'

module HRM
  class App < Route

    #
    # Get data pegawai dari database beserta child tabel yang berkaitan.
    #
    def get_employee(id, db)
      employee = HRM::Employee.get(id, db: db)

      options = { filter: { employee_id: id }, db: db }
      employee[:projects] = HRM::EmployeeProject.select(options.merge({ order: :mutation_date }))
      employee[:locations] = HRM::EmployeeLocation.select(options.merge({ order: :mutation_date }))
      employee[:positions] = HRM::EmployeePosition.select(options.merge({ order: :mutation_date }))
      employee[:driver_licenses] = HRM::EmployeeDriverLicense.select(options.merge({ order: :license_code }))
      employee[:children] = HRM::EmployeeChild.select(options.merge({ order: :birth_date }))
      employee[:educations] = HRM::EmployeeEducation.select(options.merge({ order: :year_end.desc }))
      employee[:educations_non_formal] = HRM::EmployeeEducationNonFormal.select(options.merge({ order: :year.desc }))
      employee[:experiences] = HRM::EmployeeExperience.select(options.merge({ order: :year.desc }))
      employee[:organizations] = HRM::EmployeeOrganization.select(options.merge({ order: :year.desc }))
      employee[:competences] = HRM::EmployeeCompetence.select(options.merge({ order: :competence_date.desc }))
      employee[:social_security] = HRM::EmployeeSocialSecurity.select(options.merge({ order: :ss_code }))
      employee[:letters] = HRM::Letter.select(options.merge({ order: :letter_date.desc }))

      return employee
    end

    #
    # Filter data request child items sehingga mengembalikan data valid saja.
    #
    def filter_req_employee_items(employee_id, primary_fields, data)
      return nil unless data
      data.inject([]) do |items, item|
        item.symbolize_keys!
        valid = true
        primary_fields.each do |field|
          if !item[field]
            valid = false
            break
          end
        end
        if valid
          item[:employee_id] = employee_id
          items << item
        end
        items
      end
    end

    #
    # GET /employees/:id
    #
    get '/employees/?:id?' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :employees, :read)

      DB.open_database(token: current_token) do |db|
        if id
          json get_employee(id, db)
        else
          query_params = {
            order: [:sort_project, :sort_location, :sort_position, :employee_no],
            db: db
          }
          if params['page']
            query_params[:page] = params['page'].to_i
            params.delete 'page'
          end
          query_params[:filter] = params.symbolize_keys
          json HRM::Employee.select(query_params)
        end
      end
    end

    #
    # POST, PUT /employees/:id
    #
    route :post, :put, '/employees/?:id?' do |id|
      method = request.env['REQUEST_METHOD'] == 'POST' ? :create : :update
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :employees, method)

      DB.open_database(token: current_token) do |db|
        db.transaction do
          # data
          data = JSON.parse(request.body.read).deep_symbolize_keys

          # child items
          projects = data[:projects]
          locations = data[:locations]
          positions = data[:positions]
          driver_licenses = data[:driver_licenses]
          children = data[:children]
          educations = data[:educations]
          educations_non_formal = data[:educations_non_formal]
          experiences = data[:experiences]
          organizations = data[:organizations]
          competences = data[:competences]
          social_security = data[:social_security]
          letters = data[:letters]

          if id
            HRM::Employee.update(id, data: data, db: db)
          else
            id = HRM::Employee.insert(data: data, db: db)
            status 201 # Created
          end

          # filter items
          projects = filter_req_employee_items(id, [:project_id, :mutation_date], projects)
          locations = filter_req_employee_items(id, [:location_id, :mutation_date], locations)
          positions = filter_req_employee_items(id, [:position_id, :mutation_date], positions)
          driver_licenses = filter_req_employee_items(id, [:license_code, :license_no], driver_licenses)
          children = filter_req_employee_items(id, [:child_name], children)
          educations = filter_req_employee_items(id, [:level_id, :education_name], educations)
          educations_non_formal = filter_req_employee_items(id, [:education_name], educations_non_formal)
          experiences = filter_req_employee_items(id, [:company_name], experiences)
          organizations = filter_req_employee_items(id, [:organization_name], organizations)
          competences = filter_req_employee_items(id, [:competence_id, :competence_no, :competence_name], competences)
          social_security = filter_req_employee_items(id, [:ss_code, :ss_no], social_security)
          letters = filter_req_employee_items(id, [:type_id, :letter_no], letters)

          # update items
          HRM::EmployeeProject.update_per :employee_id, id, data: projects, db: db
          HRM::EmployeeLocation.update_per :employee_id, id, data: locations, db: db
          HRM::EmployeePosition.update_per :employee_id, id, data: positions, db: db
          HRM::EmployeeDriverLicense.update_per :employee_id, id, data: driver_licenses, db: db
          HRM::EmployeeChild.update_per :employee_id, id, data: children, db: db
          HRM::EmployeeEducation.update_per :employee_id, id, data: educations, db: db
          HRM::EmployeeEducationNonFormal.update_per :employee_id, id, data: educations_non_formal, db: db
          HRM::EmployeeExperience.update_per :employee_id, id, data: experiences, db: db
          HRM::EmployeeOrganization.update_per :employee_id, id, data: organizations, db: db
          HRM::EmployeeCompetence.update_per :employee_id, id, data: competences, db: db
          HRM::EmployeeSocialSecurity.update_per :employee_id, id, data: social_security, db: db
          HRM::Letter.update_per :employee_id, id, data: letters, db: db

          # hapus file foto jika tidak ada foto
          FileUtils.rm_rf Dir.glob("#{settings.public_folder}/img/employees/#{id}-.*") unless data[:photo]
        end

        json get_employee(id, db)
      end
    end

    #
    # DELETE /employees/:id
    #
    delete '/employees/:id' do |id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :employees, :delete)
      id = id.split(',');
      json HRM::Employee.delete(id, token: current_token)
    end

    #
    # POST /employees/:id/photo
    #
    post '/employees/:id/photo' do |employee_id|
      raise Unauthorized unless Auth.allowed?(current_user, :hrm, :employees, :upload)

      # hapus foto lama
      FileUtils.rm_rf Dir.glob("#{settings.public_folder}/img/employees/#{employee_id}-*")

      # copy foto
      file = params[:file]
      file_ext = File.extname file[:filename]
      filename = "#{employee_id}-#{SecureRandom.hex(3)}#{file_ext}"
      FileUtils.cp file[:tempfile], "#{settings.public_folder}/img/employees/#{filename}"
      url = "/img/employees/#{filename}"

      # update employee's photo url
      Employee.update(employee_id, data: { photo_url: url }, token: current_token)

      json url: url
    end

  end
end
