require './server/models/base_model'

module HRM
  class Religion < Model
    columns do |c|
      c.integer :religion_id, primary_key: true, presence: true
      c.string  :religion, presence: true, label: 'Agama'
    end
  end
end
