require './server/models/base_model'

module HRM
  class Project < Model
    view_name :v_projects

    columns do |c|
      c.integer :project_id, primary_key: true
      c.integer :parent
      c.integer :type_id, presence: true, label: 'Tipe Proyek'
      c.integer :group_id, presence: true, label: 'Kelompok Proyek'
      c.string  :project_name, presence: true, label: 'Nama Proyek'
      c.string  :contract_no_s1
      c.string  :contract_no_s2
      c.date    :contract_date
      c.date    :contract_date_start
      c.date    :contract_date_end
      c.string  :project_description
      c.decimal :contract_value
      c.text    :description
    end
  end

  class ProjectLocation < Model
    table_name  :projects_locations
    view_name   :v_projects_locations

    columns do |c|
      c.integer :project_location_id, primary_key: true
      c.integer :project_id, presence: true
      c.integer :location_id, presence: true, label: 'Lokasi Proyek'
    end
  end

  class ProjectAddendum < Model
    table_name  :projects_addendums

    columns do |c|
      c.integer :addendum_id, primary_key: true
      c.integer :project_id, presence: true
      c.string  :addendum_name, presence: true, label: 'Nama Addendum'
      c.string  :addendum_no
      c.date    :addendum_date, presence: true, label: 'Tanggal Addendum'
      c.date    :addendum_date_end, presence: true, label: 'Tanggal Akhir Addendum'
      c.decimal :addendum_value, presence: true, label: 'Nilai Addendum'
    end
  end
end
