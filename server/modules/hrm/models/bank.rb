require './server/models/base_model'

module HRM
  class Bank < Model
    columns do |c|
      c.integer :bank_id, primary_key: true
      c.string  :bank_name, presence: true, label: 'Nama Bank'
      c.string  :branch
      c.string  :address
      c.string  :city
      c.string  :region
      c.string  :country
    end
  end
end
