require './server/models/base_model'

module HRM
  class Employee < Model
    view_name :v_employees

    columns do |c|
      c.integer :employee_id, primary_key: true
      c.integer :project_id
      c.integer :location_id
      c.integer :position_id

      c.string  :employee_no, presence: true, label: 'NIK'
      c.string  :employee_name, presence: true, label: 'Nama Pegawai'
      c.string  :photo_url
      c.date    :birth_date
      c.string  :birth_place
      c.integer :gender_id, presence: true, label: 'Jenis Kelamin'
      c.integer :religion_id, presence: true, label: 'Agama'
      c.string  :address
      c.string  :city
      c.string  :region
      c.string  :home_phone
      c.string  :mobile_phone
      c.string  :email, email: true, label: 'Email'

      c.string  :identity_card_no
      c.date    :identity_card_expiry_date
      c.string  :identity_card_address
      c.string  :identity_card_city

      c.integer :marital_status_id, presence: true, label: 'Status Kawin'
      c.string  :spouse_name
      c.string  :spouse_birth_date
      c.date    :spouse_birth_place
      c.string  :spouse_job

      c.string  :tax_no
      c.date    :tax_date
      c.integer :bank_id
      c.string  :bank_account_no

      c.date    :join_date, presence: true, label: 'Tanggal Bergabung'
      c.date    :employment_date, presence: true, label: 'Tanggal Masa Kerja'
      c.boolean :is_outer_employee
    end
  end

  class EmployeeProject < Model
    table_name :employees_projects

    columns do |c|
      c.integer :mutation_id, primary_key: true
      c.integer :employee_id, presence: true
      c.integer :project_id, presence: true, label: 'Proyek'
      c.date    :mutation_date, presence: true, label: 'Tanggal Mutasi Proyek'
    end
  end

  class EmployeeLocation < Model
    table_name :employees_locations

    columns do |c|
      c.integer :mutation_id, primary_key: true
      c.integer :employee_id, presence: true
      c.integer :location_id, presence: true, label: 'Lokasi'
      c.date    :mutation_date, presence: true, label: 'Tanggal Mutasi Lokasi'
    end
  end

  class EmployeePosition < Model
    table_name :employees_positions

    columns do |c|
      c.integer :mutation_id, primary_key: true
      c.integer :employee_id, presence: true
      c.integer :position_id, presence: true, label: 'Jabatan'
      c.date    :mutation_date, presence: true, label: 'Tanggal Mutasi Jabatan'
    end
  end

  class EmployeeDriverLicense < Model
    table_name :employees_driver_licenses

    columns do |c|
      c.integer :employee_license_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :license_code, presence: true, label: 'Jenis SIM'
      c.string  :license_no, presence: true, label: 'No. SIM'
      c.date    :expiry_date, presence: true, label: 'Tanggal Berlaku SIM'
    end
  end

  class EmployeeChild < Model
    table_name :employees_children

    columns do |c|
      c.integer :child_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :child_name, presence: true, label: 'Nama Anak'
      c.date    :birth_date
      c.string  :birth_place
      c.integer :gender_id
    end
  end

  class EmployeeEducation < Model
    table_name :employees_educations

    columns do |c|
      c.integer :education_id, primary_key: true
      c.integer :employee_id, presence: true
      c.integer :level_id, presence: true, label: 'Tingkat Pendidikan'
      c.string  :education_name, presence: true, label: 'Nama Pendidikan'
      c.string  :major
      c.integer :year_start
      c.integer :year_end
    end
  end

  class EmployeeEducationNonFormal < Model
    table_name :employees_educations_non_formal

    columns do |c|
      c.integer :education_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :education_name, presence: true, label: 'Nama Pendidikan'
      c.string  :year
      c.string  :duration
      c.string  :organizer
    end
  end

  class EmployeeOrganization < Model
    table_name :employees_organizations

    columns do |c|
      c.integer :organization_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :organization_name, presence: true, label: 'Nama Organisasi'
      c.string  :activity
      c.string  :year
    end
  end

  class EmployeeExperience < Model
    table_name :employees_experiences

    columns do |c|
      c.integer :experience_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :company_name, presence: true, label: 'Nama Perusahaan'
      c.string  :business_type
      c.string  :position, presence: true, label: 'Posisi Perusahaan'
      c.string  :year
    end
  end

  class EmployeeCompetence < Model
    table_name :employees_competences

    columns do |c|
      c.integer :employee_competence_id, primary_key: true
      c.integer :employee_id, presence: true
      c.integer :competence_id, presence: true
      c.string  :competence_no, presence: true, label: 'No. Kompetensi'
      c.string  :competence_name, presence: true, label: 'Keterangan Kompetensi'
      c.date    :competence_date, presence: true, label: 'Tanggal Kompetensi'
      c.date    :expiry_date, presence: true, label: 'Tanggal Akhir Kompetensi'
    end
  end

  class EmployeeSocialSecurity < Model
    table_name :employees_social_security

    columns do |c|
      c.integer :employee_ss_id, primary_key: true
      c.integer :employee_id, presence: true
      c.string  :ss_code, presence: true, label: 'Jenis Perlindungan Sosial'
      c.string  :ss_no, presence: true, label: 'No. Perlindungan Sosial'
      c.date    :ss_date, presence: true, label: 'Tanggal Perlindungan Sosial'
    end
  end

end
