require './server/models/base_model'

module HRM
  class Position < Model
    view_name :v_positions

    columns do |c|
      c.integer :position_id, primary_key: true
      c.integer :parent
      c.string  :position, presence: true, label: 'Jabatan'
    end
  end
end
