require './server/models/base_model'

module HRM
  class ProjectType < Model
    table_name :projects_types

    columns do |c|
      c.integer :type_id, primary_key: true
      c.string  :project_type, presence: true, label: 'Tipe Proyek'
    end
  end
end
