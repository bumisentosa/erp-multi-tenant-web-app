require './server/models/base_model'

module HRM
  class SocialSecurity < Model
    table_name :social_security

    columns do |c|
      c.string :ss_code, primary_key: true
      c.string :ss_name, presence: true, label: 'Perlindungan Sosial'
      c.text   :description
    end
  end
end
