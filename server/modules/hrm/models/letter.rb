require './server/models/base_model'

module HRM
  class Letter < Model
    columns do |c|
      c.integer :letter_id, primary_key: true
      c.integer :employee_id
      c.integer :type_id, presence: true, label: 'Jenis Surat'
      c.string  :letter_no, presence: true, label: 'No. Surat'
      c.date    :letter_date
      c.date    :letter_date_end
      c.string  :description
    end
  end
end
