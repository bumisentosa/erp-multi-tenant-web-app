require './server/models/base_model'

module HRM
  class Competence < Model
    columns do |c|
      c.integer :competence_id, primary_key: true
      c.string  :competence_code
      c.string  :competence_name, presence: true, label: 'Nama Kompetensi'
      c.text    :description
    end
  end
end
