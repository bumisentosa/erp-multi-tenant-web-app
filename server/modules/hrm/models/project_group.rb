require './server/models/base_model'

module HRM
  class ProjectGroup < Model
    table_name :projects_groups

    columns do |c|
      c.integer :group_id, primary_key: true
      c.string  :group_name, presence: true, label: 'Kelompok Proyek'
    end
  end
end
