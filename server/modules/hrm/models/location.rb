require './server/models/base_model'

module HRM
  class Location < Model
    view_name :v_locations

    columns do |c|
      c.integer :location_id, primary_key: true
      c.integer :parent
      c.string  :location_name, presence: true, label: 'Lokasi'
      c.string  :address
      c.string  :city
      c.string  :region
      c.string  :country
    end
  end
end
