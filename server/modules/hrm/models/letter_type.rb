require './server/models/base_model'

module HRM
  class LetterType < Model
    table_name :letters_types

    columns do |c|
      c.integer :type_id, primary_key: true
      c.string  :letter_code, presence: true, label: 'Kode Jenis'
      c.string  :letter_type, presence: true, label: 'Jenis Surat'
    end
  end
end
