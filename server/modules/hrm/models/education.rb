require './server/models/base_model'

module HRM
  class EducationLevel < Model
    table_name :educations_levels

    columns do |c|
      c.integer :level_id, primary_key: true, presence: true
      c.string  :education_level, presence: true, label: 'Tingkat Pendidikan'
    end
  end
end
