require './server/models/base_model'

module HRM
  class DriverLicense < Model
    table_name :driver_licenses

    columns do |c|
      c.string :license_code, primary_key: true
      c.text   :description
    end
  end
end
