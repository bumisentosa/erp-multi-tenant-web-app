require './server/modules/pm/app'

module PM
  class App < Route
    get '/' do
      public_folder = settings.public_folder
      @using_angular = true

      if settings.production?
        @scripts = [
          '/lib/jquery/jquery-2.0.1.min.js',
          '/lib/underscore/underscore.min.js',
          '/lib/angular/angular.min.js',
          '/lib/angular/angular-resource.min.js',
          '/lib/angular/angular-cookies.min.js',
          '/lib/ui-bootstrap/ui-bootstrap-tpls-0.3.0.min.js',
          '/lib/ui-router/angular-ui-router.min.js',
          '/app/pm.min.js'
        ]
      else
        @scripts = [
          '/lib/jquery/jquery-2.0.1.js',
          '/lib/underscore/underscore.js',
          '/lib/angular/angular.js',
          '/lib/angular/angular-resource.js',
          '/lib/angular/angular-cookies.js',
          '/lib/ui-bootstrap/ui-bootstrap-tpls-0.3.0.js',
          '/lib/ui-router/angular-ui-router.js',
          '/app/app.js'
        ]

        # App scripts (tanpa module)
        app_folders = folder_name("#{public_folder}/app", excepts: 'modules').join(',')
        @scripts.push *files_in(public_folder, "/app/{#{app_folders}}/**/*.js")

        # Module spesific scripts
        @scripts.push *files_in(public_folder, '/app/modules/pm/**/*.js')
      end

      @stylesheets = ['/css/app.min.css']

      content_type :html
      erb :index
    end
  end
end
