relative_dir = File.dirname(__FILE__)

# Require semua file pada folder listeners
Dir["#{relative_dir}/listeners/**/*.rb"].each do |file|
  require file
end

# Require routes
require_relative 'routes/index_route'
