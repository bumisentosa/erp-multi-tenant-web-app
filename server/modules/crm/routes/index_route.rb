require './server/modules/crm/app'

module CRM
  class App < Route
    get '/' do
      content_type :html
      erb :index
    end
  end
end
