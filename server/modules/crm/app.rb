# ============================================================================
#
# Customer Relationship Management
#
# ============================================================================

require './server/routes/base_route'

module CRM
  class App < Route
    set :root, File.dirname(__FILE__)
    set :erb, layout: :"../../../views/layout"

    def page_path?(path_info)
      path_info == ''
    end
  end
end
