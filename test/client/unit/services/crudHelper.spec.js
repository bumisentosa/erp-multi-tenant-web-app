'use strict';

describe('main/services/crudHelper', function() {
  var DIALOG_OPTIONS = { title: 'Data', templateUrl: 'path/to/template.html' };
  var HELPER_OPTIONS = { fieldId: 'username', fieldLabel: 'display_name' };

  var $httpBackend, helper, Model, modalDialog;

  beforeEach(module('app.services'));

  beforeEach(inject(function($rootScope, _$httpBackend_, resource, crudHelper, $modal, _modalDialog_) {
    $httpBackend = _$httpBackend_;
    modalDialog = _modalDialog_;

    spyOn($modal, 'open').andCallFake(function(options) {
      return {
        result: { then: function(callback) {
          callback(options);
        }}
      };
    });

    Model = resource('/users/:id', { id: '@username' });
    helper = crudHelper(Model, HELPER_OPTIONS);
  }));

  beforeEach(function() {
    var response = [
      { username: 'username0', display_name: 'User 0' },
      { username: 'username1', display_name: 'User 1' },
      { username: 'username2', display_name: 'User 2' },
      { username: 'username3', display_name: 'User 3' },
      { username: 'username4', display_name: 'User 4' }
    ];
    $httpBackend.expectGET('/users').respond(response);
    helper.refresh();
    $httpBackend.flush();
    expect(helper.data).toEqualData(response);
  });

  it('harus mengambil data lagi dari server', function() {
    var response = [
      { username: 'username1x', display_name: 'User 1X' },
      { username: 'username2x', display_name: 'User 2X' }
    ];
    $httpBackend.expectGET('/users').respond(response);
    helper.refresh();
    $httpBackend.flush();
    expect(helper.data).toEqualData(response);
  });

  it('harus menampilkan dialog tambah data', function() {
    var item = Model.create();
    helper.showDialog(item, DIALOG_OPTIONS).then(function(result) {
      expect(result).toEqualData({
        title: DIALOG_OPTIONS.title,
        templateUrl: DIALOG_OPTIONS.templateUrl,
        controller: 'modalDialogCtrl',
        resolve: {
          title: Function,
          message: Function,
          item: Function,
          buttons: Function
        }
      });
      expect(result.resolve.title()).toBe('Data');
      expect(result.resolve.item()).toEqual(item);
    });
  });

  it('harus dapat menambah data', function() {
    var response = { username: 'admin_x', display_name: 'Administrator X' };
    $httpBackend.expectPOST('/users').respond(response);
    var toInsert = Model.create({ username: 'admin', display_name: 'Administrator' });
    helper.updateItem(true, toInsert);
    expect(helper.data[helper.data.length - 1]).toEqualData(toInsert);

    $httpBackend.flush();
    expect(helper.data[helper.data.length - 1]).toEqualData(response);
  });

  it('harus menampilkan dialog update data', function() {
    var item = helper.data[0];
    var options = angular.copy(DIALOG_OPTIONS);
    options.resolve = { something: function() { return 'Something else'; }};
    helper.showDialog(item, options).then(function(result) {
      expect(result).toEqualData({
        title: DIALOG_OPTIONS.title,
        templateUrl: DIALOG_OPTIONS.templateUrl,
        controller: 'modalDialogCtrl',
        resolve: {
          title: Function,
          message: Function,
          item: Function,
          something: Function,
          buttons: Function
        }
      });
      expect(result.resolve.title()).toBe('Data');
      expect(result.resolve.item()).toEqual(item);
      expect(result.resolve.something()).toEqual('Something else');
    });
  });

  it('harus dapat mengubah data', function() {
    var response = { username: 'admin_x', display_name: 'Administrator X' };
    $httpBackend.expectPUT('/users/' + helper.data[0].username).respond(response);

    var toUpdate = {};
    angular.copy(helper.data[0], toUpdate);
    toUpdate.display_name = 'Administrator Edit';
    expect(helper.data[0]).not.toEqualData(toUpdate);

    helper.updateItem(false, toUpdate, helper.data[0]);
    expect(helper.data[0]).toEqualData(toUpdate);

    $httpBackend.flush();
    expect(helper.data[0]).toEqualData(response);
  });

  it('harus menampilkan dialog konfirmasi menghapus data', function() {
    helper.selectedItems.length = 0;
    helper.selectedItems.push(helper.data[0]);

    helper.confirmRemove().then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/modal-dialog.html',
        controller: 'modalDialogCtrl',
        resolve: {
          title: Function,
          message: Function,
          item: Function,
          buttons: Function
        }
      });
      expect(result.resolve.title()).toBe('Konfirmasi Hapus');
      expect(result.resolve.message()).toBe('Yakin menghapus "User 0"?');
    });

    helper.selectedItems.length = 0;
    helper.selectedItems.push(helper.data[0]);
    helper.selectedItems.push(helper.data[1]);

    helper.confirmRemove().then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/modal-dialog.html',
        controller: 'modalDialogCtrl',
        resolve: {
          title: Function,
          message: Function,
          item: Function,
          buttons: Function
        }
      });
      expect(result.resolve.title()).toBe('Konfirmasi Hapus');
      expect(result.resolve.message()).toBe('Yakin menghapus (2) item yang dipilih?');
    });
  });

  it('harus dapat menghapus data', function() {
    var itemsCount = helper.data.length;
    helper.selectedItems.length = 0;
    helper.selectedItems.push(helper.data[0]);
    helper.selectedItems.push(helper.data[1]);

    $httpBackend.expectDELETE('/users/' + helper.selectedItems[0].username + ',' +
        helper.selectedItems[1].username).respond(null);
    helper.removeItem();
    expect(helper.data.length).toBe(itemsCount - 2);

    $httpBackend.flush();
    expect(helper.data.length).toBe(itemsCount - 2); // Tidak berubah
  });

  it('harus toggle selection', function() {
    expect(helper.selectedItems.length).toBe(0);

    helper.toggleSelection();
    expect(helper.selectedItems).toEqualData(helper.data);
    helper.toggleSelection();
    expect(helper.selectedItems).toEqualData([]);

    helper.toggleSelection(helper.data[0]);
    expect(helper.selectedItems).toEqualData([helper.data[0]]);
    helper.toggleSelection(helper.data[2]);
    expect(helper.selectedItems).toEqualData([helper.data[0], helper.data[2]]);
    helper.toggleSelection(helper.data[3]);
    expect(helper.selectedItems).toEqualData([helper.data[0], helper.data[2], helper.data[3]]);
    helper.toggleSelection(helper.data[2]);
    expect(helper.selectedItems).toEqualData([helper.data[0], helper.data[3]]);

    helper.toggleSelection();
    expect(helper.selectedItems).toEqualData(helper.data);
  });
});
