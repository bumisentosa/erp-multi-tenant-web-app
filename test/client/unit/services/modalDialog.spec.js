'use strict';

describe('main/services/modalDialog', function() {
  var $modal, modalDialog;

  beforeEach(module('app.services'));

  beforeEach(inject(function(_$modal_, _modalDialog_) {
    $modal = _$modal_;
    modalDialog = _modalDialog_;

    spyOn($modal, 'open').andCallFake(function(options) {
      return {
        result: { then: function(callback) {
          callback(options);
        }}
      };
    });
  }));

  it('harus menampilkan dialog sederhana', function() {
    modalDialog.open('Dialog', 'Message...').result.then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/modal-dialog.html',
        controller: 'modalDialogCtrl',
        resolve: {
          item: Function,
          title: Function,
          message: Function,
          buttons: Function
        }
      });
      expect(result.resolve.item()).toEqual({});
      expect(result.resolve.title()).toBe('Dialog');
      expect(result.resolve.message()).toBe('Message...');
      expect(result.resolve.buttons()).toEqual([modalDialog.buttons.OK]);
    });
  });

  it('harus menampilkan dialog dengan custom button', function() {
    modalDialog.open('Dialog', 'Message...',
        [modalDialog.buttons.YES, modalDialog.buttons.NO])
        .result.then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/modal-dialog.html',
        controller: 'modalDialogCtrl',
        resolve: {
          item: Function,
          title: Function,
          message: Function,
          buttons: Function
        }
      });
      expect(result.resolve.item()).toEqual({});
      expect(result.resolve.title()).toBe('Dialog');
      expect(result.resolve.message()).toBe('Message...');
      expect(result.resolve.buttons()).toEqual([
        modalDialog.buttons.YES, modalDialog.buttons.NO
      ]);
    });
  });

  it('harus menampilkan dialog dengan custom template', function() {
    var options = {
      templateUrl: 'app/templates/custom-template.html',
      controller: 'customCtrl',
      resolve: { item: function() { return { value: 'item...' }; }}
    };

    modalDialog.open('Dialog', 'Message...', null, options).result.then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/custom-template.html',
        controller: 'customCtrl',
        resolve: {
          item: Function,
          title: Function,
          message: Function,
          buttons: Function
        }
      });
      expect(result.resolve.item()).toEqual(options.resolve.item());
      expect(result.resolve.title()).toBe('Dialog');
      expect(result.resolve.message()).toBe('Message...');
      expect(result.resolve.buttons()).toEqual([]);
    });

    modalDialog.open('Dialog', options).result.then(function(result) {
      expect(result).toEqualData({
        templateUrl: 'app/templates/custom-template.html',
        controller: 'customCtrl',
        resolve: {
          item: Function,
          title: Function,
          message: Function,
          buttons: Function
        }
      });
      expect(result.resolve.item()).toEqual(options.resolve.item());
      expect(result.resolve.title()).toBe('Dialog');
      expect(result.resolve.message()).toBe('');
      expect(result.resolve.buttons()).toEqual([]);
    });
  });

});
