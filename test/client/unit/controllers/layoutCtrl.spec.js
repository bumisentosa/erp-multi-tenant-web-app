'use strict';

describe('main/controllers/layoutCtrl', function() {
  var response = { username: 'some_user' };
  var $scope, $httpBackend, $timeout, messageSvc;

  beforeEach(module('app.controllers'));

  beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, _$timeout_, _messageSvc_) {
    $httpBackend = _$httpBackend_;
    $timeout = _$timeout_;
    messageSvc = _messageSvc_;

    $httpBackend.expectGET('/users/me/profile').respond(response);
    $scope = $rootScope.$new();
    $controller('layoutCtrl', { $scope: $scope });
    $httpBackend.flush();
  }));

  describe('Data', function() {
    it('harus mendapatkan data profile user', function() {
      expect($scope.profile).toEqualData(response);
    });
  });

  describe('Loading indicator', function() {
    it('harus dapat menentukan sedang loading atau belum', function() {
      messageSvc.requestCount = 0;
      $scope.$apply();
      expect($scope.isLoading).toBeFalsy();

      messageSvc.requestCount = 1;
      $scope.$apply();
      expect($scope.isLoading).toBeTruthy();

      messageSvc.requestCount = 42;
      $scope.$apply();
      expect($scope.isLoading).toBeTruthy();

      messageSvc.requestCount = -1;
      $scope.$apply();
      expect($scope.isLoading).toBeFalsy();
    });
  });

  describe('messages', function() {
    it('harus terdapat pesan', function() {
      expect($scope.messages).toEqual([]);

      messageSvc.addMessage('foo');
      messageSvc.addMessage('bar');
      messageSvc.addMessage('baz');
      expect($scope.messages).toEqual(['foo', 'bar', 'baz']);
    });

    it('harus dapat menghapus pesan', function() {
      messageSvc.addMessage('foo');
      messageSvc.addMessage('bar');
      messageSvc.addMessage('baz');

      $scope.removeAlert(1);
      expect($scope.messages).toEqual(['foo', 'baz']);
      expect(messageSvc.messages).toEqual(['foo', 'baz']);
    });

    it('harus menampilkan pesan beberapa saat', function() {
      messageSvc.addMessage('transient foo', 1000);
      expect($scope.messages).toEqual(['transient foo']);
      $timeout.flush();
      expect($scope.messages).toEqual([]);

      messageSvc.addMessage('bar');
      messageSvc.addMessage('transient bar', 1000);
      expect($scope.messages).toEqual(['bar', 'transient bar']);
      $timeout.flush();
      expect($scope.messages).toEqual(['bar']);
    });
  });
});
