'use strict';

describe('main/directives/form', function() {
  /**
   * formGroup
   */
  describe('formGroup', function() {
    var element;

    beforeEach(module('app.directives', 'app/directives/templates/form-group.html'));

    beforeEach(inject(function($rootScope, $compile) {
      element = angular.element(
        '<form-group>' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Label:">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Size:" size="sm-8 md-9 lg-10">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Remove:" on-remove="removeItem()">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Inline Remove (xs):" on-remove="removeItem()" action-inline="xs">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Inline Remove (sm):" on-remove="removeItem()" action-inline="sm">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Inline Remove (md):" on-remove="removeItem()" action-inline="md">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Inline Remove (lg):" on-remove="removeItem()" action-inline="lg">' +
          '-- content --' +
        '</form-group>' +
        '<form-group label="Form With Action Gap:" action-gap="true">' +
          '-- content --' +
        '</form-group>'
      );

      var $scope = $rootScope.$new();
      $compile(element)($scope);
      $scope.$digest();
    }));

    it('harus setup minimal form-group', function() {
      var fg = element.eq(0);
      expect(fg.children().length).toBe(1);

      var groupWrap = fg.children().eq(0);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(1);

      var row = groupWrap.children().eq(0);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(1);
      expect(row.children().eq(0).text()).toBe('-- content --');
    });

    it('harus setup minimal form-group dengan label', function() {
      var fg = element.eq(1);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Label:');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(1);

      var row = groupWrap.children().eq(0);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(1);
      expect(row.children().eq(0).text()).toBe('-- content --');
    });

    it('harus setup minimal form-group dengan label kosong', function() {
      var fg = element.eq(2);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(1);

      var row = groupWrap.children().eq(0);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(1);
      expect(row.children().eq(0).text()).toBe('-- content --');
    });

    it('harus setup form-group dengan ukuran tertentu', function() {
      var fg = element.eq(3);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-sm-4 col-md-3 col-lg-2');
      expect(label.text()).toBe('Form With Size:');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-sm-8 col-md-9 col-lg-10');
      expect(groupWrap.children().length).toBe(1);

      var row = groupWrap.children().eq(0);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(1);
      expect(row.children().eq(0).text()).toBe('-- content --');
    });

    it('harus setup form-group dengan action hapus', function() {
      var fg = element.eq(4);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Remove:');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(2);

      var actionsFloat = groupWrap.children().eq(0);
      expect(actionsFloat.length).toBe(1);
      expect(actionsFloat.attr('class')).toBe('pull-right ');

      var row = groupWrap.children().eq(1);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(2);
      expect(row.children().eq(0).text()).toBe('-- content --');

      var actionsInline = row.children().eq(1);
      expect(actionsInline.attr('class')).toBe('hidden');
    });

    it('harus setup form-group dengan inline action hapus (xs)', function() {
      var fg = element.eq(5);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Inline Remove (xs):');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(2);

      var actionsFloat = groupWrap.children().eq(0);
      expect(actionsFloat.length).toBe(1);
      expect(actionsFloat.attr('class')).toBe('pull-right hidden');

      var row = groupWrap.children().eq(1);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(2);
      expect(row.children().eq(0).text()).toBe('-- content --');

      var actionsInline = row.children().eq(1);
      expect(actionsInline.attr('class')).toBe('');
    });

    it('harus setup form-group dengan inline action hapus (sm)', function() {
      var fg = element.eq(6);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Inline Remove (sm):');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(2);

      var actionsFloat = groupWrap.children().eq(0);
      expect(actionsFloat.length).toBe(1);
      expect(actionsFloat.attr('class')).toBe('pull-right visible-xs');

      var row = groupWrap.children().eq(1);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(2);
      expect(row.children().eq(0).text()).toBe('-- content --');

      var actionsInline = row.children().eq(1);
      expect(actionsInline.attr('class')).toBe('hidden-xs');
    });

    it('harus setup form-group dengan inline action hapus (md)', function() {
      var fg = element.eq(7);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Inline Remove (md):');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(2);

      var actionsFloat = groupWrap.children().eq(0);
      expect(actionsFloat.length).toBe(1);
      expect(actionsFloat.attr('class')).toBe('pull-right hidden-md hidden-lg');

      var row = groupWrap.children().eq(1);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(2);
      expect(row.children().eq(0).text()).toBe('-- content --');

      var actionsInline = row.children().eq(1);
      expect(actionsInline.attr('class')).toBe('visible-md visible-lg');
    });

    it('harus setup form-group dengan inline action hapus (lg)', function() {
      var fg = element.eq(8);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Inline Remove (lg):');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(2);

      var actionsFloat = groupWrap.children().eq(0);
      expect(actionsFloat.length).toBe(1);
      expect(actionsFloat.attr('class')).toBe('pull-right hidden-lg');

      var row = groupWrap.children().eq(1);
      expect(row.attr('style')).toBe('');
      expect(row.children().length).toBe(2);
      expect(row.children().eq(0).text()).toBe('-- content --');

      var actionsInline = row.children().eq(1);
      expect(actionsInline.attr('class')).toBe('visible-lg');
    });

    it('harus setup form dengan action gap', function() {
      var fg = element.eq(9);
      expect(fg.children().length).toBe(2);

      var label = fg.children().eq(0);
      expect(label.attr('class')).toBe('control-label hidden-xs hidden-sm col-lg-12');
      expect(label.text()).toBe('Form With Action Gap:');

      var groupWrap = fg.children().eq(1);
      expect(groupWrap.attr('class')).toBe('col-lg-12');
      expect(groupWrap.children().length).toBe(1);

      var row = groupWrap.children().eq(0);
      expect(row.attr('style')).toBe('padding-right: 32px');
      expect(row.children().length).toBe(1);
      expect(row.children().eq(0).text()).toBe('-- content --');
    });
  });

  /**
   * formItem
   */
  describe('formItem', function() {
    var element;

    beforeEach(module('app.directives', 'app/directives/templates/form-item.html'));

    beforeEach(inject(function($rootScope, $compile) {
      element = angular.element(
        '<form-item>' +
          '-- content --' +
        '</form-item>' +
        '<form-item label="Label:">' +
          '-- content --' +
        '</form-item>' +
        '<form-item label="Label:" label-inline="Label Inline:">' +
          '-- content --' +
        '</form-item>' +
        '<form-item label="Label:" size="xs-12 md-10 lg-4">' +
          '-- content --' +
        '</form-item>' +
        '<form-item label="Label:" type="date">' +
          '-- content --' +
        '</form-item>' +
        '<form-item label="Label:" size="xs-12 md-10 lg-4" type="number">' +
          '-- content --' +
        '</form-item>'
      );

      var $scope = $rootScope.$new();
      $compile(element)($scope);
      $scope.$digest();
    }));

    it('harus setup minimal form-item', function() {
      var fi = element.eq(0);
      expect(fi.children().length).toBe(1);

      var col = fi.children().eq(0);
      expect(col.attr('class')).toBe('form-col col-lg-12');
      expect(col.children().eq(0).text()).toBe('');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });

    it('harus setup minimal form-item dengan label', function() {
      var fi = element.eq(1);
      expect(fi.children().length).toBe(1);

      var col = fi.children().eq(0);
      expect(col.attr('class')).toBe('form-col col-lg-12');
      expect(col.children().eq(0).text()).toBe('Label:');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });

    it('harus setup form-item dengan inline label', function() {
      var fi = element.eq(2);
      expect(fi.children().length).toBe(2);

      var labelInline = fi.children().eq(0);
      expect(labelInline.html()).toBe('&nbsp;Label Inline:&nbsp;');

      var col = fi.children().eq(1);
      expect(col.attr('class')).toBe('form-col col-lg-12');
      expect(col.children().eq(0).text()).toBe('Label:');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });

    it('harus setup form-item dengan ukuran tertentu', function() {
      var fi = element.eq(3);
      expect(fi.children().length).toBe(1);

      var col = fi.children().eq(0);
      expect(col.attr('class')).toBe('form-col col-xs-12 col-md-10 col-lg-4');
      expect(col.children().eq(0).text()).toBe('Label:');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });

    it('harus setup form-item dengan tipe tertentu', function() {
      var fi = element.eq(4);
      expect(fi.children().length).toBe(1);

      var col = fi.children().eq(0);
      expect(col.attr('class')).toBe('form-col col-lg-12 col-date');
      expect(col.children().eq(0).text()).toBe('Label:');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });

    it('harus setup form-item dengan ukuran dan tipe tertentu', function() {
      var fi = element.eq(5);
      expect(fi.children().length).toBe(1);

      var col = fi.children().eq(0);
      expect(col.attr('class')).toBe('form-col col-xs-12 col-md-10 col-lg-4 col-number');
      expect(col.children().eq(0).text()).toBe('Label:');
      expect(col.children().eq(1).text()).toBe('-- content --');
    });
  });
});
