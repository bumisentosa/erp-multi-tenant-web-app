'use strict';

describe('main/directives/miniProfile', function() {
  var element, $scope;

  beforeEach(module('app.directives', 'app/directives/templates/mini-profile.html'));

  beforeEach(inject(function($rootScope, $compile) {
    element = angular.element(
      '<sg-mini-profile display-name="{{user.displayName}}" ' +
                          'role-name="{{user.role}}" ' +
                          'photo-url="{{user.photoUrl}}">' +
      '</sg-mini-profile>'
    );

    $scope = $rootScope;
    $compile(element)($scope);
    $scope.$digest();
  }));

  it('harus membuat elemen', function() {
    expect(element.is('ul')).toBeTruthy();
    expect(element).toHaveClass('mini-profile');
  });

  it('harus bind data', function() {
    $scope.$apply(function() {
      $scope.user = {
        displayName: 'Some User',
        role: 'Administrator',
        photoUrl: '/path/to/photo.jpg'
      };
    });

    expect(element.find('li.dropdown > a.dropdown-toggle > span').text()).toBe('Some User');
    expect(element.find('li.dropdown > div.dropdown-menu > div.profile-data > div.display-name').text()).toBe('Some User');
    expect(element.find('li.dropdown > a.dropdown-toggle > img').attr('src')).toBe('/path/to/photo.jpg');
    expect(element.find('li.dropdown > div.dropdown-menu > img').attr('src')).toBe('/path/to/photo.jpg');
    expect(element.find('li.dropdown > div.dropdown-menu > div.profile-data > div.role').text()).toBe('Administrator');
  });
});
