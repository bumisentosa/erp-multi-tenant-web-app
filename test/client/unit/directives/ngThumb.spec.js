'use strict';

describe('main/directives/ngThumb', function() {
  var $compile, element, $scope, document;

  beforeEach(module('app.directives'));

  beforeEach(inject(function($rootScope, _$compile_, $document) {
    element = angular.element('<div ng-thumb="image"></div>');

    document = $document;
    $compile = _$compile_;
    $scope = $rootScope.$new();
    $compile(element)($scope);
    $scope.$digest();
  }));

  it('harus menampilkan link gambar', function() {
    $scope.image = '/path/to/image.jpg';

    $compile(element)($scope);
    $scope.$digest();
    expect(element.attr('style')).toBe('background-image: url(\'/path/to/image.jpg\');');
  });

  xit('harus menampilkan file gambar', function() {
    // TODO: cara buat object window.File?
  });
});
