'use strict';

describe('main/directives/fireOn', function() {
  var element, $scope, $rootScope, $timeout;

  beforeEach(module('app.directives'));

  beforeEach(inject(function(_$rootScope_, $compile, _$timeout_) {
    element = angular.element(
      '<button ng-click="btnClick()" fire-on="btn-click-event">Click Me</button>'
    );

    $timeout = _$timeout_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $compile(element)($scope);
    $scope.$digest();

    $scope.btnClick = function() {};
  }));

  it('harus menjalankan event click pada button', function() {
    spyOn($scope, 'btnClick');
    $rootScope.$broadcast('btn-click-event');
    $timeout.flush();
    expect($scope.btnClick).toHaveBeenCalled();
  });
});
