'use strict';

/**
 * Service
 */
describe('main/directives/toolbar (services)', function() {
  var toolbar;
  beforeEach(module('app.directives'));
  beforeEach(inject(function(_toolbar_) { toolbar = _toolbar_; }));

  it('harus reset toolbar', function() {
    toolbar.reset();

    expect(toolbar.visibleToolbars).toEqualData([]);
    expect(toolbar.buttons).toEqualData({ main: [], search: [], pages: [] });
    expect(toolbar.filterOptions).toEqualData({ filterText: '' });
    expect(toolbar.currentPage).toBe(1);
    expect(toolbar.pageCount).toBe(1);
    expect(toolbar.pages).toEqualData([1]);
  });

  it('harus initialize default value', function() {
    toolbar.init({ customProp: 42, anotherProp: ['foo', 'bar'] });

    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    expect(toolbar.buttons).toEqualData({
      main:   ['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page'],
      search: ['back', 'search'],
      pages:  ['prev-page', 'page', 'next-page']
    });
    expect(toolbar.filterOptions).toEqualData({ filterText: '' });
    expect(toolbar.currentPage).toBe(1);
    expect(toolbar.pageCount).toBe(1);
    expect(toolbar.pages).toEqualData([1]);

    expect(toolbar.customProp).toBe(42);
    expect(toolbar.anotherProp).toEqualData(['foo', 'bar']);
  });

  it('harus men-generate page', function() {
    toolbar.pageCount = 5;

    toolbar.currentPage = 1;
    expect(toolbar.generatePages(3)).toEqualData([1, 2, 3]);
    toolbar.currentPage = 2;
    expect(toolbar.generatePages(3)).toEqualData([1, 2, 3]);
    toolbar.currentPage = 3;
    expect(toolbar.generatePages(3)).toEqualData([2, 3, 4]);
    toolbar.currentPage = 4;
    expect(toolbar.generatePages(3)).toEqualData([3, 4, 5]);
    toolbar.currentPage = 5;
    expect(toolbar.generatePages(3)).toEqualData([3, 4, 5]);

    toolbar.pageCount = 5;

    toolbar.currentPage = 1;
    expect(toolbar.generatePages(4)).toEqualData([1, 2, 3, 4]);
    toolbar.currentPage = 2;
    expect(toolbar.generatePages(4)).toEqualData([1, 2, 3, 4]);
    toolbar.currentPage = 3;
    expect(toolbar.generatePages(4)).toEqualData([2, 3, 4, 5]);
    toolbar.currentPage = 4;
    expect(toolbar.generatePages(4)).toEqualData([2, 3, 4, 5]);
    toolbar.currentPage = 5;
    expect(toolbar.generatePages(4)).toEqualData([2, 3, 4, 5]);

    toolbar.pageCount = 3;

    toolbar.currentPage = 1;
    expect(toolbar.generatePages(3)).toEqualData([1, 2, 3]);
    toolbar.currentPage = 2;
    expect(toolbar.generatePages(3)).toEqualData([1, 2, 3]);
    toolbar.currentPage = 3;
    expect(toolbar.generatePages(3)).toEqualData([1, 2, 3]);
  });
});


/**
 * Main Toolbar
 */
describe('main/directives/toolbar (toolbar-main)', function() {
  var toolbar, element, $scope;

  beforeEach(module('app.directives', 'app/directives/templates/toolbar-main.html'));

  beforeEach(inject(function($rootScope, $compile, _toolbar_) {
    toolbar = _toolbar_;
    toolbar.init();
    element = angular.element('<sg-toolbar-main></sg-toolbar-main>');
    $scope = $rootScope;
    $compile(element)($scope);
    $scope.$digest();
  }));

  beforeEach(function() {
    spyOn($scope, '$broadcast');
  });

  it('harus membuat element', function() {
    expect(element.is('ul')).toBeTruthy();
    expect(element).toHaveClass('toolbar');
    expect(element).toHaveClass('toolbar-main');
  });

  it('harus broadcast event "back"', function() {
    element.children().eq(0).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'back');
  });

  it('harus broadcast event "add"', function() {
    element.children().eq(1).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'add');
  });

  it('harus broadcast event "edit"', function() {
    element.children().eq(2).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'edit');
  });

  it('harus broadcast event "save"', function() {
    element.children().eq(3).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'save');
  });

  it('harus broadcast event "remove"', function() {
    element.children().eq(4).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'remove');
  });

  it('harus broadcast event "show-search"', function() {
    element.children().eq(5).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'show-search');
  });

  it('harus broadcast event "refresh"', function() {
    element.children().eq(6).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'refresh');
  });

  it('harus broadcast event "print"', function() {
    element.children().eq(7).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-main-click', 'print');
  });

  it('harus broadcast event "page" (1/1)', function() {
    toolbar.pageCount = 1;
    toolbar.currentPage = 1;
    $scope.$apply();
    expect(element.children().eq(8)).toHaveClass('disabled');
    element.children().eq(8).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');

    expect(element.children().eq(9)).toHaveClass('disabled');
    element.children().eq(9).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
  });

  it('harus broadcast event "page" (1/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 1;
    $scope.$apply();

    expect(element.children().eq(8)).toHaveClass('disabled');
    element.children().eq(8).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');

    expect(element.children().eq(9)).not.toHaveClass('disabled');
    element.children().eq(9).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 2);
  });

  it('harus broadcast event "page" (2/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 2;
    $scope.$apply();

    expect(element.children().eq(8)).not.toHaveClass('disabled');
    element.children().eq(8).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 1);

    expect(element.children().eq(9)).not.toHaveClass('disabled');
    element.children().eq(9).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 3);
  });

  it('harus broadcast event "page" (3/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 3;
    $scope.$apply();

    expect(element.children().eq(8)).not.toHaveClass('disabled');
    element.children().eq(8).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 2);

    expect(element.children().eq(9)).toHaveClass('disabled');
    element.children().eq(9).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
  });
});

/**
 * Search toolbar
 */
describe('main/directives/toolbar (toolbar-search)', function() {
  var toolbar, element, $scope;

  beforeEach(module('app.directives', 'app/directives/templates/toolbar-search.html'));

  beforeEach(inject(function($rootScope, $compile, _toolbar_) {
    toolbar = _toolbar_;
    toolbar.init();
    element = angular.element('<sg-toolbar-search></sg-toolbar-search>');
    $scope = $rootScope;
    $compile(element)($scope);
    $scope.$digest();
  }));

  beforeEach(function() {
    spyOn($scope, '$broadcast');
  });

  it('harus membuat element', function() {
    expect(element.is('ul')).toBeTruthy();
    expect(element).toHaveClass('toolbar');
    expect(element).toHaveClass('toolbar-search');
  });

  it('harus broadcast event "back"', function() {
    element.find('li.toolbar-item-back > button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-search-click', 'back');
  });

  // TODO: Test broadcast event "search" tidak bekerja, kode dites bekerja dengan benar.
  xit('harus broadcast event "search"', function() {
    element.find('input').val('Foo');
    $scope.$apply();
    expect($scope.filterOptions).toEqualData({ filterText: 'Foo' }); // undefined! Kenapa???
    element.find('form').submit();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-search-click', 'search', { filterText: 'Foo' });
    // ...Error: Some of your tests did a full page reload!
  });
});

/**
 * Pages Toolbar
 */
describe('main/directives/toolbar (toolbar-pages)', function() {
  var toolbar, element, $scope;

  beforeEach(module('app.directives', 'app/directives/templates/toolbar-pages.html'));

  beforeEach(inject(function($rootScope, $compile, _toolbar_) {
    toolbar = _toolbar_;
    toolbar.init();
    element = angular.element('<sg-toolbar-pages></sg-toolbar-pages>');
    $scope = $rootScope;
    $compile(element)($scope);
    $scope.$digest();
  }));

  beforeEach(function() {
    spyOn($scope, '$broadcast');
  });

  it('harus membuat element', function() {
    expect(element.is('ul')).toBeTruthy();
    expect(element).toHaveClass('toolbar');
    expect(element).toHaveClass('toolbar-pages');
  });

  it('harus membuat halaman (1/1)', function() {
    toolbar.pageCount = 1;
    toolbar.currentPage = 1;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(3);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
  });

  it('harus membuat halaman (1/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 1;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(5);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
    expect(pagesElement.eq(2).find('span').text()).toBe('2');
    expect(pagesElement.eq(3).find('span').text()).toBe('3');
  });

  it('harus membuat halaman (2/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 2;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(5);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
    expect(pagesElement.eq(2).find('span').text()).toBe('2');
    expect(pagesElement.eq(3).find('span').text()).toBe('3');
  });

  it('harus membuat halaman (3/3)', function() {
    toolbar.pageCount = 3;
    toolbar.currentPage = 3;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(5);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
    expect(pagesElement.eq(2).find('span').text()).toBe('2');
    expect(pagesElement.eq(3).find('span').text()).toBe('3');
  });

  it('harus membuat halaman (1/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 1;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(7);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
    expect(pagesElement.eq(2).find('span').text()).toBe('2');
    expect(pagesElement.eq(3).find('span').text()).toBe('3');
    expect(pagesElement.eq(4).find('span').text()).toBe('4');
    expect(pagesElement.eq(5).find('span').text()).toBe('5');
  });

  it('harus membuat halaman (2/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 2;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(7);
    expect(pagesElement.eq(1).find('span').text()).toBe('1');
    expect(pagesElement.eq(2).find('span').text()).toBe('2');
    expect(pagesElement.eq(3).find('span').text()).toBe('3');
    expect(pagesElement.eq(4).find('span').text()).toBe('4');
    expect(pagesElement.eq(5).find('span').text()).toBe('5');
  });

  it('harus membuat halaman (6/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 6;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(7);
    expect(pagesElement.eq(1).find('span').text()).toBe('4');
    expect(pagesElement.eq(2).find('span').text()).toBe('5');
    expect(pagesElement.eq(3).find('span').text()).toBe('6');
    expect(pagesElement.eq(4).find('span').text()).toBe('7');
    expect(pagesElement.eq(5).find('span').text()).toBe('8');
  });

  it('harus membuat halaman (9/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 9;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(7);
    expect(pagesElement.eq(1).find('span').text()).toBe('6');
    expect(pagesElement.eq(2).find('span').text()).toBe('7');
    expect(pagesElement.eq(3).find('span').text()).toBe('8');
    expect(pagesElement.eq(4).find('span').text()).toBe('9');
    expect(pagesElement.eq(5).find('span').text()).toBe('10');
  });

  it('harus membuat halaman (10/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 10;
    $scope.$apply();

    var pagesElement = element.find('li');
    expect(pagesElement.length).toBe(7);
    expect(pagesElement.eq(1).find('span').text()).toBe('6');
    expect(pagesElement.eq(2).find('span').text()).toBe('7');
    expect(pagesElement.eq(3).find('span').text()).toBe('8');
    expect(pagesElement.eq(4).find('span').text()).toBe('9');
    expect(pagesElement.eq(5).find('span').text()).toBe('10');
  });

  it('harus broadcast event "page" (1/1)', function() {
    toolbar.pageCount = 1;
    toolbar.currentPage = 1;
    $scope.$apply();

    expect(element.children().eq(0)).toHaveClass('disabled');
    expect(element.children().eq(2)).toHaveClass('disabled');

    element.children().eq(0).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
    element.children().eq(1).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 1);
    element.children().eq(2).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
  });

  xit('harus broadcast event "page" (1/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 1;
    $scope.$apply();

    expect(element.children().eq(0)).toHaveClass('disabled');
    expect(element.children().eq(6)).not.toHaveClass('disabled');

    element.children().eq(0).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
    element.children().eq(1).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 1);
    element.children().eq(2).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 2);
    element.children().eq(3).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 3);
    element.children().eq(4).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 4);
    element.children().eq(5).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 5);
    element.children().eq(6).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 6);
  });

  it('harus broadcast event "page" (6/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 6;
    $scope.$apply();

    expect(element.children().eq(0)).not.toHaveClass('disabled');
    expect(element.children().eq(6)).not.toHaveClass('disabled');

    element.children().eq(0).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 5);
    element.children().eq(1).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 4);
    element.children().eq(2).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 5);
    element.children().eq(3).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 6);
    element.children().eq(4).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 7);
    element.children().eq(5).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 8);
    element.children().eq(6).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 7);
  });

  it('harus broadcast event "page" (10/10)', function() {
    toolbar.pageCount = 10;
    toolbar.currentPage = 10;
    $scope.$apply();

    expect(element.children().eq(0)).not.toHaveClass('disabled');
    expect(element.children().eq(6)).toHaveClass('disabled');

    element.children().eq(0).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 9);
    element.children().eq(1).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 6);
    element.children().eq(2).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 7);
    element.children().eq(3).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 8);
    element.children().eq(4).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 9);
    element.children().eq(5).find('button').click();
    expect($scope.$broadcast).toHaveBeenCalledWith('toolbar-pages-click', 'page', 10);
    element.children().eq(6).find('button').click();
    expect($scope.$broadcast).not.toHaveBeenCalledWith('toolbar-pages-click', 'page');
  });
});
