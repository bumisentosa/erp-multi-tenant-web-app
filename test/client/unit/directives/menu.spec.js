'use strict';

describe('main/directives/menu', function() {
  var element, $scope;

  beforeEach(module('app.directives'));

  beforeEach(inject(function($rootScope, $compile) {
    element = angular.element(
      '<sg-menu-bar id="menu">' +
        '<sg-menu-item id="item1" href="#item1">Item 1</sg-menu-item>' +

        '<sg-dropdown-menu id="item2" text="Item 2">' +
          '<sg-dropdown-submenu id="subitem1" text="Submenu">' +
            '<sg-menu-item id="submenu-item" href="#submenu-item">Submenu item</sg-menu-item>' +
          '</sg-dropdown-submenu>' +

          '<sg-menu-divider id="divider"></sg-menu-divider>' +
          '<sg-menu-item id="subitem2" href="#subitem2">Subitem 2</sg-menu-item>' +
        '</sg-dropdown-menu>' +

      '</sg-menu-bar>'
    );

    $scope = $rootScope;
    $compile(element)($scope);
    $scope.$digest();
  }));

  describe('sgMenuBar', function() {
    it('harus membuat element', function() {
      expect(element).toHaveClass('nav');
      expect(element).toHaveClass('navbar-nav');
    });
  });

  describe('sgMenuItem', function() {
    it('harus membuat element', function() {
      var item = element.find('li#item1 > a');
      expect(item.attr('href')).toBe('#item1');
      expect(item.text()).toBe('Item 1');

      var subitem2 = element.find('ul li#subitem2 > a');
      expect(subitem2.attr('href')).toBe('#subitem2');
      expect(subitem2.text()).toBe('Subitem 2');

      var submenuItem = element.find('ul li#submenu-item > a');
      expect(submenuItem.attr('href')).toBe('#submenu-item');
      expect(submenuItem.text()).toBe('Submenu item');
    });
  });

  describe('sgDropdownMenu', function() {
    it('harus membuat element', function() {
      var dropdown = element.find('li#item2');
      expect(dropdown).toHaveClass('dropdown');
      expect(dropdown.find('> a').html()).toBe('Item 2 <i class="caret"></i>');
      expect(dropdown.find('> ul').length).toBe(1);
    });
  });

  describe('sgDropdownSubmenu', function() {
    it('harus membuat element', function() {
      var dropdown = element.find('ul li#subitem1');
      expect(dropdown).toHaveClass('dropdown-submenu');
      expect(dropdown.find('> a').html()).toBe('Submenu');
      expect(dropdown.find('> ul').length).toBe(1);
    });
  });

  describe('sgMenuDivider', function() {
    it('harus membuat element', function() {
      var divider = element.find('li#divider');
      expect(divider).toHaveClass('divider');
    });
  });
});
