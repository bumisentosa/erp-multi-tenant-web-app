'use strict';

describe('hrm/services/constants', function() {
  beforeEach(module('hrm.constants'));

  it('harus terdapat constant TEMPLATE_DIR', inject(function(TEMPLATE_DIR) {
    expect(TEMPLATE_DIR).toBe('app/modules/hrm/templates');
  }));
});
