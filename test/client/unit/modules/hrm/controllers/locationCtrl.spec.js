'use strict';

describe('hrm/controllers/locationCtrl', function() {
  var $rootScope, $scope, $httpBackend, toolbar;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
    $httpBackend = _$httpBackend_;
    toolbar = _toolbar_;

    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    var state = { current: { data: { title: 'Data Lokasi' }}};
    $controller('locationCtrl', { $scope: $scope, $state: state });

    $httpBackend.expectGET('/hrm/locations').respond([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 3, location_name: 'Location 3' },
      { location_id: 4, location_name: 'Location 4' }
    ]);
    $httpBackend.flush();
  }));

  it('harus mengambil data', function() {
    expect($scope.data).toEqualData([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 3, location_name: 'Location 3' },
      { location_id: 4, location_name: 'Location 4' }
    ]);
  });

  it('harus merespon selectedItems pada toolbar', function() {
    $scope.selectedItems.length = 0;
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

    $scope.selectedItems.push($scope[0]);
    $scope.selectedItems.push($scope[2]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
  });

  it('harus dapat menampilkan input box search', function() {
    $rootScope.$broadcast('toolbar-main-click', 'show-search');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['search']);

    $rootScope.$broadcast('toolbar-search-click', 'back');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
  });

  it('harus me-refresh data', function() {
    expect($scope.data).toEqualData([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 3, location_name: 'Location 3' },
      { location_id: 4, location_name: 'Location 4' }
    ]);

    $httpBackend.expectGET('/hrm/locations').respond([
      { location_id: 1, location_name: 'Location 1x' },
      { location_id: 2, location_name: 'Location 2x' },
      { location_id: 3, location_name: 'Location 3x' },
      { location_id: 4, location_name: 'Location 4x' }
    ]);
    $rootScope.$broadcast('toolbar-main-click', 'refresh');
    $httpBackend.flush();
    $scope.$apply();

    expect($scope.data).toEqualData([
      { location_id: 1, location_name: 'Location 1x' },
      { location_id: 2, location_name: 'Location 2x' },
      { location_id: 3, location_name: 'Location 3x' },
      { location_id: 4, location_name: 'Location 4x' }
    ]);
  });

  it('harus menambah data', function() {
    var newItem = {
      location_id: 42,
      location_name: 'New Location'
    };
    spyOnModal().andReturn(newItem);

    $httpBackend.expectPOST('/hrm/locations').respond(newItem);
    $httpBackend.expectGET('/hrm/locations').respond([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 3, location_name: 'Location 3' },
      { location_id: 4, location_name: 'Location 4' },
      { location_id: 42, location_name: 'New Location' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'add');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data.length).toBe(5);
    expect($scope.data[4]).toEqualData(newItem);
  });

  it('harus mengubah data', function() {
    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    var editedItem = {
      location_id: $scope.selectedItems[0].location_id,
      location_name: 'Location Edit'
    };
    spyOnModal().andReturn(editedItem);

    $httpBackend.expectPUT('/hrm/locations/' + editedItem.location_id).respond(editedItem);
    $httpBackend.expectGET('/hrm/locations').respond([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 3, location_name: 'Location Edit' },
      { location_id: 4, location_name: 'Location 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'edit');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data[2]).toEqualData(editedItem);
  });

  it('harus menghapus data', function() {
    expect($scope.data.length).toBe(4);

    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    spyOnModal().andReturn($scope.selectedItems);
    $httpBackend.expectDELETE('/hrm/locations/' + $scope.selectedItems[0].location_id).respond(1);
    $httpBackend.expectGET('/hrm/locations').respond([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 4, location_name: 'Location 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'remove');
    $scope.$apply();
    expect($scope.data.length).toBe(3);

    $httpBackend.flush();
    expect($scope.data).toEqualData([
      { location_id: 1, location_name: 'Location 1' },
      { location_id: 2, location_name: 'Location 2' },
      { location_id: 4, location_name: 'Location 4' }
    ]);
  });
});
