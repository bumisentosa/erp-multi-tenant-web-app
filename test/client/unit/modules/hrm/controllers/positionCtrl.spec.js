'use strict';

describe('hrm/controllers/positionCtrl', function() {
  var $rootScope, $scope, $httpBackend, toolbar;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
    $httpBackend = _$httpBackend_;
    toolbar = _toolbar_;

    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    var state = { current: { data: { title: 'Data Posisi/Jabatan' }}};
    $controller('positionCtrl', { $scope: $scope, $state: state });

    $httpBackend.expectGET('/hrm/positions').respond([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 3, position: 'Position 3' },
      { position_id: 4, position: 'Position 4' }
    ]);
    $httpBackend.flush();
  }));

  it('harus mengambil data', function() {
    expect($scope.data).toEqualData([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 3, position: 'Position 3' },
      { position_id: 4, position: 'Position 4' }
    ]);
  });

  it('harus merespon selectedItems pada toolbar', function() {
    $scope.selectedItems.length = 0;
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.selectedItems.push($scope.data[2]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
  });

  it('harus dapat menampilkan input box search', function() {
    $rootScope.$broadcast('toolbar-main-click', 'show-search');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['search']);

    $rootScope.$broadcast('toolbar-search-click', 'back');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
  });

  it('harus me-refresh data', function() {
    expect($scope.data).toEqualData([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 3, position: 'Position 3' },
      { position_id: 4, position: 'Position 4' }
    ]);

    $httpBackend.expectGET('/hrm/positions').respond([
      { position_id: 1, position: 'Position 1x' },
      { position_id: 2, position: 'Position 2x' },
      { position_id: 3, position: 'Position 3x' },
      { position_id: 4, position: 'Position 4x' }
    ]);
    $rootScope.$broadcast('toolbar-main-click', 'refresh');
    $httpBackend.flush();
    $scope.$apply();

    expect($scope.data).toEqualData([
      { position_id: 1, position: 'Position 1x' },
      { position_id: 2, position: 'Position 2x' },
      { position_id: 3, position: 'Position 3x' },
      { position_id: 4, position: 'Position 4x' }
    ]);
  });

  it('harus menambah data', function() {
    var newItem = {
      position_id: 42,
      position: 'New Position'
    };
    spyOnModal().andReturn(newItem);

    $httpBackend.expectPOST('/hrm/positions').respond(newItem);
    $httpBackend.expectGET('/hrm/positions').respond([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 3, position: 'Position 3' },
      { position_id: 4, position: 'Position 4' },
      { position_id: 42, position: 'New Position' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'add');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data.length).toBe(5);
    expect($scope.data[4]).toEqualData(newItem);
  });

  it('harus mengubah data', function() {
    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    var editedItem = {
      position_id: $scope.selectedItems[0].position_id,
      position: 'Position Edit'
    };
    spyOnModal().andReturn(editedItem);

    $httpBackend.expectPUT('/hrm/positions/' + editedItem.position_id).respond(editedItem);
    $httpBackend.expectGET('/hrm/positions').respond([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 3, position: 'Position Edit' },
      { position_id: 4, position: 'Position 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'edit');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data[2]).toEqualData(editedItem);
  });

  it('harus menghapus data', function() {
    expect($scope.data.length).toBe(4);

    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    spyOnModal().andReturn($scope.selectedItems);
    $httpBackend.expectDELETE('/hrm/positions/' + $scope.selectedItems[0].position_id).respond(1);
    $httpBackend.expectGET('/hrm/positions').respond([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 4, position: 'Position 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'remove');
    $scope.$apply();
    expect($scope.data.length).toBe(3);

    $httpBackend.flush();
    expect($scope.data).toEqualData([
      { position_id: 1, position: 'Position 1' },
      { position_id: 2, position: 'Position 2' },
      { position_id: 4, position: 'Position 4' }
    ]);
  });
});
