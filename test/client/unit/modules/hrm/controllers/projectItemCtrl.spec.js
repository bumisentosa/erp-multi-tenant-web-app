'use strict';

describe('hrm/controllers/projectItemCtrl', function() {
  var $httpBackend, $controller, $parentScope;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function($rootScope, _$httpBackend_, _$controller_, _toolbar_) {
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;

    $parentScope = $rootScope.$new();

    // projectCtrl
    var parentState = {
      current: { name: 'project', data: { title: 'Data Proyek' }},
      transitionTo: function() {}
    };
    $controller('projectCtrl', { $scope: $parentScope, $state: parentState });

    $httpBackend.expectGET('/hrm/projects/groups').respond([
      { group_id: 1, project_group: 'Group 1' },
      { group_id: 2, project_group: 'Group 2' }
    ]);

    $httpBackend.flush();
  }));

  // projectItemCtrl: state add
  describe('state add', function() {
    var $scope, $state;

    beforeEach(function() {
      $state = {
        transitionTo: function() {}
      };

      $scope = $parentScope.$new();
      $controller('projectItemCtrl', { $scope: $scope, $state: $state, $stateParams: {} });

      $httpBackend.expectGET('/hrm/projects/types').respond([
        { type_id: 1, project_type: 'Type 1' },
        { type_id: 2, project_type: 'Type 2' }
      ]);

      $httpBackend.expectGET('/hrm/locations').respond([
        { location_id: 1, location: 'Location 1' },
        { location_id: 2, location: 'Location 2' }
      ]);

      $httpBackend.flush();
    });

    it('harus membuat data kosong', function() {
      var today = new Date();
      var todayStr = today.toISOString().substr(0, 10);

      expect($scope.item).toEqualData({
        locations: [{}],
        addendums: [{ addendum_value: 0, addendum_date: todayStr, addendum_date_end: todayStr }]
      });
    });

    describe('locations', function() {
      it('harus menambah lokasi proyek', function() {
        expect($scope.item.locations.length).toBe(1);

        $scope.addItem('locations');
        expect($scope.item.locations.length).toBe(2);

        $scope.addItem('locations');
        expect($scope.item.locations.length).toBe(3);
      });

      it('harus menghapus location', function() {
        $scope.addItem('locations');
        $scope.addItem('locations');

        expect($scope.item.locations.length).toBe(3);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(2);

        $scope.removeItem('locations', 42);
        expect($scope.item.locations.length).toBe(2);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(1);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(1);

        $scope.removeItem('locations', 0);
        expect($scope.item.locations.length).toBe(1);
      });
    });

    describe('addendums', function() {
      it('harus menambah addendum', function() {
        var today = new Date();
        var todayStr = today.toISOString().substr(0, 10);

        expect($scope.item.addendums.length).toBe(1);
        expect($scope.item.addendums[0].addendum_date).toBe(todayStr);
        expect($scope.item.addendums[0].addendum_date_end).toBe(todayStr);
        expect($scope.item.addendums[0].addendum_value).toBe(0);

        $scope.addItem('addendums');
        expect($scope.item.addendums.length).toBe(2);

        $scope.addItem('addendums');
        expect($scope.item.addendums.length).toBe(3);
      });

      it('harus menghapus addendum', function() {
        $scope.item.addendums[0].addendum_name = 'Addendum 1';
        $scope.item.addendums[0].addendum_no = 'A-001';

        $scope.addItem('addendums');
        $scope.item.addendums[1].addendum_name = 'Addendum 2';
        $scope.item.addendums[1].addendum_no = 'A-002';

        $scope.addItem('addendums');
        $scope.item.addendums[2].addendum_name = 'Addendum 3';
        $scope.item.addendums[2].addendum_no = 'A-003';

        expect($scope.item.addendums.length).toBe(3);

        $scope.removeItem('addendums', 1);
        expect($scope.item.addendums.length).toBe(2);

        $scope.removeItem('addendums', 42);
        expect($scope.item.addendums.length).toBe(2);

        $scope.removeItem('addendums', 1);
        expect($scope.item.addendums.length).toBe(1);

        $scope.removeItem('addendums', 1);
        expect($scope.item.addendums.length).toBe(1);

        $scope.removeItem('addendums', 0);
        expect($scope.item.addendums.length).toBe(1);
      });
    });

    it('harus menyimpan data', function() {
      spyOn($state, 'transitionTo');

      $scope.project_form = { $valid: true };

      $httpBackend.expectPOST('/hrm/projects').respond(
        { project_id: 42, project_name: 'Project 42' }
      );

      $scope.save();
      $httpBackend.flush();

      expect($state.transitionTo).toHaveBeenCalledWith('project.list');
    });
  });

  // projectItemCtrl: state edit
  describe('state edit', function() {
    var $scope, $state;

    beforeEach(function() {
      $state = {
        transitionTo: function() {}
      };

      $scope = $parentScope.$new();
      $controller('projectItemCtrl', { $scope: $scope, $state: $state, $stateParams: { projectId: 42 } });

      $httpBackend.expectGET('/hrm/projects/types').respond([
        { type_id: 1, project_type: 'Type 1' },
        { type_id: 2, project_type: 'Type 2' }
      ]);

      $httpBackend.expectGET('/hrm/locations').respond([
        { location_id: 1, location: 'Location 1' },
        { location_id: 2, location: 'Location 2' }
      ]);

      $httpBackend.expectGET('/hrm/projects/42').respond({
        project_id: 42,
        project_name: 'Project 42',
        locations: [
          { project_location_id: 1, project_id: 42 },
          { project_location_id: 2, project_id: 42 }
        ],
        addendums: [
          { addendum_id: 1, project_id: 42, addendum_name: 'Addendum 1' },
          { addendum_id: 2, project_id: 42, addendum_name: 'Addendum 2' }
        ]
      });

      $httpBackend.flush();
    });

    it('harus membuat item data', function() {
      expect($scope.item).toEqualData({
        project_id: 42,
        project_name: 'Project 42',
        locations: [
          { project_location_id: 1, project_id: 42 },
          { project_location_id: 2, project_id: 42 }
        ],
        addendums: [
          { addendum_id: 1, project_id: 42, addendum_name: 'Addendum 1' },
          { addendum_id: 2, project_id: 42, addendum_name: 'Addendum 2' }
        ]
      });
    });

    it('harus menyimpan data', function() {
      spyOn($state, 'transitionTo');

      $scope.project_form = { $valid: true };

      $httpBackend.expectPUT('/hrm/projects/42').respond(
        { project_id: 42, project_name: 'Project 42' }
      );

      $scope.save();
      $httpBackend.flush();

      expect($state.transitionTo).toHaveBeenCalledWith('project.list');
    });

    it('harus membatalkan add/edit dan kembali ke lokasi url sebelumnya', inject(function($window) {
      spyOn($window.history, 'back');
      $scope.cancel();
      expect($window.history.back).toHaveBeenCalled();
    }));
  });

});
