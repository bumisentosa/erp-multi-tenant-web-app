'use strict';

describe('hrm/controllers/bankCtrl', function() {
  var $rootScope, $scope, $httpBackend, toolbar;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
    $httpBackend = _$httpBackend_;
    toolbar = _toolbar_;

    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    var state = { current: { data: { title: 'Data Bank' }}};
    $controller('bankCtrl', { $scope: $scope, $state: state });

    $httpBackend.expectGET('/hrm/banks').respond([
      { bank_id: 1, bank_name: 'Bank 1', branch: 'Branch 1' },
      { bank_id: 2, bank_name: 'Bank 2', branch: 'Branch 2' },
      { bank_id: 3, bank_name: 'Bank 3', branch: 'Branch 3' },
      { bank_id: 4, bank_name: 'Bank 4', branch: 'Branch 4' }
    ]);
    $httpBackend.flush();
  }));

  it('harus mengambil data', function() {
    expect($scope.data).toEqualData([
      { bank_id: 1, bank_name: 'Bank 1', branch: 'Branch 1' },
      { bank_id: 2, bank_name: 'Bank 2', branch: 'Branch 2' },
      { bank_id: 3, bank_name: 'Bank 3', branch: 'Branch 3' },
      { bank_id: 4, bank_name: 'Bank 4', branch: 'Branch 4' }
    ]);
  });

  it('harus merespon selectedItems pada toolbar', function() {
    $scope.selectedItems.length = 0;
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.selectedItems.push($scope.data[2]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
  });

  it('harus dapat menampilkan input box search', function() {
    $rootScope.$broadcast('toolbar-main-click', 'show-search');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['search']);

    $rootScope.$broadcast('toolbar-search-click', 'back');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
  });

  it('harus me-refresh data', function() {
    expect($scope.data).toEqualData([
      { bank_id: 1, bank_name: 'Bank 1', branch: 'Branch 1' },
      { bank_id: 2, bank_name: 'Bank 2', branch: 'Branch 2' },
      { bank_id: 3, bank_name: 'Bank 3', branch: 'Branch 3' },
      { bank_id: 4, bank_name: 'Bank 4', branch: 'Branch 4' }
    ]);

    $httpBackend.expectGET('/hrm/banks').respond([
      { bank_id: 1, bank_name: 'Bank 1x', branch: 'Branch 1x' },
      { bank_id: 2, bank_name: 'Bank 2x', branch: 'Branch 2x' },
      { bank_id: 3, bank_name: 'Bank 3x', branch: 'Branch 3x' },
      { bank_id: 4, bank_name: 'Bank 4x', branch: 'Branch 4x' }
    ]);
    $rootScope.$broadcast('toolbar-main-click', 'refresh');
    $httpBackend.flush();
    $scope.$apply();

    expect($scope.data).toEqualData([
      { bank_id: 1, bank_name: 'Bank 1x', branch: 'Branch 1x' },
      { bank_id: 2, bank_name: 'Bank 2x', branch: 'Branch 2x' },
      { bank_id: 3, bank_name: 'Bank 3x', branch: 'Branch 3x' },
      { bank_id: 4, bank_name: 'Bank 4x', branch: 'Branch 4x' }
    ]);
  });

  it('harus menambah data', function() {
    var newItem = {
      bank_id: 42,
      bank_name: 'New Bank',
      branch: 'New'
    };
    spyOnModal().andReturn(newItem);

    $httpBackend.expectPOST('/hrm/banks').respond(newItem);

    $rootScope.$broadcast('toolbar-main-click', 'add');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data.length).toBe(5);
    expect($scope.data[4]).toEqualData(newItem);
  });

  it('harus mengubah data', function() {
    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    var editedItem = {
      bank_id: $scope.selectedItems[0].bank_id,
      bank_name: 'Bank Edit',
      branch: 'Cabang Edit'
    };
    spyOnModal().andReturn(editedItem);

    $httpBackend.expectPUT('/hrm/banks/' + editedItem.bank_id).respond(editedItem);

    $rootScope.$broadcast('toolbar-main-click', 'edit');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data[2]).toEqualData(editedItem);
  });

  it('harus menghapus data', function() {
    expect($scope.data.length).toBe(4);

    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    spyOnModal().andReturn($scope.selectedItems);
    $httpBackend.expectDELETE('/hrm/banks/' + $scope.selectedItems[0].bank_id).respond(1);
    $httpBackend.expectGET('/hrm/banks').respond([
      { bank_id: 1, bank_name: 'Bank 1', branch: 'Branch 1' },
      { bank_id: 2, bank_name: 'Bank 2', branch: 'Branch 2' },
      { bank_id: 4, bank_name: 'Bank 4', branch: 'Branch 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'remove');
    $scope.$apply();
    expect($scope.data.length).toBe(3);

    $httpBackend.flush();
    expect($scope.data).toEqualData([
      { bank_id: 1, bank_name: 'Bank 1', branch: 'Branch 1' },
      { bank_id: 2, bank_name: 'Bank 2', branch: 'Branch 2' },
      { bank_id: 4, bank_name: 'Bank 4', branch: 'Branch 4' }
    ]);
  });
});
