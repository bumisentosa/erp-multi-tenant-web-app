'use strict';

describe('hrm/controllers/projectCtrl', function() {
  /**
   * ProjectCtrl
   */
  describe('projectCtrl', function() {
    var groups, $rootScope, $scope, $httpBackend, state, toolbar;

    beforeEach(module('hrm.controllers'));

    beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
      $httpBackend = _$httpBackend_;
      toolbar = _toolbar_;

      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      state = {
        current: { name: 'project.list', data: { title: 'Data Proyek' }},
        transitionTo: function() {}
      };
      $controller('projectCtrl', { $scope: $scope, $state: state });

      groups = [
        { group_id: 1, project_group: 'Group 1' },
        { group_id: 2, project_group: 'Group 2' },
        { group_id: 3, project_group: 'Group 3' },
        { group_id: 4, project_group: 'Group 4' },
        { group_id: 5, project_group: 'Group 5' }
      ];
      $httpBackend.expectGET('/hrm/projects/groups').respond(groups);

      $httpBackend.flush();
    }));

    it('harus mengambil data awal', function() {
      expect($scope.groups).toEqualData(groups);
    });

    it('harus merespon selectedItems pada toolbar', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 3, project_name: 'Project 3' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
      expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

      $scope.selectedItems.push($scope.data[0]);
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main']);
      expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

      $scope.selectedItems.push($scope.data[0]);
      $scope.selectedItems.push($scope.data[2]);
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main']);
      expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
    });

    it('harus dapat menampilkan input box search', function() {
      $rootScope.$broadcast('toolbar-main-click', 'show-search');
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['search']);

      $rootScope.$broadcast('toolbar-search-click', 'back');
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    });

    it('harus merespon event tambah data', function() {
      spyOn($scope, 'edit');
      $rootScope.$broadcast('toolbar-main-click', 'add');
      expect($scope.edit.mostRecentCall.args[0]).toEqualData({});
    });

    it('harus merespon event ubah data', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 3, project_name: 'Project 3' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.selectedItems.push($scope.data[2]);

      spyOn($scope, 'edit');
      $rootScope.$broadcast('toolbar-main-click', 'edit');
      expect($scope.edit.mostRecentCall.args[0]).not.toBe($scope.data[2]);
      expect($scope.edit.mostRecentCall.args[0]).toEqualData($scope.data[2]);
    });

    it('harus transisi ke project.add', function() {
      spyOn(state, 'transitionTo');
      $scope.edit();
      expect(state.transitionTo).toHaveBeenCalledWith('project.add');
    });

    it('harus transisi ke project.edit', function() {
      spyOn(state, 'transitionTo');
      $scope.edit({ project_id: 42, project_name: 'Project To Edit' });
      expect(state.transitionTo).toHaveBeenCalledWith('project.edit', { projectId: 42 });
    });

    it('harus merespon event hapus data', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 3, project_name: 'Project 3' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.selectedItems.push($scope.data[2]);

      spyOnModal().andReturn($scope.selectedItems);
      $httpBackend.expectDELETE('/hrm/projects/' + $scope.selectedItems[0].project_id).respond(1);
      $httpBackend.expectGET('/hrm/projects').respond([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 4, project_name: 'Project 4' }
      ]);

      $rootScope.$broadcast('toolbar-main-click', 'remove');
      $scope.$apply();
      expect($scope.data.length).toBe(3);

      $httpBackend.flush();
      expect($scope.data).toEqualData([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
    });

    it('harus merespon event refresh data', function() {
      $httpBackend.expectGET('/hrm/projects').respond([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 3, project_name: 'Project 3' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
      $rootScope.$broadcast('toolbar-main-click', 'refresh');
      $httpBackend.flush();
      $scope.$apply();

      expect($scope.data).toEqualData([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' },
        { project_id: 3, project_name: 'Project 3' },
        { project_id: 4, project_name: 'Project 4' }
      ]);
    });

    it('harus dapat mengelompokkan items', function() {
      $scope.groups.length = 0;
      $scope.groups.push.apply($scope.groups, [
        { group_id: 1, group_name: 'Group 1' },
        { group_id: 2, group_name: 'Group 2' },
        { group_id: 3, group_name: 'Group 3' },
        { group_id: 4, group_name: 'Group 4' }
      ]);

      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { project_id: 1, group_id: 1, project_name: 'Project 1' },
        { project_id: 2, group_id: 1, project_name: 'Project 2' },
        { project_id: 3, group_id: 2, project_name: 'Project 3' },
        { project_id: 4, group_id: 2, project_name: 'Project 4' },
        { project_id: 5, group_id: 2, project_name: 'Project 5' },
        { project_id: 6, group_id: 3, project_name: 'Project 6' },
        { project_id: 7, group_id: 4, project_name: 'Project 7' },
        { project_id: 8, group_id: 4, project_name: 'Project 8' },
        { project_id: 9, group_id: 4, project_name: 'Project 9' },
        { project_id: 10, group_id: 42, project_name: 'Project 10' }
      ]);

      $scope.$apply();

      expect($scope.dataGroup).toEqualData([
        { isGroup: true, group_id: 1, group_name: 'Group 1' },
        { project_id: 1, group_id: 1, project_name: 'Project 1' },
        { project_id: 2, group_id: 1, project_name: 'Project 2' },

        { isGroup: true, group_id: 2, group_name: 'Group 2' },
        { project_id: 3, group_id: 2, project_name: 'Project 3' },
        { project_id: 4, group_id: 2, project_name: 'Project 4' },
        { project_id: 5, group_id: 2, project_name: 'Project 5' },

        { isGroup: true, group_id: 3, group_name: 'Group 3' },
        { project_id: 6, group_id: 3, project_name: 'Project 6' },

        { isGroup: true, group_id: 4, group_name: 'Group 4' },
        { project_id: 7, group_id: 4, project_name: 'Project 7' },
        { project_id: 8, group_id: 4, project_name: 'Project 8' },
        { project_id: 9, group_id: 4, project_name: 'Project 9' },

        { isGroup: true, group_name: 'Unknown Group' },
        { project_id: 10, group_id: 42, project_name: 'Project 10' }
      ]);
    });

    it('harus menampilkan/menyembunyikan children', function() {
      $scope.groups.length = 0;
      $scope.groups.push.apply($scope.groups, [
        { group_id: 1, group_name: 'Group 1' },
        { group_id: 2, group_name: 'Group 2' },
        { group_id: 3, group_name: 'Group 3' },
        { group_id: 4, group_name: 'Group 4' }
      ]);

      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { project_id: 1, group_id: 1, project_name: 'Project 1' },
        { project_id: 2, group_id: 1, project_name: 'Project 2' },
        { project_id: 3, group_id: 2, project_name: 'Project 3' },
        { project_id: 4, group_id: 2, project_name: 'Project 4' },
        { project_id: 5, group_id: 2, project_name: 'Project 5' },
        { project_id: 6, group_id: 3, project_name: 'Project 6' },
        { project_id: 7, group_id: 4, project_name: 'Project 7' },
        { project_id: 8, group_id: 4, project_name: 'Project 8' },
        { project_id: 9, group_id: 4, project_name: 'Project 9' },
        { project_id: 10, group_id: 42, project_name: 'Project 10' }
      ]);

      $scope.$apply();

      $scope.showHideChildren($scope.groups[0]);
      expect($scope.hiddenGroups).toEqualData([1]);
      expect($scope.hiddenItems).toEqualData([1, 2]);

      $scope.showHideChildren($scope.groups[2]);
      expect($scope.hiddenGroups).toEqualData([1, 3]);
      expect($scope.hiddenItems).toEqualData([1, 2, 6]);

      $scope.showHideChildren($scope.groups[0]);
      expect($scope.hiddenGroups).toEqualData([3]);
      expect($scope.hiddenItems).toEqualData([6]);

      $scope.showHideChildren({ group_id: 42 });
      expect($scope.hiddenGroups).toEqualData([3, 42]);
      expect($scope.hiddenItems).toEqualData([6, 10]);
    });
  });

  /**
   * projectListCtrl
   */
  describe('projectListCtrl', function() {
    var $rootScope, $scope, $httpBackend, toolbar;

    beforeEach(module('hrm.controllers'));

    beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
      $httpBackend = _$httpBackend_;
      toolbar = _toolbar_;

      $rootScope = _$rootScope_;

      // projectCtrl
      var $parentScope = $rootScope.$new();
      var parentState = {
        current: { name: 'project', data: { title: 'Data Proyek' }},
        transitionTo: function() {}
      };
      $controller('projectCtrl', { $scope: $parentScope, $state: parentState });

      $httpBackend.expectGET('/hrm/projects/groups').respond([
        { group_id: 1, project_group: 'Group 1' },
        { group_id: 2, project_group: 'Group 2' }
      ]);

      $httpBackend.flush();

      // projectListCtrl
      $scope = $parentScope.$new();
      $controller('projectListCtrl', { $scope: $scope });

      $httpBackend.expectGET('/hrm/projects').respond([
        { project_id: 1, project_name: 'Project 1'},
        { project_id: 2, project_name: 'Project 2'},
        { project_id: 3, project_name: 'Project 3'}
      ]);

      $httpBackend.flush();
    }));

    it('harus update toolbar', function() {
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
      expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);
    });

    it('harus langsung mengambil data', function() {
      expect($scope.$parent.data).toEqualData([
        { project_id: 1, project_name: 'Project 1'},
        { project_id: 2, project_name: 'Project 2'},
        { project_id: 3, project_name: 'Project 3'}
      ]);
    });
  });
});
