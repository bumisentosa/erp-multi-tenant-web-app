'use strict';

describe('hrm/controllers/employeeItemCtrl', function() {
  var $httpBackend, $controller, $parentScope;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function($rootScope, _$httpBackend_, _$controller_, _toolbar_) {
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;

    $parentScope = $rootScope.$new();

    // employeeCtrl
    var parentState = {
      current: { name: 'employee', data: { title: 'Data Pegawai' }},
      transitionTo: function() {}
    };
    $controller('employeeCtrl', { $scope: $parentScope, $state: parentState });

    $httpBackend.expectGET('/hrm/projects').respond([
      { project_id: 1, project_name: 'Proyek 1' },
      { project_id: 2, project_name: 'Proyek 2' }
    ]);

    $httpBackend.flush();
  }));

  // employeeItemCtrl: state add
  describe('state add', function() {
    var $scope, $state;

    beforeEach(function() {
      $state = {
        transitionTo: function() {}
      };

      $scope = $parentScope.$new();
      $controller('employeeItemCtrl', { $scope: $scope, $state: $state, $stateParams: {} });

      $httpBackend.expectGET('/hrm/locations').respond([
        { location_id: 1, location: 'Location 1' },
        { location_id: 2, location: 'Location 2' }
      ]);

      $httpBackend.expectGET('/hrm/positions').respond([
        { position_id: 1, position: 'Position 1' },
        { position_id: 2, position: 'Position 2' }
      ]);

      $httpBackend.expectGET('/hrm/driver-licenses').respond([
        { license_code: 'A', description: 'Description 1' },
        { license_code: 'B', description: 'Description 2' }
      ]);

      $httpBackend.expectGET('/hrm/religions').respond([
        { religion_id: 1, religion: 'Religion 1' },
        { religion_id: 2, religion: 'Religion 2' }
      ]);

      $httpBackend.expectGET('/hrm/educations/levels').respond([
        { level_id: 1, education_level: 'Education Level 1' },
        { level_id: 2, education_level: 'Education Level 2' }
      ]);

      $httpBackend.expectGET('/hrm/competences').respond([
        { competence_id: 1, competence_name: 'Position 1' },
        { competence_id: 2, competence_name: 'Position 2' }
      ]);

      $httpBackend.expectGET('/hrm/social-security').respond([
        { ss_code: 1, ss_name: 'Social Security 1' },
        { ss_code: 2, ss_name: 'Social Security 2' }
      ]);

      $httpBackend.expectGET('/hrm/banks').respond([
        { bank_id: 1, bank_name: 'Bank 1' },
        { bank_id: 2, bank_name: 'Bank 2' }
      ]);

      $httpBackend.expectGET('/hrm/letters/types').respond([
        { type_id: 1, letter_type: 'Letter Type 1' },
        { type_id: 2, letter_type: 'Letter Type 2' }
      ]);

      $httpBackend.flush();
    });

    it('harus membuat data kosong', function() {
      expect($scope.item).toEqualData({
        projects: [{}],
        locations: [{}],
        positions: [{}],
        driver_licenses: [{}],
        children: [{}],
        educations: [{}],
        educations_non_formal: [{}],
        experiences: [{}],
        organizations: [{}],
        competences: [{}],
        social_security: [{}],
        letters: [{}]
      });
    });

    describe('photo', function() {
      it('harus tidak ada foto', function() {
        expect($scope.item.photo_url).toBeUndefined();
        expect($scope.item.photo_url_old).toBeUndefined();
        expect($scope.item.photos).toBeUndefined();
      });

      it('harus menandai foto diganti', function() {
        $scope.onPhotoChanged();
        expect($scope.item.photo_changed).toBeTruthy();
      });

      it('harus menghapus foto', function() {
        $scope.item = {};
        $scope.removePhoto();
        expect($scope.item.photo_url).toBeNull();
        expect($scope.item.photos).toBeNull();
        expect($scope.item.photo_changed).toBeTruthy();
      });

      it('harus mereset foto', function() {
        $scope.item = {
          photo_url: '/path/to/photo.jpg',
          photos: ['/path/to/photo.jpg'],
          photo_url_old: '/path/to/photo_old.jpg'
        };
        $scope.resetPhoto();
        expect($scope.item.photo_url).toBe('/path/to/photo_old.jpg');
        expect($scope.item.photos).toEqualData(['/path/to/photo_old.jpg']);
        expect($scope.item.photo_url_old).toBe('/path/to/photo_old.jpg');
        expect($scope.item.photo_changed).toBeFalsy();

        $scope.item = {};
        $scope.resetPhoto();
        expect($scope.item.photo_url).toBeUndefined();
        expect($scope.item.photos).toEqualData([undefined]);
        expect($scope.item.photo_url_old).toBeUndefined();
        expect($scope.item.photo_changed).toBeFalsy();
      });

      it('harus meng-upload foto', function() {
        spyOn($state, 'transitionTo');
        $scope.employee_form = { $valid: true };
        $scope.item.photo_changed = true;
        $scope.item.photos = ['/path/to/photo.jpg'];

        $httpBackend.expectPOST('/hrm/employees').respond(
          { employee_id: 42, employee_name: 'Employee 42' }
        );
        $httpBackend.expectPOST('/hrm/employees/42/photo').respond(
          { url: '/path/to/photo_42.jpg' }
        );
        $scope.save();
        $httpBackend.flush();

        expect($state.transitionTo).toHaveBeenCalledWith('employee.list');
      });
    });

    describe('projects', function() {
      it('harus menambah proyek', function() {
        expect($scope.item.projects.length).toBe(1);

        $scope.addItem('projects');
        expect($scope.item.projects.length).toBe(2);

        $scope.addItem('projects');
        expect($scope.item.projects.length).toBe(3);
      });

      it('harus menghapus proyek', function() {
        $scope.addItem('projects');
        $scope.addItem('projects');

        expect($scope.item.projects.length).toBe(3);

        $scope.removeItem('projects', 1);
        expect($scope.item.projects.length).toBe(2);

        $scope.removeItem('projects', 42);
        expect($scope.item.projects.length).toBe(2);

        $scope.removeItem('projects', 1);
        expect($scope.item.projects.length).toBe(1);

        $scope.removeItem('projects', 1);
        expect($scope.item.projects.length).toBe(1);

        $scope.removeItem('projects', 0);
        expect($scope.item.projects.length).toBe(1);
      });
    });

    describe('locations', function() {
      it('harus menambah lokasi', function() {
        expect($scope.item.locations.length).toBe(1);

        $scope.addItem('locations');
        expect($scope.item.locations.length).toBe(2);

        $scope.addItem('locations');
        expect($scope.item.locations.length).toBe(3);
      });

      it('harus menghapus lokasi', function() {
        $scope.addItem('locations');
        $scope.addItem('locations');

        expect($scope.item.locations.length).toBe(3);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(2);

        $scope.removeItem('locations', 42);
        expect($scope.item.locations.length).toBe(2);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(1);

        $scope.removeItem('locations', 1);
        expect($scope.item.locations.length).toBe(1);

        $scope.removeItem('locations', 0);
        expect($scope.item.locations.length).toBe(1);
      });
    });

    describe('positions', function() {
      it('harus menambah jabatan', function() {
        expect($scope.item.positions.length).toBe(1);

        $scope.addItem('positions');
        expect($scope.item.positions.length).toBe(2);

        $scope.addItem('positions');
        expect($scope.item.positions.length).toBe(3);
      });

      it('harus menghapus jabatan', function() {
        $scope.addItem('positions');
        $scope.addItem('positions');

        expect($scope.item.positions.length).toBe(3);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(2);

        $scope.removeItem('positions', 42);
        expect($scope.item.positions.length).toBe(2);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(1);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(1);

        $scope.removeItem('positions', 0);
        expect($scope.item.positions.length).toBe(1);
      });
    });

    describe('positions', function() {
      it('harus menambah jabatan', function() {
        expect($scope.item.positions.length).toBe(1);

        $scope.addItem('positions');
        expect($scope.item.positions.length).toBe(2);

        $scope.addItem('positions');
        expect($scope.item.positions.length).toBe(3);
      });

      it('harus menghapus jabatan', function() {
        $scope.addItem('positions');
        $scope.addItem('positions');

        expect($scope.item.positions.length).toBe(3);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(2);

        $scope.removeItem('positions', 42);
        expect($scope.item.positions.length).toBe(2);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(1);

        $scope.removeItem('positions', 1);
        expect($scope.item.positions.length).toBe(1);

        $scope.removeItem('positions', 0);
        expect($scope.item.positions.length).toBe(1);
      });
    });

    describe('children', function() {
      it('harus menambah anak', function() {
        expect($scope.item.children.length).toBe(1);

        $scope.addItem('children');
        expect($scope.item.children.length).toBe(2);

        $scope.addItem('children');
        expect($scope.item.children.length).toBe(3);
      });

      it('harus menghapus anak', function() {
        $scope.addItem('children');
        $scope.addItem('children');

        expect($scope.item.children.length).toBe(3);

        $scope.removeItem('children', 1);
        expect($scope.item.children.length).toBe(2);

        $scope.removeItem('children', 42);
        expect($scope.item.children.length).toBe(2);

        $scope.removeItem('children', 1);
        expect($scope.item.children.length).toBe(1);

        $scope.removeItem('children', 1);
        expect($scope.item.children.length).toBe(1);

        $scope.removeItem('children', 0);
        expect($scope.item.children.length).toBe(1);
      });
    });

    describe('educations', function() {
      it('harus menambah pendidikan', function() {
        expect($scope.item.educations.length).toBe(1);

        $scope.addItem('educations');
        expect($scope.item.educations.length).toBe(2);

        $scope.addItem('educations');
        expect($scope.item.educations.length).toBe(3);
      });

      it('harus menghapus pendidikan', function() {
        $scope.addItem('educations');
        $scope.addItem('educations');

        expect($scope.item.educations.length).toBe(3);

        $scope.removeItem('educations', 1);
        expect($scope.item.educations.length).toBe(2);

        $scope.removeItem('educations', 42);
        expect($scope.item.educations.length).toBe(2);

        $scope.removeItem('educations', 1);
        expect($scope.item.educations.length).toBe(1);

        $scope.removeItem('educations', 1);
        expect($scope.item.educations.length).toBe(1);

        $scope.removeItem('educations', 0);
        expect($scope.item.educations.length).toBe(1);
      });
    });

    describe('educations_non_formal', function() {
      it('harus menambah pendidikan non formal', function() {
        expect($scope.item.educations_non_formal.length).toBe(1);

        $scope.addItem('educations_non_formal');
        expect($scope.item.educations_non_formal.length).toBe(2);

        $scope.addItem('educations_non_formal');
        expect($scope.item.educations_non_formal.length).toBe(3);
      });

      it('harus menghapus pendidikan non formal', function() {
        $scope.addItem('educations_non_formal');
        $scope.addItem('educations_non_formal');

        expect($scope.item.educations_non_formal.length).toBe(3);

        $scope.removeItem('educations_non_formal', 1);
        expect($scope.item.educations_non_formal.length).toBe(2);

        $scope.removeItem('educations_non_formal', 42);
        expect($scope.item.educations_non_formal.length).toBe(2);

        $scope.removeItem('educations_non_formal', 1);
        expect($scope.item.educations_non_formal.length).toBe(1);

        $scope.removeItem('educations_non_formal', 1);
        expect($scope.item.educations_non_formal.length).toBe(1);

        $scope.removeItem('educations_non_formal', 0);
        expect($scope.item.educations_non_formal.length).toBe(1);
      });
    });

    describe('experiences', function() {
      it('harus menambah pengalaman', function() {
        expect($scope.item.experiences.length).toBe(1);

        $scope.addItem('experiences');
        expect($scope.item.experiences.length).toBe(2);

        $scope.addItem('experiences');
        expect($scope.item.experiences.length).toBe(3);
      });

      it('harus menghapus pengalaman', function() {
        $scope.addItem('experiences');
        $scope.addItem('experiences');

        expect($scope.item.experiences.length).toBe(3);

        $scope.removeItem('experiences', 1);
        expect($scope.item.experiences.length).toBe(2);

        $scope.removeItem('experiences', 42);
        expect($scope.item.experiences.length).toBe(2);

        $scope.removeItem('experiences', 1);
        expect($scope.item.experiences.length).toBe(1);

        $scope.removeItem('experiences', 1);
        expect($scope.item.experiences.length).toBe(1);

        $scope.removeItem('experiences', 0);
        expect($scope.item.experiences.length).toBe(1);
      });
    });

    describe('organizations', function() {
      it('harus menambah organisasi', function() {
        expect($scope.item.organizations.length).toBe(1);

        $scope.addItem('organizations');
        expect($scope.item.organizations.length).toBe(2);

        $scope.addItem('organizations');
        expect($scope.item.organizations.length).toBe(3);
      });

      it('harus menghapus organisasi', function() {
        $scope.addItem('organizations');
        $scope.addItem('organizations');

        expect($scope.item.organizations.length).toBe(3);

        $scope.removeItem('organizations', 1);
        expect($scope.item.organizations.length).toBe(2);

        $scope.removeItem('organizations', 42);
        expect($scope.item.organizations.length).toBe(2);

        $scope.removeItem('organizations', 1);
        expect($scope.item.organizations.length).toBe(1);

        $scope.removeItem('organizations', 1);
        expect($scope.item.organizations.length).toBe(1);

        $scope.removeItem('organizations', 0);
        expect($scope.item.organizations.length).toBe(1);
      });
    });

    describe('competences', function() {
      it('harus menambah kompetensi', function() {
        expect($scope.item.competences.length).toBe(1);

        $scope.addItem('competences');
        expect($scope.item.competences.length).toBe(2);

        $scope.addItem('competences');
        expect($scope.item.competences.length).toBe(3);
      });

      it('harus menghapus kompetensi', function() {
        $scope.addItem('competences');
        $scope.addItem('competences');

        expect($scope.item.competences.length).toBe(3);

        $scope.removeItem('competences', 1);
        expect($scope.item.competences.length).toBe(2);

        $scope.removeItem('competences', 42);
        expect($scope.item.competences.length).toBe(2);

        $scope.removeItem('competences', 1);
        expect($scope.item.competences.length).toBe(1);

        $scope.removeItem('competences', 1);
        expect($scope.item.competences.length).toBe(1);

        $scope.removeItem('competences', 0);
        expect($scope.item.competences.length).toBe(1);
      });
    });

    describe('social_security', function() {
      it('harus menambah perlindungan sosial', function() {
        expect($scope.item.social_security.length).toBe(1);

        $scope.addItem('social_security');
        expect($scope.item.social_security.length).toBe(2);

        $scope.addItem('social_security');
        expect($scope.item.social_security.length).toBe(3);
      });

      it('harus menghapus perlindungan sosial', function() {
        $scope.addItem('social_security');
        $scope.addItem('social_security');

        expect($scope.item.social_security.length).toBe(3);

        $scope.removeItem('social_security', 1);
        expect($scope.item.social_security.length).toBe(2);

        $scope.removeItem('social_security', 42);
        expect($scope.item.social_security.length).toBe(2);

        $scope.removeItem('social_security', 1);
        expect($scope.item.social_security.length).toBe(1);

        $scope.removeItem('social_security', 1);
        expect($scope.item.social_security.length).toBe(1);

        $scope.removeItem('social_security', 0);
        expect($scope.item.social_security.length).toBe(1);
      });
    });

    describe('letters', function() {
      it('harus menambah pencatatan surat', function() {
        expect($scope.item.letters.length).toBe(1);

        $scope.addItem('letters');
        expect($scope.item.letters.length).toBe(2);

        $scope.addItem('letters');
        expect($scope.item.letters.length).toBe(3);
      });

      it('harus menghapus pencatatan surat', function() {
        $scope.addItem('letters');
        $scope.addItem('letters');

        expect($scope.item.letters.length).toBe(3);

        $scope.removeItem('letters', 1);
        expect($scope.item.letters.length).toBe(2);

        $scope.removeItem('letters', 42);
        expect($scope.item.letters.length).toBe(2);

        $scope.removeItem('letters', 1);
        expect($scope.item.letters.length).toBe(1);

        $scope.removeItem('letters', 1);
        expect($scope.item.letters.length).toBe(1);

        $scope.removeItem('letters', 0);
        expect($scope.item.letters.length).toBe(1);
      });
    });

    it('harus menyimpan data', function() {
      spyOn($state, 'transitionTo');

      $scope.employee_form = { $valid: true };

      $httpBackend.expectPOST('/hrm/employees').respond(
        { employee_id: 42, employee_name: 'Employee 42' }
      );

      $scope.save();
      $httpBackend.flush();

      expect($state.transitionTo).toHaveBeenCalledWith('employee.list');
    });
  });

  // employeeItemCtrl: state edit
  describe('state edit', function() {
    var $scope, $state;

    beforeEach(function() {
      $state = {
        transitionTo: function() {}
      };

      $scope = $parentScope.$new();
      $controller('employeeItemCtrl', { $scope: $scope, $state: $state, $stateParams: { employeeId: 42 } });

      $httpBackend.expectGET('/hrm/locations').respond([
        { mutation_id: 1, location: 'Location 1' },
        { mutation_id: 2, location: 'Location 2' }
      ]);

      $httpBackend.expectGET('/hrm/positions').respond([
        { position_id: 1, position: 'Position 1' },
        { position_id: 2, position: 'Position 2' }
      ]);

      $httpBackend.expectGET('/hrm/driver-licenses').respond([
        { license_code: 'A', description: 'Description 1' },
        { license_code: 'B', description: 'Description 2' }
      ]);

      $httpBackend.expectGET('/hrm/religions').respond([
        { religion_id: 1, religion: 'Religion 1' },
        { religion_id: 2, religion: 'Religion 2' }
      ]);

      $httpBackend.expectGET('/hrm/educations/levels').respond([
        { level_id: 1, education_level: 'Education Level 1' },
        { level_id: 2, education_level: 'Education Level 2' }
      ]);

      $httpBackend.expectGET('/hrm/competences').respond([
        { competence_id: 1, competence_name: 'Position 1' },
        { competence_id: 2, competence_name: 'Position 2' }
      ]);

      $httpBackend.expectGET('/hrm/social-security').respond([
        { ss_code: 1, ss_name: 'Social Security 1' },
        { ss_code: 2, ss_name: 'Social Security 2' }
      ]);

      $httpBackend.expectGET('/hrm/banks').respond([
        { bank_id: 1, bank_name: 'Bank 1' },
        { bank_id: 2, bank_name: 'Bank 2' }
      ]);

      $httpBackend.expectGET('/hrm/letters/types').respond([
        { type_id: 1, letter_type: 'Letter Type 1' },
        { type_id: 2, letter_type: 'Letter Type 2' }
      ]);

      $httpBackend.expectGET('/hrm/employees/42').respond({
        employee_id: 42,
        employee_name: 'Employee 42',
        photo_url: '/path/to/photo.jpg',
        projects: [
          { mutation_id: 1, employee_id: 42, project_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, project_id: 2, mutation_date: '2015-12-01' }
        ],
        locations: [
          { mutation_id: 1, employee_id: 42, location_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, location_id: 2, mutation_date: '2015-12-01' }
        ],
        positions: [
          { mutation_id: 1, employee_id: 42, position_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, position_id: 2, mutation_date: '2015-12-01' }
        ],
        driver_licenses: [
          { employee_license_id: 1, employee_id: 42, license_code: 'A', license_no: 'License 1' },
          { employee_license_id: 2, employee_id: 42, license_code: 'B', license_no: 'License 2' }
        ],
        children: [
          { child_id: 1, employee_id: 42, child_name: 'Child 1' },
          { child_id: 2, employee_id: 42, child_name: 'Child 2' }
        ],
        educations: [
          { education_id: 1, employee_id: 42, education_name: 'Education 1' },
          { education_id: 2, employee_id: 42, education_name: 'Education 2' }
        ],
        educations_non_formal: [
          { education_id: 1, employee_id: 42, education_name: 'Education Non Formal 1' },
          { education_id: 2, employee_id: 42, education_name: 'Education Non Formal 2' }
        ],
        experiences: [
          { experience_id: 1, employee_id: 42, company_name: 'Company 1' },
          { experience_id: 2, employee_id: 42, company_name: 'Company 2' }
        ],
        organizations: [
          { organization_id: 1, employee_id: 42, organization_name: 'Organization 1' },
          { organization_id: 2, employee_id: 42, organization_name: 'Organization 2' }
        ],
        competences: [
          { employee_competence_id: 1, employee_id: 42, competence_id: 1, competence_no: 'Competence 1' },
          { employee_competence_id: 2, employee_id: 42, competence_id: 2, competence_no: 'Competence 2' }
        ],
        social_security: [
          { employee_ss_id: 1, employee_id: 42, ss_code: 'A', ss_no: 'Social Security 1' },
          { employee_ss_id: 2, employee_id: 42, ss_code: 'B', ss_no: 'Social Security 2' }
        ],
        letters: [
          { letter_id: 1, employee_id: 42, letter_no: 'Letter 1' },
          { letter_id: 2, employee_id: 42, letter_no: 'Letter 2' }
        ]
      });

      $httpBackend.flush();
    });

    it('harus membuat item data', function() {
      expect($scope.item).toEqualData({
        employee_id: 42,
        employee_name: 'Employee 42',
        photo_url: '/path/to/photo.jpg',
        photos: ['/path/to/photo.jpg'],
        photo_url_old: '/path/to/photo.jpg',
        projects: [
          { mutation_id: 1, employee_id: 42, project_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, project_id: 2, mutation_date: '2015-12-01' }
        ],
        locations: [
          { mutation_id: 1, employee_id: 42, location_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, location_id: 2, mutation_date: '2015-12-01' }
        ],
        positions: [
          { mutation_id: 1, employee_id: 42, position_id: 1, mutation_date: '2015-01-01' },
          { mutation_id: 2, employee_id: 42, position_id: 2, mutation_date: '2015-12-01' }
        ],
        driver_licenses: [
          { employee_license_id: 1, employee_id: 42, license_code: 'A', license_no: 'License 1' },
          { employee_license_id: 2, employee_id: 42, license_code: 'B', license_no: 'License 2' }
        ],
        children: [
          { child_id: 1, employee_id: 42, child_name: 'Child 1' },
          { child_id: 2, employee_id: 42, child_name: 'Child 2' }
        ],
        educations: [
          { education_id: 1, employee_id: 42, education_name: 'Education 1' },
          { education_id: 2, employee_id: 42, education_name: 'Education 2' }
        ],
        educations_non_formal: [
          { education_id: 1, employee_id: 42, education_name: 'Education Non Formal 1' },
          { education_id: 2, employee_id: 42, education_name: 'Education Non Formal 2' }
        ],
        experiences: [
          { experience_id: 1, employee_id: 42, company_name: 'Company 1' },
          { experience_id: 2, employee_id: 42, company_name: 'Company 2' }
        ],
        organizations: [
          { organization_id: 1, employee_id: 42, organization_name: 'Organization 1' },
          { organization_id: 2, employee_id: 42, organization_name: 'Organization 2' }
        ],
        competences: [
          { employee_competence_id: 1, employee_id: 42, competence_id: 1, competence_no: 'Competence 1' },
          { employee_competence_id: 2, employee_id: 42, competence_id: 2, competence_no: 'Competence 2' }
        ],
        social_security: [
          { employee_ss_id: 1, employee_id: 42, ss_code: 'A', ss_no: 'Social Security 1' },
          { employee_ss_id: 2, employee_id: 42, ss_code: 'B', ss_no: 'Social Security 2' }
        ],
        letters: [
          { letter_id: 1, employee_id: 42, letter_no: 'Letter 1' },
          { letter_id: 2, employee_id: 42, letter_no: 'Letter 2' }
        ]
      });
    });

    it('harus menyimpan data', function() {
      spyOn($state, 'transitionTo');

      $scope.employee_form = { $valid: true };

      $httpBackend.expectPUT('/hrm/employees/42').respond(
        { employee_id: 42, employee_name: 'Employee 42' }
      );

      $scope.save();
      $httpBackend.flush();

      expect($state.transitionTo).toHaveBeenCalledWith('employee.list');
    });

    it('harus membatalkan add/edit dan kembali ke lokasi url sebelumnya', inject(function($window) {
      spyOn($window.history, 'back');
      $scope.cancel();
      expect($window.history.back).toHaveBeenCalled();
    }));
  });

});
