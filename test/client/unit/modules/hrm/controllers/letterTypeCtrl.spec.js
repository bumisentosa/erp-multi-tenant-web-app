'use strict';

describe('hrm/controllers/letterTypeCtrl', function() {
  var $rootScope, $scope, $httpBackend, toolbar;

  beforeEach(module('hrm.controllers'));

  beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
    $httpBackend = _$httpBackend_;
    toolbar = _toolbar_;

    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    var state = { current: { data: { title: 'Data Kategori Surat' }}};
    $controller('letterTypeCtrl', { $scope: $scope, $state: state });

    $httpBackend.expectGET('/hrm/letters/types').respond([
      { type_id: 1, letter_type: 'Type 1' },
      { type_id: 2, letter_type: 'Type 2' },
      { type_id: 3, letter_type: 'Type 3' },
      { type_id: 4, letter_type: 'Type 4' }
    ]);
    $httpBackend.flush();
  }));

  it('harus mengambil data', function() {
    expect($scope.data).toEqualData([
      { type_id: 1, letter_type: 'Type 1' },
      { type_id: 2, letter_type: 'Type 2' },
      { type_id: 3, letter_type: 'Type 3' },
      { type_id: 4, letter_type: 'Type 4' }
    ]);
  });

  it('harus merespon selectedItems pada toolbar', function() {
    $scope.selectedItems.length = 0;
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

    $scope.selectedItems.push($scope.data[0]);
    $scope.selectedItems.push($scope.data[2]);
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main']);
    expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
  });

  it('harus dapat menampilkan input box search', function() {
    $rootScope.$broadcast('toolbar-main-click', 'show-search');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['search']);

    $rootScope.$broadcast('toolbar-search-click', 'back');
    $scope.$apply();
    expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
  });

  it('harus me-refresh data', function() {
    expect($scope.data).toEqualData([
      { type_id: 1, letter_type: 'Type 1' },
      { type_id: 2, letter_type: 'Type 2' },
      { type_id: 3, letter_type: 'Type 3' },
      { type_id: 4, letter_type: 'Type 4' }
    ]);

    $httpBackend.expectGET('/hrm/letters/types').respond([
      { type_id: 1, letter_type: 'Type 1x' },
      { type_id: 2, letter_type: 'Type 2x' },
      { type_id: 3, letter_type: 'Type 3x' },
      { type_id: 4, letter_type: 'Type 4x' }
    ]);
    $rootScope.$broadcast('toolbar-main-click', 'refresh');
    $httpBackend.flush();
    $scope.$apply();

    expect($scope.data).toEqualData([
      { type_id: 1, letter_type: 'Type 1x' },
      { type_id: 2, letter_type: 'Type 2x' },
      { type_id: 3, letter_type: 'Type 3x' },
      { type_id: 4, letter_type: 'Type 4x' }
    ]);
  });

  it('harus menambah data', function() {
    var newItem = {
      type_id: 42,
      letter_type: 'New Type'
    };
    spyOnModal().andReturn(newItem);

    $httpBackend.expectPOST('/hrm/letters/types').respond(newItem);

    $rootScope.$broadcast('toolbar-main-click', 'add');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data.length).toBe(5);
    expect($scope.data[4]).toEqualData(newItem);
  });

  it('harus mengubah data', function() {
    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    var editedItem = {
      type_id: $scope.selectedItems[0].type_id,
      letter_type: 'Type Edit'
    };
    spyOnModal().andReturn(editedItem);

    $httpBackend.expectPUT('/hrm/letters/types/' + editedItem.type_id).respond(editedItem);

    $rootScope.$broadcast('toolbar-main-click', 'edit');
    $scope.$apply();
    $httpBackend.flush();

    expect($scope.data[2]).toEqualData(editedItem);
  });

  it('harus menghapus data', function() {
    expect($scope.data.length).toBe(4);

    $scope.selectedItems.length = 0;
    $scope.selectedItems.push($scope.data[2]);

    spyOnModal().andReturn($scope.selectedItems);
    $httpBackend.expectDELETE('/hrm/letters/types/' + $scope.selectedItems[0].type_id).respond(1);
    $httpBackend.expectGET('/hrm/letters/types').respond([
      { type_id: 1, letter_type: 'Type 1' },
      { type_id: 2, letter_type: 'Type 2' },
      { type_id: 4, letter_type: 'Type 4' }
    ]);

    $rootScope.$broadcast('toolbar-main-click', 'remove');
    $scope.$apply();
    expect($scope.data.length).toBe(3);

    $httpBackend.flush();
    expect($scope.data).toEqualData([
      { type_id: 1, letter_type: 'Type 1' },
      { type_id: 2, letter_type: 'Type 2' },
      { type_id: 4, letter_type: 'Type 4' }
    ]);
  });
});
