'use strict';

describe('hrm/controllers/employeeCtrl', function() {
  /**
   * employeeCtrl
   */
  describe('employeeCtrl', function() {
    var $rootScope, $scope, $httpBackend, state, toolbar;

    beforeEach(module('hrm.controllers'));

    beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
      $httpBackend = _$httpBackend_;
      toolbar = _toolbar_;

      $rootScope = _$rootScope_;
      $scope = _$rootScope_.$new();
      state = {
        current: { name: 'employee.list', data: { title: 'Data Pegawai' }},
        transitionTo: function() {}
      };
      $controller('employeeCtrl', { $scope: $scope, $state: state });

      $httpBackend.expectGET('/hrm/projects').respond([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' }
      ]);
      $httpBackend.flush();
    }));

    it('harus mengambil data awal', function() {
      expect($scope.projects).toEqualData([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' }
      ]);
    });

    it('harus merespon selectedItems pada toolbar', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 3, employee_name: 'Employee 3' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
      expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);

      $scope.selectedItems.push($scope.data[0]);
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main']);
      expect(toolbar.buttons.main).toEqualData(['edit', 'remove', 'print']);

      $scope.selectedItems.push($scope.data[0]);
      $scope.selectedItems.push($scope.data[2]);
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main']);
      expect(toolbar.buttons.main).toEqualData(['remove', 'print']);
    });

    it('harus dapat menampilkan input box search', function() {
      $rootScope.$broadcast('toolbar-main-click', 'show-search');
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['search']);

      $rootScope.$broadcast('toolbar-search-click', 'back');
      $scope.$apply();
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
    });

    it('harus merespon event tambah data', function() {
      spyOn($scope, 'edit');
      $rootScope.$broadcast('toolbar-main-click', 'add');
      expect($scope.edit.mostRecentCall.args[0]).toEqualData({});
    });

    it('harus merespon event ubah data', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 3, employee_name: 'Employee 3' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.selectedItems.push($scope.data[2]);

      spyOn($scope, 'edit');
      $rootScope.$broadcast('toolbar-main-click', 'edit');
      expect($scope.edit.mostRecentCall.args[0]).not.toBe($scope.data[2]);
      expect($scope.edit.mostRecentCall.args[0]).toEqualData($scope.data[2]);
    });

    it('harus transisi ke employee.add', function() {
      spyOn(state, 'transitionTo');
      $scope.edit();
      expect(state.transitionTo).toHaveBeenCalledWith('employee.add');
    });

    it('harus transisi ke employee.edit', function() {
      spyOn(state, 'transitionTo');
      $scope.edit({ employee_id: 42, employee_name: 'Employee To Edit' });
      expect(state.transitionTo).toHaveBeenCalledWith('employee.edit', { employeeId: 42 });
    });

    it('harus merespon event hapus data', function() {
      $scope.data.length = 0;
      $scope.data.push.apply($scope.data, [
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 3, employee_name: 'Employee 3' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
      $scope.selectedItems.length = 0;
      $scope.selectedItems.push($scope.data[2]);

      spyOnModal().andReturn($scope.selectedItems);
      $httpBackend.expectDELETE('/hrm/employees/' + $scope.selectedItems[0].employee_id).respond(1);
      $httpBackend.expectGET('/hrm/employees').respond([
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);

      $rootScope.$broadcast('toolbar-main-click', 'remove');
      $scope.$apply();
      expect($scope.data.length).toBe(3);

      $httpBackend.flush();
      expect($scope.data).toEqualData([
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
    });

    it('harus merespon event refresh data', function() {
      $httpBackend.expectGET('/hrm/employees').respond([
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 3, employee_name: 'Employee 3' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
      $rootScope.$broadcast('toolbar-main-click', 'refresh');
      $httpBackend.flush();
      $scope.$apply();

      expect($scope.data).toEqualData([
        { employee_id: 1, employee_name: 'Employee 1' },
        { employee_id: 2, employee_name: 'Employee 2' },
        { employee_id: 3, employee_name: 'Employee 3' },
        { employee_id: 4, employee_name: 'Employee 4' }
      ]);
    });
  });

  /**
   * employeeListCtrl
   */
  describe('employeeListCtrl', function() {
    var $rootScope, $scope, $httpBackend, toolbar;

    beforeEach(module('hrm.controllers'));

    beforeEach(inject(function(_$rootScope_, _$httpBackend_, $controller, _toolbar_) {
      $httpBackend = _$httpBackend_;
      toolbar = _toolbar_;

      $rootScope = _$rootScope_;

      // employeeCtrl
      var $parentScope = $rootScope.$new();
      var parentState = {
        current: { name: 'employee', data: { title: 'Data Pegawai' }},
        transitionTo: function() {}
      };
      $controller('employeeCtrl', { $scope: $parentScope, $state: parentState });

      $httpBackend.expectGET('/hrm/projects').respond([
        { project_id: 1, project_name: 'Project 1' },
        { project_id: 2, project_name: 'Project 2' }
      ]);
      $httpBackend.flush();

      // employeeListCtrl
      $scope = $parentScope.$new();
      $controller('employeeListCtrl', { $scope: $scope });

      $httpBackend.expectGET('/hrm/employees').respond([
        { employee_id: 1, employee_name: 'Employee 1'},
        { employee_id: 2, employee_name: 'Employee 2'},
        { employee_id: 3, employee_name: 'Employee 3'}
      ]);

      $httpBackend.flush();
    }));

    it('harus update toolbar', function() {
      expect(toolbar.visibleToolbars).toEqualData(['main', 'pages']);
      expect(toolbar.buttons.main).toEqualData(['add', 'show-search', 'refresh', 'print', 'prev-page', 'next-page']);
    });

    it('harus langsung mengambil data', function() {
      expect($scope.$parent.data).toEqualData([
        { employee_id: 1, employee_name: 'Employee 1'},
        { employee_id: 2, employee_name: 'Employee 2'},
        { employee_id: 3, employee_name: 'Employee 3'}
      ]);
    });
  });
});
