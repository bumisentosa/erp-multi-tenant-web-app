'use strict';

describe('hrm/routes/projectGroup', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/project-group/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state kelompok proyek', function() {
    expect($state.href('projectGroup')).toBe('#/kelompok-proyek');

    $state.go('projectGroup');
    $scope.$apply();
    expect($state.current.name).toBe('projectGroup');
    expect($state.current.data).toEqualData({ title: 'Data Kelompok Proyek' });
  });
});
