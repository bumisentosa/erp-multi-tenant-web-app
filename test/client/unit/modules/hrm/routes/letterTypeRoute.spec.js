'use strict';

describe('hrm/routes/letterType', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/letter-type/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state kategori surat', function() {
    expect($state.href('letterType')).toBe('#/kategori-surat');

    $state.go('letterType');
    $scope.$apply();
    expect($state.current.name).toBe('letterType');
    expect($state.current.data).toEqualData({ title: 'Data Kategori Surat' });
  });
});
