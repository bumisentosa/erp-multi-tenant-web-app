'use strict';

describe('hrm/routes/bank', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/bank/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state bank', function() {
    expect($state.href('bank')).toBe('#/bank');

    $state.go('bank');
    $scope.$apply();
    expect($state.current.name).toBe('bank');
    expect($state.current.data).toEqualData({ title: 'Data Bank' });
  });
});
