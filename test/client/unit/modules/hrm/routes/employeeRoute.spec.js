'use strict';

describe('hrm/routes/employee', function() {
  var $state, $scope;

  beforeEach(module(
    'hrm.routes',
    'app/modules/hrm/templates/employee/employee.html',
    'app/modules/hrm/templates/employee/list.html',
    'app/modules/hrm/templates/employee/item.html'
  ));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus tidak dapat ke state employee secara langsung', function() {
    expect(function() {
      $state.go('employee');
    }).toThrow('Cannot transition to abstract state \'employee\'');
  });

  it('harus ke state employee.list', function() {
    expect($state.href('employee.list')).toBe('#/pegawai');

    $state.go('employee.list');
    $scope.$apply();
    expect($state.current.name).toBe('employee.list');
    expect($state.current.data).toEqualData({ title: 'Data Pegawai' });
  });

  it('harus ke state employee.add', function() {
    expect($state.href('employee.add')).toBe('#/pegawai/tambah');

    $state.go('employee.add');
    $scope.$apply();
    expect($state.current.name).toBe('employee.add');
    expect($state.current.data).toEqualData({ title: 'Tambah Data Pegawai' });
  });

  it('harus ke state employee.edit', function() {
    expect($state.href('employee.edit', { employeeId: 42 })).toBe('#/pegawai/ubah/42');

    $state.go('employee.edit', { employeeId: 42 });
    $scope.$apply();
    expect($state.current.name).toBe('employee.edit');
    expect($state.current.data).toEqualData({ title: 'Ubah Data Pegawai' });
  });
});
