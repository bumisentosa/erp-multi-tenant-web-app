'use strict';

describe('hrm/routes/competence', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/competence/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state kompetensi pegawai', function() {
    expect($state.href('competence')).toBe('#/kompetensi');

    $state.go('competence');
    $scope.$apply();
    expect($state.current.name).toBe('competence');
    expect($state.current.data).toEqualData({ title: 'Data Kompetensi Pegawai' });
  });
});
