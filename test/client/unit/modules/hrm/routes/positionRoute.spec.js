'use strict';

describe('hrm/routes/position', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/position/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state posisi/jabatan', function() {
    expect($state.href('position')).toBe('#/jabatan');

    $state.go('position');
    $scope.$apply();
    expect($state.current.name).toBe('position');
    expect($state.current.data).toEqualData({ title: 'Data Jabatan' });
  });
});
