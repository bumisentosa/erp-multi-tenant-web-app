'use strict';

describe('hrm/routes/location', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/location/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state unit/location', function() {
    expect($state.href('location')).toBe('#/lokasi');

    $state.go('location');
    $scope.$apply();
    expect($state.current.name).toBe('location');
    expect($state.current.data).toEqualData({ title: 'Data Lokasi' });
  });
});
