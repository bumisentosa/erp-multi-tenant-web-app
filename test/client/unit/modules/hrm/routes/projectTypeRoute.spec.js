'use strict';

describe('hrm/routes/projectType', function() {
  var $state, $scope;

  beforeEach(module('hrm.routes', 'app/modules/hrm/templates/project-type/list.html'));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus ke state tipe proyek', function() {
    expect($state.href('projectType')).toBe('#/tipe-proyek');

    $state.go('projectType');
    $scope.$apply();
    expect($state.current.name).toBe('projectType');
    expect($state.current.data).toEqualData({ title: 'Data Tipe Proyek' });
  });
});
