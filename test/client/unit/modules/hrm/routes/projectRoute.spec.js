'use strict';

describe('hrm/routes/project', function() {
  var $state, $scope;

  beforeEach(module(
    'hrm.routes',
    'app/modules/hrm/templates/project/project.html',
    'app/modules/hrm/templates/project/list.html',
    'app/modules/hrm/templates/project/item.html'
  ));

  beforeEach(inject(function($rootScope, _$state_) {
    $state = _$state_;
    $scope = $rootScope;
  }));

  it('harus tidak dapat ke state project secara langsung', function() {
    expect(function() {
      $state.go('project');
    }).toThrow('Cannot transition to abstract state \'project\'');
  });

  it('harus ke state project.list', function() {
    expect($state.href('project.list')).toBe('#/proyek');

    $state.go('project.list');
    $scope.$apply();
    expect($state.current.name).toBe('project.list');
    expect($state.current.data).toEqualData({ title: 'Data Proyek' });
  });

  it('harus ke state project.add', function() {
    expect($state.href('project.add')).toBe('#/proyek/tambah');

    $state.go('project.add');
    $scope.$apply();
    expect($state.current.name).toBe('project.add');
    expect($state.current.data).toEqualData({ title: 'Tambah Data Proyek' });
  });

  it('harus ke state project.edit', function() {
    expect($state.href('project.edit', { projectId: 42 })).toBe('#/proyek/ubah/42');

    $state.go('project.edit', { projectId: 42 });
    $scope.$apply();
    expect($state.current.name).toBe('project.edit');
    expect($state.current.data).toEqualData({ title: 'Ubah Data Proyek' });
  });
});
