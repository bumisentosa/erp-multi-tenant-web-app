'use strict';

describe('main/filters', function() {
  beforeEach(module('app.filters'));

  describe('join', function() {
    it('harus join dengan separator default', inject(function(joinFilter) {
      expect(joinFilter(['foo', 'bar', 'baz'])).toBe('foo, bar, baz');
    }));

    it('harus join dengan custom separator', inject(function(joinFilter) {
      expect(joinFilter(['foo', 'bar', 'baz'], ' - ')).toBe('foo - bar - baz');
    }));

    it('harus join dengan custom separator #2', inject(function(joinFilter) {
      expect(joinFilter(['foo', 'bar', 'baz'], ' + ', ' * ')).toBe('foo + bar * baz');
      expect(joinFilter(['foo', 'bar', 'baz', 'qux'], ' + ', ' * ')).toBe('foo + bar + baz * qux');
      expect(joinFilter(['foo', 'bar'], 'ignore', ' dan ')).toBe('foo dan bar');
    }));

    it('harus meng-handle array dengan length <= 1', inject(function(joinFilter) {
      expect(joinFilter(['foo'])).toBe('foo');
      expect(joinFilter([])).toBe('');
    }));

    it('harus mengabaikan data kosong', inject(function(joinFilter) {
      expect(joinFilter(['foo', ''])).toBe('foo');
      expect(joinFilter(['', 'bar'])).toBe('bar');
      expect(joinFilter(['foo', '', 'bar'])).toBe('foo, bar');
      expect(joinFilter(['', null, 'foo', 'bar', undefined, '', 'qux'])).toBe('foo, bar, qux');
    }));
  });
});
