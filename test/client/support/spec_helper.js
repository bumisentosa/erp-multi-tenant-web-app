/* jshint unused:false */
'use strict';

function spyOnModal() {
  return { andReturn: function(result) {
    inject(function($modal, $q) {
      spyOn($modal, 'open').andCallFake(function(options) {
        var deferred = $q.defer();
        deferred.resolve(result);
        return { result: deferred.promise };
      });
    });
  }};
}

beforeEach(function() {
  this.addMatchers({
    toEqualData: function(expected) {
      return angular.equals(this.actual, expected);
    },

    toHaveClass: function(expected) {
      this.message = function() {
        return 'Expected "' + angular.mock.dump(this.actual) + '" to have class "' + expected + '".';
      };
      return this.actual.hasClass(expected);
    }
  });
});
