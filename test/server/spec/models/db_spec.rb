require './server/models/auth'
require './server/models/db'

describe 'main/models/db' do
  it 'harus membuka database tanpa melewatkan parameter (buka database utama)' do
    DB.open_database do |db|
      db.should be_kind_of(Sequel::Database)
    end
  end

  it 'harus membuka database tanpa block' do
    db = DB.open_database
    expect { db.connect test: true }.not_to raise_error
    db.disconnect
  end

  it 'harus membuka database dengan melewatkan params[:db]' do
    db1 = DB.open_database
    DB.open_database db: db1 do |db2|
      db1.should eq(db2)
    end
    db1.disconnect
  end

  it 'harus membuka database dengan melewatkan params[:token]' do
    username = 'admin'
    password = 's3creT!'
    password_hash = Auth.create_password_hash(username, password)
    token = Auth.create_token(username, password_hash)

    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_demo',
      db_user:     'root',
      db_password: Auth.encrypt('password'),
      db_host:     '127.0.0.1',
      db_port:     3306
    })

    db_tenant = DB.open_database token: token
    expect { db_tenant.connect test: true }.not_to raise_error
    db_tenant.disconnect
  end

  context 'SQL query' do
    before(:each) do
      @db = DB.open_database
    end

    after(:each) do
      @db.disconnect
    end

    it 'harus mengenerate query select semua data' do
      dataset = DB.get_dataset(@db, :users)
      dataset.sql.should eq("SELECT * FROM `users`")
    end

    it 'harus mengenerate query select data per halaman' do
      dataset = DB.get_dataset(@db, :users, page: 3)
      dataset.sql.should eq("SELECT * FROM `users` LIMIT 20 OFFSET 40")

      dataset = DB.get_dataset(@db, :users, limit: 42)
      dataset.sql.should eq("SELECT * FROM `users` LIMIT 42 OFFSET 0")

      dataset = DB.get_dataset(@db, :users, page: 3, limit: 42)
      dataset.sql.should eq("SELECT * FROM `users` LIMIT 42 OFFSET 84")
    end

    it 'harus mengenerate query select field tertentu' do
      dataset = DB.get_dataset(@db, :users, columns: [:username, :password])
      dataset.sql.should eq("SELECT `username`, `password` FROM `users`")
    end

    it 'harus mengenerate query filter' do
      dataset = DB.get_dataset(@db, :users, filter: { role: 'admin' })
      dataset.sql.should eq("SELECT * FROM `users` WHERE (`role` = 'admin')")

      dataset = DB.get_dataset(@db, :users, filter: { username: 'adi', role: 'admin' })
      dataset.sql.should eq("SELECT * FROM `users` WHERE ((`username` = 'adi') AND (`role` = 'admin'))")
    end

    it 'harus mengenerate query sort order' do
      dataset = DB.get_dataset(@db, :users, order: :username)
      dataset.sql.should eq("SELECT * FROM `users` ORDER BY `username`")

      dataset = DB.get_dataset(@db, :users, order: [:role.desc, :username])
      dataset.sql.should eq("SELECT * FROM `users` ORDER BY `role` DESC, `username`")
    end
  end
end
