require './server/models/user_session'

describe 'main/models/user_session' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    UserSession.table_name.should eq(:user_sessions)
    UserSession.view_name.should be_nil
    UserSession.primary_key.should eq(:username)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    data = insert_sample_user_sessions(@db, amount: 3)
    initial_data = data[:data]

    # Read
    inserted_data = UserSession.get(initial_data[0][:username], db: @db)
    inserted_data.should eq({
      username: initial_data[0][:username],
      token:    initial_data[0][:token]
    })

    # Update
    to_update = { token: '*updated*' }
    update_count = UserSession.update(initial_data[0][:username], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = UserSession.get(initial_data[0][:username], db: @db)
    updated_data.should eq({
      username: initial_data[0][:username],
      token:    '*updated*'
    })

    # Delete
    delete_count = UserSession.delete(initial_data[0][:username], db: @db)
    delete_count.should eq(1)
    remaining_data = UserSession.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = UserSession.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = UserSession.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
