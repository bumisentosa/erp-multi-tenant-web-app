require './server/models/user_profile'

describe 'main/models/user_profile' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    UserProfile.table_name.should eq(:user_profiles)
    UserProfile.view_name.should eq(:v_user_profiles)
    UserProfile.primary_key.should eq(:username)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    data = insert_sample_user_profiles(@db, amount: 3)
    initial_data = data[:data]
    role = data[:role]

    # Read
    inserted_data = UserProfile.get(initial_data[0][:username], db: @db)
    inserted_data.should eq({
      username:     initial_data[0][:username],
      display_name: initial_data[0][:display_name],
      photo_url:    initial_data[0][:photo_url],
      role_id:      role[:role_id],
      role:         role[:role]
    })

    # Update
    to_update = { display_name: '*updated*' }
    update_count = UserProfile.update(initial_data[0][:username], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = UserProfile.get(initial_data[0][:username], db: @db)
    updated_data.should eq({
      username:     initial_data[0][:username],
      display_name: '*updated*',
      photo_url:    initial_data[0][:photo_url],
      role_id:      role[:role_id],
      role:         role[:role]
    })

    # Delete
    delete_count = UserProfile.delete(initial_data[0][:username], db: @db)
    delete_count.should eq(1)
    remaining_data = UserProfile.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = UserProfile.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = UserProfile.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
