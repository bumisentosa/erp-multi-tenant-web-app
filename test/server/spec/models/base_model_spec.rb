require './server/models/base_model'

describe 'main/models/base_model' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  class SomeTableSpec < Model
  end

  class RoleSpec < Model
    table_name :user_roles

    columns do |c|
      c.integer :role_id, primary_key: true
      c.string  :role, presence: true
    end
  end

  class UserSpec < Model
    table_name :users

    columns do |c|
      c.string  :username, primary_key: true
      c.string  :password
      c.integer :role_id
      c.integer :tenant_id
    end

    def self.justify_data(id, options)
      data = super(id, options)
      if data && data[:password]
        data[:password] = Auth.encrypt(data[:password])
      end
      data
    end
  end

  it 'harus menentukan atribut tabel default' do
    SomeTableSpec.table_name.should eq(:some_table_specs)
    SomeTableSpec.view_name.should be_nil
    SomeTableSpec.primary_key.should eq(:some_table_spec_id)
  end

  it 'harus menentukan atribut tabel' do
    UserSpec.table_name.should eq(:users)
    UserSpec.view_name.should be_nil
    UserSpec.primary_key.should eq(:username)
  end

  it 'harus memfilter data yang masuk ke database' do
    data = { username: 'admin', password: 's3cr!T', nothing: '', bad_field: '' }
    filtered = UserSpec.justify_data(nil, data: data)
    filtered.should eq({ username: 'admin', password: Auth.encrypt('s3cr!T') })
  end

  it 'harus menambah data' do
    expect { insert_sample_users @db }.not_to raise_error
  end

  it 'harus mengambil data' do
    insert_sample_users @db, amount: 2
    data_taken = UserSpec.select(db: @db)
    data_taken.length.should eq(2)
  end

  it 'harus meng-encrypt password sebelum disimpan' do
    username = 'admin'
    password = 's3cr!T'

    tenant = insert_sample_tenants(@db)[:data]
    role = insert_sample_user_roles(@db)[:data]
    UserSpec.delete_all!
    UserSpec.insert(data: {
      username:  username,
      password:  password,
      role_id:   role[:role_id],
      tenant_id: tenant[:tenant_id]
    }, db: @db)

    data_taken = UserSpec.get(username, db: @db)
    data_taken[:password].should_not eq(password)
    data_taken[:password].should eq(Auth.encrypt(password))
  end

  it 'harus mengambil data hanya field tertentu' do
    initial_data = insert_sample_users(@db)[:data]
    data_taken = UserSpec.get(initial_data[:username], columns: [:username, :role_id])
    data_taken.should eq({
      username: initial_data[:username],
      role_id: initial_data[:role_id]
    })
  end

  it 'harus mengambil data per halaman' do
    insert_sample_users @db, amount: 7
    data_taken = UserSpec.select(page: 2, limit: 5, db: @db)
    data_taken[:data].length.should eq(2)
    data_taken[:page].should eq({ count: 2, current: 2, next: nil, prev: 1 })
  end

  it 'harus mengambil data berdasarkan id tabel' do
    initial_data = insert_sample_users(@db, amount: 2)[:data]
    data_taken = UserSpec.get(initial_data[0][:username], db: @db)
    data_taken.should include(initial_data[0])
  end

  it 'harus mengubah data' do
    initial_data = insert_sample_users(@db, amount: 2)[:data]
    to_update = { username: '*updated*' }
    result = UserSpec.update(initial_data[0][:username], data: to_update, db: @db)
    result.should eq(1)
  end

  it 'harus menghapus data' do
    initial_data = insert_sample_users(@db, amount: 3)[:data]
    result = UserSpec.delete(initial_data[1][:username], db: @db)
    result.should eq(1)

    remaining_data = UserSpec.select(db: @db)
    remaining_data.length.should eq(2)

    result = UserSpec.delete_all!(db: @db)
    result.should eq(2)
  end

  it 'harus meng-update data per kolom' do
    tenant = insert_sample_tenants(@db)[:data]
    roles = insert_sample_user_roles(@db, amount: 3)[:data]

    # Insert sample user
    UserSpec.delete_all!

    tenant_id = tenant[:tenant_id]
    role_id_0 = roles[0][:role_id]
    role_id_1 = roles[1][:role_id]

    UserSpec.insert(data: { username: 'user_00', password: '?', role_id: role_id_0, tenant_id: tenant_id }, db: @db)
    UserSpec.insert(data: { username: 'user_01', password: '?', role_id: role_id_0, tenant_id: tenant_id }, db: @db)
    UserSpec.insert(data: { username: 'user_02', password: '?', role_id: role_id_0, tenant_id: tenant_id }, db: @db)
    UserSpec.insert(data: { username: 'user_10', password: '?', role_id: role_id_1, tenant_id: tenant_id }, db: @db)
    UserSpec.insert(data: { username: 'user_11', password: '?', role_id: role_id_1, tenant_id: tenant_id }, db: @db)

    users = UserSpec.select(db: @db)
    users.length.should eq(5)

    # Update per column
    UserSpec.update_per(:role_id, role_id_0, data: [
      { username: 'user_00', password: 'u0' },
      { username: 'user_01', password: 'u1' },
      { username: 'user_02', password: 'u2' }
    ], db: @db)

    users = UserSpec.select(filter: { role_id: role_id_0 }, db: @db)
    users.length.should eq(3)
    users[0].should include({ username: 'user_00', password: Auth.encrypt('u0'), role_id: role_id_0 })
    users[1].should include({ username: 'user_01', password: Auth.encrypt('u1'), role_id: role_id_0 })
    users[2].should include({ username: 'user_02', password: Auth.encrypt('u2'), role_id: role_id_0 })

    # Update per column (data baru dan lama)
    UserSpec.update_per(:role_id, role_id_0, data: [
      { username: 'user_01', password: 'x1' }, # update
      { username: 'user_0X', password: 'xx', role_id: role_id_0, tenant_id: tenant_id }, # new
      { username: 'user_0Y', password: 'yy', role_id: role_id_0, tenant_id: tenant_id }  # new
    ], db: @db)

    users = UserSpec.select(filter: { role_id: role_id_0 }, db: @db)
    users.length.should eq(3)
    users[0].should include({ username: 'user_01', password: Auth.encrypt('x1'), role_id: role_id_0 })
    users[1].should include({ username: 'user_0X', password: Auth.encrypt('xx'), role_id: role_id_0 })
    users[2].should include({ username: 'user_0Y', password: Auth.encrypt('yy'), role_id: role_id_0 })
  end

end
