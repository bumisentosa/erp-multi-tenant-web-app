require './server/models/user_role'

describe 'main/models/user_role' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    UserRole.table_name.should eq(:user_roles)
    UserRole.view_name.should be_nil
    UserRole.primary_key.should eq(:role_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_user_roles(@db, amount: 3)[:data]

    # Read
    inserted_data = UserRole.get(initial_data[0][:role_id], db: @db)
    inserted_data.should eq({
      role_id: initial_data[0][:role_id],
      role:    initial_data[0][:role]
    })

    # Update
    to_update = { role: '*updated*' }
    update_count = UserRole.update(initial_data[0][:role_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = UserRole.get(initial_data[0][:role_id], db: @db)
    updated_data.should eq({
      role_id: initial_data[0][:role_id],
      role:    '*updated*'
    })

    # Delete
    delete_count = UserRole.delete(initial_data[0][:role_id], db: @db)
    delete_count.should eq(1)
    remaining_data = UserRole.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = UserRole.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = UserRole.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
