require './server/models/tenant'

describe 'main/models/tenant' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    Tenant.table_name.should eq(:tenants)
    Tenant.view_name.should be_nil
    Tenant.primary_key.should eq(:tenant_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_tenants(@db, amount: 3)[:data]

    # Read
    inserted_data = Tenant.get(initial_data[0][:tenant_id], db: @db)
    inserted_data.should eq({
      tenant_id:    initial_data[0][:tenant_id],
      name:         initial_data[0][:name],
      address:      initial_data[0][:address],
      city:         initial_data[0][:city],
      region:       initial_data[0][:region],
      country:      initial_data[0][:country],
      phone_number: initial_data[0][:phone_number],
      email:        initial_data[0][:email],
      contact_name: initial_data[0][:contact_name],
      db_name:      initial_data[0][:db_name],
      db_host:      initial_data[0][:db_host],
      db_port:      initial_data[0][:db_port],
      db_user:      initial_data[0][:db_user],
      db_password:  initial_data[0][:db_password]
    })

    # Update
    to_update = { name: '*updated*', address: '*updated*' }
    update_count = Tenant.update(initial_data[0][:tenant_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = Tenant.get(initial_data[0][:tenant_id], db: @db)
    updated_data.should eq({
      tenant_id:    initial_data[0][:tenant_id],
      name:         '*updated*',
      address:      '*updated*',
      city:         initial_data[0][:city],
      region:       initial_data[0][:region],
      country:      initial_data[0][:country],
      phone_number: initial_data[0][:phone_number],
      email:        initial_data[0][:email],
      contact_name: initial_data[0][:contact_name],
      db_name:      initial_data[0][:db_name],
      db_host:      initial_data[0][:db_host],
      db_port:      initial_data[0][:db_port],
      db_user:      initial_data[0][:db_user],
      db_password:  initial_data[0][:db_password]
    })

    # Delete
    delete_count = Tenant.delete(initial_data[0][:tenant_id], db: @db)
    delete_count.should eq(1)
    remaining_data = Tenant.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = Tenant.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = Tenant.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
