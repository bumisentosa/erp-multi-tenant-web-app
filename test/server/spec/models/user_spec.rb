require './server/models/user'

describe 'main/models/user' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    User.table_name.should eq(:users)
    User.view_name.should be_nil
    User.primary_key.should eq(:username)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_users(@db, amount: 3)[:data]

    # Read
    inserted_data = User.get(initial_data[0][:username], db: @db)
    inserted_data.should eq({
      username:  initial_data[0][:username],
      password:  initial_data[0][:password],
      role_id:   initial_data[0][:role_id],
      tenant_id: initial_data[0][:tenant_id]
    })

    # Update
    to_update = { username: '*updated*' }
    update_count = User.update(initial_data[0][:username], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = User.get('*updated*', db: @db)
    updated_data.should eq({
      username:  '*updated*',
      password:  initial_data[0][:password],
      role_id:   initial_data[0][:role_id],
      tenant_id: initial_data[0][:tenant_id]
    })

    # Delete
    delete_count = User.delete(initial_data[1][:username], db: @db)
    delete_count.should eq(1)
    remaining_data = User.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = User.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = User.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
