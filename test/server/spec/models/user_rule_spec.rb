require './server/models/user_rule'

describe 'main/models/user_rule' do
  include UserSpecHelper

  before(:each) do
    @db = DB.open_database
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    UserRule.table_name.should eq(:user_rules)
    UserRule.view_name.should be_nil
    UserRule.primary_key.should eq(:rule_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    data = insert_sample_user_rules(@db, amount: 3)
    initial_data = data[:data]
    role = data[:role]

    # Read
    inserted_data = UserRule.get(initial_data[0][:rule_id], db: @db)
    inserted_data.should eq({
      rule_id: initial_data[0][:rule_id],
      role_id: role[:role_id],
      rule:    initial_data[0][:rule]
    })

    # Update
    to_update = { rule: '*updated*' }
    update_count = UserRule.update(initial_data[0][:rule_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = UserRule.get(initial_data[0][:rule_id], db: @db)
    updated_data.should eq({
      rule_id: initial_data[0][:rule_id],
      role_id: role[:role_id],
      rule:    '*updated*'
    })

    # Delete
    delete_count = UserRule.delete(initial_data[0][:rule_id], db: @db)
    delete_count.should eq(1)
    remaining_data = UserRule.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = UserRule.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = UserRule.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
