require './server/models/auth'
require './server/models/user_rule'

describe 'main/models/auth' do
  include UserSpecHelper

  before(:all) do
    @username = 'admin'
    @password = 'secr3T'

    @db = DB.open_database
    @user = insert_sample_users(@db, username: @username, password: @password)
  end

  after(:all) do
    @db.disconnect
  end

  it 'harus dapat login (otentikasi)' do
    token = Auth.login(@username, @password)
    token.should_not be_empty
    Auth.logged_in?(token).should be_true
  end

  it 'harus dapat logout' do
    token = Auth.login(@username, @password)
    Auth.logout(token)
    Auth.logged_in?(token).should_not be_true
  end

  it 'harus membatasi user yang login (otorisasi)' do
    UserRule.delete_all! db: @db
    insert_sample_user_rules @db, role: @user[:role], rule: 'users.read', append_mode: true
    insert_sample_user_rules @db, role: @user[:role], rule: 'hrm.pegawai.all', append_mode: true
    insert_sample_user_rules @db, role: @user[:role], rule: 'hrm.absen.read', append_mode: true
    insert_sample_user_rules @db, role: @user[:role], rule: 'prm.all', append_mode: true

    Auth.allowed?(@username, :users, :create).should_not be_true
    Auth.allowed?(@username, :users, :read).should be_true
    Auth.allowed?(@username, :users).should_not be_true

    Auth.allowed?(@username, :hrm, :pegawai, :create).should be_true
    Auth.allowed?(@username, :hrm, :pegawai, :foo).should be_true
    Auth.allowed?(@username, :hrm, :pegawai).should be_true

    Auth.allowed?(@username, :hrm, :absen, :create).should_not be_true
    Auth.allowed?(@username, :hrm, :absen, :read).should be_true
    Auth.allowed?(@username, :hrm, :absen).should_not be_true

    Auth.allowed?(@username, :prm).should be_true
    Auth.allowed?(@username, :prm, :proyek, :create).should be_true
  end
end
