require './server/models/auth'
require './server/modules/hrm/models/letter'

describe 'hrm/models/letter' do
  include LetterSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Letter.table_name.should eq(:letters)
    HRM::Letter.view_name.should be_nil
    HRM::Letter.primary_key.should eq(:letter_id)
  end

  it 'harus melakukan operasi CRUD' do
    initial_data = insert_sample_letters(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Letter.get(initial_data[0][:letter_id], db: @db)
    inserted_data.should eq({
      letter_id:        initial_data[0][:letter_id],
      employee_id:      initial_data[0][:employee_id],
      type_id:          initial_data[0][:type_id],
      letter_no:        initial_data[0][:letter_no],
      letter_date:      initial_data[0][:letter_date],
      letter_date_end:  initial_data[0][:letter_date_end],
      description:      initial_data[0][:description]
    })

    # Update
    to_update = { letter_no: '*updated*' }
    update_count = HRM::Letter.update(initial_data[0][:letter_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Letter.get(initial_data[0][:letter_id], db: @db)
    updated_data.should eq({
      letter_id:        initial_data[0][:letter_id],
      employee_id:      initial_data[0][:employee_id],
      type_id:          initial_data[0][:type_id],
      letter_no:        '*updated*',
      letter_date:      initial_data[0][:letter_date],
      letter_date_end:  initial_data[0][:letter_date_end],
      description:      initial_data[0][:description]
    })

    # Delete
    delete_count = HRM::Letter.delete(initial_data[0][:letter_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Letter.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Letter.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Letter.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
