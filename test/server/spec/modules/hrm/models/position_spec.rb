require './server/models/auth'
require './server/modules/hrm/models/position'
require './server/modules/hrm/models/employee'

describe 'hrm/models/position' do
  include PositionSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Position.table_name.should eq(:positions)
    HRM::Position.view_name.should eq(:v_positions)
    HRM::Position.primary_key.should eq(:position_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db

    # Create
    initial_data = insert_sample_positions(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Position.get(initial_data[0][:position_id], db: @db)
    inserted_data.should include({
      position_id: initial_data[0][:position_id],
      position:    initial_data[0][:position]
    })

    # Update
    to_update = { position: '*updated*' }
    update_count = HRM::Position.update(initial_data[0][:position_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Position.get(initial_data[0][:position_id], db: @db)
    updated_data.should include({
      position_id: initial_data[0][:position_id],
      position:    '*updated*'
    })

    # Delete
    delete_count = HRM::Position.delete(initial_data[0][:position_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Position.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Position.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Position.select(db: @db)
    remaining_data.length.should eq(0)
  end

  it 'harus mengelola hirarki data' do
    # constraint: Data pegawai harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db

    root    = insert_sample_positions(@db, position: 'Root', parent: nil)[:data]
    child1  = insert_sample_positions(@db, position: 'aaaa', parent: root[:position_id], append_mode: true)[:data]
    child2  = insert_sample_positions(@db, position: 'cccc', parent: root[:position_id], append_mode: true)[:data]
    child21 = insert_sample_positions(@db, position: 'xxxx', parent: child2[:position_id], append_mode: true)[:data]
    child22 = insert_sample_positions(@db, position: 'yyyy', parent: child2[:position_id], append_mode: true)[:data]
    child3  = insert_sample_positions(@db, position: 'bbbb', parent: root[:position_id], append_mode: true)[:data]

    tree = HRM::Position.select(db: @db, order: :sort_asc)

    tree[0][:depth].should eq(0)
    tree[0][:path].should eq(root[:position_id].to_s)
    tree[0][:sort_asc].should eq('001')
    tree[0][:sort_desc].should eq('001')

    tree[1][:depth].should eq(1)
    tree[1][:path].should eq("#{root[:position_id]}/#{child1[:position_id]}")
    tree[1][:sort_asc].should eq('001/001')
    tree[1][:sort_desc].should eq('001/003')

    tree[2][:depth].should eq(1)
    tree[2][:path].should eq("#{root[:position_id]}/#{child3[:position_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    tree[3][:depth].should eq(1)
    tree[3][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}")
    tree[3][:sort_asc].should eq('001/003')
    tree[3][:sort_desc].should eq('001/001')

    tree[4][:depth].should eq(2)
    tree[4][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}/#{child21[:position_id]}")
    tree[4][:sort_asc].should eq('001/003/001')
    tree[4][:sort_desc].should eq('001/001/002')

    tree[5][:depth].should eq(2)
    tree[5][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}/#{child22[:position_id]}")
    tree[5][:sort_asc].should eq('001/003/002')
    tree[5][:sort_desc].should eq('001/001/001')

    # Reorder
    HRM::Position.update(child2[:position_id], data: { position: 'aabb' }, db: @db)
    tree = HRM::Position.select(db: @db, order: :sort_asc)

    tree[2][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    tree[3][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}/#{child21[:position_id]}")
    tree[3][:sort_asc].should eq('001/002/001')
    tree[3][:sort_desc].should eq('001/002/002')

    tree[4][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}/#{child22[:position_id]}")
    tree[4][:sort_asc].should eq('001/002/002')
    tree[4][:sort_desc].should eq('001/002/001')

    tree[5][:path].should eq("#{root[:position_id]}/#{child3[:position_id]}")
    tree[5][:sort_asc].should eq('001/003')
    tree[5][:sort_desc].should eq('001/001')

    # Hapus
    HRM::Position.delete(child21[:position_id], db: @db)
    tree = HRM::Position.select(db: @db, order: :sort_asc)

    tree[3][:depth].should eq(2)
    tree[3][:path].should eq("#{root[:position_id]}/#{child2[:position_id]}/#{child22[:position_id]}")
    tree[3][:sort_asc].should eq('001/002/001')
    tree[3][:sort_desc].should eq('001/002/001')

    # Hapus parent
    HRM::Position.delete(child2[:position_id], db: @db)
    tree = HRM::Position.select(db: @db, order: :sort_asc)

    tree[3][:depth].should eq(0)
    tree[3][:path].should eq(child22[:position_id].to_s)
    tree[3][:sort_asc].should eq('002')
    tree[3][:sort_desc].should eq('001')
  end
end
