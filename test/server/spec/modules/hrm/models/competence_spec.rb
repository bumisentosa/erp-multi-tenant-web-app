require './server/models/auth'
require './server/modules/hrm/models/competence'

describe 'hrm/models/competence' do
  include CompetenceSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Competence.table_name.should eq(:competences)
    HRM::Competence.view_name.should be_nil
    HRM::Competence.primary_key.should eq(:competence_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_competences(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Competence.get(initial_data[0][:competence_id], db: @db)
    inserted_data.should eq({
      competence_id:   initial_data[0][:competence_id],
      competence_code: initial_data[0][:competence_code],
      competence_name: initial_data[0][:competence_name],
      description:     initial_data[0][:description]
    })

    # Update
    to_update = { competence_name: '*updated*', description: '*updated*' }
    update_count = HRM::Competence.update(initial_data[0][:competence_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Competence.get(initial_data[0][:competence_id], db: @db)
    updated_data.should eq({
      competence_id:   initial_data[0][:competence_id],
      competence_code: initial_data[0][:competence_code],
      competence_name: '*updated*',
      description:     '*updated*'
    })

    # Delete
    delete_count = HRM::Competence.delete(initial_data[0][:competence_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Competence.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Competence.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Competence.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
