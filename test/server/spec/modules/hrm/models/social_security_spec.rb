require './server/models/auth'
require './server/modules/hrm/models/social_security'

describe 'hrm/models/social_security' do
  include SocialSecuritySpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::SocialSecurity.table_name.should eq(:social_security)
    HRM::SocialSecurity.view_name.should be_nil
    HRM::SocialSecurity.primary_key.should eq(:ss_code)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_social_security(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::SocialSecurity.get(initial_data[0][:ss_code], db: @db)
    inserted_data.should eq({
      ss_code:     initial_data[0][:ss_code],
      ss_name:     initial_data[0][:ss_name],
      description: initial_data[0][:description]
    })

    # Update
    to_update = { ss_name: '*updated*', description: '*updated*' }
    update_count = HRM::SocialSecurity.update(initial_data[0][:ss_code], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::SocialSecurity.get(initial_data[0][:ss_code], db: @db)
    updated_data.should eq({
      ss_code:     initial_data[0][:ss_code],
      ss_name:     '*updated*',
      description: '*updated*'
    })

    # Delete
    delete_count = HRM::SocialSecurity.delete(initial_data[0][:ss_code], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::SocialSecurity.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::SocialSecurity.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::SocialSecurity.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
