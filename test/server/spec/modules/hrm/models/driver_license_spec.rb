require './server/models/auth'
require './server/modules/hrm/models/driver_license'

describe 'hrm/models/driver_license' do
  include DriverLicenseSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::DriverLicense.table_name.should eq(:driver_licenses)
    HRM::DriverLicense.view_name.should be_nil
    HRM::DriverLicense.primary_key.should eq(:license_code)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_driver_licenses(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::DriverLicense.get(initial_data[0][:license_code], db: @db)
    inserted_data.should eq({
      license_code: initial_data[0][:license_code],
      description:  initial_data[0][:description]
    })

    # Update
    to_update = { description: '*updated*' }
    update_count = HRM::DriverLicense.update(initial_data[0][:license_code], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::DriverLicense.get(initial_data[0][:license_code], db: @db)
    updated_data.should eq({
      license_code: initial_data[0][:license_code],
      description:  '*updated*'
    })

    # Delete
    delete_count = HRM::DriverLicense.delete(initial_data[0][:license_code], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::DriverLicense.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::DriverLicense.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::DriverLicense.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
