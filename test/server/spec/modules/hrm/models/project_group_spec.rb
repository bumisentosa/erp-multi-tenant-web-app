require './server/models/auth'
require './server/modules/hrm/models/project_group'
require './server/modules/hrm/models/project'
require './server/modules/hrm/models/employee'

describe 'hrm/models/project_group' do
  include ProjectSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::ProjectGroup.table_name.should eq(:projects_groups)
    HRM::ProjectGroup.view_name.should be_nil
    HRM::ProjectGroup.primary_key.should eq(:group_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai dan proyek harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db
    HRM::Project.delete_all! db: @db

    # Create
    initial_data = insert_sample_projects_groups(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::ProjectGroup.get(initial_data[0][:group_id], db: @db)
    inserted_data.should eq({
      group_id:   initial_data[0][:group_id],
      group_name: initial_data[0][:group_name]
    })

    # Update
    to_update = { group_name: '*updated*' }
    update_count = HRM::ProjectGroup.update(initial_data[0][:group_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::ProjectGroup.get(initial_data[0][:group_id], db: @db)
    updated_data.should eq({
      group_id:   initial_data[0][:group_id],
      group_name: '*updated*'
    })

    # Delete
    delete_count = HRM::ProjectGroup.delete(initial_data[0][:group_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::ProjectGroup.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::ProjectGroup.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::ProjectGroup.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
