require './server/models/auth'
require './server/modules/hrm/models/education'

describe 'hrm/models/education' do
  include EducationSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  context 'educations_level' do
    it 'harus menentukan atribut tabel' do
      HRM::EducationLevel.table_name.should eq(:educations_levels)
      HRM::EducationLevel.view_name.should be_nil
      HRM::EducationLevel.primary_key.should eq(:level_id)
    end

    it 'harus melakukan operasi CRUD' do
      # Create
      initial_data = insert_sample_educations_levels(@db, amount: 3)[:data]

      # Read
      inserted_data = HRM::EducationLevel.get(initial_data[0][:level_id], db: @db)
      inserted_data.should eq({
        level_id:        initial_data[0][:level_id],
        education_level: initial_data[0][:education_level]
      })

      # Update
      to_update = { education_level: '*updated*' }
      update_count = HRM::EducationLevel.update(initial_data[0][:level_id], data: to_update, db: @db)
      update_count.should eq(1)
      updated_data = HRM::EducationLevel.get(initial_data[0][:level_id], db: @db)
      updated_data.should eq({
        level_id:        initial_data[0][:level_id],
        education_level: '*updated*'
      })

      # Delete
      delete_count = HRM::EducationLevel.delete(initial_data[0][:level_id], db: @db)
      delete_count.should eq(1)
      remaining_data = HRM::EducationLevel.select(db: @db)
      remaining_data.length.should eq(2)

      delete_count = HRM::EducationLevel.delete_all!(db: @db)
      delete_count.should eq(2)
      remaining_data = HRM::EducationLevel.select(db: @db)
      remaining_data.length.should eq(0)
    end
  end
end
