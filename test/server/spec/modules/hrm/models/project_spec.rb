require './server/models/auth'
require './server/modules/hrm/models/project'

describe 'hrm/models/project' do
  include ProjectSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Project.table_name.should eq(:projects)
    HRM::Project.view_name.should eq(:v_projects)
    HRM::Project.primary_key.should eq(:project_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_projects(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Project.get(initial_data[0][:project_id], db: @db)
    inserted_data.should include({
      project_id:           initial_data[0][:project_id],
      type_id:              initial_data[0][:type_id],
      group_id:             initial_data[0][:group_id],
      project_name:         initial_data[0][:project_name],
      contract_no_s1:       initial_data[0][:contract_no_s1],
      contract_no_s2:       initial_data[0][:contract_no_s2],
      contract_date:        initial_data[0][:contract_date],
      contract_date_start:  initial_data[0][:contract_date_start],
      contract_date_end:    initial_data[0][:contract_date_end],
      project_description:  initial_data[0][:project_description],
      contract_value:       initial_data[0][:contract_value],
      description:          initial_data[0][:description]
    })

    # Update
    to_update = { project_name: '*updated*', contract_value: 42 }
    update_count = HRM::Project.update(initial_data[0][:project_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Project.get(initial_data[0][:project_id], db: @db)
    updated_data.should include({
      project_id:           initial_data[0][:project_id],
      type_id:              initial_data[0][:type_id],
      group_id:             initial_data[0][:group_id],
      project_name:         '*updated*',
      contract_no_s1:       initial_data[0][:contract_no_s1],
      contract_no_s2:       initial_data[0][:contract_no_s2],
      contract_date:        initial_data[0][:contract_date],
      contract_date_start:  initial_data[0][:contract_date_start],
      contract_date_end:    initial_data[0][:contract_date_end],
      project_description:  initial_data[0][:project_description],
      contract_value:       BigDecimal(42),
      description:          initial_data[0][:description]
    })

    # Delete
    delete_count = HRM::Project.delete(initial_data[0][:project_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Project.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Project.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Project.select(db: @db)
    remaining_data.length.should eq(0)
  end

  context 'Lokasi proyek' do
    it 'harus menentukan atribut tabel' do
      HRM::ProjectLocation.table_name.should eq(:projects_locations)
      HRM::ProjectLocation.view_name.should eq(:v_projects_locations)
      HRM::ProjectLocation.primary_key.should eq(:project_location_id)
    end

    it 'harus melakukan operasi CRUD' do
      # Create
      sample = insert_sample_projects_locations(@db, amount: 3)
      initial_data = sample[:data]
      projects = sample[:projects]
      locations = sample[:locations]

      # Read
      inserted_data = HRM::ProjectLocation.get(initial_data[0][:project_location_id], db: @db)
      inserted_data.should include({
        project_location_id: initial_data[0][:project_location_id],
        project_id:          initial_data[0][:project_id],
        location_id:         initial_data[0][:location_id]
      })

      # Update
      to_update = { project_id: projects[1][:project_id] }
      update_count = HRM::ProjectLocation.update(initial_data[0][:project_location_id], data: to_update, db: @db)
      update_count.should eq(1)
      updated_data = HRM::ProjectLocation.get(initial_data[0][:project_location_id], db: @db)
      updated_data.should include({
        project_location_id: initial_data[0][:project_location_id],
        project_id:          initial_data[1][:project_id],
        location_id:         initial_data[0][:location_id]
      })
      to_update = { location_id: locations[2][:location_id] }
      update_count = HRM::ProjectLocation.update(initial_data[0][:project_location_id], data: to_update, db: @db)
      update_count.should eq(1)
      updated_data = HRM::ProjectLocation.get(initial_data[0][:project_location_id], db: @db)
      updated_data.should include({
        project_location_id: initial_data[0][:project_location_id],
        project_id:          initial_data[1][:project_id],
        location_id:         initial_data[2][:location_id]
      })

      # Delete
      delete_count = HRM::ProjectLocation.delete(initial_data[0][:project_location_id], db: @db)
      delete_count.should eq(1)
      remaining_data = HRM::ProjectLocation.select(db: @db)
      remaining_data.length.should eq(2)

      delete_count = HRM::ProjectLocation.delete_all!(db: @db)
      delete_count.should eq(2)
      remaining_data = HRM::ProjectLocation.select(db: @db)
      remaining_data.length.should eq(0)
    end
  end

  context 'Addendum' do
    it 'harus menentukan atribut tabel' do
      HRM::ProjectAddendum.table_name.should eq(:projects_addendums)
      HRM::ProjectAddendum.view_name.should be_nil
      HRM::ProjectAddendum.primary_key.should eq(:addendum_id)
    end

    it 'harus mengelola data addendum' do
      project = insert_sample_projects(@db)[:data]

      addendums = insert_sample_projects_addendums(@db, amount: 3, project: project)
      data = HRM::ProjectAddendum.select(db: @db, order: :addendum_id)
      data.length.should eq(3)

      data[0][:addendum_name] = '*updated*'
      data.delete_at(2)
      data << {
        project_id:        project[:project_id],
        addendum_name:     Faker::Lorem.words(2).join(' ').capitalize,
        addendum_no:       Faker::Number.number(12),
        addendum_date:     Date.today,
        addendum_date_end: Date.today + 60,
        addendum_value:    BigDecimal(rand(10) * 15_000_000)
      }
      updated_items = HRM::ProjectAddendum.update_per(:project_id, project[:project_id], db: @db, data: data)
      updated_items.count.should eq(3)

      updated_data = HRM::ProjectAddendum.select(db: @db, order: :addendum_id)

      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end

    it 'harus meng-update status dan addendum_value pada tabel projects' do
      inserted_project = insert_sample_projects(@db, project_description: nil)[:data]

      project = HRM::Project.get(inserted_project[:project_id], db: @db)
      project[:addendum_value].should be_nil
      project[:addendum_count].should eq(0)
      project[:status_id].should eq(0)

      HRM::Project.update(project[:project_id], data: { project_description: Faker::Lorem.word }, db: @db)
      project = HRM::Project.get(project[:project_id], db: @db)
      project[:addendum_value].should be_nil
      project[:addendum_count].should eq(0)
      project[:status_id].should eq(1)

      addendum1 = insert_sample_projects_addendums(@db, addendum_date: '2013/12/01', project: project)[:data]
      updated_project = HRM::Project.get(project[:project_id], db: @db)
      updated_project[:addendum_value].should eq(addendum1[:addendum_value])
      updated_project[:addendum_count].should eq(1)
      updated_project[:status_id].should eq(2)

      addendum2 = insert_sample_projects_addendums(@db, addendum_date: '2013/12/20', project: project, append_mode: true)[:data]
      updated_project = HRM::Project.get(project[:project_id], db: @db)
      updated_project[:addendum_value].should eq(addendum2[:addendum_value])
      updated_project[:addendum_count].should eq(2)
      updated_project[:status_id].should eq(2)

      addendum3 = insert_sample_projects_addendums(@db, addendum_date: '2013/12/10', project: project, append_mode: true)[:data]
      updated_project = HRM::Project.get(project[:project_id], db: @db)
      updated_project[:addendum_value].should eq(addendum2[:addendum_value])
      updated_project[:addendum_count].should eq(3)
      updated_project[:status_id].should eq(2)

      HRM::ProjectAddendum.delete(addendum2[:addendum_id], db: @db)
      updated_project = HRM::Project.get(project[:project_id], db: @db)
      updated_project[:addendum_value].should eq(addendum3[:addendum_value])
      updated_project[:addendum_count].should eq(2)
      updated_project[:status_id].should eq(2)

      HRM::ProjectAddendum.delete_all!(db: @db)
      updated_project = HRM::Project.get(project[:project_id], db: @db)
      updated_project[:addendum_value].should be_nil
      updated_project[:addendum_count].should eq(0)
      updated_project[:status_id].should eq(1)
    end
  end

  it 'harus mengelola hirarki data' do
    data    = insert_sample_projects(@db, contract_date: '2013/12/31', parent: nil)
    root    = data[:data]
    child1  = insert_sample_projects(@db, contract_date: '2013/12/30', parent: root[:project_id], append_mode: true, type: data[:type], group: data[:group])[:data]
    child2  = insert_sample_projects(@db, contract_date: '2013/12/05', parent: root[:project_id], append_mode: true, type: data[:type], group: data[:group])[:data]
    child21 = insert_sample_projects(@db, contract_date: '2013/11/20', parent: child2[:project_id], append_mode: true, type: data[:type], group: data[:group])[:data]
    child22 = insert_sample_projects(@db, contract_date: '2013/11/10', parent: child2[:project_id], append_mode: true, type: data[:type], group: data[:group])[:data]
    child3  = insert_sample_projects(@db, contract_date: '2013/12/10', parent: root[:project_id], append_mode: true, type: data[:type], group: data[:group])[:data]

    tree = HRM::Project.select(db: @db, order: :sort_desc)

    tree[0][:depth].should eq(0)
    tree[0][:path].should eq(root[:project_id].to_s)
    tree[0][:sort_asc].should eq('001')
    tree[0][:sort_desc].should eq('001')

    tree[1][:depth].should eq(1)
    tree[1][:path].should eq("#{root[:project_id]}/#{child1[:project_id]}")
    tree[1][:sort_asc].should eq('001/003')
    tree[1][:sort_desc].should eq('001/001')

    tree[3][:depth].should eq(1)
    tree[3][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}")
    tree[3][:sort_asc].should eq('001/001')
    tree[3][:sort_desc].should eq('001/003')

    tree[4][:depth].should eq(2)
    tree[4][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}/#{child21[:project_id]}")
    tree[4][:sort_asc].should eq('001/001/002')
    tree[4][:sort_desc].should eq('001/003/001')

    tree[5][:depth].should eq(2)
    tree[5][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}/#{child22[:project_id]}")
    tree[5][:sort_asc].should eq('001/001/001')
    tree[5][:sort_desc].should eq('001/003/002')

    tree[2][:depth].should eq(1)
    tree[2][:path].should eq("#{root[:project_id]}/#{child3[:project_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    # Reorder
    HRM::Project.update(child2[:project_id], data: { contract_date: '2013/12/20' }, db: @db)
    tree = HRM::Project.select(db: @db, order: :sort_desc)

    tree[2][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    tree[3][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}/#{child21[:project_id]}")
    tree[3][:sort_asc].should eq('001/002/002')
    tree[3][:sort_desc].should eq('001/002/001')

    tree[4][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}/#{child22[:project_id]}")
    tree[4][:sort_asc].should eq('001/002/001')
    tree[4][:sort_desc].should eq('001/002/002')

    tree[5][:path].should eq("#{root[:project_id]}/#{child3[:project_id]}")
    tree[5][:sort_asc].should eq('001/001')
    tree[5][:sort_desc].should eq('001/003')

    # Hapus
    HRM::Project.delete(child21[:project_id], db: @db)
    tree = HRM::Project.select(db: @db, order: :sort_desc)

    tree[3][:depth].should eq(2)
    tree[3][:path].should eq("#{root[:project_id]}/#{child2[:project_id]}/#{child22[:project_id]}")
    tree[3][:sort_asc].should eq('001/002/001')
    tree[3][:sort_desc].should eq('001/002/001')

    # Hapus parent
    HRM::Project.delete(child2[:project_id], db: @db)
    tree = HRM::Project.select(db: @db, order: :sort_desc)

    tree[3][:depth].should eq(0)
    tree[3][:path].should eq(child22[:project_id].to_s)
    tree[3][:sort_asc].should eq('001')
    tree[3][:sort_desc].should eq('002')
  end
end
