require './server/models/auth'
require './server/modules/hrm/models/letter_type'

describe 'hrm/models/letter_type' do
  include LetterSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::LetterType.table_name.should eq(:letters_types)
    HRM::LetterType.view_name.should be_nil
    HRM::LetterType.primary_key.should eq(:type_id)
  end

  it 'harus melakukan operasi CRUD' do
    initial_data = insert_sample_letters_types(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::LetterType.get(initial_data[0][:type_id], db: @db)
    inserted_data.should eq({
      type_id:     initial_data[0][:type_id],
      letter_code: initial_data[0][:letter_code],
      letter_type: initial_data[0][:letter_type]
    })

    # Update
    to_update = { letter_type: '*updated*' }
    update_count = HRM::LetterType.update(initial_data[0][:type_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::LetterType.get(initial_data[0][:type_id], db: @db)
    updated_data.should eq({
      type_id:     initial_data[0][:type_id],
      letter_code: initial_data[0][:letter_code],
      letter_type: '*updated*'
    })

    # Delete
    delete_count = HRM::LetterType.delete(initial_data[0][:type_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::LetterType.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::LetterType.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::LetterType.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
