require './server/models/auth'
require './server/modules/hrm/models/project_type'
require './server/modules/hrm/models/project'
require './server/modules/hrm/models/employee'

describe 'hrm/models/project_type' do
  include ProjectSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::ProjectType.table_name.should eq(:projects_types)
    HRM::ProjectType.view_name.should be_nil
    HRM::ProjectType.primary_key.should eq(:type_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai dan proyek harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db
    HRM::Project.delete_all! db: @db

    # Create
    initial_data = insert_sample_projects_types(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::ProjectType.get(initial_data[0][:type_id], db: @db)
    inserted_data.should eq({
      type_id:      initial_data[0][:type_id],
      project_type: initial_data[0][:project_type]
    })

    # Update
    to_update = { project_type: '*updated*' }
    update_count = HRM::ProjectType.update(initial_data[0][:type_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::ProjectType.get(initial_data[0][:type_id], db: @db)
    updated_data.should eq({
      type_id:      initial_data[0][:type_id],
      project_type: '*updated*'
    })

    # Delete
    delete_count = HRM::ProjectType.delete(initial_data[0][:type_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::ProjectType.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::ProjectType.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::ProjectType.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
