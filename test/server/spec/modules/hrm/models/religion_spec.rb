require './server/models/auth'
require './server/modules/hrm/models/religion'
require './server/modules/hrm/models/employee'

describe 'hrm/models/religion' do
  include ReligionSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Religion.table_name.should eq(:religions)
    HRM::Religion.view_name.should be_nil
    HRM::Religion.primary_key.should eq(:religion_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db

    # Create
    initial_data = insert_sample_religions(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Religion.get(initial_data[0][:religion_id], db: @db)
    inserted_data.should eq({
      religion_id: initial_data[0][:religion_id],
      religion:    initial_data[0][:religion]
    })

    # Update
    to_update = { religion: '*updated*' }
    update_count = HRM::Religion.update(initial_data[0][:religion_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Religion.get(initial_data[0][:religion_id], db: @db)
    updated_data.should eq({
      religion_id: initial_data[0][:religion_id],
      religion:    '*updated*'
    })

    # Delete
    delete_count = HRM::Religion.delete(initial_data[0][:religion_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Religion.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Religion.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Religion.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
