require './server/models/auth'
require './server/modules/hrm/models/location'
require './server/modules/hrm/models/project'
require './server/modules/hrm/models/employee'

describe 'hrm/models/location' do
  include LocationSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Location.table_name.should eq(:locations)
    HRM::Location.view_name.should eq(:v_locations)
    HRM::Location.primary_key.should eq(:location_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai dan proyek harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db
    HRM::Project.delete_all! db: @db

    # Create
    initial_data = insert_sample_locations(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Location.get(initial_data[0][:location_id], db: @db)
    inserted_data.should include({
      location_id:   initial_data[0][:location_id],
      location_name: initial_data[0][:location_name],
      address:       initial_data[0][:address],
      city:          initial_data[0][:city],
      region:        initial_data[0][:region],
      country:       initial_data[0][:country]
    })

    # Update
    to_update = { location_name: '*updated*', address: '*updated*' }
    update_count = HRM::Location.update(initial_data[0][:location_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Location.get(initial_data[0][:location_id], db: @db)
    updated_data.should include({
      location_id:   initial_data[0][:location_id],
      location_name: '*updated*',
      address:       '*updated*',
      city:          initial_data[0][:city],
      region:        initial_data[0][:region],
      country:       initial_data[0][:country]
    })

    # Delete
    delete_count = HRM::Location.delete(initial_data[0][:location_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Location.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Location.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Location.select(db: @db)
    remaining_data.length.should eq(0)
  end

  it 'harus mengelola hirarki data' do
    # constraint: Data pegawai dan proyek harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db
    HRM::Project.delete_all! db: @db

    root    = insert_sample_locations(@db, location_name: 'Root', parent: nil)[:data]
    child1  = insert_sample_locations(@db, location_name: 'aaaa', parent: root[:location_id], append_mode: true)[:data]
    child2  = insert_sample_locations(@db, location_name: 'cccc', parent: root[:location_id], append_mode: true)[:data]
    child21 = insert_sample_locations(@db, location_name: 'xxxx', parent: child2[:location_id], append_mode: true)[:data]
    child22 = insert_sample_locations(@db, location_name: 'yyyy', parent: child2[:location_id], append_mode: true)[:data]
    child3  = insert_sample_locations(@db, location_name: 'bbbb', parent: root[:location_id], append_mode: true)[:data]

    tree = HRM::Location.select(db: @db, order: :sort_asc)

    tree[0][:depth].should eq(0)
    tree[0][:path].should eq(root[:location_id].to_s)
    tree[0][:sort_asc].should eq('001')
    tree[0][:sort_desc].should eq('001')

    tree[1][:depth].should eq(1)
    tree[1][:path].should eq("#{root[:location_id]}/#{child1[:location_id]}")
    tree[1][:sort_asc].should eq('001/001')
    tree[1][:sort_desc].should eq('001/003')

    tree[2][:depth].should eq(1)
    tree[2][:path].should eq("#{root[:location_id]}/#{child3[:location_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    tree[3][:depth].should eq(1)
    tree[3][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}")
    tree[3][:sort_asc].should eq('001/003')
    tree[3][:sort_desc].should eq('001/001')

    tree[4][:depth].should eq(2)
    tree[4][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}/#{child21[:location_id]}")
    tree[4][:sort_asc].should eq('001/003/001')
    tree[4][:sort_desc].should eq('001/001/002')

    tree[5][:depth].should eq(2)
    tree[5][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}/#{child22[:location_id]}")
    tree[5][:sort_asc].should eq('001/003/002')
    tree[5][:sort_desc].should eq('001/001/001')

    # Reorder
    HRM::Location.update(child2[:location_id], data: { location_name: 'aabb' }, db: @db)
    tree = HRM::Location.select(db: @db, order: :sort_asc)

    tree[2][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}")
    tree[2][:sort_asc].should eq('001/002')
    tree[2][:sort_desc].should eq('001/002')

    tree[3][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}/#{child21[:location_id]}")
    tree[3][:sort_asc].should eq('001/002/001')
    tree[3][:sort_desc].should eq('001/002/002')

    tree[4][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}/#{child22[:location_id]}")
    tree[4][:sort_asc].should eq('001/002/002')
    tree[4][:sort_desc].should eq('001/002/001')

    tree[5][:path].should eq("#{root[:location_id]}/#{child3[:location_id]}")
    tree[5][:sort_asc].should eq('001/003')
    tree[5][:sort_desc].should eq('001/001')

    # Hapus
    HRM::Location.delete(child21[:location_id], db: @db)
    tree = HRM::Location.select(db: @db, order: :sort_asc)

    tree[3][:depth].should eq(2)
    tree[3][:path].should eq("#{root[:location_id]}/#{child2[:location_id]}/#{child22[:location_id]}")
    tree[3][:sort_asc].should eq('001/002/001')
    tree[3][:sort_desc].should eq('001/002/001')

    # Hapus parent
    HRM::Location.delete(child2[:location_id], db: @db)
    tree = HRM::Location.select(db: @db, order: :sort_asc)

    tree[3][:depth].should eq(0)
    tree[3][:path].should eq(child22[:location_id].to_s)
    tree[3][:sort_asc].should eq('002')
    tree[3][:sort_desc].should eq('001')
  end
end
