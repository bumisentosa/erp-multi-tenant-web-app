require './server/models/auth'
require './server/modules/hrm/models/bank'
require './server/modules/hrm/models/employee'

describe 'hrm/models/bank' do
  include BankSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Bank.table_name.should eq(:banks)
    HRM::Bank.view_name.should be_nil
    HRM::Bank.primary_key.should eq(:bank_id)
  end

  it 'harus melakukan operasi CRUD' do
    # constraint: Data pegawai harus dikosongkan terlebih dahulu
    HRM::Employee.delete_all! db: @db

    # Create
    initial_data = insert_sample_banks(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Bank.get(initial_data[0][:bank_id], db: @db)
    inserted_data.should eq({
      bank_id:        initial_data[0][:bank_id],
      bank_name:      initial_data[0][:bank_name],
      branch:         initial_data[0][:branch],
      address:        initial_data[0][:address],
      city:           initial_data[0][:city],
      region:         initial_data[0][:region],
      country:        initial_data[0][:country]
    })

    # Update
    to_update = { bank_name: '*updated*', address: '*updated*' }
    update_count = HRM::Bank.update(initial_data[0][:bank_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Bank.get(initial_data[0][:bank_id], db: @db)
    updated_data.should eq({
      bank_id:        initial_data[0][:bank_id],
      bank_name:      '*updated*',
      branch:         initial_data[0][:branch],
      address:        '*updated*',
      city:           initial_data[0][:city],
      region:         initial_data[0][:region],
      country:        initial_data[0][:country]
    })

    # Delete
    delete_count = HRM::Bank.delete(initial_data[0][:bank_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Bank.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Bank.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Bank.select(db: @db)
    remaining_data.length.should eq(0)
  end
end
