require './server/models/auth'
require './server/modules/hrm/models/employee'

describe 'hrm/models/employee' do
  include EmployeeSpecHelper

  before(:each) do
    db_config = DB_CONFIG[ENV['RACK_ENV']]
    DB.stub(:get_tenant).and_return({
      db_name:     'erp_u_test',
      db_user:     db_config['username'],
      db_password: Auth.encrypt(db_config['password']),
      db_host:     db_config['host'],
      db_port:     db_config['port']
    })
    @db = DB.open_database token: 'some_token'
  end

  after(:each) do
    @db.disconnect
  end

  it 'harus menentukan atribut tabel' do
    HRM::Employee.table_name.should eq(:employees)
    HRM::Employee.view_name.should eq(:v_employees)
    HRM::Employee.primary_key.should eq(:employee_id)
  end

  it 'harus melakukan operasi CRUD' do
    # Create
    initial_data = insert_sample_employees(@db, amount: 3)[:data]

    # Read
    inserted_data = HRM::Employee.get(initial_data[0][:employee_id], db: @db)
    inserted_data.should include({
      employee_id: initial_data[0][:employee_id],
      project_id:  initial_data[0][:project_id],
      location_id: initial_data[0][:location_id],
      position_id: initial_data[0][:position_id],

      employee_no:   initial_data[0][:employee_no],
      employee_name: initial_data[0][:employee_name],
      photo_url:     initial_data[0][:photo_url],
      birth_date:    initial_data[0][:birth_date],
      birth_place:   initial_data[0][:birth_place],
      gender_id:     initial_data[0][:gender_id],
      religion_id:   initial_data[0][:religion_id],
      address:       initial_data[0][:address],
      city:          initial_data[0][:city],
      region:        initial_data[0][:region],
      home_phone:    initial_data[0][:home_phone],
      mobile_phone:  initial_data[0][:mobile_phone],
      email:         initial_data[0][:email],

      identity_card_no:          initial_data[0][:identity_card_no],
      identity_card_expiry_date: initial_data[0][:identity_card_expiry_date],
      identity_card_address:     initial_data[0][:identity_card_address],
      identity_card_city:        initial_data[0][:identity_card_city],

      marital_status_id:  initial_data[0][:marital_status_id],
      spouse_name:        initial_data[0][:spouse_name],
      spouse_birth_date:  initial_data[0][:spouse_birth_date],
      spouse_birth_place: initial_data[0][:spouse_birth_place],
      spouse_job:         initial_data[0][:spouse_job],

      tax_no:          initial_data[0][:tax_no],
      tax_date:        initial_data[0][:tax_date],
      bank_id:         initial_data[0][:bank_id],
      bank_account_no: initial_data[0][:bank_account_no],

      join_date:          initial_data[0][:join_date],
      employment_date:    initial_data[0][:employment_date],
      is_outer_employee:  initial_data[0][:is_outer_employee]
    })

    # Update
    to_update = { employee_name: '*updated*', address: '*updated*' }
    update_count = HRM::Employee.update(initial_data[0][:employee_id], data: to_update, db: @db)
    update_count.should eq(1)
    updated_data = HRM::Employee.get(initial_data[0][:employee_id], db: @db)
    updated_data.should include({
      employee_id: initial_data[0][:employee_id],
      project_id:  initial_data[0][:project_id],
      location_id: initial_data[0][:location_id],
      position_id: initial_data[0][:position_id],

      employee_no:   initial_data[0][:employee_no],
      employee_name: '*updated*',
      photo_url:     initial_data[0][:photo_url],
      birth_date:    initial_data[0][:birth_date],
      birth_place:   initial_data[0][:birth_place],
      gender_id:     initial_data[0][:gender_id],
      religion_id:   initial_data[0][:religion_id],
      address:       '*updated*',
      city:          initial_data[0][:city],
      region:        initial_data[0][:region],
      home_phone:    initial_data[0][:home_phone],
      mobile_phone:  initial_data[0][:mobile_phone],
      email:         initial_data[0][:email],

      identity_card_no:          initial_data[0][:identity_card_no],
      identity_card_expiry_date: initial_data[0][:identity_card_expiry_date],
      identity_card_address:     initial_data[0][:identity_card_address],
      identity_card_city:        initial_data[0][:identity_card_city],

      marital_status_id:  initial_data[0][:marital_status_id],
      spouse_name:        initial_data[0][:spouse_name],
      spouse_birth_date:  initial_data[0][:spouse_birth_date],
      spouse_birth_place: initial_data[0][:spouse_birth_place],
      spouse_job:         initial_data[0][:spouse_job],

      tax_no:          initial_data[0][:tax_no],
      tax_date:        initial_data[0][:tax_date],
      bank_id:         initial_data[0][:bank_id],
      bank_account_no: initial_data[0][:bank_account_no],

      join_date:          initial_data[0][:join_date],
      employment_date:    initial_data[0][:employment_date],
      is_outer_employee:  initial_data[0][:is_outer_employee]
    })

    # Delete
    delete_count = HRM::Employee.delete(initial_data[0][:employee_id], db: @db)
    delete_count.should eq(1)
    remaining_data = HRM::Employee.select(db: @db)
    remaining_data.length.should eq(2)

    delete_count = HRM::Employee.delete_all!(db: @db)
    delete_count.should eq(2)
    remaining_data = HRM::Employee.select(db: @db)
    remaining_data.length.should eq(0)
  end

  context 'Proyek' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeProject.table_name.should eq(:employees_projects)
      HRM::EmployeeProject.view_name.should be_nil
      HRM::EmployeeProject.primary_key.should eq(:mutation_id)
    end

    it 'harus mengelola data mutasi proyek pegawai' do
      HRM::Employee.delete_all db: @db
      HRM::Project.delete_all! db: @db
      HRM::Location.delete_all! db: @db

      locations = insert_sample_locations(@db, amount: 4)[:data]
      projects = insert_sample_projects(@db, amount: 4, location: locations[0])[:data]
      employee = insert_sample_employees(@db, project: projects[0], location: locations[0])[:data]

      insert_sample_employees_projects(@db, amount: 3, employee: employee, projects: projects)
      data = HRM::EmployeeProject.select(db: @db)
      data.length.should eq(3)

      data[0][:mutation_date] = Date.today
      data.delete_at(2)
      data << {
        employee_id:   employee[:employee_id],
        project_id:    projects[3][:project_id],
        mutation_date: Date.today + rand(18_250)
      }
      updated_ids = HRM::EmployeeProject.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeProject.select(db: @db)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Unit Lokasi' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeLocation.table_name.should eq(:employees_locations)
      HRM::EmployeeLocation.view_name.should be_nil
      HRM::EmployeeLocation.primary_key.should eq(:mutation_id)
    end

    it 'harus mengelola data mutasi lokasi pegawai' do
      HRM::Employee.delete_all! db: @db
      HRM::Project.delete_all! db: @db
      HRM::Location.delete_all! db: @db

      locations = insert_sample_locations(@db, amount: 4)[:data]
      projects = insert_sample_projects(@db, amount: 4, location: locations[0])[:data]
      employee = insert_sample_employees(@db, project: projects[0], location: locations[0])[:data]

      insert_sample_employees_locations(@db, amount: 3, employee: employee, locations: locations)
      data = HRM::EmployeeLocation.select(db: @db)
      data.length.should eq(3)

      data[0][:mutation_date] = Date.today
      data.delete_at(2)
      data << {
        employee_id:   employee[:employee_id],
        location_id:   locations[3][:location_id],
        mutation_date: Date.today + rand(18_250)
      }
      updated_ids = HRM::EmployeeLocation.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeLocation.select(db: @db)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Jabatan' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeePosition.table_name.should eq(:employees_positions)
      HRM::EmployeePosition.view_name.should be_nil
      HRM::EmployeePosition.primary_key.should eq(:mutation_id)
    end

    it 'harus mengelola data mutasi jabatan pegawai' do
      HRM::Employee.delete_all! db: @db
      HRM::Project.delete_all! db: @db
      HRM::Location.delete_all! db: @db
      HRM::Position.delete_all! db: @db

      locations = insert_sample_locations(@db, amount: 4)[:data]
      projects = insert_sample_projects(@db, amount: 4, location: locations[0])[:data]
      positions = insert_sample_positions(@db, amount: 4)[:data]
      employee = insert_sample_employees(@db, project: projects[0], location: locations[0], position: positions[0])[:data]

      insert_sample_employees_positions(@db, amount: 3, employee: employee, positions: positions)
      data = HRM::EmployeePosition.select(db: @db)
      data.length.should eq(3)

      data[0][:mutation_date] = Date.today
      data.delete_at(2)
      data << {
        employee_id:   employee[:employee_id],
        position_id:   positions[3][:position_id],
        mutation_date: Date.today + rand(18_250)
      }
      updated_ids = HRM::EmployeePosition.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeePosition.select(db: @db)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'SIM' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeDriverLicense.table_name.should eq(:employees_driver_licenses)
      HRM::EmployeeDriverLicense.view_name.should be_nil
      HRM::EmployeeDriverLicense.primary_key.should eq(:employee_license_id)
    end

    it 'harus mengelola data SIM pegawai' do
      employee = insert_sample_employees(@db)[:data]
      driver_licenses = insert_sample_driver_licenses(@db, amount: 4)[:data]

      insert_sample_employees_driver_licenses(@db, amount: 3, employee: employee, driver_licenses: driver_licenses)
      data = HRM::EmployeeDriverLicense.select(db: @db, order: :license_code)
      data.length.should eq(3)

      data[0][:license_no] = '*updated*'
      data.delete_at(2)
      data << {
        employee_id:  employee[:employee_id],
        license_code: driver_licenses[3][:license_code],
        license_no:   random_string(13, :number),
        expiry_date:  Date.today + rand(18_250)
      }
      updated_ids = HRM::EmployeeDriverLicense.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeDriverLicense.select(db: @db, order: :license_code)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Anak' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeChild.table_name.should eq(:employees_children)
      HRM::EmployeeChild.view_name.should be_nil
      HRM::EmployeeChild.primary_key.should eq(:child_id)
    end

    it 'harus mengelola data anak pegawai' do
      employee = insert_sample_employees(@db)[:data]

      insert_sample_employees_children(@db, amount: 3, employee: employee)
      data = HRM::EmployeeChild.select(db: @db, order: :child_id)
      data.length.should eq(3)

      data[0][:child_name] = '*updated*'
      data.delete_at(2)
      data << {
        employee_id: employee[:employee_id],
        child_name:  Faker::Name.name,
        birth_date:  Date.today - rand(18_250),
        birth_place: Faker::Address.city,
        gender_id:   rand(2) == 1
      }
      updated_ids = HRM::EmployeeChild.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeChild.select(db: @db, order: :child_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Status kawin' do
    it 'harus meng-update marital_status' do
      # Insert
      marital_status_ids = [1, 2, 3]
      insert_sample_employees(@db, amount: 3, marital_status_ids: marital_status_ids)
      data = HRM::Employee.select(db: @db, order: :employee_id, columns: [:employee_id, :marital_status_id, :marital_status, :children_count])

      data[0].should include({ marital_status: 'TK', children_count: 0 })
      data[1].should include({ marital_status: 'K/0', children_count: 0 })
      data[2].should include({ marital_status: '?/0', children_count: 0 })

      # Update
      marital_status_ids = [3, 1, 2]
      (0..marital_status_ids.length - 1).each do |i|
        HRM::Employee.update(data[i][:employee_id], db: @db, data: {
          marital_status_id: marital_status_ids[i]
        })
      end
      updated_data = HRM::Employee.select(db: @db, order: :employee_id, columns: [:employee_id, :marital_status_id, :marital_status, :children_count])

      updated_data[0].should include({ marital_status: '?/0', children_count: 0 })
      updated_data[1].should include({ marital_status: 'TK', children_count: 0 })
      updated_data[2].should include({ marital_status: 'K/0', children_count: 0 })
    end

    it 'harus meng-update marital_status jika data anak berubah' do
      data = insert_sample_employees(@db, amount: 3, marital_status_ids: [1, 2, 3])[:data]

      # Insert
      children = []
      children << insert_sample_employees_children(@db, employee: data[1])[:data]
      updated_data = HRM::Employee.select(db: @db, order: :employee_id, columns: [:employee_id, :marital_status_id, :marital_status, :children_count])

      updated_data[0].should include({ marital_status: 'TK', children_count: 0 })
      updated_data[1].should include({ marital_status: 'K/1', children_count: 1 })
      updated_data[2].should include({ marital_status: '?/0', children_count: 0 })

      children << insert_sample_employees_children(@db, employee: data[2], append_mode: true)[:data]
      children << insert_sample_employees_children(@db, employee: data[2], append_mode: true)[:data]
      updated_data = HRM::Employee.select(db: @db, order: :employee_id, columns: [:employee_id, :marital_status_id, :marital_status, :children_count])

      updated_data[0].should include({ marital_status: 'TK', children_count: 0 })
      updated_data[1].should include({ marital_status: 'K/1', children_count: 1 })
      updated_data[2].should include({ marital_status: '?/2', children_count: 2 })

      # Update
      HRM::EmployeeChild.update(children[0][:child_id], db: @db, data: {
        employee_id: updated_data[0][:employee_id]
      })
      updated_data = HRM::Employee.select(db: @db, order: :employee_id)

      updated_data[0].should include({ marital_status: 'TK', children_count: 1 })
      updated_data[1].should include({ marital_status: 'K/0', children_count: 0 })
      updated_data[2].should include({ marital_status: '?/2', children_count: 2 })

      # Delete
      HRM::EmployeeChild.delete(children[1][:child_id], db: @db)
      updated_data = HRM::Employee.select(db: @db, order: :employee_id)

      updated_data[0].should include({ marital_status: 'TK', children_count: 1 })
      updated_data[1].should include({ marital_status: 'K/0', children_count: 0 })
      updated_data[2].should include({ marital_status: '?/1', children_count: 1 })

      HRM::EmployeeChild.delete_all! db: @db
      updated_data = HRM::Employee.select(db: @db, order: :employee_id)

      updated_data[0].should include({ marital_status: 'TK', children_count: 0 })
      updated_data[1].should include({ marital_status: 'K/0', children_count: 0 })
      updated_data[2].should include({ marital_status: '?/0', children_count: 0 })
    end
  end

  context 'Berhenti/keluar' do
    pending 'Belum diimplementasikan'
  end

  context 'Pendidikan formal' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeEducation.table_name.should eq(:employees_educations)
      HRM::EmployeeEducation.view_name.should be_nil
      HRM::EmployeeEducation.primary_key.should eq(:education_id)
    end

    it 'harus mengelola data pendidikan' do
      employee = insert_sample_employees(@db)[:data]
      levels = insert_sample_educations_levels(@db, amount: 4)[:data]

      insert_sample_employees_educations(@db, amount: 3, employee: employee, levels: levels)
      data = HRM::EmployeeEducation.select(db: @db, order: :education_id)
      data.length.should eq(3)

      data[0][:education_name] = '*updated*'
      data.delete_at(2)
      year = rand(1800..2050)
      data << {
        employee_id:    employee[:employee_id],
        level_id:       levels[3][:level_id],
        education_name: Faker::Company.name,
        major:          Faker::Commerce.department,
        year_start:     year,
        year_end:       year + rand(1..10)
      }
      updated_ids = HRM::EmployeeEducation.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeEducation.select(db: @db, order: :education_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Pendidikan non formal' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeEducationNonFormal.table_name.should eq(:employees_educations_non_formal)
      HRM::EmployeeEducationNonFormal.view_name.should be_nil
      HRM::EmployeeEducationNonFormal.primary_key.should eq(:education_id)
    end

    it 'harus mengelola data pendidikan non formal' do
      employee = insert_sample_employees(@db)[:data]

      insert_sample_employees_educations_non_formal(@db, amount: 3, employee: employee)
      data = HRM::EmployeeEducationNonFormal.select(db: @db, order: :education_id)
      data.length.should eq(3)

      data[0][:education_name] = '*updated*'
      data.delete_at(2)
      year = rand(1800..2050)
      data << {
        employee_id:    employee[:employee_id],
        education_name: Faker::Company.name,
        year:           "#{year}",
        duration:       "#{rand(1..10)}",
        organizer:      Faker::Company.name
      }
      updated_ids = HRM::EmployeeEducationNonFormal.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeEducationNonFormal.select(db: @db, order: :education_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Keikutsertaan dalam organisasi' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeOrganization.table_name.should eq(:employees_organizations)
      HRM::EmployeeOrganization.view_name.should be_nil
      HRM::EmployeeOrganization.primary_key.should eq(:organization_id)
    end

    it 'harus mengelola data keikutsertaan dalam organisasi' do
      employee = insert_sample_employees(@db)[:data]

      insert_sample_employees_organizations(@db, amount: 3, employee: employee)
      data = HRM::EmployeeOrganization.select(db: @db, order: :organization_id)
      data.length.should eq(3)

      data[0][:organization_name] = '*updated*'
      data.delete_at(2)
      year = rand(1800..2050)
      data << {
        employee_id:       employee[:employee_id],
        organization_name: Faker::Company.name,
        activity:          Faker::Commerce.department,
        year:              "#{year}"
      }
      updated_ids = HRM::EmployeeOrganization.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeOrganization.select(db: @db, order: :organization_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Pengalaman kerja' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeExperience.table_name.should eq(:employees_experiences)
      HRM::EmployeeExperience.view_name.should be_nil
      HRM::EmployeeExperience.primary_key.should eq(:experience_id)
    end

    it 'harus mengelola data pengalaman kerja' do
      employee = insert_sample_employees(@db)[:data]

      insert_sample_employees_experiences(@db, amount: 3, employee: employee)
      data = HRM::EmployeeExperience.select(db: @db, order: :experience_id)
      data.length.should eq(3)

      data[0][:company_name] = '*updated*'
      data.delete_at(2)
      year = rand(1800..2050)
      data << {
        employee_id:   employee[:employee_id],
        company_name:  Faker::Company.name,
        business_type: Faker::Commerce.department,
        position:      Faker::Lorem.words.map(&:capitalize).join(' '),
        year:          "#{year}"
      }
      updated_ids = HRM::EmployeeExperience.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeExperience.select(db: @db, order: :experience_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Kompetensi pegawai' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeCompetence.table_name.should eq(:employees_competences)
      HRM::EmployeeCompetence.view_name.should be_nil
      HRM::EmployeeCompetence.primary_key.should eq(:employee_competence_id)
    end

    it 'harus mengelola data kompetensi pegawai' do
      employee = insert_sample_employees(@db)[:data]
      competences = insert_sample_competences(@db, amount: 4)[:data]

      insert_sample_employees_competences(@db, amount: 3, employee: employee, competences: competences)
      data = HRM::EmployeeCompetence.select(db: @db, order: :employee_competence_id)
      data.length.should eq(3)

      data[0][:competence_no] = '*updated*'
      data.delete_at(2)
      date = Date.today - (365 * rand(10))
      data << {
        employee_id:     employee[:employee_id],
        competence_id:   competences[3][:competence_id],
        competence_no:   "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        competence_name: Faker::Lorem.words(1..3).join(' '),
        competence_date: date,
        expiry_date:     date - (30 * rand(5))
      }
      updated_ids = HRM::EmployeeCompetence.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeCompetence.select(db: @db, order: :employee_competence_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

  context 'Jaminan sosial (BPJS)' do
    it 'harus menentukan atribut tabel' do
      HRM::EmployeeSocialSecurity.table_name.should eq(:employees_social_security)
      HRM::EmployeeSocialSecurity.view_name.should be_nil
      HRM::EmployeeSocialSecurity.primary_key.should eq(:employee_ss_id)
    end

    it 'harus mengelola data jaminan sosial pegawai' do
      employee = insert_sample_employees(@db)[:data]
      social_security = insert_sample_social_security(@db, amount: 4)[:data]

      insert_sample_employees_social_security(@db, amount: 3, employee: employee, social_security: social_security)
      data = HRM::EmployeeSocialSecurity.select(db: @db, order: :employee_ss_id)
      data.length.should eq(3)

      data[0][:ss_no] = '*updated*'
      data.delete_at(2)
      data << {
        employee_id: employee[:employee_id],
        ss_code:     social_security[3][:ss_code],
        ss_no:       random_string(13, :number),
        ss_date:     Date.today + rand(18_250)
      }
      updated_ids = HRM::EmployeeSocialSecurity.update_per(:employee_id, employee[:employee_id], db: @db, data: data)
      updated_ids.count.should eq(3)

      updated_data = HRM::EmployeeSocialSecurity.select(db: @db, order: :employee_ss_id)
      updated_data[0].should eq(data[0])
      updated_data[1].should eq(data[1])
      updated_data[2].should_not equal(data[2])
      updated_data[2].should include(data[2])
    end
  end

end
