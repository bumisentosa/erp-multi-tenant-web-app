require './server/models/auth'
require './server/modules/hrm/models/employee'
require './server/modules/hrm/routes/employee_route'

describe 'hrm/routes/employee' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.employees.all'])

    @data = [
      { employee_id: 42, employee_name: Faker::Lorem.word, address: Faker::Lorem.word },
      { employee_id: 43, employee_name: Faker::Lorem.word, address: Faker::Lorem.word },
      { employee_id: 44, employee_name: Faker::Lorem.word, address: Faker::Lorem.word }
    ]
    @projects = [
      { mutation_id: 1, mutation_date: Date.today },
      { mutation_id: 2, mutation_date: Date.today + 30 },
      { mutation_id: 3, mutation_date: Date.today + 60 }
    ]
    @locations = [
      { mutation_id: 1, mutation_date: Date.today },
      { mutation_id: 2, mutation_date: Date.today + 30 },
      { mutation_id: 3, mutation_date: Date.today + 60 }
    ]
    @positions = [
      { mutation_id: 1, mutation_date: Date.today },
      { mutation_id: 2, mutation_date: Date.today + 30 },
      { mutation_id: 3, mutation_date: Date.today + 60 }
    ]
    @driver_licenses = [
      { employee_license_id: 3, license_code: 'A', license_no: Faker::Lorem.word },
      { employee_license_id: 4, license_code: 'B', license_no: Faker::Lorem.word },
      { employee_license_id: 5, license_code: 'C', license_no: Faker::Lorem.word }
    ]
    @children = [
      { child_id: 3, child_name: Faker::Lorem.word },
      { child_id: 4, child_name: Faker::Lorem.word },
      { child_id: 5, child_name: Faker::Lorem.word }
    ]
    @educations = [
      { education_id: 3, level_id: 1, education_name: Faker::Lorem.word },
      { education_id: 4, level_id: 2, education_name: Faker::Lorem.word },
      { education_id: 5, level_id: 3, education_name: Faker::Lorem.word }
    ]
    @educations_non_formal = [
      { education_id: 3, education_name: Faker::Lorem.word },
      { education_id: 4, education_name: Faker::Lorem.word },
      { education_id: 5, education_name: Faker::Lorem.word }
    ]
    @experiences = [
      { experience_id: 3, company_name: Faker::Lorem.word },
      { experience_id: 4, company_name: Faker::Lorem.word },
      { experience_id: 5, company_name: Faker::Lorem.word }
    ]
    @organizations = [
      { organization_id: 3, organization_name: Faker::Lorem.word },
      { organization_id: 4, organization_name: Faker::Lorem.word },
      { organization_id: 5, organization_name: Faker::Lorem.word }
    ]
    @competences = [
      { employee_competence_id: 3, competence_id: 1, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
      { employee_competence_id: 4, competence_id: 2, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
      { employee_competence_id: 5, competence_id: 3, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word }
    ]
    @social_security = [
      { employee_ss_id: 3, ss_code: 'A', ss_no: Faker::Lorem.word },
      { employee_ss_id: 4, ss_code: 'B', ss_no: Faker::Lorem.word },
      { employee_ss_id: 5, ss_code: 'C', ss_no: Faker::Lorem.word }
    ]
    @letters = [
      { letter_id: 3, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
      { letter_id: 4, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
      { letter_id: 5, employee_id: 1, type_id: 3, letter_no: Faker::Lorem.word }
    ]
    @item = {
      employee_id: 42,
      employee_name: Faker::Lorem.word,
      address: Faker::Lorem.word,
      projects: @projects,
      locations: @locations,
      positions: @positions,
      driver_licenses: @driver_licenses,
      children: @children,
      educations: @educations,
      educations_non_formal: @educations_non_formal,
      experiences: @experiences,
      organizations: @organizations,
      competences: @competences,
      social_security: @social_security,
      letters: @letters
    }

    HRM::Employee.stub(:select).and_return(@data.clone)
    HRM::Employee.stub(:get) { |id| id == '42' ? @item.clone : nil }
    HRM::Employee.stub(:insert).and_return('42')
    HRM::Employee.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Employee.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }

    HRM::EmployeeProject.stub(:select).and_return(@projects.clone)
    HRM::EmployeeProject.stub(:update_per).and_return(@projects.clone)

    HRM::EmployeeLocation.stub(:select).and_return(@locations.clone)
    HRM::EmployeeLocation.stub(:update_per).and_return(@locations.clone)

    HRM::EmployeePosition.stub(:select).and_return(@positions.clone)
    HRM::EmployeePosition.stub(:update_per).and_return(@positions.clone)

    HRM::EmployeeDriverLicense.stub(:select).and_return(@driver_licenses.clone)
    HRM::EmployeeDriverLicense.stub(:update_per).and_return(@driver_licenses.clone)

    HRM::EmployeeChild.stub(:select).and_return(@children.clone)
    HRM::EmployeeChild.stub(:update_per).and_return(@children.clone)

    HRM::EmployeeEducation.stub(:select).and_return(@educations.clone)
    HRM::EmployeeEducation.stub(:update_per).and_return(@educations.clone)

    HRM::EmployeeEducationNonFormal.stub(:select).and_return(@educations_non_formal.clone)
    HRM::EmployeeEducationNonFormal.stub(:update_per).and_return(@educations_non_formal.clone)

    HRM::EmployeeExperience.stub(:select).and_return(@experiences.clone)
    HRM::EmployeeExperience.stub(:update_per).and_return(@experiences.clone)

    HRM::EmployeeOrganization.stub(:select).and_return(@organizations.clone)
    HRM::EmployeeOrganization.stub(:update_per).and_return(@organizations.clone)

    HRM::EmployeeCompetence.stub(:select).and_return(@competences.clone)
    HRM::EmployeeCompetence.stub(:update_per).and_return(@competences.clone)

    HRM::EmployeeSocialSecurity.stub(:select).and_return(@social_security.clone)
    HRM::EmployeeSocialSecurity.stub(:update_per).and_return(@social_security.clone)

    HRM::Letter.stub(:select).and_return(@letters.clone)
    HRM::Letter.stub(:update_per).and_return(@letters.clone)
  end

  it 'harus mengakses data' do
    get '/employees/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/employees/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus menambah data' do
    post_data = {
      employee_name: Faker::Lorem.word,
      address:       Faker::Lorem.word,
      projects: [
        { mutation_id: 1, mutation_date: Date.today },
        { mutation_id: 2, mutation_date: Date.today + 30 },
        { mutation_id: 3, mutation_date: Date.today + 60 }
      ],
      locations: [
        { mutation_id: 1, mutation_date: Date.today },
        { mutation_id: 2, mutation_date: Date.today + 30 },
        { mutation_id: 3, mutation_date: Date.today + 60 }
      ],
      positions: [
        { mutation_id: 1, mutation_date: Date.today },
        { mutation_id: 2, mutation_date: Date.today + 30 },
        { mutation_id: 3, mutation_date: Date.today + 60 }
      ],
      driver_licenses: [
        { license_code: 'A', license_no: Faker::Lorem.word },
        { license_code: 'B', license_no: Faker::Lorem.word },
        { license_code: 'C', license_no: Faker::Lorem.word }
      ],
      children: [
        { child_name: Faker::Lorem.word },
        { child_name: Faker::Lorem.word },
        { child_name: Faker::Lorem.word }
      ],
      educations: [
        { level_id: 1, education_name: Faker::Lorem.word },
        { level_id: 2, education_name: Faker::Lorem.word },
        { level_id: 3, education_name: Faker::Lorem.word }
      ],
      educations_non_formal: [
        { education_name: Faker::Lorem.word },
        { education_name: Faker::Lorem.word },
        { education_name: Faker::Lorem.word }
      ],
      experiences: [
        { company_name: Faker::Lorem.word },
        { company_name: Faker::Lorem.word },
        { company_name: Faker::Lorem.word }
      ],
      organizations: [
        { organization_name: Faker::Lorem.word },
        { organization_name: Faker::Lorem.word },
        { organization_name: Faker::Lorem.word }
      ],
      competences: [
        { competence_id: 1, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
        { competence_id: 2, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
        { competence_id: 3, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word }
      ],
      social_security: [
        { ss_code: 'A', ss_no: Faker::Lorem.word },
        { ss_code: 'B', ss_no: Faker::Lorem.word },
        { ss_code: 'C', ss_no: Faker::Lorem.word }
      ],
      letters: [
        { letter_id: 3, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
        { letter_id: 4, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
        { letter_id: 5, employee_id: 1, type_id: 3, letter_no: Faker::Lorem.word }
      ]
    }
    post '/employees', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      employee_name: Faker::Lorem.word,
      address:       Faker::Lorem.word,
      driver_licenses: [
        { license_code: 'A', license_no: Faker::Lorem.word },
        { license_code: 'B', license_no: Faker::Lorem.word },
        { license_code: 'C', license_no: Faker::Lorem.word }
      ],
      children: [
        { child_name: Faker::Lorem.word },
        { child_name: Faker::Lorem.word },
        { child_name: Faker::Lorem.word }
      ],
      educations: [
        { level_id: 1, education_name: Faker::Lorem.word },
        { level_id: 2, education_name: Faker::Lorem.word },
        { level_id: 3, education_name: Faker::Lorem.word }
      ],
      educations_non_formal: [
        { education_name: Faker::Lorem.word },
        { education_name: Faker::Lorem.word },
        { education_name: Faker::Lorem.word }
      ],
      experiences: [
        { company_name: Faker::Lorem.word },
        { company_name: Faker::Lorem.word },
        { company_name: Faker::Lorem.word }
      ],
      organizations: [
        { organization_name: Faker::Lorem.word },
        { organization_name: Faker::Lorem.word },
        { organization_name: Faker::Lorem.word }
      ],
      competences: [
        { competence_id: 1, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
        { competence_id: 2, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word },
        { competence_id: 3, competence_no: Faker::Lorem.word.upcase, competence_name: Faker::Lorem.word }
      ],
      social_security: [
        { ss_code: 'A', ss_no: Faker::Lorem.word },
        { ss_code: 'B', ss_no: Faker::Lorem.word },
        { ss_code: 'C', ss_no: Faker::Lorem.word }
      ],
      letters: [
        { letter_id: 3, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
        { letter_id: 4, employee_id: 1, type_id: 2, letter_no: Faker::Lorem.word },
        { letter_id: 5, employee_id: 1, type_id: 3, letter_no: Faker::Lorem.word }
      ]
    }

    put '/employees/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus menghapus data' do
    delete '/employees/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end

end
