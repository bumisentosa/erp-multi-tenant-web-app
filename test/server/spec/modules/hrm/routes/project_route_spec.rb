require './server/models/auth'
require './server/modules/hrm/models/project'
require './server/modules/hrm/routes/project_route'

describe 'hrm/routes/project' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.projects.all'])

    @data = [
      { project_id: 42, project_name: Faker::Lorem.word, contract_value: rand(42_000_000_000) },
      { project_id: 43, project_name: Faker::Lorem.word, contract_value: rand(42_000_000_000) },
      { project_id: 44, project_name: Faker::Lorem.word, contract_value: rand(42_000_000_000) }
    ]
    @locations = [
      { location_id: 1 },
      { location_id: 2 },
      { location_id: 3 }
    ]
    @addendums = [
      { addendum_id: 3, addendum_name: Faker::Lorem.word, addendum_value: rand(42_000_000_000) },
      { addendum_id: 4, addendum_name: Faker::Lorem.word, addendum_value: rand(42_000_000_000) },
      { addendum_id: 5, addendum_name: Faker::Lorem.word, addendum_value: rand(42_000_000_000) }
    ]
    @item = {
      project_id: 42,
      project_name: Faker::Lorem.word,
      contract_value: rand(42_000_000_000),
      locations: @locations,
      addendums: @addendums
    }

    HRM::Project.stub(:select).and_return(@data.clone)
    HRM::Project.stub(:get) { |id| id == '42' ? @item.clone : nil }
    HRM::Project.stub(:insert).and_return('42')
    HRM::Project.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Project.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }

    HRM::ProjectLocation.stub(:select).and_return(@locations.clone)
    HRM::ProjectLocation.stub(:update_per).and_return(@locations.clone)

    HRM::ProjectAddendum.stub(:select).and_return(@addendums.clone)
    HRM::ProjectAddendum.stub(:update_per).and_return(@addendums.clone)
  end

  it 'harus mengakses data' do
    get '/projects/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/projects/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus menambah data' do
    post_data = {
      parent:         '',
      project_name:   Faker::Lorem.word,
      contract_value: rand(42_000_000_000),
      locations: [
        { location_id: 1 },
        { location_id: 2 },
        { location_id: 3 }
      ],
      addendums: [
        { addendum_name: Faker::Lorem.word },
        { addendum_name: Faker::Lorem.word },
        { addendum_name: Faker::Lorem.word }
      ]
    }
    post '/projects', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      project_id:     rand(1..42),
      parent:         rand(1..42),
      project_name:   Faker::Lorem.word,
      contract_value: rand(42_000_000_000),
      locations: [
        { location_id: 1 },
        { location_id: 2 },
        { location_id: 3 }
      ],
      addendums: [
        { addendum_id: 3, addendum_name: Faker::Lorem.word },
        { addendum_id: 4, addendum_name: Faker::Lorem.word },
        { addendum_id: 5, addendum_name: Faker::Lorem.word }
      ]
    }

    put '/projects/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(@item.to_json)
  end

  it 'harus menghapus data' do
    delete '/projects/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end

end
