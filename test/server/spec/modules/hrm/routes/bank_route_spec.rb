require './server/models/auth'
require './server/modules/hrm/models/bank'
require './server/modules/hrm/routes/bank_route'

describe 'hrm/routes/bank' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.banks.all'])

    @data = [
      { bank_id: 42, bank_name: Faker::Lorem.word },
      { bank_id: 43, bank_name: Faker::Lorem.word },
      { bank_id: 44, bank_name: Faker::Lorem.word }
    ]
    HRM::Bank.stub(:select).and_return(@data)
    HRM::Bank.stub(:get) { |id| @data.select{ |item| item[:bank_id] == id.to_i }[0] }
    HRM::Bank.stub(:insert).and_return(42)
    HRM::Bank.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Bank.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/banks/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/banks/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      bank_name:      Faker::Lorem.word,
      branch:         Faker::Lorem.word,
      address:        Faker::Lorem.word,
      city:           Faker::Lorem.word,
      region:         Faker::Lorem.word,
      country:        Faker::Lorem.word
    }
    result = post_data.clone
    result[:bank_id] = 42

    post '/banks', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      bank_id:        rand(1..42),
      bank_name:      Faker::Lorem.word,
      branch:         Faker::Lorem.word,
      address:        Faker::Lorem.word,
      city:           Faker::Lorem.word,
      region:         Faker::Lorem.word,
      country:        Faker::Lorem.word
    }

    put '/banks/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/banks/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
