require './server/models/auth'
require './server/modules/hrm/models/religion'
require './server/modules/hrm/routes/religion_route'

describe 'hrm/routes/religion' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.religions.all'])

    @data = [
      { religion_id: 42, religion_name: Faker::Lorem.word },
      { religion_id: 43, religion_name: Faker::Lorem.word },
      { religion_id: 44, religion_name: Faker::Lorem.word }
    ]
    HRM::Religion.stub(:select).and_return(@data)
    HRM::Religion.stub(:get) { |id| @data.select{ |item| item[:religion_id] == id.to_i }[0] }
    HRM::Religion.stub(:insert).and_return(42)
    HRM::Religion.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Religion.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/religions/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/religions/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      religion_id: 42,
      religion:    Faker::Lorem.word
    }
    result = post_data.clone
    result[:religion_id] = 42

    post '/religions', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      religion_id: rand(1..42),
      religion:    Faker::Lorem.word
    }

    put '/religions/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/religions/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
