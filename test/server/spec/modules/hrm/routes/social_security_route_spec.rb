require './server/models/auth'
require './server/modules/hrm/models/social_security'
require './server/modules/hrm/routes/social_security_route'

describe 'hrm/routes/social_security' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.social_security.all'])

    @data = [
      { ss_code: 'SS-001', ss_name: Faker::Lorem.word, description: Faker::Lorem.sentence },
      { ss_code: 'SS-002', ss_name: Faker::Lorem.word, description: Faker::Lorem.sentence },
      { ss_code: 'SS-003', ss_name: Faker::Lorem.word, description: Faker::Lorem.sentence }
    ]
    HRM::SocialSecurity.stub(:select).and_return(@data)
    HRM::SocialSecurity.stub(:get) { |id| @data.select{ |item| item[:ss_code] == id }[0] }
    HRM::SocialSecurity.stub(:insert).and_return('SS-004')
    HRM::SocialSecurity.stub(:update) { |id| (id == 'SS-001') ? 1 : 0 }
    HRM::SocialSecurity.stub(:delete) { |ids| (ids == ['SS-001']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/social-security/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/social-security/SS-001'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      ss_code:     'SS-004',
      ss_name:     Faker::Lorem.word,
      description: Faker::Lorem.sentence
    }
    result = post_data.clone

    post '/social-security', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      ss_code:     'SS-001',
      ss_name:     Faker::Lorem.word,
      description: Faker::Lorem.sentence
    }

    put '/social-security/SS-001', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/social-security/SS-001'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
