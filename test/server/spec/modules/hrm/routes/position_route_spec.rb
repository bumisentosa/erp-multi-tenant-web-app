require './server/models/auth'
require './server/modules/hrm/models/position'
require './server/modules/hrm/routes/position_route'

describe 'hrm/routes/position' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.positions.all'])

    @data = [
      { position_id: 42, position: Faker::Lorem.word },
      { position_id: 43, position: Faker::Lorem.word },
      { position_id: 44, position: Faker::Lorem.word }
    ]
    HRM::Position.stub(:select).and_return(@data)
    HRM::Position.stub(:get) { |id| @data.select{ |item| item[:position_id] == id.to_i }[0] }
    HRM::Position.stub(:insert).and_return(42)
    HRM::Position.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Position.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/positions/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/positions/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      parent:   rand(1..42),
      position: Faker::Lorem.word
    }

    post '/positions', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      position_id: rand(1..42),
      parent:      rand(1..42),
      position:    Faker::Lorem.word
    }

    put '/positions/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menghapus data' do
    delete '/positions/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
