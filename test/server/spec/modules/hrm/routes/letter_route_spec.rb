require './server/models/auth'
require './server/modules/hrm/models/letter'
require './server/modules/hrm/routes/letter_route'

describe 'hrm/routes/letter' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.letters.all'])

    @data = [
      { letter_id: 42, letter_no: Faker::Lorem.word },
      { letter_id: 43, letter_no: Faker::Lorem.word },
      { letter_id: 44, letter_no: Faker::Lorem.word }
    ]
    HRM::Letter.stub(:select).and_return(@data)
    HRM::Letter.stub(:get) { |id| @data.select{ |item| item[:letter_id] == id.to_i }[0] }
    HRM::Letter.stub(:insert).and_return(42)
    HRM::Letter.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Letter.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/letters/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/letters/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      letter: Faker::Lorem.word
    }
    result = post_data.clone
    result[:letter_id] = 42

    post '/letters', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      letter_id:   rand(1..42),
      letter: Faker::Lorem.word
    }

    put '/letters/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/letters/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
