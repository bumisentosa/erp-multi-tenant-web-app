require './server/models/auth'
require './server/modules/hrm/models/location'
require './server/modules/hrm/routes/location_route'

describe 'hrm/routes/location' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.locations.all'])

    @data = [
      { location_id: 42, location_name: Faker::Lorem.word },
      { location_id: 43, location_name: Faker::Lorem.word },
      { location_id: 44, location_name: Faker::Lorem.word }
    ]
    HRM::Location.stub(:select).and_return(@data)
    HRM::Location.stub(:get) { |id| @data.select{ |item| item[:location_id] == id.to_i }[0] }
    HRM::Location.stub(:insert).and_return(42)
    HRM::Location.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Location.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/locations/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/locations/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      parent:         rand(1..42),
      location_name:  Faker::Lorem.word,
      address:        Faker::Lorem.word,
      city:           Faker::Lorem.word,
      region:         Faker::Lorem.word,
      country:        Faker::Lorem.word
    }

    post '/locations', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      location_id:    rand(1..42),
      parent:         rand(1..42),
      location_name:  Faker::Lorem.word,
      address:        Faker::Lorem.word,
      city:           Faker::Lorem.word,
      region:         Faker::Lorem.word,
      country:        Faker::Lorem.word
    }

    put '/locations/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menghapus data' do
    delete '/locations/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
