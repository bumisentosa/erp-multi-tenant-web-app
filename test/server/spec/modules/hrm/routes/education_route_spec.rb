require './server/models/auth'
require './server/modules/hrm/models/education'
require './server/modules/hrm/routes/education_route'

describe 'hrm/routes/education' do
  def app; HRM::App; end

  context 'Education level' do
    before(:each) do
      Auth.stub(:logged_in?).and_return(true)
      Auth.stub(:get_rules).and_return(['hrm.educations.levels.all'])

      @data = [
        { level_id: 42, education_level: Faker::Lorem.word },
        { level_id: 43, education_level: Faker::Lorem.word },
        { level_id: 44, education_level: Faker::Lorem.word }
      ]
      HRM::EducationLevel.stub(:select).and_return(@data)
      HRM::EducationLevel.stub(:get) { |id| @data.select{ |item| item[:level_id] == id.to_i }[0] }
      HRM::EducationLevel.stub(:insert).and_return(42)
      HRM::EducationLevel.stub(:update) { |id| (id == '42') ? 1 : 0 }
      HRM::EducationLevel.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
    end

    it 'harus mengakses data' do
      get '/educations/levels/'
      last_response.status.should eq(200)
      last_response.body.should eq(@data.to_json)

      get '/educations/levels/42'
      last_response.status.should eq(200)
      last_response.body.should eq(@data[0].to_json)
    end

    it 'harus menambah data' do
      post_data = {
        level_id:        42,
        education_level: Faker::Lorem.word
      }
      result = post_data.clone
      result[:level_id] = 42

      post '/educations/levels', post_data.to_json
      last_response.status.should eq(201)
      last_response.body.should eq(result.to_json)
    end

    it 'harus mengubah data' do
      put_data = {
        level_id:        rand(1..42),
        education_level: Faker::Lorem.word
      }

      put '/educations/levels/42', put_data.to_json
      last_response.status.should eq(200)
      last_response.body.should eq(put_data.to_json)
    end

    it 'harus menghapus data' do
      delete '/educations/levels/42'
      last_response.status.should eq(200)
      last_response.body.should eq('1')
    end
  end
end
