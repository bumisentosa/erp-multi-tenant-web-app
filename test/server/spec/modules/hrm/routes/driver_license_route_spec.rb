require './server/models/auth'
require './server/modules/hrm/models/driver_license'
require './server/modules/hrm/routes/driver_license_route'

describe 'hrm/routes/driver_license' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.driver_licenses.all'])

    @data = [
      { license_code: 'A', description: Faker::Lorem.sentence },
      { license_code: 'B', description: Faker::Lorem.sentence },
      { license_code: 'C', description: Faker::Lorem.sentence }
    ]
    HRM::DriverLicense.stub(:select).and_return(@data)
    HRM::DriverLicense.stub(:get) { |id| @data.select{ |item| item[:license_code] == id }[0] }
    HRM::DriverLicense.stub(:insert).and_return('D')
    HRM::DriverLicense.stub(:update) { |id| (id == 'A') ? 1 : 0 }
    HRM::DriverLicense.stub(:delete) { |ids| (ids == ['A']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/driver-licenses/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/driver-licenses/A'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      license_code: 'D',
      description:  Faker::Lorem.sentence
    }
    result = post_data.clone

    post '/driver-licenses', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      license_code: 'A',
      description:  Faker::Lorem.sentence
    }

    put '/driver-licenses/A', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/driver-licenses/A'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
