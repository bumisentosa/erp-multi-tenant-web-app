require './server/models/auth'
require './server/modules/hrm/routes/index_route'

describe 'hrm/routes/index' do
  def app; HRM::App; end

  before(:each) do
    User.stub(:get).and_return({ username: 'admin', tenant_id: 42 })
    Tenant.stub(:get).and_return({ tenant_id: 42, name: 'Some Company' })
    Auth.stub(:logged_in?).and_return(true)
  end

  it 'harus mengakses halaman index hrm' do
    get '/'
    last_response.should be_ok
  end
end
