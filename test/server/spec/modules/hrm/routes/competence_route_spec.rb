require './server/models/auth'
require './server/modules/hrm/models/competence'
require './server/modules/hrm/routes/competence_route'

describe 'hrm/routes/competence' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.competences.all'])

    @data = [
      { competence_id: 42, competence_name: Faker::Lorem.word },
      { competence_id: 43, competence_name: Faker::Lorem.word },
      { competence_id: 44, competence_name: Faker::Lorem.word }
    ]
    HRM::Competence.stub(:select).and_return(@data)
    HRM::Competence.stub(:get) { |id| @data.select{ |item| item[:competence_id] == id.to_i }[0] }
    HRM::Competence.stub(:insert).and_return(42)
    HRM::Competence.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::Competence.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/competences/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/competences/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      competence_no:   Faker::Lorem.word,
      competence_name: Faker::Lorem.word
    }
    result = post_data.clone
    result[:competence_id] = 42

    post '/competences', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      competence_id:   rand(1..42),
      competence_no:   Faker::Lorem.word,
      competence_name: Faker::Lorem.word
    }

    put '/competences/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/competences/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
