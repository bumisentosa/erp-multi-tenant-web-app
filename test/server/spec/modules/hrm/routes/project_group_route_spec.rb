require './server/models/auth'
require './server/modules/hrm/models/project_group'
require './server/modules/hrm/routes/project_group_route'

describe 'hrm/routes/project_group' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.projects.groups.all'])

    @data = [
      { group_id: 42, group_name: Faker::Lorem.word },
      { group_id: 43, group_name: Faker::Lorem.word },
      { group_id: 44, group_name: Faker::Lorem.word }
    ]
    HRM::ProjectGroup.stub(:select).and_return(@data)
    HRM::ProjectGroup.stub(:get) { |id| @data.select{ |item| item[:group_id] == id.to_i }[0] }
    HRM::ProjectGroup.stub(:insert).and_return(42)
    HRM::ProjectGroup.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::ProjectGroup.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/projects/groups/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/projects/groups/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      group_name: Faker::Lorem.word
    }
    result = post_data.clone
    result[:group_id] = 42

    post '/projects/groups', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      group_id:   rand(1..42),
      group_name: Faker::Lorem.word
    }

    put '/projects/groups/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/projects/groups/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
