require './server/models/auth'
require './server/modules/hrm/models/project_type'
require './server/modules/hrm/routes/project_type_route'

describe 'hrm/routes/project_type' do
  def app; HRM::App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['hrm.projects.types.all'])

    @data = [
      { type_id: 42, project_type: Faker::Lorem.word },
      { type_id: 43, project_type: Faker::Lorem.word },
      { type_id: 44, project_type: Faker::Lorem.word }
    ]
    HRM::ProjectType.stub(:select).and_return(@data)
    HRM::ProjectType.stub(:get) { |id| @data.select{ |item| item[:type_id] == id.to_i }[0] }
    HRM::ProjectType.stub(:insert).and_return(42)
    HRM::ProjectType.stub(:update) { |id| (id == '42') ? 1 : 0 }
    HRM::ProjectType.stub(:delete) { |ids| (ids == ['42']) ? 1 : 0 }
  end

  it 'harus mengakses data' do
    get '/projects/types/'
    last_response.status.should eq(200)
    last_response.body.should eq(@data.to_json)

    get '/projects/types/42'
    last_response.status.should eq(200)
    last_response.body.should eq(@data[0].to_json)
  end

  it 'harus menambah data' do
    post_data = {
      project_type: Faker::Lorem.word
    }
    result = post_data.clone
    result[:type_id] = 42

    post '/projects/types', post_data.to_json
    last_response.status.should eq(201)
    last_response.body.should eq(result.to_json)
  end

  it 'harus mengubah data' do
    put_data = {
      type_id:   rand(1..42),
      project_type: Faker::Lorem.word
    }

    put '/projects/types/42', put_data.to_json
    last_response.status.should eq(200)
    last_response.body.should eq(put_data.to_json)
  end

  it 'harus menghapus data' do
    delete '/projects/types/42'
    last_response.status.should eq(200)
    last_response.body.should eq('1')
  end
end
