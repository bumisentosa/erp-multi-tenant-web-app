# Routes harus dalam order
require './server/modules/hrm/routes/index_route'

require './server/modules/hrm/routes/bank_route'
require './server/modules/hrm/routes/location_route'
require './server/modules/hrm/routes/position_route'

require './server/modules/hrm/routes/project_group_route'
require './server/modules/hrm/routes/project_type_route'
require './server/modules/hrm/routes/project_route'

require './server/modules/hrm/routes/driver_license_route'
require './server/modules/hrm/routes/religion_route'
require './server/modules/hrm/routes/education_route'
require './server/modules/hrm/routes/competence_route'
require './server/modules/hrm/routes/social_security_route'
require './server/modules/hrm/routes/employee_route'
