require './server/helpers/html_helper'

class HtmlHelperSpec
  include HtmlHelper
  attr_writer :title, :stylesheets, :scripts
end

describe 'main/helpers/html_helper' do
  let(:helper) { HtmlHelperSpec.new }

  it 'harus menampilkan hanya product_name' do
    helper.title.should eq('Enterprise Resource Planning')
  end

  it 'harus menampilkan title dan product_name' do
    helper.title = 'Welcome'
    helper.title.should eq('Welcome - Enterprise Resource Planning')
  end

  it 'harus men-generate tag stylesheets' do
    helper.stylesheets = nil
    helper.stylesheets.should eq('')

    helper.stylesheets = ['/css/style1.css', '/css/style2.css', '/less/style.less']
    helper.stylesheets.should eq(
      '<link rel="stylesheet" href="/css/style1.css">' +
      '<link rel="stylesheet" href="/css/style2.css">' +
      '<link rel="stylesheet/less" href="/less/style.less">'
    )
  end

  it 'harus men-generate tag scripts' do
    helper.scripts = nil
    helper.scripts.should eq('')

    helper.scripts = ['/js/app/script1.js', '/js/app/script2.js']
    helper.scripts.should eq(
      '<script type="text/javascript" src="/js/app/script1.js"></script>' +
      '<script type="text/javascript" src="/js/app/script2.js"></script>'
    )
  end
end
