require './server/helpers/validation_helper'

class ValidationHelperSpec
  include ValidationHelper
end

describe 'main/helpers/validation_helper' do
  let(:helper) { ValidationHelperSpec.new }

  before(:each) do
    @columns = {
      name:     { presence: true, label: 'Nama' },
      address:  { max_length: 10, label: 'Alamat' },
      email:    { email: true, min_length: 15, label: 'Email' },
      username: { min_length: 4, max_length: 16 },
      password: { password: true, min_length: 6 }
    }
  end

  it 'harus memvalidasi presence' do
    message = 'tidak boleh kosong'
    helper.get_error_presence('foo').should be_nil
    helper.get_error_presence('').should eq(message)
    helper.get_error_presence(nil).should eq(message)
  end

  it 'harus memvalidasi email' do
    message = 'bukan merupakan email yang benar'
    helper.get_error_email('adisayoga@gmail.com').should be_nil
    helper.get_error_email('foo').should eq(message)
    helper.get_error_email('@bad.email').should eq(message)
  end

  it 'harus memvalidasi lebar minimal' do
    message = 'terlalu pendek, minimal %{min} karakter'
    helper.get_error_min_length('something', 5).should be_nil
    helper.get_error_min_length('something', 42).should eq(message % { min: 42 })
  end

  it 'harus memvalidasi lebar maksimal' do
    message = 'terlalu panjang, maksimal %{max} karakter'
    helper.get_error_max_length('something', 42).should be_nil
    helper.get_error_max_length('something', 5).should eq(message % { max: 5 })
  end

  it 'harus memvalidasi password' do
    message = 'harus terdapat setidaknya satu alfanumeric, huruf kapital, ' +
              'dan huruf kecil'
    helper.get_error_password('aSdf10p').should be_nil
    helper.get_error_password('asdfiop').should eq(message)
    helper.get_error_password('aSdfiop').should eq(message)
  end

  it 'harus memvalidasi error (error pertama)' do
    error = helper.get_error(@columns, {
      address:  'From far far away',
      email:    '@bad.email',
      username: 'adi',
      password: 'foo'
    })
    error.should eq({ column: :name, type: :presence, message: 'Nama tidak boleh kosong' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'From far far away',
      email:    '@bad.email',
      username: 'adi',
      password: 'foo'
    })
    error.should eq({ column: :address, type: :max_length, message: 'Alamat terlalu panjang, maksimal 10 karakter' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    '@bad.email',
      username: 'adi',
      password: 'foo'
    })
    error.should eq({ column: :email, type: :min_length, message: 'Email terlalu pendek, minimal 15 karakter' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    '@bad.email.1234567890',
      username: 'adi',
      password: 'foo'
    })
    error.should eq({ column: :email, type: :email, message: 'Email bukan merupakan email yang benar' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    'adisayoga@gmail.com',
      username: 'adi',
      password: 'foo'
    })
    error.should eq({ column: :username, type: :min_length, message: 'Terlalu pendek, minimal 4 karakter' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    'adisayoga@gmail.com',
      username: 'adisayoga',
      password: 'foo'
    })
    error.should eq({ column: :password, type: :min_length, message: 'Terlalu pendek, minimal 6 karakter' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    'adisayoga@gmail.com',
      username: 'adisayoga',
      password: 'fooooooo'
    })
    error.should eq({ column: :password, type: :password, message: 'Harus terdapat setidaknya satu alfanumeric, huruf kapital, dan huruf kecil' })

    error = helper.get_error(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    'adisayoga@gmail.com',
      username: 'adisayoga',
      password: 'aSdf10p'
    })
    error.should be_nil
  end

  it 'harus memvalidasi semua errors' do
    errors = helper.get_errors(@columns, {
      name:     'Adi Sayoga',
      address:  'Tabanan',
      email:    'adisayoga@gmail.com',
      username: 'adisayoga',
      password: 'aSdf10p'
    })
    errors.should be_nil

    errors = helper.get_errors(@columns, {
      address:  'From far far away',
      email:    '@bad.email',
      username: 'adi',
      password: 'foo'
    })

    errors[:name].should eq([
      { type: :presence, message: 'Nama tidak boleh kosong' }])

    errors[:address].should eq([
      { type: :max_length, message: 'Alamat terlalu panjang, maksimal 10 karakter' }])

    errors[:email].should eq([
      { type: :min_length, message: 'Email terlalu pendek, minimal 15 karakter' },
      { type: :email, message: 'Email bukan merupakan email yang benar' }])

    errors[:username].should eq([
      { type: :min_length, message: 'Terlalu pendek, minimal 4 karakter' }])

    errors[:password].should eq([
      { type: :min_length, message: 'Terlalu pendek, minimal 6 karakter' },
      { type: :password, message: 'Harus terdapat setidaknya satu alfanumeric, huruf kapital, dan huruf kecil' }])
  end
end
