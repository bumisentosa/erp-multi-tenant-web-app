require './server/helpers/encryption_helper'

class EncryptionHelperSpec
  include EncryptionHelper
end

describe 'main/helpers/encryption_helper' do
  let(:helper) { EncryptionHelperSpec.new }

  it 'harus meng-encrypt dan men-decrypt data (hex)' do
    value = 'foo'
    encrypted_value = helper.encrypt(value)
    decrypted_value = helper.decrypt(encrypted_value)
    value.should eq(decrypted_value)
  end

  it 'harus meng-encrypt dan men-decrypt data (binary)' do
    value = 'foo'
    encrypted_value = helper.encrypt(value, hex: false)
    decrypted_value = helper.decrypt(encrypted_value, hex: false)
    value.should eq(decrypted_value)
  end

  it 'harus membuat token dan mengekstraknya' do
    username = 'admin'
    password = helper.create_password_hash(username, 's3cr!t')
    date = Date.today

    token = helper.create_token(username, password)
    extracted_token = helper.extract_token(token)

    extracted_token[:username].should eq(username)
    extracted_token[:password].should eq(password)
    extracted_token[:date].should eq(date)
  end
end
