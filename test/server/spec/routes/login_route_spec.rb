require './server/routes/login_route'

describe 'main/routes/login' do
  def app; App; end

  before(:each) do
    @username = 'admin'
    @password = 'secr!T'
    @password_hash = Auth.create_password_hash(@username, @password)
    @token = Auth.create_token(@username, @password_hash)
    Auth.stub(:login) { |username, password| @token if username == @username && password == @password }
    Auth.stub(:logged_in?) { |param| param == @token }
    Auth.stub(:logout) { |token| token == @token }
  end

  it 'harus mengakses halaman login' do
    get '/login'
    last_response.should be_ok
  end

  it 'harus gagal login jika user/password salah' do
    post '/login', 'username' => 'user', 'password' => 'wrong'
    last_response.should be_redirect
    follow_redirect!
    last_request.cookies['token'].should be_nil
  end

  it 'harus dapat login' do
    post '/login', 'username' => @username, 'password' => @password
    last_response.should be_redirect
    follow_redirect!
    last_request.cookies['token'].should eq(@token)
  end

  it 'harus login dan redirect ke url yang ditentukan' do
    post '/login?next=welcome', 'username' => @username, 'password' => @password
    last_response.should redirect_to('/welcome')
  end

  it 'harus dapat logout' do
    post '/login', 'username' => @username, 'password' => @password

    get '/logout'
    last_response.should redirect_to('/login')
    follow_redirect!
    last_request.cookies['token'].should be_empty
  end

  it 'harus mengakses halaman reset-password' do
    get '/reset-password'
    last_response.should be_ok
    pending 'Belum diimplementasikan'
  end
end
