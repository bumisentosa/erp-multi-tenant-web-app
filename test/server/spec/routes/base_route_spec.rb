require './server/models/auth'
require './server/routes/base_route'

class AppSpec < Route
  set :root, './server/'

  def public_path?(path_info)
    path_info == '/login'
  end

  def page_path?(path_info)
    path_info == '/'
  end

  get '/login' do
    content_type :html
    'Login!'
  end

  post '/login' do
    token = Auth.login(params[:username], params[:password])
    response.set_cookie 'token', token
  end

  get '/' do
    content_type :html
    'Hello World!'
  end

  get '/secret' do
    json({ data: 'Secret Area!' })
  end

  get '/users/:username' do |username|
    content_type :text
    normalize_username(username)
  end

  get '/tenant' do
    json get_tenant(current_user)
  end
end

describe 'main/routes/base_route' do
  include UserSpecHelper
  def app; AppSpec; end

  before(:each) do
    @username = 'admin'
    @password = 'secr!T'
    @password_hash = Auth.create_password_hash(@username, @password)
    @token = Auth.create_token(@username, @password_hash)
    Auth.stub(:login) { |username, password| @token if username == @username && password == @password }
    Auth.stub(:logged_in?) { |param| param == @token }
  end

  it 'harus mengakses halaman public' do
    get '/login'
    last_response.should be_ok
    last_response.content_type.should include('text/html')
    last_response.body.should eq('Login!')
  end

  it 'harus tidak dapat mengakses halaman jika belum login' do
    get '/'
    last_response.should_not be_ok

    get '/secret'
    last_response.should_not be_ok
  end

  it 'harus dapat mengakses halaman utama (setelah login)' do
    post '/login', username: @username, password: @password
    get '/'
    last_response.should be_ok
    last_response.content_type.should include('text/html')
    last_response.body.should eq('Hello World!')
  end

  it 'harus dapat mengakses halaman api (setelah login)' do
    post '/login', username: @username, password: @password
    get '/secret'
    last_response.should be_ok
    last_response.content_type.should include('application/json')
    last_response.body.should eq('{"data":"Secret Area!"}')
  end

  it 'harus menormalkan nama user' do
    post '/login', username: @username, password: @password
    get '/users/me'
    last_response.should be_ok
    last_response.body.should eq(@username)
  end

  it 'harus mendapatkan data tenant' do
    User.stub(:get).and_return({ username: 'some_user', tenant_id: 42 })
    Tenant.stub(:get).and_return({ tenant_id: 42, company_name: 'Some Company' })

    post '/login', username: @username, password: @password
    get '/tenant'
    last_response.body.should eq('{"tenant_id":42,"company_name":"Some Company"}')
  end
end
