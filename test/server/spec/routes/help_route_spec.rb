require './server/routes/help_route'

describe 'main/routes/help' do
  def app; App; end

  it 'harus mengakses halaman help' do
    get '/help'
    last_response.should be_ok
    pending 'Belum diimplementasikan'
  end

  it 'harus mengakses halaman ketentuan' do
    get '/ketentuan'
    last_response.should be_ok
    pending 'Belum diimplementasikan'
  end

  it 'harus mengakses halaman register' do
    get '/register'
    last_response.should be_ok
    pending 'Belum diimplementasikan'
  end
end
