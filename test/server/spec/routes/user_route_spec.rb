require './server/models/auth'
require './server/routes/user_route'

describe 'main/routes/user' do
  def app; App; end

  before(:each) do
    Auth.stub(:logged_in?).and_return(true)
    Auth.stub(:get_rules).and_return(['users.me.profile'])
  end

  it 'harus mengakses data profil user' do
    UserProfile.stub(:get).and_return('my profile')

    get 'users/me/profile'
    last_response.body.should eq('"my profile"')
  end
end
