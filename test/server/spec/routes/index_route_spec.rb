require './server/models/auth'
require './server/routes/login_route'
require './server/routes/index_route'

describe 'main/routes/index' do
  def app; App; end

  before(:each) do
    @username = 'admin'
    @password = 'secr!T'
    @password_hash = Auth.create_password_hash(@username, @password)
    @token = Auth.create_token(@username, @password_hash)
    Auth.stub(:login) { |username, password| @token if username == @username && password == @password }
    Auth.stub(:logged_in?) { |param| param == @token }
  end

  it 'harus mengakses halaman index' do
    get '/'
    last_response.should_not be_ok

    post '/login', 'username' => @username, 'password' => @password
    get '/'
    last_response.should be_ok
  end
end
