require 'rspec'
require 'rack/test'
require 'faker'

ENV['RACK_ENV'] ||= 'test'

require './server/global'

path = File.dirname(__FILE__)
Dir["#{path}/support/**/*.rb"].each { |file| require file }

# Ini harus di-load terlebih dahulu
Dir["#{path}/spec/**/init.rb"].each { |file| require file }

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
end
