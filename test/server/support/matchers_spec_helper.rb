RSpec::Matchers.define :redirect_to do |expected|
  match do |actual|
    actual.should be_redirect
    actual.location.should eq("http://example.org#{expected}")
  end

  failure_message_for_should do |actual|
    location = actual.location
    location.slice! 'http://example.org' if location
    "expected redirect to '#{location}', got '#{expected}'"
  end
end
