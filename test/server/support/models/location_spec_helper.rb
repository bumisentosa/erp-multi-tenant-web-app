# Module untuk insert sample data unit/lokasi ke database.
module LocationSpecHelper

  def insert_sample_locations(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/location'
    HRM::Location.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        parent:         options[:parent],
        location_name:  options[:location_name] || Faker::Company.name,
        address:        Faker::Address.street_address,
        city:           Faker::Address.city,
        region:         Faker::Address.state,
        country:        Faker::Address.country,
      }
      to_insert[:location_id] = HRM::Location.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
