# Module untuk insert sample data posisi jabatan ke database.
module PositionSpecHelper

  def insert_sample_positions(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/position'
    HRM::Position.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        parent:   options[:parent],
        position: options[:position] || Faker::Commerce.department,
      }
      to_insert[:position_id] = HRM::Position.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
