require_relative '../utils_spec_helper'
require_relative 'location_spec_helper'

# Module untuk insert sample data proyek ke database.
module ProjectSpecHelper
  include UtilsSpecHelper
  include LocationSpecHelper

  def insert_sample_projects_groups(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/project_group'
    HRM::ProjectGroup.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        group_name: options[:group_name] || Faker::Lorem.words(1..3).join(' '),
      }
      to_insert[:group_id] = HRM::ProjectGroup.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

  def insert_sample_projects_types(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/project_type'
    HRM::ProjectType.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        project_type: options[:project_type] || Faker::Lorem.word,
      }
      to_insert[:type_id] = HRM::ProjectType.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

  def insert_sample_projects(db, options = {})
    options = { amount: 1 }.merge(options)
    require './server/modules/hrm/models/project'
    HRM::Project.delete_all! db: db unless options[:append_mode]

    # Dependencies
    dependencies = {}
    dependencies[:type] = options[:type] if options[:type]
    dependencies[:group] = options[:group] if options[:group]

    unless dependencies[:type]
      type_dependencies = insert_sample_projects_types(db)
      type_dependencies[:type] = type_dependencies.delete(:data)
      dependencies.merge! type_dependencies
    end

    unless dependencies[:group]
      group_dependencies = insert_sample_projects_groups(db)
      group_dependencies[:group] = group_dependencies.delete(:data)
      dependencies.merge! group_dependencies
    end

    result = dependencies
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        parent:               options[:parent],
        type_id:              dependencies[:type][:type_id],
        group_id:             dependencies[:group][:group_id],
        project_name:         Faker::Lorem.words(3..6).map(&:capitalize).join(' '),
        contract_no_s1:       "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        contract_no_s2:       "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        contract_date:        options[:contract_date] || Date.today,
        contract_date_start:  options[:contract_date_start] || options[:contract_date] || Date.today,
        contract_date_end:    options[:contract_date_end] || Date.today + 60,
        project_description:  (options.has_key? :project_description) ? options[:project_description] : Faker::Lorem.sentence,
        contract_value:       BigDecimal(rand(10) * 15_000_000),
        description:          Faker::Lorem.paragraph
      }
      to_insert[:project_id] = HRM::Project.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  def insert_sample_projects_locations(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:projects] = options[:projects] if options[:projects]
    dependencies[:locations] = options[:locations] if options[:locations]

    unless dependencies[:projects]
      projects_dependencies = insert_sample_projects(db, amount: options[:amount])
      projects_dependencies[:projects] = projects_dependencies.delete(:data)
      dependencies.merge! projects_dependencies
    end

    unless dependencies[:locations]
      locations_dependencies = insert_sample_locations(db, amount: options[:amount])
      locations_dependencies[:locations] = locations_dependencies.delete(:data)
      dependencies.merge! locations_dependencies
    end

    require './server/modules/hrm/models/project'
    result = dependencies
    HRM::ProjectLocation.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        project_id:  options[:amount] == 1 ? dependencies[:projects][:project_id] : dependencies[:projects][i - 1][:project_id],
        location_id: options[:amount] == 1 ? dependencies[:locations][:location_id] : dependencies[:locations][i - 1][:location_id]
      }
      to_insert[:project_location_id] = HRM::ProjectLocation.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  def insert_sample_projects_addendums(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:project] = options[:project] if options[:project]

    unless dependencies[:project]
      project_dependencies = insert_sample_projects(db)
      project_dependencies[:project] = project_dependencies.delete(:data)
      dependencies.merge! project_dependencies
    end

    require './server/modules/hrm/models/project'
    result = dependencies
    HRM::ProjectAddendum.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        project_id:        dependencies[:project][:project_id],
        addendum_name:     options[:addendum_name] || Faker::Lorem.words(1..3).map(&:capitalize).join(' '),
        addendum_no:       "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        addendum_date:     options[:addendum_date] || Date.today,
        addendum_date_end: options[:addendum_date_end] || Date.today + 60,
        addendum_value:    BigDecimal(rand(10) * 15_000_000)
      }
      to_insert[:addendum_id] = HRM::ProjectAddendum.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end
end
