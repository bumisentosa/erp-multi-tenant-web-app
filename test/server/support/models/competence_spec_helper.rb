require_relative '../utils_spec_helper'

# Module untuk insert sample data komptensi ke database.
module CompetenceSpecHelper
  include UtilsSpecHelper

  def insert_sample_competences(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/competence'
    HRM::Competence.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        competence_code: "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        competence_name: Faker::Lorem.sentence,
        description:     Faker::Lorem.paragraph
      }
      to_insert[:competence_id] = HRM::Competence.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
