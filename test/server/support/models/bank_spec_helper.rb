require_relative '../utils_spec_helper'

# Module untuk insert sample data bank ke database.
module BankSpecHelper
  include UtilsSpecHelper

  def insert_sample_banks(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/bank'
    HRM::Bank.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        bank_name:      Faker::Company.name,
        branch:         Faker::Address.city,
        address:        Faker::Address.street_address,
        city:           Faker::Address.city,
        region:         Faker::Address.state,
        country:        Faker::Address.country
      }
      to_insert[:bank_id] = HRM::Bank.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
