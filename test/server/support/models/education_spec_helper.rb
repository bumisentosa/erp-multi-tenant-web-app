# Module untuk insert sample data pendidikan ke database.
module EducationSpecHelper

  def insert_sample_educations_levels(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/education'
    HRM::EducationLevel.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        level_id:        i,
        education_level: Faker::Lorem.word.upcase
      }
      HRM::EducationLevel.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
