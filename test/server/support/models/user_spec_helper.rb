require_relative '../utils_spec_helper'

# Module untuk insert sample data user ke database.
module UserSpecHelper
  include UtilsSpecHelper

  # Insert sample data tenant.
  def insert_sample_tenants(db, options = {})
    options = { amount: 1 }.merge(options)
    db_config = DB_CONFIG[ENV['RACK_ENV']]

    require './server/models/tenant'
    Tenant.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        name:         Faker::Company.name,
        address:      Faker::Address.street_address,
        city:         Faker::Address.city,
        region:       Faker::Address.state,
        country:      Faker::Address.country,
        phone_number: Faker::PhoneNumber.phone_number,
        email:        Faker::Internet.free_email,
        contact_name: Faker::Name.name,
        db_name:      options[:db_name] || "erp_u_#{Faker::Lorem.word.downcase}_#{i}",
        db_host:      options[:db_host] || db_config['host'],
        db_port:      options[:db_port] || db_config['port'],
        db_user:      options[:db_user] || db_config['username'],
        db_password:  options[:db_password] || db_config['password']
      }
      to_insert[:tenant_id] = Tenant.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

  # Insert sample data role user.
  def insert_sample_user_roles(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/models/user_role'
    UserRole.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = { role: options[:role] || "#{Faker::Lorem.word}_#{i}" }
      to_insert[:role_id] = UserRole.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

  # Insert sample data rule user.
  def insert_sample_user_rules(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:role] = options[:role] if options[:role]

    unless dependencies[:role]
      role_dependencies = insert_sample_user_roles(db)
      role_dependencies[:role] = role_dependencies.delete(:data)
      dependencies.merge! role_dependencies
    end

    require './server/models/user_role'
    result = dependencies
    UserRule.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        role_id: dependencies[:role][:role_id],
        rule:    options[:rule] || Faker::Lorem.words(1..5).join('.')
      }
      to_insert[:rule_id] = UserRule.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data user.
  def insert_sample_users(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:tenant] = options[:tenant] if options[:tenant]
    dependencies[:role] = options[:role] if options[:role]

    unless dependencies[:tenant]
      tenant_dependencies = insert_sample_tenants(db)
      tenant_dependencies[:tenant] = tenant_dependencies.delete(:data)
      dependencies.merge! tenant_dependencies
    end

    unless dependencies[:role]
      role_dependencies = insert_sample_user_roles(db)
      role_dependencies[:role] = role_dependencies.delete(:data)
      dependencies.merge! role_dependencies
    end

    require './server/models/user'
    result = dependencies
    User.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      username = options[:username] || "#{Faker::Internet.user_name}_#{i}"
      password = options[:password] || generate_password(8)

      to_insert = {
        username:  username,
        password:  password,
        role_id:   dependencies[:role][:role_id],
        tenant_id: dependencies[:tenant][:tenant_id]
      }
      User.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data profile user.
  def insert_sample_user_profiles(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:users] = options[:users] if options[:users]

    unless dependencies[:users]
      users_dependencies = insert_sample_users(db, amount: options[:amount])
      users_dependencies[:users] = users_dependencies.delete(:data)
      dependencies.merge! users_dependencies
    end

    require './server/models/user_profile'
    result = dependencies
    UserProfile.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        username:     options[:amount] == 1 ? dependencies[:users][:username] : dependencies[:users][i - 1][:username],
        display_name: Faker::Name.name,
        photo_url:    "path/to/photo/#{Faker::Lorem.word.downcase}.jpg"
      }
      UserProfile.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data session user.
  def insert_sample_user_sessions(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:users] = options[:users] if options[:users]

    unless dependencies[:users]
      users_dependencies = insert_sample_users(db, amount: options[:amount])
      users_dependencies[:users] = users_dependencies.delete(:data)
      dependencies.merge! users_dependencies
    end

    require './server/models/user_session'
    result = dependencies
    UserSession.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        username: options[:amount] == 1 ? dependencies[:users][:username] : dependencies[:users][i - 1][:username],
        token:    generate_password(128)
      }
      UserSession.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

end
