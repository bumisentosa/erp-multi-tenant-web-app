require_relative '../utils_spec_helper'
require_relative 'project_spec_helper'
require_relative 'location_spec_helper'
require_relative 'position_spec_helper'
require_relative 'bank_spec_helper'
require_relative 'driver_license_spec_helper'
require_relative 'education_spec_helper'
require_relative 'competence_spec_helper'
require_relative 'religion_spec_helper'
require_relative 'social_security_spec_helper'

# Module untuk insert sample data pegawai ke database.
module EmployeeSpecHelper
  include UtilsSpecHelper
  include ProjectSpecHelper
  include LocationSpecHelper
  include PositionSpecHelper
  include DriverLicenseSpecHelper
  include BankSpecHelper
  include EducationSpecHelper
  include CompetenceSpecHelper
  include ReligionSpecHelper
  include SocialSecuritySpecHelper

  # Insert sample data pegawai
  def insert_sample_employees(db, options = {})
    options = { amount: 1 }.merge(options)
    require './server/modules/hrm/models/employee'
    HRM::Employee.delete_all! db: db unless options[:append_mode]

    # Dependencies
    dependencies = {}
    dependencies[:project] = options[:project] if options[:project]
    dependencies[:location] = options[:location] if options[:location]
    dependencies[:position] = options[:position] if options[:position]
    dependencies[:bank] = options[:bank] if options[:bank]
    dependencies[:religion] = options[:religion] if options[:religion]

    unless dependencies[:project]
      project_dependencies = insert_sample_projects(db)
      project_dependencies[:project] = project_dependencies.delete(:data)
      dependencies.merge! project_dependencies
    end

    unless dependencies[:location]
      location_dependencies = insert_sample_locations(db)
      location_dependencies[:location] = location_dependencies.delete(:data)
      dependencies.merge! location_dependencies
    end

    unless dependencies[:position]
      position_dependencies = insert_sample_positions(db)
      position_dependencies[:position] = position_dependencies.delete(:data)
      dependencies.merge! position_dependencies
    end

    unless dependencies[:bank]
      bank_dependencies = insert_sample_banks(db)
      bank_dependencies[:bank] = bank_dependencies.delete(:data)
      dependencies.merge! bank_dependencies
    end

    unless dependencies[:religion]
      religion_dependencies = insert_sample_religions(db)
      religion_dependencies[:religion] = religion_dependencies.delete(:data)
      dependencies.merge! religion_dependencies
    end

    result = dependencies
    data = (1..options[:amount]).inject([]) do |items, i|
      marital_status_id = rand(3)
      if options[:marital_status_ids]
        if options[:marital_status_ids].is_a? Array
          marital_status_id = options[:marital_status_ids][i - 1]
        else
          marital_status_id = options[:marital_status_ids]
        end
      end
      to_insert = {
        project_id:  dependencies[:project][:project_id],
        location_id: dependencies[:location][:location_id],
        position_id: dependencies[:position][:position_id],

        employee_no:   "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        employee_name: Faker::Name.name,
        photo_url:     "/path/to/photo_#{random_string(4, :number)}.jpg",
        birth_date:    Date.today - rand(7_300..21_900),
        birth_place:   Faker::Address.city,
        gender_id:     rand(2) == 1,
        religion_id:   dependencies[:religion][:religion_id],
        address:       Faker::Address.street_name,
        city:          Faker::Address.city,
        region:        Faker::Address.state,
        home_phone:    Faker::PhoneNumber.phone_number,
        mobile_phone:  Faker::PhoneNumber.cell_phone,
        email:         Faker::Internet.free_email,

        identity_card_no:          random_string(14, :number),
        identity_card_expiry_date: Date.today + rand(-60..1_825),
        identity_card_address:     Faker::Address.street_name,
        identity_card_city:        Faker::Address.city,

        marital_status_id:  marital_status_id,
        spouse_name:        Faker::Name.name,
        spouse_birth_date:  Date.today - rand(7_300..21_900),
        spouse_birth_place: Faker::Address.city,
        spouse_job:         Faker::Lorem.words(2..5).join(' ').capitalize,

        tax_no:               random_string(14, :number),
        tax_date:             Date.today - rand(18_250),
        social_security_date: Date.today - rand(18_250),
        bank_id:              dependencies[:bank][:bank_id],
        bank_account_no:      random_string(14, :number),

        join_date:         Date.today - rand(18_250),
        employment_date:   Date.today - rand(18_250),
        is_outer_employee: rand(2) == 1
      }
      to_insert[:employee_id] = HRM::Employee.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data mutasi proyek pegawai
  def insert_sample_employees_projects(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:projects] = options[:projects] if options[:projects]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:projects]
      projects_dependencies = insert_sample_projects(db, amount: options[:amount])
      projects_dependencies[:projects] = projects_dependencies.delete(:data)
      dependencies.merge! projects_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeProject.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:   dependencies[:employee][:employee_id],
        project_id:    options[:amount] == 1 ? dependencies[:projects][:project_id] : dependencies[:projects][i - 1][:project_id],
        mutation_date: Date.today + rand(18_250)
      }
      to_insert[:employee_project_id] = HRM::EmployeeProject.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data mutasi unit lokasi pegawai
  def insert_sample_employees_locations(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:locations] = options[:locations] if options[:locations]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:locations]
      locations_dependencies = insert_sample_locations(db, amount: options[:amount])
      locations_dependencies[:locations] = locations_dependencies.delete(:data)
      dependencies.merge! locations_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeLocation.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:   dependencies[:employee][:employee_id],
        location_id:   options[:amount] == 1 ? dependencies[:locations][:location_id] : dependencies[:locations][i - 1][:location_id],
        mutation_date: Date.today + rand(18_250)
      }
      to_insert[:employee_location_id] = HRM::EmployeeLocation.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data mutasi jabatan pegawai
  def insert_sample_employees_positions(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:positions] = options[:positions] if options[:positions]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:positions]
      positions_dependencies = insert_sample_positions(db, amount: options[:amount])
      positions_dependencies[:positions] = positions_dependencies.delete(:data)
      dependencies.merge! positions_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeePosition.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:   dependencies[:employee][:employee_id],
        position_id:   options[:amount] == 1 ? dependencies[:positions][:position_id] : dependencies[:positions][i - 1][:position_id],
        mutation_date: Date.today + rand(18_250)
      }
      to_insert[:employee_position_id] = HRM::EmployeePosition.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data SIM pegawai
  def insert_sample_employees_driver_licenses(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:driver_licenses] = options[:driver_licenses] if options[:driver_licenses]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:driver_licenses]
      driver_licenses_dependencies = insert_sample_driver_licenses(db, amount: options[:amount])
      driver_licenses_dependencies[:driver_licenses] = driver_licenses_dependencies.delete(:data)
      dependencies.merge! driver_licenses_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeDriverLicense.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:  dependencies[:employee][:employee_id],
        license_code: options[:amount] == 1 ? dependencies[:driver_licenses][:license_code] : dependencies[:driver_licenses][i - 1][:license_code],
        license_no:   random_string(13, :number),
        expiry_date:  Date.today + rand(18_250)
      }
      to_insert[:employee_driver_license_id] = HRM::EmployeeDriverLicense.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data anak dari pegawai
  def insert_sample_employees_children(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeChild.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:  dependencies[:employee][:employee_id],
        child_name:   Faker::Name.name,
        birth_date:   Date.today - rand(3650),
        birth_place:  Faker::Address.city,
        gender_id:    rand(2) == 1
      }
      to_insert[:child_id] = HRM::EmployeeChild.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data pendidikan pegawai
  def insert_sample_employees_educations(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:levels] = options[:levels] if options[:levels]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:levels]
      levels_dependencies = insert_sample_educations_levels(db, amount: options[:amount])
      levels_dependencies[:levels] = levels_dependencies.delete(:data)
      dependencies.merge! levels_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeEducation.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      year = rand(1800..2050)
      to_insert = {
        employee_id:    dependencies[:employee][:employee_id],
        level_id:       options[:amount] == 1 ? dependencies[:levels][:level_id] : dependencies[:levels][i - 1][:level_id],
        education_name: Faker::Company.name,
        major:          Faker::Commerce.department,
        year_start:     year,
        year_end:       year + rand(1..10)
      }
      to_insert[:education_id] = HRM::EmployeeEducation.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data pendidikan non formal pegawai
  def insert_sample_employees_educations_non_formal(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeEducationNonFormal.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      year = rand(1800..2050)
      to_insert = {
        employee_id:    dependencies[:employee][:employee_id],
        education_name: Faker::Company.name,
        year:           "#{year}",
        duration:       "#{rand(1..10)}",
        organizer:      Faker::Company.name
      }
      to_insert[:education_id] = HRM::EmployeeEducationNonFormal.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data keikutsertaan dalam organisasi
  def insert_sample_employees_organizations(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeOrganization.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      year = rand(1800..2050)
      to_insert = {
        employee_id:       dependencies[:employee][:employee_id],
        organization_name: Faker::Company.name,
        activity:          Faker::Commerce.department,
        year:              "#{year}"
      }
      to_insert[:organization_id] = HRM::EmployeeOrganization.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data pengalaman pegawai
  def insert_sample_employees_experiences(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeExperience.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      year = rand(1800..2050)
      to_insert = {
        employee_id:   dependencies[:employee][:employee_id],
        company_name:  Faker::Company.name,
        business_type: Faker::Commerce.department,
        position:      Faker::Lorem.words.map(&:capitalize).join(' '),
        year:          "#{year}"
      }
      to_insert[:experience_id] = HRM::EmployeeExperience.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data kompetensi pegawai
  def insert_sample_employees_competences(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:competences] = options[:competences] if options[:competences]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:competences]
      competences_dependencies = insert_sample_competences(db, amount: options[:amount])
      competences_dependencies[:competences] = competences_dependencies.delete(:data)
      dependencies.merge! competences_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeCompetence.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      date = Date.today - (365 * rand(10))
      to_insert = {
        employee_id:       dependencies[:employee][:employee_id],
        competence_id:     options[:amount] == 1 ? dependencies[:competences][:competence_id] : dependencies[:competences][i - 1][:competence_id],
        competence_no:     "#{random_string(4, :uppercase)}-#{random_string(10, :number)}",
        competence_name:   Faker::Lorem.words(1..3).join(' '),
        competence_date:   date,
        expiry_date:       date - (30 * rand(5))
      }
      to_insert[:employee_competence_id] = HRM::EmployeeCompetence.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

  # Insert sample data jaminan sosial (BPJS) pegawai
  def insert_sample_employees_social_security(db, options = {})
    options = { amount: 1 }.merge(options)

    # Dependencies
    dependencies = {}
    dependencies[:employee] = options[:employee] if options[:employee]
    dependencies[:social_security] = options[:social_security] if options[:social_security]

    unless dependencies[:employee]
      employee_dependencies = insert_sample_employees(db)
      employee_dependencies[:employee] = employee_dependencies.delete(:data)
      dependencies.merge! employee_dependencies
    end

    unless dependencies[:social_security]
      social_security_dependencies = insert_sample_social_security(db, amount: options[:amount])
      social_security_dependencies[:social_security] = social_security_dependencies.delete(:data)
      dependencies.merge! social_security_dependencies
    end

    require './server/modules/hrm/models/employee'
    result = dependencies
    HRM::EmployeeSocialSecurity.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id: dependencies[:employee][:employee_id],
        ss_code:     options[:amount] == 1 ? dependencies[:social_security][:ss_code] : dependencies[:social_security][i - 1][:ss_code],
        ss_no:       random_string(14, :number),
        ss_date:     Date.today - rand(18_250)
      }
      to_insert[:employee_ss_id] = HRM::EmployeeSocialSecurity.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

end
