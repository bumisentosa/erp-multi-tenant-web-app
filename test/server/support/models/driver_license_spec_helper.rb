require_relative '../utils_spec_helper'

# Module untuk insert sample data SIM ke database.
module DriverLicenseSpecHelper
  include UtilsSpecHelper

  def insert_sample_driver_licenses(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/driver_license'
    HRM::DriverLicense.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        license_code: i.to_s.rjust(2, '0'),
        description:  Faker::Lorem.paragraph
      }
      HRM::DriverLicense.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
