# Module untuk insert sample data agama ke database.
module ReligionSpecHelper

  def insert_sample_religions(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/religion'
    HRM::Religion.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        religion_id: i,
        religion:    Faker::Lorem.words(1..2).map(&:capitalize).join(' ')
      }
      HRM::Religion.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
