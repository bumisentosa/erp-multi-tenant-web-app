require_relative '../utils_spec_helper'

# Module untuk insert sample data komptensi ke database.
module SocialSecuritySpecHelper
  include UtilsSpecHelper

  def insert_sample_social_security(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/social_security'
    HRM::SocialSecurity.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        ss_code:     "#{random_string(4, :uppercase)}-#{i.to_s.rjust(2, '0')}",
        ss_name:     Faker::Lorem.words(1..3).join(' '),
        description: Faker::Lorem.paragraph
      }
      HRM::SocialSecurity.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

end
