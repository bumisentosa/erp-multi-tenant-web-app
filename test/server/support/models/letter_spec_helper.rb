require_relative '../utils_spec_helper'

# Module untuk insert sample data surat-surat ke database.
module LetterSpecHelper
  include UtilsSpecHelper

  def insert_sample_letters_types(db, options = {})
    options = { amount: 1 }.merge(options)

    require './server/modules/hrm/models/letter_type'
    HRM::LetterType.delete_all! db: db unless options[:append_mode]
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        letter_code: options[:letter_code] || random_string(8, :uppercase),
        letter_type: options[:letter_type] || Faker::Lorem.word
      }
      to_insert[:type_id] = HRM::LetterType.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    { data: data }
  end

  def insert_sample_letters(db, options = {})
    options = { amount: 1 }.merge(options)
    require './server/modules/hrm/models/letter'
    HRM::Letter.delete_all! db: db unless options[:append_mode]

    # Dependencies
    dependencies = {}
    dependencies[:type] = options[:type] if options[:type]

    unless dependencies[:type]
      type_dependencies = insert_sample_letters_types(db)
      type_dependencies[:type] = type_dependencies.delete(:data)
      dependencies.merge! type_dependencies
    end

    result = dependencies
    data = (1..options[:amount]).inject([]) do |items, i|
      to_insert = {
        employee_id:      dependencies[:employee] ? dependencies[:employee][:employee_id] : nil,
        type_id:          dependencies[:type][:type_id],
        letter_no:        "#{random_string(3, :number)}/#{dependencies[:type][:letter_code]}/#{random_string(4, :number)}",
        letter_date:      Date.today,
        letter_date_end:  Date.today + 60,
        description:      Faker::Lorem.paragraph
      }
      to_insert[:letter_id] = HRM::Letter.insert(data: to_insert, db: db)
      items << to_insert
    end

    data = data[0] if options[:amount] == 1
    result[:data] = data
    result
  end

end
