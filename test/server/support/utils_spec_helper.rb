module UtilsSpecHelper
  # Generate random password yang terdapat setidaknya satu alfanumeric, huruf
  # kapital dan huruf kecil.
  def generate_password(length)
    random = ''
    begin
      random = Array.new(length) { [*'0'..'9', *'a'..'z', *'A'..'Z'].sample }.join
    end until length <= 3 || random =~ /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).*$/
    random
  end

  # Membuat random string
  def random_string(length, characters = nil)
    characters = ['a'..'z', 'A'..'Z'] unless characters
    characters = [characters] unless characters.is_a? Array

    o = characters.map do |i|
      i = '0'..'9' if i == :number
      i = 'A'..'Z' if i == :uppercase
      i = 'a'..'Z' if i == :lowercase
      i = i.to_a if i.is_a? Range
      i
    end.flatten
    (0..length - 1).map{ o[rand(o.length)] }.join
  end
end
