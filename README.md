# Enterprise Resource Planning

## Setup Environment

Install dependensi dengan menjalankan perintah ini di console:

    $ bundle install
    $ bower install
    $ npm install

Environment terdiri dari tiga, yaitu: `development`, `test`, dan `production`.


## Menjalankan Server

Untuk menjalankan server jalankan perintah ini pada console:

    $ grunt serve

Dan pada console terpisah, jalankan perintah ini:

    $ rackup

Default environment adalah `development`. Jika ingin menjalankan server dengan
environment lainnya jalankan:

    $ rackup -E environment_name

Buka browser dan navigasi ke [http://localhost:9292](http://localhost:9292)


## Tes

Untuk mengetes server, jalankan perintah ini:

    $ rspec

Sedangkan untuk mengetes client:

    $ grunt karma


## Build

Setelah semuanya selesai, saatnya untuk didistribusikan. Pada console, jalankan
perintah berikut:

    $ grunt build

Outputnya terdapat di folder `dist`.
