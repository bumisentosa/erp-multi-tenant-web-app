-- ----------------------------------------------------------------------------
-- View Posisi Jabatan
-- ----------------------------------------------------------------------------

drop view if exists v_positions;
create view v_positions as

select p.position_id, p.parent, t.depth, t.path, t.sort_asc, t.sort_desc, p.position
from positions p
  inner join positions_tree t on p.position_id = t.position_id;
