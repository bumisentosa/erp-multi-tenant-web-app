-- ----------------------------------------------------------------------------
-- locations: Tree After Insert
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists locations_tree_ai $$

create trigger locations_tree_ai after insert on locations
for each row begin
  declare _to_depth tinyint default -1;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';

  if new.location_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if new.parent is not null then
    select depth, concat(path, '/'), sort_asc, sort_desc
      into _to_depth, _to_path, _to_sort_asc, _to_sort_desc
    from locations_tree
    where location_id = new.parent;
  end if;

  insert into locations_tree(location_id, parent, depth, path, sort_asc, sort_desc)
  values (new.location_id, new.parent, _to_depth + 1, concat(_to_path, new.location_id),
          _to_sort_asc, _to_sort_desc);

  call locations_tree_reorder(new.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- locations: Tree After Update
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists locations_tree_au $$

create trigger locations_tree_au after update on locations
for each row begin
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';
  declare _to_depth tinyint default -1;

  if new.location_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if ifnull(old.parent, -1) != ifnull(new.parent, -1) then
    select path, depth, sort_asc, sort_desc into _path, _depth, _sort_asc, _sort_desc
    from locations_tree
    where location_id = new.location_id;

    if new.parent is not null then
      select concat(path, '/'), depth, concat(sort_asc, '/'), concat(sort_desc, '/')
        into _to_path, _to_depth, _to_sort_asc, _to_sort_desc
      from locations_tree
      where location_id = new.parent;
    end if;

    if instr(_to_path, _path) != 0 then
      update Error__Node_ini_tidak_dapat_pindah_ke_anaknya set x = '42';
    end if;

    update locations_tree set
      parent    = new.parent,
      path      = concat(_to_path, substring_index(path, '/', _depth - depth - 1)),
      sort_asc  = concat(_to_sort_asc, substring_index(sort_asc, '/', _depth - depth - 1)),
      sort_desc = concat(_to_sort_desc, substring_index(sort_desc, '/', _depth - depth - 1)),
      depth     = _to_depth + 1 + depth - _depth
    where path like concat(_path, '%');

    call locations_tree_reorder(old.parent);
    call locations_tree_reorder(new.parent);

  elseif old.location_name != new.location_name then
    call locations_tree_reorder(new.parent);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- locations: Tree Before Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists locations_tree_bd $$

create trigger locations_tree_bd before delete on locations
for each row begin
  declare _child_count int unsigned;
  declare _path varchar(255);
  declare _depth tinyint;

  select count(*) into _child_count from locations_tree where parent = old.location_id;
  if _child_count > 0 then
    select path, depth into _path, _depth from locations_tree
    where location_id = old.location_id;

    -- Update parent
    update locations_tree set parent = null where parent = old.location_id;

    -- Move its children to root
    update locations_tree set
      path      = substring_index(path, '/', _depth - depth),
      sort_asc  = substring_index(sort_asc, '/', _depth - depth),
      sort_desc = substring_index(sort_desc, '/', _depth - depth),
      depth     = depth - _depth - 1
    where path like concat(_path, '/%');

    call locations_tree_reorder(null);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- locations: Tree After Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists locations_tree_ad $$

create trigger locations_tree_ad after delete on locations
for each row begin
  -- Just reorder
  call locations_tree_reorder(old.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- locations: Reorder Tree
-- ----------------------------------------------------------------------------

delimiter $$
drop procedure if exists locations_tree_reorder $$

create procedure locations_tree_reorder(_parent int unsigned)
begin
  declare _exit_loop int default false;
  declare _id int unsigned;
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;

  -- Cursor descendant
  declare _descendant cursor for
  select tb.location_id, tr.depth, tr.path, tr.sort_asc, tr.sort_desc
  from locations tb
    inner join locations_tree tr on tb.location_id = tr.location_id
  where (_parent is null and tr.parent is null) or (tr.parent = _parent)
  order by tb.location_name;

  declare continue handler for not found set _exit_loop = true;

  open _descendant;

  set @cnt_up = 1;
  set @cnt_down = found_rows();

  for_loop: loop
    fetch _descendant into _id, _depth, _path, _sort_asc, _sort_desc;
    if _exit_loop then leave for_loop; end if;

    if _parent is null then
      set @new_sort_asc = lpad(@cnt_up, 3, 0);
      set @new_sort_desc = lpad(@cnt_down, 3, 0);
    else
      set @new_sort_asc = concat(left(_sort_asc, _depth * 4 - 1), '/', lpad(@cnt_up, 3, 0));
      set @new_sort_desc = concat(left(_sort_desc, _depth * 4 - 1), '/', lpad(@cnt_down, 3, 0));
    end if;

    -- Update current node
    update locations_tree set sort_asc = @new_sort_asc, sort_desc = @new_sort_desc
      where location_id = _id;

    -- Update child tree
    update locations_tree set
      sort_asc = concat(@new_sort_asc, '/', substring_index(sort_asc, '/', _depth - depth)),
      sort_desc = concat(@new_sort_desc, '/', substring_index(sort_desc, '/', _depth - depth))
    where location_id != _id and path like concat(_path, '/%');

    set @cnt_up = @cnt_up + 1;
    set @cnt_down = @cnt_down - 1;
  end loop;

  close _descendant;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- positions: Tree After Insert
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists positions_tree_ai $$

create trigger positions_tree_ai after insert on positions
for each row begin
  declare _to_depth tinyint default -1;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';

  if new.position_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if new.parent is not null then
    select depth, concat(path, '/'), sort_asc, sort_desc
      into _to_depth, _to_path, _to_sort_asc, _to_sort_desc
    from positions_tree
    where position_id = new.parent;
  end if;

  insert into positions_tree(position_id, parent, depth, path, sort_asc, sort_desc)
  values (new.position_id, new.parent, _to_depth + 1, concat(_to_path, new.position_id),
          _to_sort_asc, _to_sort_desc);

  call positions_tree_reorder(new.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- positions: Tree After Update
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists positions_tree_au $$

create trigger positions_tree_au after update on positions
for each row begin
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';
  declare _to_depth tinyint default -1;

  if new.position_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if ifnull(old.parent, -1) != ifnull(new.parent, -1) then
    select path, depth, sort_asc, sort_desc into _path, _depth, _sort_asc, _sort_desc
    from positions_tree
    where position_id = new.position_id;

    if new.parent is not null then
      select concat(path, '/'), depth, concat(sort_asc, '/'), concat(sort_desc, '/')
        into _to_path, _to_depth, _to_sort_asc, _to_sort_desc
      from positions_tree
      where position_id = new.parent;
    end if;

    if instr(_to_path, _path) != 0 then
      update Error__Node_ini_tidak_dapat_pindah_ke_anaknya set x = '42';
    end if;

    update positions_tree set
      parent    = new.parent,
      path      = concat(_to_path, substring_index(path, '/', _depth - depth - 1)),
      sort_asc  = concat(_to_sort_asc, substring_index(sort_asc, '/', _depth - depth - 1)),
      sort_desc = concat(_to_sort_desc, substring_index(sort_desc, '/', _depth - depth - 1)),
      depth     = _to_depth + 1 + depth - _depth
    where path like concat(_path, '%');

    call positions_tree_reorder(old.parent);
    call positions_tree_reorder(new.parent);

  elseif old.position != new.position then
    call positions_tree_reorder(new.parent);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- positions: Tree Before Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists positions_tree_bd $$

create trigger positions_tree_bd before delete on positions
for each row begin
  declare _child_count int unsigned;
  declare _path varchar(255);
  declare _depth tinyint;

  select count(*) into _child_count from positions_tree where parent = old.position_id;
  if _child_count > 0 then
    select path, depth into _path, _depth from positions_tree
    where position_id = old.position_id;

    -- Update parent
    update positions_tree set parent = null where parent = old.position_id;

    -- Move its children to root
    update positions_tree set
      path      = substring_index(path, '/', _depth - depth),
      sort_asc  = substring_index(sort_asc, '/', _depth - depth),
      sort_desc = substring_index(sort_desc, '/', _depth - depth),
      depth     = depth - _depth - 1
    where path like concat(_path, '/%');

    call positions_tree_reorder(null);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- positions: Tree After Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists positions_tree_ad $$

create trigger positions_tree_ad after delete on positions
for each row begin
  -- Just reorder
  call positions_tree_reorder(old.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- positions: Reorder Tree
-- ----------------------------------------------------------------------------

delimiter $$
drop procedure if exists positions_tree_reorder $$

create procedure positions_tree_reorder(_parent int unsigned)
begin
  declare _exit_loop int default false;
  declare _id int unsigned;
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;

  -- Cursor descendant
  declare _descendant cursor for
  select tb.position_id, tr.depth, tr.path, tr.sort_asc, tr.sort_desc
  from positions tb
    inner join positions_tree tr on tb.position_id = tr.position_id
  where (_parent is null and tr.parent is null) or (tr.parent = _parent)
  order by tb.position;

  declare continue handler for not found set _exit_loop = true;

  open _descendant;

  set @cnt_up = 1;
  set @cnt_down = found_rows();

  for_loop: loop
    fetch _descendant into _id, _depth, _path, _sort_asc, _sort_desc;
    if _exit_loop then leave for_loop; end if;

    if _parent is null then
      set @new_sort_asc = lpad(@cnt_up, 3, 0);
      set @new_sort_desc = lpad(@cnt_down, 3, 0);
    else
      set @new_sort_asc = concat(left(_sort_asc, _depth * 4 - 1), '/', lpad(@cnt_up, 3, 0));
      set @new_sort_desc = concat(left(_sort_desc, _depth * 4 - 1), '/', lpad(@cnt_down, 3, 0));
    end if;

    -- Update current node
    update positions_tree set sort_asc = @new_sort_asc, sort_desc = @new_sort_desc
      where position_id = _id;

    -- Update child tree
    update positions_tree set
      sort_asc = concat(@new_sort_asc, '/', substring_index(sort_asc, '/', _depth - depth)),
      sort_desc = concat(@new_sort_desc, '/', substring_index(sort_desc, '/', _depth - depth))
    where position_id != _id and path like concat(_path, '/%');

    set @cnt_up = @cnt_up + 1;
    set @cnt_down = @cnt_down - 1;
  end loop;

  close _descendant;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- projects: Tree After Insert
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists projects_tree_ai $$

create trigger projects_tree_ai after insert on projects
for each row begin
  declare _to_depth tinyint default -1;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';

  if new.project_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if new.parent is not null then
    select depth, concat(path, '/'), sort_asc, sort_desc
      into _to_depth, _to_path, _to_sort_asc, _to_sort_desc
    from projects_tree
    where project_id = new.parent;
  end if;

  insert into projects_tree(project_id, parent, depth, path, sort_asc, sort_desc)
  values (new.project_id, new.parent, _to_depth + 1, concat(_to_path, new.project_id),
          _to_sort_asc, _to_sort_desc);

  call projects_tree_reorder(new.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- projects: Tree After Update
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists projects_tree_au $$

create trigger projects_tree_au after update on projects
for each row begin
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';
  declare _to_depth tinyint default -1;

  if new.project_id = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if ifnull(old.parent, -1) != ifnull(new.parent, -1) then
    select path, depth, sort_asc, sort_desc into _path, _depth, _sort_asc, _sort_desc
    from projects_tree
    where project_id = new.project_id;

    if new.parent is not null then
      select concat(path, '/'), depth, concat(sort_asc, '/'), concat(sort_desc, '/')
        into _to_path, _to_depth, _to_sort_asc, _to_sort_desc
      from projects_tree
      where project_id = new.parent;
    end if;

    if instr(_to_path, _path) != 0 then
      update Error__Node_ini_tidak_dapat_pindah_ke_anaknya set x = '42';
    end if;

    update projects_tree set
      parent    = new.parent,
      path      = concat(_to_path, substring_index(path, '/', _depth - depth - 1)),
      sort_asc  = concat(_to_sort_asc, substring_index(sort_asc, '/', _depth - depth - 1)),
      sort_desc = concat(_to_sort_desc, substring_index(sort_desc, '/', _depth - depth - 1)),
      depth     = _to_depth + 1 + depth - _depth
    where path like concat(_path, '%');

    call projects_tree_reorder(old.parent);
    call projects_tree_reorder(new.parent);

  elseif old.contract_date != new.contract_date || old.project_name != new.project_name then
    call projects_tree_reorder(new.parent);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- projects: Tree Before Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists projects_tree_bd $$

create trigger projects_tree_bd before delete on projects
for each row begin
  declare _child_count int unsigned;
  declare _path varchar(255);
  declare _depth tinyint;

  select count(*) into _child_count from projects_tree where parent = old.project_id;
  if _child_count > 0 then
    select path, depth into _path, _depth from projects_tree
    where project_id = old.project_id;

    -- Update parent
    update projects_tree set parent = null where parent = old.project_id;

    -- Move its children to root
    update projects_tree set
      path      = substring_index(path, '/', _depth - depth),
      sort_asc  = substring_index(sort_asc, '/', _depth - depth),
      sort_desc = substring_index(sort_desc, '/', _depth - depth),
      depth     = depth - _depth - 1
    where path like concat(_path, '/%');

    call projects_tree_reorder(null);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- projects: Tree After Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists projects_tree_ad $$

create trigger projects_tree_ad after delete on projects
for each row begin
  -- Just reorder
  call projects_tree_reorder(old.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- projects: Reorder Tree
-- ----------------------------------------------------------------------------

delimiter $$
drop procedure if exists projects_tree_reorder $$

create procedure projects_tree_reorder(_parent int unsigned)
begin
  declare _exit_loop int default false;
  declare _id int unsigned;
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;

  -- Cursor descendant
  declare _descendant cursor for
  select tb.project_id, tr.depth, tr.path, tr.sort_asc, tr.sort_desc
  from projects tb
    inner join projects_tree tr on tb.project_id = tr.project_id
  where (_parent is null and tr.parent is null) or (tr.parent = _parent)
  order by tb.contract_date, tb.project_name DESC;

  declare continue handler for not found set _exit_loop = true;

  open _descendant;

  set @cnt_up = 1;
  set @cnt_down = found_rows();

  for_loop: loop
    fetch _descendant into _id, _depth, _path, _sort_asc, _sort_desc;
    if _exit_loop then leave for_loop; end if;

    if _parent is null then
      set @new_sort_asc = lpad(@cnt_up, 3, 0);
      set @new_sort_desc = lpad(@cnt_down, 3, 0);
    else
      set @new_sort_asc = concat(left(_sort_asc, _depth * 4 - 1), '/', lpad(@cnt_up, 3, 0));
      set @new_sort_desc = concat(left(_sort_desc, _depth * 4 - 1), '/', lpad(@cnt_down, 3, 0));
    end if;

    -- Update current node
    update projects_tree set sort_asc = @new_sort_asc, sort_desc = @new_sort_desc
      where project_id = _id;

    -- Update child tree
    update projects_tree set
      sort_asc = concat(@new_sort_asc, '/', substring_index(sort_asc, '/', _depth - depth)),
      sort_desc = concat(@new_sort_desc, '/', substring_index(sort_desc, '/', _depth - depth))
    where project_id != _id and path like concat(_path, '/%');

    set @cnt_up = @cnt_up + 1;
    set @cnt_down = @cnt_down - 1;
  end loop;

  close _descendant;
end $$

delimiter ;


