tables = [{
  table_name:   'locations',
  column_id:    'location_id',
  order_by:     'tb.location_name',
  sort_changed: 'old.location_name != new.location_name'
}, {
  table_name:   'positions',
  column_id:    'position_id',
  order_by:     'tb.position',
  sort_changed: 'old.position != new.position'
}, {
  table_name:   'projects',
  column_id:    'project_id',
  order_by:     'tb.contract_date, tb.project_name DESC',
  sort_changed: 'old.contract_date != new.contract_date || old.project_name != new.project_name'
}]

statement = <<EOS
-- ----------------------------------------------------------------------------
-- %{table_name}: Tree After Insert
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists %{table_name}_tree_ai $$

create trigger %{table_name}_tree_ai after insert on %{table_name}
for each row begin
  declare _to_depth tinyint default -1;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';

  if new.%{column_id} = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if new.parent is not null then
    select depth, concat(path, '/'), sort_asc, sort_desc
      into _to_depth, _to_path, _to_sort_asc, _to_sort_desc
    from %{table_name}_tree
    where %{column_id} = new.parent;
  end if;

  insert into %{table_name}_tree(%{column_id}, parent, depth, path, sort_asc, sort_desc)
  values (new.%{column_id}, new.parent, _to_depth + 1, concat(_to_path, new.%{column_id}),
          _to_sort_asc, _to_sort_desc);

  call %{table_name}_tree_reorder(new.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- %{table_name}: Tree After Update
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists %{table_name}_tree_au $$

create trigger %{table_name}_tree_au after update on %{table_name}
for each row begin
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;
  declare _to_path, _to_sort_asc, _to_sort_desc varchar(255) default '';
  declare _to_depth tinyint default -1;

  if new.%{column_id} = new.parent then
    update Error__Parent_node_ini_tidak_boleh_dirinya_sendiri set x = '42';
  end if;

  if ifnull(old.parent, -1) != ifnull(new.parent, -1) then
    select path, depth, sort_asc, sort_desc into _path, _depth, _sort_asc, _sort_desc
    from %{table_name}_tree
    where %{column_id} = new.%{column_id};

    if new.parent is not null then
      select concat(path, '/'), depth, concat(sort_asc, '/'), concat(sort_desc, '/')
        into _to_path, _to_depth, _to_sort_asc, _to_sort_desc
      from %{table_name}_tree
      where %{column_id} = new.parent;
    end if;

    if instr(_to_path, _path) != 0 then
      update Error__Node_ini_tidak_dapat_pindah_ke_anaknya set x = '42';
    end if;

    update %{table_name}_tree set
      parent    = new.parent,
      path      = concat(_to_path, substring_index(path, '/', _depth - depth - 1)),
      sort_asc  = concat(_to_sort_asc, substring_index(sort_asc, '/', _depth - depth - 1)),
      sort_desc = concat(_to_sort_desc, substring_index(sort_desc, '/', _depth - depth - 1)),
      depth     = _to_depth + 1 + depth - _depth
    where path like concat(_path, '%%');

    call %{table_name}_tree_reorder(old.parent);
    call %{table_name}_tree_reorder(new.parent);

  elseif %{sort_changed} then
    call %{table_name}_tree_reorder(new.parent);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- %{table_name}: Tree Before Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists %{table_name}_tree_bd $$

create trigger %{table_name}_tree_bd before delete on %{table_name}
for each row begin
  declare _child_count int unsigned;
  declare _path varchar(255);
  declare _depth tinyint;

  select count(*) into _child_count from %{table_name}_tree where parent = old.%{column_id};
  if _child_count > 0 then
    select path, depth into _path, _depth from %{table_name}_tree
    where %{column_id} = old.%{column_id};

    -- Update parent
    update %{table_name}_tree set parent = null where parent = old.%{column_id};

    -- Move its children to root
    update %{table_name}_tree set
      path      = substring_index(path, '/', _depth - depth),
      sort_asc  = substring_index(sort_asc, '/', _depth - depth),
      sort_desc = substring_index(sort_desc, '/', _depth - depth),
      depth     = depth - _depth - 1
    where path like concat(_path, '/%%');

    call %{table_name}_tree_reorder(null);
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- %{table_name}: Tree After Delete
-- ----------------------------------------------------------------------------

delimiter $$
drop trigger if exists %{table_name}_tree_ad $$

create trigger %{table_name}_tree_ad after delete on %{table_name}
for each row begin
  -- Just reorder
  call %{table_name}_tree_reorder(old.parent);
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- %{table_name}: Reorder Tree
-- ----------------------------------------------------------------------------

delimiter $$
drop procedure if exists %{table_name}_tree_reorder $$

create procedure %{table_name}_tree_reorder(_parent int unsigned)
begin
  declare _exit_loop int default false;
  declare _id int unsigned;
  declare _path, _sort_asc, _sort_desc varchar(255);
  declare _depth tinyint;

  -- Cursor descendant
  declare _descendant cursor for
  select tb.%{column_id}, tr.depth, tr.path, tr.sort_asc, tr.sort_desc
  from %{table_name} tb
    inner join %{table_name}_tree tr on tb.%{column_id} = tr.%{column_id}
  where (_parent is null and tr.parent is null) or (tr.parent = _parent)
  order by %{order_by};

  declare continue handler for not found set _exit_loop = true;

  open _descendant;

  set @cnt_up = 1;
  set @cnt_down = found_rows();

  for_loop: loop
    fetch _descendant into _id, _depth, _path, _sort_asc, _sort_desc;
    if _exit_loop then leave for_loop; end if;

    if _parent is null then
      set @new_sort_asc = lpad(@cnt_up, 3, 0);
      set @new_sort_desc = lpad(@cnt_down, 3, 0);
    else
      set @new_sort_asc = concat(left(_sort_asc, _depth * 4 - 1), '/', lpad(@cnt_up, 3, 0));
      set @new_sort_desc = concat(left(_sort_desc, _depth * 4 - 1), '/', lpad(@cnt_down, 3, 0));
    end if;

    -- Update current node
    update %{table_name}_tree set sort_asc = @new_sort_asc, sort_desc = @new_sort_desc
      where %{column_id} = _id;

    -- Update child tree
    update %{table_name}_tree set
      sort_asc = concat(@new_sort_asc, '/', substring_index(sort_asc, '/', _depth - depth)),
      sort_desc = concat(@new_sort_desc, '/', substring_index(sort_desc, '/', _depth - depth))
    where %{column_id} != _id and path like concat(_path, '/%%');

    set @cnt_up = @cnt_up + 1;
    set @cnt_down = @cnt_down - 1;
  end loop;

  close _descendant;
end $$

delimiter ;
EOS

filename = File.dirname(__FILE__) + '/tree.out.sql';
File.open(filename, 'w') do |f|
  tables.each do |table|
    f.write "#{statement % table}\n\n"
  end
end
puts "Output file: #{filename}"
