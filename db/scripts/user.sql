-- ----------------------------------------------------------------------------
-- Profile User
-- ----------------------------------------------------------------------------

drop view if exists v_user_profiles;
create view v_user_profiles as

select u.username, p.display_name, p.photo_url, u.role_id, r.role
from users u
  inner join user_profiles p on p.username = u.username
  inner join user_roles r on r.role_id = u.role_id;
