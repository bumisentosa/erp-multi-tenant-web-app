-- ----------------------------------------------------------------------------
-- View Project
-- ----------------------------------------------------------------------------

drop view if exists v_projects;
create view v_projects as

select p.project_id,
  p.parent, t.depth, t.path, t.sort_asc, t.sort_desc,
  p.type_id, y.project_type, p.group_id, g.group_name,
  p.project_name,
  p.contract_no_s1, p.contract_no_s2, p.contract_date, p.contract_date_start, p.contract_date_end,
  p.project_description, p.contract_value, p.addendum_value, p.addendum_count,
  p.description,
  p.status_id, case p.status_id when 0 then 'Pengajuan' when 1 then 'Kontrak' when 2 then 'Amandemen/Addendum' end as `status`
from projects as p
  inner join projects_tree t on t.project_id = p.project_id
  inner join projects_types y on y.type_id = p.type_id
  inner join projects_groups g on g.group_id = p.group_id;


-- ----------------------------------------------------------------------------
-- View Lokasi Project
-- ----------------------------------------------------------------------------

drop view if exists v_projects_locations;
create view v_projects_locations as

select pl.project_location_id, 
  pl.project_id, pt.sort_asc as project_sort_asc, pt.sort_desc as project_sort_desc, p.project_name,
  pl.location_id, lt.sort_asc as location_sort_asc, lt.sort_desc as location_sort_desc, l.location_name
from projects_locations pl
  inner join projects p on p.project_id = pl.project_id
  inner join projects_tree pt on pt.project_id = p.project_id
  inner join locations l on l.location_id = pl.location_id
  inner join locations_tree lt on lt.location_id = l.location_id;


-- ----------------------------------------------------------------------------
-- Trigger Project
-- ----------------------------------------------------------------------------

-- Before Update
-- --------------------------------------

delimiter $$
drop trigger if exists projects_bu $$
create trigger projects_bu before update on projects
for each row begin
  -- Status proyek
  if new.project_description is null || new.project_description = '' then
    set new.status_id = 0; -- Pengajuan
  elseif new.addendum_count = 0 then
    set new.status_id = 1; -- Kontrak
  else
    set new.status_id = 2; -- Amandemen/addendum
  end if;
end $$
delimiter ;

-- ----------------------------------------------------------------------------
-- Trigger Addendum
-- ----------------------------------------------------------------------------

-- After Insert
-- --------------------------------------

delimiter $$
drop trigger if exists projects_addendums_ai $$
create trigger projects_addendums_ai after insert on projects_addendums
for each row begin
  -- Nilai addendum
  update projects set
    addendum_count = (
      select count(project_id) from projects_addendums where project_id = new.project_id
    ),
    addendum_value = (
      select addendum_value from projects_addendums where project_id = new.project_id
      order by addendum_date desc limit 1
    )
  where project_id = new.project_id;
end $$
delimiter ;


-- After Update
-- --------------------------------------

delimiter $$
drop trigger if exists projects_addendums_au $$
create trigger projects_addendums_au after update on projects_addendums
for each row begin
  -- Status proyek dan nilai addendum
  if old.project_id != new.project_id then
    update projects set
      addendum_count = (
        select count(project_id) from projects_addendums where project_id = old.project_id
      ),
      addendum_value = (
        select addendum_value from projects_addendums where project_id = old.project_id
        order by addendum_date desc limit 1
      )
    where project_id = old.project_id;
  end if;

  update projects set
    addendum_count = (
      select count(project_id) from projects_addendums where project_id = new.project_id
    ),
    addendum_value = (
      select addendum_value from projects_addendums where project_id = new.project_id
      order by addendum_date desc limit 1
    )
  where project_id = new.project_id;
end $$
delimiter ;


-- After Delete
-- --------------------------------------

delimiter $$
drop trigger if exists projects_addendums_ad $$
create trigger projects_addendums_ad after delete on projects_addendums
for each row begin
  -- Nilai addendum
  update projects set
    addendum_count = (
      select count(project_id) from projects_addendums where project_id = old.project_id
    ),
    addendum_value = (
      select addendum_value from projects_addendums where project_id = old.project_id
      order by addendum_date desc limit 1
    )
  where project_id = old.project_id;
end $$

delimiter ;
