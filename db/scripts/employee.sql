-- ----------------------------------------------------------------------------
-- View Pegawai
-- ----------------------------------------------------------------------------

drop view if exists v_employees;
create view v_employees as

select e.employee_id,
  e.project_id, p.project_name, pt.sort_asc as sort_project,
  e.location_id, l.location_name, lt.sort_asc as sort_location,
  e.position_id as position_id, n.position as position, nt.sort_asc as sort_position,

  e.employee_no, e.employee_name, e.photo_url, e.birth_date, e.birth_place,
  e.gender_id, case e.gender_id when 0 then 'Perempuan' when 1 then 'Laki-Laki' end as gender,
  e.religion_id, r.religion,
  e.address, e.city, e.region, e.home_phone, e.mobile_phone, e.email,

  e.identity_card_no, e.identity_card_expiry_date, e.identity_card_address, e.identity_card_city,
  e.identity_card_region,

  e.marital_status_id, e.marital_status,
  e.spouse_name, e.spouse_birth_date, e.spouse_birth_place, e.spouse_job, e.children_count,

  e.tax_no, e.tax_date,

  e.bank_id as bank_id, b.bank_name, e.bank_account_no,
  e.join_date, e.employment_date, e.is_outer_employee,

  e.is_active, e.retire_date,
  e.retire_type_id, case e.retire_type_id when 1 then 'Resign' when 2 then 'PHK' end as retire_type,
  e.retire_reason, e.severance

from employees e
  left join projects p on p.project_id = e.project_id
  left join projects_tree pt on pt.project_id = p.project_id
  left join locations l on l.location_id = e.location_id
  left join locations_tree lt on lt.location_id = l.location_id
  left join positions n on n.position_id = e.position_id
  left join positions_tree nt on nt.position_id = n.position_id

  left join religions r on r.religion_id = e.religion_id
  left join banks b on b.bank_id = e.bank_id;


-- ----------------------------------------------------------------------------
-- Marital Status
-- ----------------------------------------------------------------------------

delimiter $$
drop function if exists marital_status $$

create function marital_status(marital_status_id int unsigned, children_count int)
returns varchar(5)
begin
  declare _marital_status varchar(5);

  case marital_status_id
  when 1 then
    set _marital_status = 'TK';
  when 2 then
    set _marital_status = concat('K/', children_count);
  else
    set _marital_status = concat('?/', children_count);
  end case;

  return _marital_status;
end $$

delimiter ;

-- ----------------------------------------------------------------------------
-- Trigger Pegawai
-- ----------------------------------------------------------------------------

-- Before Insert
-- --------------------------------------

delimiter $$
drop trigger if exists employees_bi $$
create trigger employees_bi before insert on employees
for each row begin
  -- Status kawin
  set new.children_count = 0;
  set new.marital_status = marital_status(new.marital_status_id, new.children_count);
end $$
delimiter ;


-- Before Update
-- --------------------------------------

delimiter $$
drop trigger if exists employees_bu $$
create trigger employees_bu before update on employees
for each row begin
  -- Status kawin
  if old.marital_status_id != new.marital_status_id || old.children_count != new.children_count then
    set new.marital_status = marital_status(new.marital_status_id, new.children_count);
  end if;

  -- Berhenti -> set is_active = true
  if old.retire_date is null && new.retire_date is not null then
    set new.is_active = 0;
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- Trigger Anak dari Pegawai
-- ----------------------------------------------------------------------------

-- After Insert
-- --------------------------------------

delimiter $$
drop trigger if exists employees_children_ai $$
create trigger employees_children_ai after insert on employees_children
for each row begin
  -- Status kawin
  declare _children_count int;
  select count(employee_id) into _children_count from employees_children
  where employee_id = new.employee_id;

  -- Ini akan men-trigger update employees
  update employees set children_count = _children_count where employee_id = new.employee_id;
end $$
delimiter ;


-- After Update
-- --------------------------------------

delimiter $$
drop trigger if exists employees_children_au $$
create trigger employees_children_au after update on employees_children
for each row begin
  -- Status kawin
  declare _children_count_old, _children_count_new int;

  if old.employee_id != new.employee_id then
    -- Old
    select count(employee_id) into _children_count_old from employees_children
    where employee_id = old.employee_id;
    update employees set children_count = _children_count_old where employee_id = old.employee_id;

    -- New
    select count(employee_id) into _children_count_new from employees_children
    where employee_id = new.employee_id;
    update employees set children_count = _children_count_new where employee_id = new.employee_id;
  end if;
end $$
delimiter ;


-- After Delete
-- --------------------------------------

delimiter $$
drop trigger if exists employees_children_ad $$
create trigger employees_children_ad after delete on employees_children
for each row begin
  -- Status kawin
  declare _children_count int;
  select count(employee_id) into _children_count from employees_children
  where employee_id = old.employee_id;

  -- Ini akan men-trigger update employees
  update employees set children_count = _children_count where employee_id = old.employee_id;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- Trigger Mutasi Proyek Pegawai
-- ----------------------------------------------------------------------------

-- After Insert
-- --------------------------------------

delimiter $$
drop trigger if exists employees_projects_ai $$
create trigger employees_projects_ai after insert on employees_projects
for each row begin
  -- Update project_id pada tabel employees
  declare _last_mutation_date date;

  select mutation_date into _last_mutation_date from employees_projects
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or new.mutation_date > _last_mutation_date then
    update employees set project_id = new.project_id where employee_id = new.employee_id;
  end if;
end $$

delimiter ;


-- After Update
-- --------------------------------------

delimiter $$
drop trigger if exists employees_projects_au $$
create trigger employees_projects_au after update on employees_projects
for each row begin
  -- Update project_id pada tabel employees
  declare _last_old_mutation_date, _last_new_mutation_date date;
  declare _last_old_project_id int unsigned;

  select mutation_date into _last_new_mutation_date from employees_projects
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_new_mutation_date is null or new.mutation_date > _last_new_mutation_date then
    update employees set project_id = new.project_id where employee_id = new.employee_id;
  end if;

  if old.employee_id != new.employee_id then
    select mutation_date, project_id into _last_old_mutation_date, _last_old_project_id
    from employees_projects
    where employee_id = old.employee_id and mutation_id <> new.mutation_id
    order by mutation_date desc limit 1;

    if _last_old_mutation_date is null or new.mutation_date > _last_old_mutation_date then
      update employees set project_id = _last_old_project_id where employee_id = old.employee_id;
    end if;
  end if;
end $$

delimiter ;


-- After Delete
-- --------------------------------------

delimiter $$
drop trigger if exists employees_projects_ad $$
create trigger employees_projects_ad after delete on employees_projects
for each row begin
  -- Update project_id pada tabel employees
  declare _last_mutation_date date;
  declare _last_project_id int unsigned;

  select mutation_date, project_id into _last_mutation_date, _last_project_id
  from employees_projects
  where employee_id = old.employee_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or old.mutation_date > _last_mutation_date then
    update employees set project_id = _last_project_id where employee_id = old.employee_id;
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- Trigger Mutasi Lokasi Pegawai
-- ----------------------------------------------------------------------------

-- After Insert
-- --------------------------------------

delimiter $$
drop trigger if exists employees_locations_ai $$
create trigger employees_locations_ai after insert on employees_locations
for each row begin
  -- Update location_id pada tabel employees
  declare _last_mutation_date date;

  select mutation_date into _last_mutation_date from employees_locations
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or new.mutation_date > _last_mutation_date then
    update employees set location_id = new.location_id where employee_id = new.employee_id;
  end if;
end $$

delimiter ;


-- After Update
-- --------------------------------------

delimiter $$
drop trigger if exists employees_locations_au $$
create trigger employees_locations_au after update on employees_locations
for each row begin
  -- Update location_id pada tabel employees
  declare _last_old_mutation_date, _last_new_mutation_date date;
  declare _last_old_location_id int unsigned;

  select mutation_date into _last_new_mutation_date from employees_locations
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_new_mutation_date is null or new.mutation_date > _last_new_mutation_date then
    update employees set location_id = new.location_id where employee_id = new.employee_id;
  end if;

  if old.employee_id != new.employee_id then
    select mutation_date, location_id into _last_old_mutation_date, _last_old_location_id
    from employees_locations
    where employee_id = old.employee_id and mutation_id <> new.mutation_id
    order by mutation_date desc limit 1;

    if _last_old_mutation_date is null or new.mutation_date > _last_old_mutation_date then
      update employees set location_id = _last_old_location_id where employee_id = old.employee_id;
    end if;
  end if;
end $$

delimiter ;


-- After Delete
-- --------------------------------------

delimiter $$
drop trigger if exists employees_locations_ad $$
create trigger employees_locations_ad after delete on employees_locations
for each row begin
  -- Update location_id pada tabel employees
  declare _last_mutation_date date;
  declare _last_location_id int unsigned;

  select mutation_date, location_id into _last_mutation_date, _last_location_id
  from employees_locations
  where employee_id = old.employee_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or old.mutation_date > _last_mutation_date then
    update employees set location_id = _last_location_id where employee_id = old.employee_id;
  end if;
end $$

delimiter ;


-- ----------------------------------------------------------------------------
-- Trigger Mutasi Jabatan Pegawai
-- ----------------------------------------------------------------------------

-- After Insert
-- --------------------------------------

delimiter $$
drop trigger if exists employees_positions_ai $$
create trigger employees_positions_ai after insert on employees_positions
for each row begin
  -- Update position_id pada tabel employees
  declare _last_mutation_date date;

  select mutation_date into _last_mutation_date from employees_positions
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or new.mutation_date > _last_mutation_date then
    update employees set position_id = new.position_id where employee_id = new.employee_id;
  end if;
end $$

delimiter ;


-- After Update
-- --------------------------------------

delimiter $$
drop trigger if exists employees_positions_au $$
create trigger employees_positions_au after update on employees_positions
for each row begin
  -- Update position_id pada tabel employees
  declare _last_old_mutation_date, _last_new_mutation_date date;
  declare _last_old_position_id int unsigned;

  select mutation_date into _last_new_mutation_date from employees_positions
  where employee_id = new.employee_id and mutation_id <> new.mutation_id
  order by mutation_date desc limit 1;

  if _last_new_mutation_date is null or new.mutation_date > _last_new_mutation_date then
    update employees set position_id = new.position_id where employee_id = new.employee_id;
  end if;

  if old.employee_id != new.employee_id then
    select mutation_date, position_id into _last_old_mutation_date, _last_old_position_id
    from employees_positions
    where employee_id = old.employee_id and mutation_id <> new.mutation_id
    order by mutation_date desc limit 1;

    if _last_old_mutation_date is null or new.mutation_date > _last_old_mutation_date then
      update employees set position_id = _last_old_position_id where employee_id = old.employee_id;
    end if;
  end if;
end $$

delimiter ;


-- After Delete
-- --------------------------------------

delimiter $$
drop trigger if exists employees_positions_ad $$
create trigger employees_positions_ad after delete on employees_positions
for each row begin
  -- Update position_id pada tabel employees
  declare _last_mutation_date date;
  declare _last_position_id int unsigned;

  select mutation_date, position_id into _last_mutation_date, _last_position_id
  from employees_positions
  where employee_id = old.employee_id
  order by mutation_date desc limit 1;

  if _last_mutation_date is null or old.mutation_date > _last_mutation_date then
    update employees set position_id = _last_position_id where employee_id = old.employee_id;
  end if;
end $$

delimiter ;
