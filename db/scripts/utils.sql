-- ----------------------------------------------------------------------------
-- Split String
-- ----------------------------------------------------------------------------

delimiter $$
drop function if exists split_str $$

create function split_str(str varchar(255), delim varchar(12), pos int)
returns varchar(255)
begin
  declare result varchar(255);

  set @piece = substring_index(str, delim, pos);
  set @cut = substring_index(str, delim, pos - if(pos > 0, 1, -1));

  if pos > 0 then
    set result = substr(@piece, length(@cut) + 1);
  else
   set result = left(@piece, length(@piece) - length(@cut));
  end if;
  set result = replace(result, delim, '');
  if result = '' then set result = null; end if;
  return result;
end $$

delimiter ;
