-- ----------------------------------------------------------------------------
-- View Location
-- ----------------------------------------------------------------------------

drop view if exists v_locations;
create view v_locations as

select l.location_id, l.parent, t.depth, t.path, t.sort_asc, t.sort_desc,
  l.location_name, l.address, l.city, l.region, l.country
from locations l
  inner join locations_tree t on l.location_id = t.location_id;
