'use strict';
var shared = require('./karma-shared.conf');

module.exports = function(config) {
  shared(config);
  config.files = shared.files.concat(shared.templates, [
    '../test/client/unit/{controllers,directives,filters,services}/**/*.js'
  ]);
};
