'use strict';
var shared = require('./karma-shared.conf');

module.exports = function(config) {
  shared(config);

  config.set({
    framework: ['ng-scenario'],
    files: ['../test/client/e2e/**/*.js'],
    urlRoot: '/_karma_/',
    proxies: {
      '/': 'http://localhost:9292/'
    }
  });
};
