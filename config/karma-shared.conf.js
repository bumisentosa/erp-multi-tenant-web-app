'use strict';

var shared = function(config) {
  config.set({
    basePath: '../client',
    frameworks: ['jasmine'],

    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['ChromeCanary'],

    reporters: ['progress', 'junit'],
    junitReporter: { outputFile: '../test/client/junit.xml' },
    reportSlowerThan: 500,

    preprocessor: { '**/*.html': ['ng-html2js'] },
    ngHtml2JsPreprocessor: { stripPrefix: 'base/' },

    autoWatch: true,
    colors: true,
    singleRun: false
  });
};

shared.files = [
  // Libs
  'lib/jquery/jquery.js',
  'lib/underscore/underscore.js',
  'lib/angular/angular.js',
  'lib/angular-bootstrap/ui-bootstrap-tpls.js',
  'lib/angular-ui-router/release/angular-ui-router.js',
  'lib/angular-resource/src/resource.js',
  'lib/ng-file-upload/angular-file-upload.js',
  'lib/faker/build/build/faker.js',

  // Test libs
  'lib/angular-mocks/angular-mocks.js',

  // App-specific code
  'app/app.js',
  'app/{controllers,directives,filters,services}/**/*.js',

  // Test helper
  '../test/client/support/**/*.js',
];

shared.templates = [
  'app/directives/templates/**/*.html',
  'app/templates/**/*.html'
];

module.exports = shared;
