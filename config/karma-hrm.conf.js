'use strict';
var shared = require('./karma-shared.conf');

module.exports = function(config) {
  shared(config);
  config.files = shared.files.concat(shared.templates, [
    'app/modules/hrm/**/*.{js,html}',
    '../test/client/unit/modules/hrm/**/*.js'
  ]);
};
