require './server/init'

map('/')    { run App }
map('/hrm') { run HRM::App }
map('/frm') { run FRM::App }
map('/crm') { run CRM::App }
map('/pm')  { run PM::App }
