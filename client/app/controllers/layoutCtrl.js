'use strict';
controllers

.controller('layoutCtrl', function($window, $rootScope, $scope, $state, messageSvc, resource, toolbar) {
  $scope.$state = $state;

  $scope.inViewport = function(viewports) {
    var width = $window.innerWidth;
    if (!angular.isArray(viewports)) viewports = [viewports];
    for (var i = 0; i < viewports.length; i++) {
      var viewport = viewports[i];
      switch (viewport) {
      case 'xs':
        if (width <= 768) return true;
        break;
      case 'sm':
        if (width > 768 && width <= 992) return true;
        break;
      case 'md':
        if (width > 992 && width <= 1200) return true;
        break;
      case 'lg':
        if (width > 1200) return true;
        break;
      }
    }
    return false;
  };

  // Sidebar
  $scope.sidebarOpen = $window.innerWidth > 992;
  $scope.toggleSidebar = function() {
    $scope.sidebarOpen = !$scope.sidebarOpen;
  };

  // Toolbar
  $scope.visibleToolbars = toolbar.visibleToolbars;

  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!angular.equals(toState.name.match(/[a-zA-Z0-9]+/), fromState.name.match(/[a-zA-Z0-9]+/))) toolbar.reset();
    if ($scope.inViewport(['xs', 'sm'])) $scope.sidebarOpen = false;
  });

  // User
  var User = resource('/users/:username/profile', { username: '@username' });
  User.get({ username: 'me' }).then(function(profile) {
    $scope.profile = profile;
  });

  // Loading
  $scope.$watch(
    function() { return messageSvc.isLoading(); },
    function(value) { $scope.isLoading = value; }
  );

  // Message
  $scope.messages = messageSvc.messages;
  $scope.removeAlert = function($index) {
    messageSvc.messages.splice($index, 1);
  };
});
