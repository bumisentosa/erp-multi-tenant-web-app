'use strict';
services

.factory('messageSvc', function($timeout) {
  return {
    requestCount: 0,
    messages: [],
    isLoading: function() {
      return this.requestCount > 0;
    },
    addMessage: function(message, delay) {
      var self = this;
      this.messages.push(message);
      if (delay) {
        $timeout(function() {
          self.messages.splice(self.messages.indexOf(message), 1);
        }, delay);
      }
    }
  };
})

.factory('requestInterceptor', function($q, messageSvc) {
  function errorMessage(rejection) {
    messageSvc.addMessage({
      title: 'Error',
      message: rejection.data || 'yah...!, ada sesuatu yang salah dengan server!',
      type: 'danger'
    }, 10000);
  }
  return {
    request: function(config) {
      messageSvc.requestCount++;
      return config || $q.when(config);
    },
    requestError: function(rejection) {
      messageSvc.requestCount--;
      errorMessage(rejection);
      return $q.reject(rejection);
    },
    response: function(response) {
      messageSvc.requestCount--;
      return response || $q.when(response);
    },
    responseError: function(rejection) {
      messageSvc.requestCount--;
      errorMessage(rejection);
      return $q.reject(rejection);
    }
  };
})

.config(function($httpProvider) {
  $httpProvider.interceptors.push('requestInterceptor');
});
