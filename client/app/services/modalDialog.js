'use strict';
services

.controller('modalDialogCtrl', function($scope, $modalInstance, title, message, item, buttons) {
  $scope.title = title;
  $scope.message = message;
  $scope.item = angular.copy(item);
  $scope.original = item;
  $scope.buttons = buttons;

  $scope.buttonClick = function(button) {
    button.action($modalInstance);
  };

  $scope.ok = function() {
    $modalInstance.close($scope.item);
  };

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
})

.factory('modalDialog', function($modal) {
  var defaultOptions = {
    templateUrl: 'app/templates/modal-dialog.html',
    controller: 'modalDialogCtrl',
    resolve: {}
  };

  return {
    buttons: {
      OK: {
        label: 'OK',
        className: 'btn-primary',
        action: function($modalInstance) { $modalInstance.close(true); }
      },
      YES: {
        label: 'Ya',
        className: 'btn-primary',
        action: function($modalInstance) { $modalInstance.close(true); }
      },
      NO: {
        label: 'Tidak',
        action: function($modalInstance) { $modalInstance.close(false); }
      },
      CANCEL: {
        label: 'Batal',
        action: function($modalInstance) { $modalInstance.dismiss('cancel'); }
      }
    },

    open: function(title, message, buttons, options) {
      if (arguments.length === 2) {
        if (angular.isObject(message)) {
          options = message;
          message = '';
          buttons = [];
        } else {
          buttons = [this.buttons.OK];
        }
      }
      buttons = buttons || [];
      options = angular.extend({}, defaultOptions, options);

      options.resolve.title = function() { return title; };
      options.resolve.message = function() { return message; };
      options.resolve.buttons = function() { return buttons; };
      options.resolve.item = options.resolve.item || function() { return {}; };

      return $modal.open(options);
    }
  };
});
