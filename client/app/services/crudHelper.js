'use strict';
services

.factory('crudHelper', function($timeout, modalDialog) {
  return function(Model, options) {
    options = angular.extend({
      fieldId:    'id',
      fieldLabel: 'name',
      queryParams: function() { return {}; },
    }, options);

    var crudHelper = {
      Model: Model,
      data: [],
      selectedItems: [],

      refresh: function() {
        return Model.query(options.queryParams()).then(function(response) {
          var isArray = angular.isArray(response);
          var isWrap = response && angular.isArray(response.data);
          var data = isArray ? response : isWrap ? response.data : [];
          if (angular.equals(crudHelper.data, data)) return;

          // Store old selection
          var oldSelections = crudHelper.selectedItems.map(function(item) {
            return item[options.fieldId];
          });

          // Data
          crudHelper.data.length = 0;
          crudHelper.data.push.apply(crudHelper.data, data);
          if (isWrap) {
            angular.forEach(response, function(value, key) {
              if (key !== 'data') angular.extend(crudHelper[key], value);
            });
          }

          // Re-select
          crudHelper.selectedItems.length = 0;
          if (oldSelections.length) {
            angular.forEach(crudHelper.data, function(item, index) {
              if (oldSelections.indexOf(item[options.fieldId]) !== -1) {
                crudHelper.selectedItems.push(item);
              }
            });
          }
        });
      },

      showDialog: function(item, dialogOptions) {
        dialogOptions = dialogOptions || {};
        dialogOptions.resolve = dialogOptions.resolve || {};
        dialogOptions.resolve.item = function() { return item; };
        return modalDialog.open(dialogOptions.title, dialogOptions).result;
      },

      updateItem: function(saveNew, item, original) {
        var originalCopy = angular.copy(original);
        // Update data sesegera mungkin tanpa harus menunggu server
        if (saveNew) {
          item = Model.create(item);
          original = new Model(item);
          crudHelper.data.push(original);
        } else {
          item = new Model(item);
          angular.copy(item, original);
        }

        return item.save().then(function(newItem) {
          // Data disimpan, update kembali data agar sesuai dengan server
          angular.copy(newItem, original);
        }, function(error) {
          // Terdapat error, batalkan update data
          if (saveNew) {
            crudHelper.data.pop();
          } else {
            angular.copy(originalCopy, original);
          }
        });
      },

      confirmRemove: function() {
        var message = 'Yakin menghapus data ini?';
        if (crudHelper.selectedItems.length) {
          message = (crudHelper.selectedItems.length === 1) ?
            'Yakin menghapus "' + crudHelper.selectedItems[0][options.fieldLabel] + '"?' :
            'Yakin menghapus (' + crudHelper.selectedItems.length + ') item yang dipilih?';
        }

        return modalDialog.open('Konfirmasi Hapus', message, [
          angular.extend({}, modalDialog.buttons.OK, { className: 'btn-danger' }),
          modalDialog.buttons.CANCEL
        ]).result;
      },

      removeItem: function() {
        var ids = [];
        angular.forEach(crudHelper.selectedItems, function(item) {
          ids.push(item[options.fieldId]);
          crudHelper.data.splice(crudHelper.data.indexOf(item), 1); // Hapus item
        });
        crudHelper.selectedItems.length = 0; // Update selected items

        // Hapus data. Jika terdapat error, hanya refresh data
        return Model.remove({ id: ids }).then(null, crudHelper.refresh);
      },

      toggleSelection: function(selectedItem) {
        if (angular.isObject(selectedItem)) { // Single selection
          var selectedIndex = crudHelper.selectedItems.indexOf(selectedItem);
          if (selectedIndex >= 0) {
            crudHelper.selectedItems.splice(selectedIndex, 1); // Remove selection
          } else {
            crudHelper.selectedItems.push(selectedItem); // Add selection
          }
        } else { // All selection
          if (crudHelper.selectedItems.length !== crudHelper.data.length) {
            crudHelper.selectedItems.length = 0;
            crudHelper.selectedItems.push.apply(crudHelper.selectedItems, crudHelper.data);
          } else {
            crudHelper.selectedItems.length = 0;
          }
        }
      }
    };

    return crudHelper;
  };
});
