'use strict';
filters

.filter('join', function() {
  return function(input, separator1, separator2) {
    if (!angular.isArray(input)) return input;

    separator1 = separator1 || ', ';
    separator2 = separator2 || separator1;

    var str = '';
    for (var i = 0, length = input.length; i < length; i++) {
      var item = input[i];
      if (!item) continue;
      if (str !== '') {
        str += (i === length - 1 ? separator2 : separator1);
      }
      str += item;
    }
    return str;
  };
});
