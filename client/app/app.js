// ============================================================================
//
// ENTERPRISE RESOURCE PLANNING
//
// ============================================================================

/* jshint unused:false, bitwise:false */
'use strict';

// Module definition
var services = angular.module('app.services', ['ui.bootstrap', 'sg-resource']);
var directives = angular.module('app.directives', []);
var filters = angular.module('app.filters', []);
var controllers = angular.module('app.controllers', ['ui.router', 'app.services', 'app.directives']);

// HACK: Angular tidak punya event on-blur, jadi dibuatkan disini
directives.directive('ngBlur', function() {
  return function (scope, elem, attrs) {
    elem.bind('blur', function() {
      scope.$apply(attrs.ngBlur);
    });
  };
});

// HACK: Repeat string
String.prototype.repeat = String.prototype.repeat || function(count) {
  if (count < 1) return '';
  var result = '', pattern = this.valueOf();
  while (count > 0) {
    if (count & 1) result += pattern;
    count >>= 1, pattern += pattern;
  }
  return result;
};

// HACK: Find array by key
Array.prototype.get = function(key, value) {
  for (var i = 0, len = this.length; i < len; i++) {
    if (typeof this[i] !== 'object') continue;
    if (this[i][key] === value) return this[i];
  }
  return null;
};

// HACK: Number to roman
Number.prototype.toRoman = Number.prototype.toRoman || function() {
  var n = Math.floor(this);
  if (n < 1 || n > 4000) return '';

  var d = [1000, 9000, 500, 400,  100, 90,   50,  40,   10,  9,    5,   4,    1];
  var r = ['M',  'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

  var ret = '';
  for (var i = 0, len = d.length; i < len; i++) {
    while (n >= d[i]) {
      n -= d[i];
      ret += r[i];
    }
  }
  return ret;
};

// HACK: Pad number
Number.lpad = function(number, width, z) {
  z = z || '0';
  number = number + '';
  return number.length >= width ? number : new Array(width - number.length + 1).join(z) + number;
};
