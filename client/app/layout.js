'use strict';

// Sidebar full height
$(function() {
  var $targetHeight = $('html, body, .sidebar');
  var $window = $(window);
  var $wrapper = $('.wrapper');

  var resizeTimer = null;
  $window.resize(function() {
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(fixSidebarHeight, 100);
  });

  function fixSidebarHeight() {
    resizeTimer = null;
    var width = $window.width();
    if (width >= 992) {
      var height = $window.height();
      $wrapper.css('min-height', height + 'px');
      var contentHeight = $wrapper.height();
      var maxHeight = (contentHeight > height) ? contentHeight : height;
      $targetHeight.css('min-height', maxHeight + 'px');
    } else {
      $targetHeight.css('min-height', 'initial');
    }
  }

  fixSidebarHeight();
});

// Shrink header on scroll
$(function() {
  var minHeaderHeight = 50;
  var maxHeaderHeight = 155;

  var $window = $(window);
  var $navbar = $('.navbar');
  var $navbarBg = $('.navbar-bg');
  var $loadingIndicator = $('.loading-indicator');

  var scrollTimer = null;
  $window.scroll(function() {
    if (scrollTimer) clearTimeout(scrollTimer);
    scrollTimer = setTimeout(handleScroll, 10);
  });

  function handleScroll() {
    scrollTimer = null;
    var top = $window.scrollTop();
    var height = maxHeaderHeight - top;
    var diff = maxHeaderHeight - minHeaderHeight;
    if (top < diff) {
      $navbar.css('height', height + 'px');
      $loadingIndicator.css('top', (height - 2) + 'px');
      $navbarBg.css('opacity', (diff - top) / diff);
    } else {
      $navbar.css('height', minHeaderHeight + 'px');
      $loadingIndicator.css('top', (minHeaderHeight - 2) + 'px');
      $navbarBg.css('opacity', 0);
    }
    $navbar.toggleClass('shrink', top > minHeaderHeight);
  }
});
