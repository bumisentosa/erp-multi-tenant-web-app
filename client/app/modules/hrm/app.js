// ============================================================================
//
// Human Resource Management
//
// ============================================================================

/* jshint unused:false */
'use strict';

// Module definition
var hrmConstants = angular.module('hrm.constants', []);
var hrmFilters = angular.module('hrm.filters', ['app.filters']);
var hrmControllers = angular.module('hrm.controllers', ['angularFileUpload', 'app.controllers', 'app.directives', 'hrm.constants']);
var hrmRoutes = angular.module('hrm.routes', ['ui.router', 'hrm.controllers', 'hrm.constants']);

var hrm = angular.module('hrm', [
  'ngAnimate',
  'ngTouch',
  'ui.bootstrap',
  'hrm.constants',
  'hrm.filters',
  'hrm.controllers',
  'hrm.routes'
]);

hrm.run(function($rootScope, $state, $stateParams, TEMPLATE_DIR) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  $rootScope.TEMPLATE_DIR = TEMPLATE_DIR;
  $rootScope.doneLoading = true;

  $rootScope.$on('$stateChangeSuccess', function() {
    // HACK: Fix scrolling issue
    $('html, body').animate({ scrollTop: 0 }, 200);

    // HACK: Trigger resize event
    $(window).trigger('resize');
  });
});

// Index route
hrmRoutes.config(function($urlRouterProvider, $stateProvider, TEMPLATE_DIR) {
  $urlRouterProvider.otherwise('/');

  $stateProvider.state('index', {
    url: '/',
    controller: 'indexCtrl',
    templateUrl: TEMPLATE_DIR + '/index.html'
  });
});
