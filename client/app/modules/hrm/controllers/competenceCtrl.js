'use strict';
hrmControllers

/**
 * Controller kompetensi pegawai.
 */
.controller('competenceCtrl', function($scope, $state, crudHelper, toolbar, resource, messageSvc, TEMPLATE_DIR) {
  var DIALOG_TPL   = TEMPLATE_DIR + '/competence/item.html';
  var TITLE        = $state.current.data.title;
  var RESOURCE_URL = '/hrm/competences/:id';
  var FIELD_ID     = 'competence_id';
  var FIELD_LABEL  = 'competence_name';

  toolbar.init();

  var Competence = resource(RESOURCE_URL, { id: '@' + FIELD_ID });
  var helper = crudHelper(Competence, { fieldId: FIELD_ID, fieldLabel: FIELD_LABEL });
  helper.refresh();

  $scope.data = helper.data;
  $scope.selectedItems = helper.selectedItems;
  $scope.toggleSelection = helper.toggleSelection;
  $scope.filterOptions = toolbar.filterOptions;

  $scope.edit = function(item) {
    var dialogOptions = {
      title: (item.isNew() ? 'Tambah ' : 'Ubah ') + TITLE,
      templateUrl: DIALOG_TPL
    };
    helper.showDialog(item, dialogOptions).then(function(updatedItem) {
      return helper.updateItem(item.isNew(), updatedItem, item);
    });
  };

  $scope.$on('toolbar-main-click', function(event, key, options) {
    switch (key) {
    case 'add':
    case 'edit':
      var item = (key === 'add') ? Competence.create() : $scope.selectedItems[0];
      $scope.edit(item);
      break;
    case 'remove':
      helper.confirmRemove().then(helper.removeItem);
      break;
    case 'show-search':
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_SEARCH);
      break;
    case 'refresh':
      toolbar.clearFilters();
      helper.refresh();
      break;
    case 'print':
      messageSvc.addMessage({
        title:   'Under construction!',
        message: 'Bagian ini masih sedang dalam proses pengerjaan',
        type:    'info'
      }, 5000);
      break;
    }
  });

  $scope.$on('toolbar-search-click', function(event, key, options) {
    switch (key) {
    case 'back':
      toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
      break;
    }
  });

  $scope.$watch(function() { return $scope.selectedItems; }, function(selectedItems) {
    var numItems = selectedItems.length;
    if (numItems === 1) {
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
      toolbar.setMainButtons(toolbar.getDefaultSelectButtons());
    } else if (numItems > 1) {
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
      toolbar.setMainButtons(toolbar.getDefaultSelectsButtons());
    } else {
      toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
      toolbar.setMainButtons(toolbar.getDefaultMainButtons());
    }
  }, true);

});
