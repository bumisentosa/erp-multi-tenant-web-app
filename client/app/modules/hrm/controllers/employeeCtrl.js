'use strict';
hrmControllers

/**
 * Controller pegawai.
 */
.controller('employeeCtrl', function($scope, $state, $window, crudHelper, toolbar, resource, messageSvc, TEMPLATE_DIR) {
  var RESOURCE_URL = '/hrm/employees/:id';
  var FIELD_ID     = 'employee_id';
  var FIELD_LABEL  = 'employee_name';

  toolbar.init();

  // Employee
  var Employee = resource(RESOURCE_URL, { id: '@' + FIELD_ID });
  var helper = crudHelper(Employee, { fieldId: FIELD_ID, fieldLabel: FIELD_LABEL,
    queryParams: function() {
      var params = {};
      if ($scope.filterOptions.projectId) params.project_id = $scope.filterOptions.projectId;
      return params;
    }
  });

  // Master data
  $scope.master = resource(null, null, {
    queryProjects: { method: 'GET', url: '/hrm/projects', isArray: true },
    queryLocations: { method: 'GET', url: '/hrm/locations', isArray: true },
    queryPositions: { method: 'GET', url: '/hrm/positions', isArray: true },
    queryDriverLicenses: { method: 'GET', url: '/hrm/driver-licenses', isArray: true },
    queryReligions: { method: 'GET', url: '/hrm/religions', isArray: true },
    queryEducationsLevels: { method: 'GET', url: '/hrm/educations/levels', isArray: true },
    queryCompetences: { method: 'GET', url: '/hrm/competences', isArray: true },
    querySocialSecurity: { method: 'GET', url: '/hrm/social-security', isArray: true },
    queryBanks: { method: 'GET', url: '/hrm/banks', isArray: true },
    queryLettersTypes: { method: 'GET', url: '/hrm/letters/types', isArray: true }
  });

  $scope.helper = helper;
  $scope.data = helper.data;
  $scope.selectedItems = helper.selectedItems;
  $scope.toggleSelection = helper.toggleSelection;
  $scope.filterOptions = toolbar.filterOptions;

  $scope.master.queryProjects().then(function(response) {
    $scope.projects = response;
  });

  $scope.refresh = function() {
    toolbar.clearFilters();
    helper.refresh();
  };

  $scope.edit = function(item) {
    if (item && item.employee_id) {
      $state.transitionTo('employee.edit', { employeeId: item.employee_id });
    } else {
      $state.transitionTo('employee.add');
    }
  };

  $scope.updateToolbars = function() {
    if ($state.current.name === 'employee.list') {
      var numItems = $scope.selectedItems.length;
      if (numItems === 1) {
        toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
        toolbar.setMainButtons(toolbar.getDefaultSelectButtons());
      } else if (numItems > 1) {
        toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
        toolbar.setMainButtons(toolbar.getDefaultSelectsButtons());
      } else {
        toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
        toolbar.setMainButtons(toolbar.getDefaultMainButtons());
      }
    } else if ($state.current.name === 'employee.add' || $state.current.name === 'employee.edit') {
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
      toolbar.setMainButtons(toolbar.getDefaultItemButtons());
    }
  };

  $scope.$watch(function() { return $scope.selectedItems; }, $scope.updateToolbars, true);

  $scope.$on('toolbar-main-click', function(event, key, options) {
    switch (key) {
    case 'add':
    case 'edit':
      var item = (key === 'add') ? Employee.create() : angular.copy($scope.selectedItems[0]);
      $scope.edit(item);
      break;
    case 'back':
      $window.history.back();
      break;
    case 'remove':
      helper.confirmRemove().then(helper.removeItem);
      break;
    case 'show-search':
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_SEARCH);
      break;
    case 'refresh':
      $scope.refresh();
      break;
    case 'print':
      messageSvc.addMessage({
        title:   'Under construction!',
        message: 'Bagian ini masih sedang dalam proses pengerjaan',
        type:    'info'
      }, 5000);
      break;
    }
  });

  $scope.$on('toolbar-search-click', function(event, key, options) {
    switch (key) {
    case 'back':
      toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
      break;
    }
  });
})

/**
 * Controller daftar pegawai.
 */
.controller('employeeListCtrl', function($scope) {
  $scope.$parent.selectedItems.length = 0;
  $scope.$parent.updateToolbars();
  $scope.$parent.helper.refresh();
})

/**
 * Controller item data pegawai.
 */
.controller('employeeItemCtrl', function($scope, $state, $stateParams, $upload, $window, modalDialog, resource) {
  function getEmptyItem(key) {
    return {};
  }

  var ITEMS = ['projects', 'locations', 'positions', 'driver_licenses',
               'children', 'educations', 'educations_non_formal',
               'experiences', 'organizations', 'competences', 'social_security',
               'letters'];

  var parent = $scope.$parent;
  parent.updateToolbars();

  // Master data
  parent.locations || parent.master.queryLocations().then(function(response) {
    parent.locations = response;
  });
  parent.positions || parent.master.queryPositions().then(function(response) {
    parent.positions = response;
  });
  parent.driverLicenses || parent.master.queryDriverLicenses().then(function(response) {
    parent.driverLicenses = response;
  });
  parent.religions || parent.master.queryReligions().then(function(response) {
    parent.religions = response;
  });
  parent.educationsLevels || parent.master.queryEducationsLevels().then(function(response) {
    parent.educationsLevels = response;
  });
  parent.competences || parent.master.queryCompetences().then(function(response) {
    parent.competences = response;
  });
  parent.socialSecurity || parent.master.querySocialSecurity().then(function(response) {
    parent.socialSecurity = response;
  });
  parent.banks || parent.master.queryBanks().then(function(response) {
    parent.banks = response;
  });
  parent.lettersTypes || parent.master.queryLettersTypes().then(function(response) {
    parent.lettersTypes = response;
  });
  parent.genders = [{ gender_id: true, gender: 'Laki-Laki' }, { gender_id: false, gender: 'Perempuan' }];

  var helper = parent.helper;
  var Employee = helper.Model;

  $scope.addItem = function(key) {
    $scope.item[key].push(getEmptyItem(key));
  };

  $scope.removeItem = function(key, index) {
    $scope.item[key].splice(index, 1);
    if ($scope.item[key].length === 0) {
      $scope.item[key].push(getEmptyItem(key));
    }
  };

  $scope.removePhoto = function() {
    $scope.item.photo_url = null;
    $scope.item.photos = null;
    $scope.item.photo_changed = true;
  };

  $scope.resetPhoto = function() {
    $scope.item.photo_url = $scope.item.photo_url_old;
    $scope.item.photos = [$scope.item.photo_url];
    $scope.item.photo_changed = false;
  };

  $scope.onPhotoChanged = function() {
    $scope.item.photo_changed = true;
  };

  $scope.save = function() {
    $scope.employee_form.$valid && $scope.item.save().then(function(response) {
      if ($scope.item.photo_changed && $scope.item.photos) {
        $upload.upload({
          url: '/hrm/employees/' + response.employee_id + '/photo',
          file: $scope.item.photos[0]
        });
      }
      $state.transitionTo('employee.list');
    });
  };

  $scope.cancel = function() {
    $window.history.back();
  };

  if ($stateParams.employeeId) {
    Employee.get({ id: $stateParams.employeeId }).then(function(response) {
      $scope.item = response;
      for (var i = 0, length = ITEMS.length; i < length; i++) {
        if (!$scope.item[ITEMS[i]].length) {
          $scope.item[ITEMS[i]].push(getEmptyItem(ITEMS[i]));
        }
      }
      // photo
      $scope.item.photo_url_old = $scope.item.photo_url;
      $scope.item.photos = [$scope.item.photo_url];
    });
  } else {
    $scope.item = Employee.create();
    for (var i = 0, length = ITEMS.length; i < length; i++) {
      $scope.item[ITEMS[i]] = [getEmptyItem(ITEMS[i])];
    }
  }

  $scope.$on('toolbar-main-click', function(event, key, options) {
    switch (key) {
    case 'save':
      $scope.$broadcast('save-employee');
      break;
    }
  });
});
