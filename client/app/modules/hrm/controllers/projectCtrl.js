'use strict';
hrmControllers

/**
 * Controller proyek.
 */
.controller('projectCtrl', function($scope, $state, $window, crudHelper, toolbar, resource, messageSvc, TEMPLATE_DIR) {
  var RESOURCE_URL = '/hrm/projects/:id';
  var FIELD_ID     = 'project_id';
  var FIELD_LABEL  = 'project_name';

  toolbar.init();

  // Project
  var Project = resource(RESOURCE_URL, { id: '@' + FIELD_ID });
  var helper = crudHelper(Project, { fieldId: FIELD_ID, fieldLabel: FIELD_LABEL });

  // Master data
  $scope.master = resource(null, null, {
    queryTypes: { method: 'GET', url: '/hrm/projects/types', isArray: true },
    queryGroup: { method: 'GET', url: '/hrm/projects/groups', isArray: true },
    queryLocations: { method: 'GET', url: '/hrm/locations', isArray: true }
  });

  $scope.master.queryGroup().then(function(response) { $scope.groups = response; });

  $scope.helper = helper;
  $scope.data = helper.data;
  $scope.dataGroup = [];
  $scope.selectedItems = helper.selectedItems;
  $scope.toggleSelection = helper.toggleSelection;
  $scope.filterOptions = toolbar.filterOptions;
  $scope.hiddenItems = [];
  $scope.hiddenGroups = [];

  $scope.edit = function(item) {
    if (item && item.project_id) {
      $state.transitionTo('project.edit', { projectId: item.project_id });
    } else {
      $state.transitionTo('project.add');
    }
  };

  $scope.updateToolbars = function() {
    if ($state.current.name === 'project.list') {
      var numItems = $scope.selectedItems.length;
      if (numItems === 1) {
        toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
        toolbar.setMainButtons(toolbar.getDefaultSelectButtons());
      } else if (numItems > 1) {
        toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
        toolbar.setMainButtons(toolbar.getDefaultSelectsButtons());
      } else {
        toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
        toolbar.setMainButtons(toolbar.getDefaultMainButtons());
      }
    } else if ($state.current.name === 'project.add' || $state.current.name === 'project.edit') {
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_MAIN);
      toolbar.setMainButtons(toolbar.getDefaultItemButtons());
    }
  };

  $scope.updateDataGroup = function() {
    $scope.dataGroup.length = 0;
    if (!angular.isArray($scope.data) || $scope.data.length === 0) return;
    var lastGroup = 0;
    angular.forEach($scope.data, function(item) {
      if (lastGroup !== item.group_id) {
        var group;
        if (angular.isArray($scope.groups)) {
          group = $scope.groups.get('group_id', item.group_id);
        }
        if (!group) {
          group = { group_name: 'Unknown Group' };
        }
        group.isGroup = true;
        $scope.dataGroup.push(group);
        lastGroup = item.group_id;
      }
      $scope.dataGroup.push(item);
    });
  };

  $scope.showHideChildren = function(group) {
    // Groups
    var groupIdx = $scope.hiddenGroups.indexOf(group.group_id);
    if (groupIdx !== -1) {
      $scope.hiddenGroups.splice(groupIdx, 1);
    } else {
      $scope.hiddenGroups.push(group.group_id);
    }

    // Items
    for (var i = 0, length = $scope.data.length; i < length; i++) {
      var item = $scope.data[i];
      if (group.group_id !== item.group_id) continue;
      var idx = $scope.hiddenItems.indexOf(item.project_id);
      if (idx !== -1) {
        $scope.hiddenItems.splice(idx, 1);
      } else {
        $scope.hiddenItems.push(item.project_id);
      }
    }
  };

  $scope.$watch(function() { return $scope.groups; }, $scope.updateDataGroup, true);
  $scope.$watch(function() { return $scope.data; }, $scope.updateDataGroup, true);
  $scope.$watch(function() { return $scope.selectedItems; }, $scope.updateToolbars, true);

  $scope.$on('toolbar-main-click', function(event, key, options) {
    switch (key) {
    case 'add':
    case 'edit':
      var item = (key === 'add') ? Project.create() : angular.copy($scope.selectedItems[0]);
      $scope.edit(item);
      break;
    case 'back':
      $window.history.back();
      break;
    case 'remove':
      helper.confirmRemove().then(helper.removeItem);
      break;
    case 'show-search':
      toolbar.setVisibleToolbars(toolbar.TOOLBAR_SEARCH);
      break;
    case 'refresh':
      toolbar.clearFilters();
      helper.refresh();
      break;
    case 'print':
      messageSvc.addMessage({
        title:   'Under construction!',
        message: 'Bagian ini masih sedang dalam proses pengerjaan',
        type:    'info'
      }, 5000);
      break;
    }
  });

  $scope.$on('toolbar-search-click', function(event, key, options) {
    switch (key) {
    case 'back':
      toolbar.setVisibleToolbars([toolbar.TOOLBAR_MAIN, toolbar.TOOLBAR_PAGES]);
      break;
    }
  });
})

/**
 * Controller daftar proyek.
 */
.controller('projectListCtrl', function($scope) {
  $scope.$parent.selectedItems.length = 0;
  $scope.$parent.updateToolbars();
  $scope.$parent.helper.refresh();
})

/**
 * Controller item data proyek.
 */
.controller('projectItemCtrl', function($scope, $state, $stateParams, $window, modalDialog, resource) {
  function getEmptyItem(key) {
    var today = new Date();
    var todayStr = today.toISOString().substr(0, 10);
    switch (key) {
      case 'locations':
        return {};
      case 'addendums':
        return { addendum_value: 0, addendum_date: todayStr, addendum_date_end: todayStr };
    }
  }

  var ITEMS = ['locations', 'addendums'];
  var parent = $scope.$parent;
  parent.updateToolbars();

  parent.types || parent.master.queryTypes().then(function(response) {
    parent.types = response;
  });
  parent.locations || parent.master.queryLocations().then(function(response) {
    parent.locations = response;
  });

  var helper = parent.helper;
  var Project = helper.Model;

  $scope.addItem = function(key) {
    $scope.item[key].push(getEmptyItem(key));
  };

  $scope.removeItem = function(key, index) {
    $scope.item[key].splice(index, 1);
    if ($scope.item[key].length === 0) {
      $scope.item[key].push(getEmptyItem(key));
    }
  };

  $scope.save = function() {
    $scope.project_form.$valid && $scope.item.save().then(function(response) {
      $state.transitionTo('project.list');
    });
  };

  $scope.cancel = function() {
    $window.history.back();
  };

  if ($stateParams.projectId) {
    Project.get({ id: $stateParams.projectId }).then(function(response) {
      $scope.item = response;
      for (var i = 0, length = ITEMS.length; i < length; i++) {
        if (!$scope.item[ITEMS[i]].length) {
          $scope.item[ITEMS[i]].push(getEmptyItem(ITEMS[i]));
        }
      }
    });
  } else {
    $scope.item = Project.create();
    for (var i = 0, length = ITEMS.length; i < length; i++) {
      $scope.item[ITEMS[i]] = [getEmptyItem(ITEMS[i])];
    }
  }

  // Parent
  $scope.$watch(function() { return $scope.item ? $scope.item.group_id : null; },
    function(value) {
      if (!value) return;
      Project.query({ group_id: value }).then(function(response) {
        $scope.parents = response;
      });
    }, true);

  $scope.$on('toolbar-main-click', function(event, key, options) {
    switch (key) {
    case 'save':
      $scope.$broadcast('save-project');
      break;
    }
  });
});
