'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('letterType', {
      url: '/kategori-surat',
      controller: 'letterTypeCtrl',
      templateUrl: TEMPLATE_DIR + '/letter-type/list.html',
      data: { title: 'Data Kategori Surat' }
    });

});
