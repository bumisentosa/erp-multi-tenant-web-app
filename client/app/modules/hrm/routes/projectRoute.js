'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('project', {
      url: '/proyek',
      controller: 'projectCtrl',
      templateUrl: TEMPLATE_DIR + '/project/project.html',
      abstract: true,
      data: { title: 'Data Proyek' }
    })

    .state('project.list', {
      url: '',
      controller: 'projectListCtrl',
      templateUrl: TEMPLATE_DIR + '/project/list.html'
    })

    .state('project.add', {
      url: '/tambah',
      controller: 'projectItemCtrl',
      templateUrl: TEMPLATE_DIR + '/project/item.html',
      data: { title: 'Tambah Data Proyek' }
    })

    .state('project.edit', {
      url: '/ubah/:projectId',
      controller: 'projectItemCtrl',
      templateUrl: TEMPLATE_DIR + '/project/item.html',
      data: { title: 'Ubah Data Proyek' }
    });

});
