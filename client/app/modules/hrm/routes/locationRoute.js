'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('location', {
      url: '/lokasi',
      controller: 'locationCtrl',
      templateUrl: TEMPLATE_DIR + '/location/list.html',
      data: { title: 'Data Lokasi' }
    });

});
