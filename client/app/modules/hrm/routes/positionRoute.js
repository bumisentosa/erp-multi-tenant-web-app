'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('position', {
      url: '/jabatan',
      controller: 'positionCtrl',
      templateUrl: TEMPLATE_DIR + '/position/list.html',
      data: { title: 'Data Jabatan' }
    });

});
