'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('projectType', {
      url: '/tipe-proyek',
      controller: 'projectTypeCtrl',
      templateUrl: TEMPLATE_DIR + '/project-type/list.html',
      data: { title: 'Data Tipe Proyek' }
    });

});
