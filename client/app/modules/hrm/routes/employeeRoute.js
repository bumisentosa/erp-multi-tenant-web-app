'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('employee', {
      url: '/pegawai',
      controller: 'employeeCtrl',
      templateUrl: TEMPLATE_DIR + '/employee/employee.html',
      abstract: true,
      data: { title: 'Data Pegawai' }
    })

    .state('employee.list', {
      url: '',
      controller: 'employeeListCtrl',
      templateUrl: TEMPLATE_DIR + '/employee/list.html'
    })

    .state('employee.add', {
      url: '/tambah',
      controller: 'employeeItemCtrl',
      templateUrl: TEMPLATE_DIR + '/employee/item.html',
      data: { title: 'Tambah Data Pegawai' }
    })

    .state('employee.edit', {
      url: '/ubah/:employeeId',
      controller: 'employeeItemCtrl',
      templateUrl: TEMPLATE_DIR + '/employee/item.html',
      data: { title: 'Ubah Data Pegawai' }
    });

});
