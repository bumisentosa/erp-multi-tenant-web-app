'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('bank', {
      url: '/bank',
      controller: 'bankCtrl',
      templateUrl: TEMPLATE_DIR + '/bank/list.html',
      data: { title: 'Data Bank' }
    });

});
