'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('projectGroup', {
      url: '/kelompok-proyek',
      controller: 'projectGroupCtrl',
      templateUrl: TEMPLATE_DIR + '/project-group/list.html',
      data: { title: 'Data Kelompok Proyek' }
    });

});
