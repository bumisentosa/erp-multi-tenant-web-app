'use strict';
hrmRoutes

.config(function($stateProvider, TEMPLATE_DIR) {
  $stateProvider

    .state('competence', {
      url: '/kompetensi',
      controller: 'competenceCtrl',
      templateUrl: TEMPLATE_DIR + '/competence/list.html',
      data: { title: 'Data Kompetensi Pegawai' }
    });

});
