'use strict';
directives

.directive('formGroup', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      size:         '@',
      removeItem:   '&onRemove',
      groupLabel:   '@label',
      actionInline: '@',
      actionGap:    '='
    },
    transclude: true,
    templateUrl: 'app/directives/templates/form-group.html',

    link: function(scope, element, attrs) {
      if (attrs.label === undefined) scope.groupLabel = undefined;
      var labelClass = '', contentClass = '';
      if (scope.size) {
        var sizes = scope.size.split(' ');
        for (var i = 0, len = sizes.length; i < len; i++) {
          var s = sizes[i];
          var n = Number.parseInt(s.substr(3));
          labelClass += 'col-' + s.substr(0, 3) + (12 - n) + ' ';
          contentClass += 'col-' + s + ' ';
        }
      } else {
        labelClass = 'col-lg-12';
        contentClass = 'col-lg-12';
      }
      scope.groupLabelClass = labelClass.trim();
      scope.groupContentClass = contentClass.trim();
      scope.hasDeleteAction = !!attrs.onRemove;

      switch (scope.actionInline) {
      case 'lg':
        scope.removeInlineVisibility = 'visible-lg';
        scope.removeFloatVisibility = 'hidden-lg';
        break;
      case 'md':
        scope.removeInlineVisibility = 'visible-md visible-lg';
        scope.removeFloatVisibility = 'hidden-md hidden-lg';
        break;
      case 'sm':
        scope.removeInlineVisibility = 'hidden-xs';
        scope.removeFloatVisibility = 'visible-xs';
        break;
      case 'xs':
        scope.removeInlineVisibility = ''; // visible
        scope.removeFloatVisibility = 'hidden';
        break;
      default:
        scope.removeInlineVisibility = 'hidden';
        scope.removeFloatVibilityis = ''; // visible
        break;
      }

      scope.actionGapStyle = scope.actionGap ? 'padding-right: 32px' : '';
    }
  };
})

.directive('formItem', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: { label: '@', labelInline: '@', size: '@', type: '@' },
    transclude: true,
    templateUrl: 'app/directives/templates/form-item.html',

    link: function(scope, element, attrs) {
      var className = '';
      if (scope.size) {
        className = (' ' + scope.size).replace(/ /g, ' col-').trim();
      } else {
        className = 'col-lg-12';
      }
      className += scope.type ? ' col-' + scope.type : '';
      scope.className = className.trim();
    }
  };
});
