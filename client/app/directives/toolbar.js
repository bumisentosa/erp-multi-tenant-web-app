'use strict';
directives

.factory('toolbar', function() {
  return {
    TOOLBAR_MAIN: 'main',
    TOOLBAR_SEARCH: 'search',
    TOOLBAR_PAGES: 'pages',

    BUTTON_BACK: 'back',
    BUTTON_ADD: 'add',
    BUTTON_EDIT: 'edit',
    BUTTON_SAVE: 'save',
    BUTTON_REMOVE: 'remove',
    BUTTON_SHOW_SEARCH: 'show-search',
    BUTTON_SEARCH: 'search',
    BUTTON_REFRESH: 'refresh',
    BUTTON_PRINT: 'print',
    BUTTON_PREV_PAGE: 'prev-page',
    BUTTON_PAGE: 'page',
    BUTTON_NEXT_PAGE: 'next-page',

    visibleToolbars: [],
    buttons: { main: [], search: [], pages: [] },
    filterOptions: { filterText: '' },
    currentPage: 1,
    pageCount: 1,
    pages: [1],

    init: function(options) {
      this.reset();
      this.visibleToolbars.push(this.TOOLBAR_MAIN);
      this.visibleToolbars.push(this.TOOLBAR_PAGES);
      this.setButtons(this.getDefaultButtons());
      angular.extend(this, options);
    },

    generatePages: function(maxPages) {
      var min = this.currentPage - Math.floor((maxPages - 1) / 2);
      if (min < 1) min = 1;
      var max = min + maxPages - 1;
      if (max > this.pageCount) max = this.pageCount;
      if (min > max - maxPages + 1 && max - maxPages + 1 > 0) min = max - maxPages + 1;
      var pages = [];
      for (var page = min; page <= max; page++) {
        pages.push(page);
      }
      this.pages = pages;
      return pages;
    },

    reset: function() {
      this.visibleToolbars.length = 0;
      this.buttons.main.length = 0;
      this.buttons.search.length = 0;
      this.buttons.pages.length = 0;
      this.clearFilters();
      this.currentPage = 1;
      this.pageCount = 1;
      this.pages.length = 0;
      this.pages.push(1);
    },

    clearFilters: function() {
      this.filterOptions.filterText = '';
    },

    setVisibleToolbars: function(toolbars) {
      this.visibleToolbars.length = 0;
      if (!toolbars) return;
      if (!angular.isArray(toolbars)) toolbars = [toolbars];
      angular.extend(this.visibleToolbars, toolbars);
    },

    setButtons: function(buttons) {
      if (!angular.isObject(buttons)) buttons = {};
      this.setMainButtons(buttons.main);
      this.setSearchButtons(buttons.search);
      this.setPagesButtons(buttons.pages);
    },

    setMainButtons: function(buttons) {
      this.buttons.main.length = 0;
      if (!buttons) return;
      if (!angular.isArray(buttons)) buttons = [buttons];
      angular.extend(this.buttons.main, buttons);
    },

    setSearchButtons: function(buttons) {
      this.buttons.search.length = 0;
      if (!buttons) return;
      if (!angular.isArray(buttons)) buttons = [buttons];
      angular.extend(this.buttons.search, buttons);
    },

    setPagesButtons: function(buttons) {
      this.buttons.pages.length = 0;
      if (!buttons) return;
      if (!angular.isArray(buttons)) buttons = [buttons];
      angular.extend(this.buttons.pages, buttons);
    },

    getDefaultButtons: function() {
      return {
        main:   this.getDefaultMainButtons(),
        search: this.getDefaultSearchButtons(),
        pages:  this.getDefaultPagesButtons()
      };
    },

    getDefaultMainButtons: function() {
      return [
        this.BUTTON_ADD, this.BUTTON_SHOW_SEARCH, this.BUTTON_REFRESH,
        this.BUTTON_PRINT, this.BUTTON_PREV_PAGE, this.BUTTON_NEXT_PAGE
      ];
    },

    getDefaultSelectButtons: function() {
      return [this.BUTTON_EDIT, this.BUTTON_REMOVE, this.BUTTON_PRINT];
    },

    getDefaultSelectsButtons: function() {
      return [this.BUTTON_REMOVE, this.BUTTON_PRINT];
    },

    getDefaultItemButtons: function() {
      return [this.BUTTON_BACK, this.BUTTON_SAVE];
    },

    getDefaultSearchButtons: function() {
      return [this.BUTTON_BACK, this.BUTTON_SEARCH];
    },

    getDefaultPagesButtons: function() {
      return [this.BUTTON_PREV_PAGE, this.BUTTON_PAGE, this.BUTTON_NEXT_PAGE];
    }
  };
})

.directive('sgToolbarMain', function($rootScope, toolbar) {
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: 'app/directives/templates/toolbar-main.html',

    link: function(scope, element, attrs) {
      scope.buttons = toolbar.buttons.main;
      scope.$watch(function() { return toolbar.currentPage; }, function(value) { scope.currentPage = value; });
      scope.$watch(function() { return toolbar.pageCount; }, function(value) { scope.pageCount = value; });

      scope.btnClick = function(key) {
        var eventName = (key === 'prev-page' || key === 'next-page') ? 'toolbar-pages-click' : 'toolbar-main-click';
        switch (key) {
        case 'prev-page':
          if (toolbar.currentPage > 1) {
            $rootScope.$broadcast(eventName, 'page', toolbar.currentPage - 1);
          }
          break;
        case 'next-page':
          if (toolbar.currentPage < toolbar.pageCount) {
            $rootScope.$broadcast(eventName, 'page', toolbar.currentPage + 1);
          }
          break;
        }
        $rootScope.$broadcast(eventName, key);
      };
    }
  };
})

.directive('sgToolbarSearch', function($rootScope, toolbar) {
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: 'app/directives/templates/toolbar-search.html',

    link: function(scope, element, attrs) {
      scope.buttons = toolbar.buttons.search;
      scope.filterOptions = toolbar.filterOptions;

      scope.btnClick = function(key, options) {
        if (options) {
          $rootScope.$broadcast('toolbar-search-click', key, options);
        } else {
          $rootScope.$broadcast('toolbar-search-click', key);
        }
      };
    }
  };
})

.directive('sgToolbarPages', function($rootScope, toolbar) {
  var MAX_PAGES = 5;

  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: 'app/directives/templates/toolbar-pages.html',

    link: function(scope, element, attrs) {
      scope.buttons = toolbar.buttons.pages;
      scope.$watch(function() { return toolbar.currentPage; }, function(value) { scope.currentPage = value; });
      scope.$watch(function() { return toolbar.pageCount; }, function(value) { scope.pageCount = value; });

      // Watch current page
      scope.$watch(function() { return toolbar.currentPage; }, function(current) {
        scope.pages = toolbar.generatePages(MAX_PAGES);
      });

      // Watch page count
      scope.$watch(function() { return toolbar.pageCount; }, function(count) {
        scope.pages = toolbar.generatePages(MAX_PAGES);
      });

      scope.btnClick = function(key, options) {
        switch (key) {
        case 'prev-page':
          if (toolbar.currentPage > 1) {
            $rootScope.$broadcast('toolbar-pages-click', 'page', toolbar.currentPage - 1);
          }
          break;
        case 'next-page':
          if (toolbar.currentPage < toolbar.pageCount) {
            $rootScope.$broadcast('toolbar-pages-click', 'page', toolbar.currentPage + 1);
          }
          break;
        }

        if (options) {
          $rootScope.$broadcast('toolbar-pages-click', key, options);
        } else {
          $rootScope.$broadcast('toolbar-pages-click', key);
        }
      };
    }
  };
});
