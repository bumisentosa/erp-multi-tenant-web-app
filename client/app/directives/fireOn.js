'use strict';
directives

/**
 * Trigger click event dari elemen saat menerima broadcast sesuai dengan
 * nama event parameter.
 */
.directive('fireOn', function($timeout) {
  return {
    link: function(scope, elm, attrs) {
      scope.$on(attrs.fireOn, function() {
        $timeout(function() {
            // TODO: Harus menggunakan jQuery
            $(elm).trigger('click');
        });
      });
    }
  };
});
