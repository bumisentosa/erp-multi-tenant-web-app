'use strict';
directives

.directive('ngThumb', function($window) {
  function isFile(item) {
    return angular.isObject(item) && item instanceof $window.File;
  }
  function isImage(file) {
    if (!isFile(file)) return false;
    var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
  }

  return {
    restrict: 'A',
    scope: { file: '=ngThumb' },

    link: function(scope, element, attrs) {
      var style = element.attr('style') || '';
      if (style) style += ' ';

      if (isImage(scope.file)) {
        if (!$window.FileReader) return;
        var reader = new FileReader();
        reader.readAsDataURL(scope.file);
        reader.onload = function(file) {
          style += 'background-image: url(' + file.target.result + ');';
          element.attr('style', style);
        };
      } else if (scope.file) {
        style += 'background-image: url(\'' + scope.file + '\');';
        element.attr('style', style);
      }
    }
  };
});
