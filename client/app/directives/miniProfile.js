'use strict';
directives

.directive('sgMiniProfile', function() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: { displayName: '@', roleName: '@', photoUrl: '@' },
    templateUrl: 'app/directives/templates/mini-profile.html'
  };
});
