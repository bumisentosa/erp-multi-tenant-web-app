'use strict';
directives

.directive('sgMenuBar', function() {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    template: '<ul class="nav navbar-nav" ng-transclude></ul>'
  };
})

.directive('sgMenuItem', function() {
  return {
    restrict: 'E',
    require: '^sgMenuBar',
    replace: true,
    transclude: true,
    scope: { href: '@' },
    template: '<li><a href="{{href}}" ng-transclude></a></li>'
  };
})

.directive('sgDropdownMenu', function() {
  return {
    restrict: 'E',
    require: '^sgMenuBar',
    replace: true,
    transclude: true,
    scope: { text: '@' },

    template: '<li class="dropdown">' +
                '<a class="dropdown-toggle" href="#">' +
                  '{{text}} <i class="caret"></i>' +
                '</a>' +
                '<ul class="dropdown-menu" ng-transclude></ul>' +
              '</li>'
  };
})

.directive('sgDropdownSubmenu', function() {
  return {
    restrict: 'E',
    require: '^sgDropdownMenu',
    replace: true,
    transclude: true,
    scope: { text: '@' },

    template: '<li class="dropdown-submenu">' +
                '<a class="dropdown-toggle" href="#">{{text}}</a>' +
                '<ul class="dropdown-menu" ng-transclude></ul>' +
              '</li>'
  };
})

.directive('sgMenuDivider', function() {
  return {
    restrict: 'E',
    require: '^sgMenuBar',
    replace: true,
    transclude: true,
    template: '<li class="divider"></li>'
  };
});
