'use strict';

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    path: {
      client: 'client',
      dist: 'dist'
    },

    // Clean
    // ------------------------------------------
    clean: {
      dist: '<%= path.dist %>/*'
    },

    // Watch Files
    // ------------------------------------------
    watch: {
      less: {
        files: ['<%= path.client %>/less/*.less'],
        tasks: ['less:dev']
      },
      livereload: {
        options: {
          livereload: true
        },
        files: [
          '<%= path.client %>/app/**/*.{html,js}',
          '<%= path.client %>/css/*.css',
          '<%= path.client %>/{css,img}/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // Auto Prefix CSS3
    // ------------------------------------------
    autoprefixer: {
      options: ['last 2 version'],
      client: {
        files: [{
          expand: true,
          src: ['<%= path.client %>/css/*.css']
        }]
      }
    },

    // Less CSS
    // ------------------------------------------
    less: {
      dev: {
        options: {
          paths: '<%= path.client %>',
          sourceMap: true,
          sourceMapRootpath: '/',
          sourceMapBasepath: 'client',
          ieCompat: true
        },
        files: [{
          expand: true,
          cwd: '<%= path.client %>/less',
          dest: '<%= path.client %>/css',
          src: [
            'bootstrap.less',
            'login.less',
            'home.less',
            'app.less'
          ],
          ext: '.css'
        }]
      },

      dist: {
        options: {
          paths: '<%= path.client %>',
          cleancss: true,
          ieCompat: true
        },
        files: [{
          expand: true,
          cwd: '<%= path.client %>/less',
          dest: '<%= path.dist %>/css',
          src: [
            'bootstrap.less',
            'login.less',
            'home.less',
            'app.less'
          ],
          ext: '.css'
        }]
      }
    },

    // JSHint
    // ------------------------------------------
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: ['<%= path.client %>/app/**/*.js']
    },

    // Rev
    // ------------------------------------------
    rev: {
      dist: {
        files: {
          src: [
            '<%= path.dist %>/app/*.js',
            '<%= path.dist %>/css/*.css',
            '<%= path.dist %>/img/**/*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= path.dist %>/fonts/*.{eot,svg,ttf,woff}'
          ]
        }
      }
    },

    // Concat Scripts
    // ------------------------------------------
    concat: {
      options: {
        banner: '(function($, angular, undefined) {\n',
        footer: '})(jQuery, angular);'
      },
      crm: {
        files: {
          '<%= path.dist %>/app/crm.min.js': [
            '<%= path.client %>/app/app.js',
            '<%= path.client %>/app/layout.js',
            '<%= path.client %>/app/{controllers,directives,filters,services}/**/*.js',
            '<%= path.client %>/app/modules/crm/**/*.js',
          ]
        }
      },
      frm: {
        files: {
          '<%= path.dist %>/app/frm.min.js': [
            '<%= path.client %>/app/app.js',
            '<%= path.client %>/app/layout.js',
            '<%= path.client %>/app/{controllers,directives,filters,services}/**/*.js',
            '<%= path.client %>/app/modules/frm/**/*.js',
          ]
        }
      },
      hrm: {
        files: {
          '<%= path.dist %>/app/hrm.min.js': [
            '<%= path.client %>/app/app.js',
            '<%= path.client %>/app/layout.js',
            '<%= path.client %>/app/{controllers,directives,filters,services}/**/*.js',
            '<%= path.client %>/app/modules/hrm/**/*.js',
          ]
        }
      },
      pm: {
        files: {
          '<%= path.dist %>/app/pm.min.js': [
            '<%= path.client %>/app/app.js',
            '<%= path.client %>/app/layout.js',
            '<%= path.client %>/app/{controllers,directives,filters,services}/**/*.js',
            '<%= path.client %>/app/modules/pm/**/*.js',
          ]
        }
      }
    },

    // NG Min
    // ------------------------------------------
    ngmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= path.dist %>/app',
          dest: '<%= path.dist %>/app',
          src: '*.js'
        }]
      }
    },

    // Uglify JS
    // ------------------------------------------
    uglify: {
      dist: {
        options: {
          banner: '/*! <%= pkg.name %> v<%= pkg.version %> | (c) 2013-<%= grunt.template.today("yyyy") %> Adi Sayoga */\n'
        },
        files: [{
          expand: true,
          cwd: '<%= path.dist %>/app',
          dest: '<%= path.dist %>/app',
          src: '*.js'
        }]
      }
    },

    // Image Min
    // ------------------------------------------
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= path.client %>',
          dest: '<%= path.dist %>',
          src: '{css,img}/**/*.{png,jpg,jpeg,gif}'
        }]
      }
    },

    // SVG Min
    // ------------------------------------------
    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= path.client %>',
          dest: '<%= path.dist %>',
          src: '{css,img}/**/*.svg',
        }]
      }
    },

    // HTML Min
    // ------------------------------------------
    htmlmin: {
      ngtemplates: {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        removeComments: true,
        removeCommentsFromCDATA: true,
        removeAttributeQuotes: true,
        removeRedundantAttributes: true,
        removeEmptyAttributes: true,
        removeOptionalTags: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    },

    // Angular Templates
    // ------------------------------------------
    ngtemplates: {
      crm: {
        options: {
          base: 'app',
          module: 'crm',
          htmlmin: '<%= htmlmin.ngtemplates %>'
        },
        cwd: '<%= path.client %>',
        src: [
          'app/directives/**/*.html',
          'app/templates/**/*.html',
          'app/modules/crm/**/*.html'
        ],
        dest: '<%= path.dist %>/app/crm-tpls.min.js'
      },

      frm: {
        options: {
          base: 'app',
          module: 'frm',
          htmlmin: '<%= htmlmin.ngtemplates %>'
        },
        cwd: '<%= path.client %>',
        src: [
          'app/directives/**/*.html',
          'app/templates/**/*.html',
          'app/modules/frm/**/*.html'
        ],
        dest: '<%= path.dist %>/app/frm-tpls.min.js'
      },

      hrm: {
        options: {
          base: 'app',
          module: 'hrm',
          htmlmin: '<%= htmlmin.ngtemplates %>'
        },
        cwd: '<%= path.client %>',
        src: [
          'app/directives/**/*.html',
          'app/templates/**/*.html',
          'app/modules/hrm/**/*.html'
        ],
        dest: '<%= path.dist %>/app/hrm-tpls.min.js'
      },

      pm: {
        options: {
          base: 'app',
          module: 'pm',
          htmlmin: '<%= htmlmin.ngtemplates %>'
        },
        cwd: '<%= path.client %>',
        src: [
          'app/directives/**/*.html',
          'app/templates/**/*.html',
          'app/modules/pm/**/*.html'
        ],
        dest: '<%= path.dist %>/app/pm-tpls.min.js'
      },
    },

    // Copy. Taruh file yang tidak di-handle task lain disini.
    // ------------------------------------------
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= path.client %>',
          src: ['*.{ico,txt}', '*.png', '*.xml', '{css,img}/**/*.webp', 'fonts/*'],
          dest: '<%= path.dist %>'
        }, {
          expand: true,
          cwd: '<%= path.client %>/lib',
          src: [
            'html5shiv/dist/{html5shiv.js,html5shiv-printshiv.js}',
            'respond/dest/respond.min.js',
            'jquery/jquery.min.js',
            'underscore/underscore-min.js',
            'underscore/underscore-min.map',
            'angular/angular.min.js',
            'angular/angular.min.js.map',
            'angular-animate/angular-animate.min.js',
            'angular-animate/angular-animate.min.js.map',
            'angular-bootstrap/ui-bootstrap-tpls.min.js',
            'angular-ui-router/release/angular-ui-router.min.js',
            'angular-touch/angular-touch.min.js',
            'angular-touch/angular-touch.min.js.map',
            'angular-resource/dist/resource.min.js'
          ],
          dest: '<%= path.dist %>/lib'
        }]
      }
    },

    // concurrent
    // ------------------------------------------
    concurrent: {
      dev: [
        'less:dev'
      ],
      dist: [
        'copy:dist',
        'less:dist',
        'ngtemplates',
        'imagemin:dist',
        'svgmin:dist',
      ]
    },

    // Karma Test
    // ------------------------------------------
    karma: {
      unit: {
        singleRun: true,
        configFile: 'config/karma.conf.js'
      },
      'crm-unit': {
        singleRun: true,
        configFile: 'config/karma-crm.conf.js'
      },
      'frm-unit': {
        singleRun: true,
        configFile: 'config/karma-frm.conf.js'
      },
      'hrm-unit': {
        singleRun: true,
        configFile: 'config/karma-hrm.conf.js'
      },
      'pm-unit': {
        singleRun: true,
        configFile: 'config/karma-pm.conf.js'
      }
    }
  });

  // Build
  // ------------------------------------------
  grunt.registerTask('build', [
    'clean:dist',
    'concurrent:dist',
    'concat',
    'ngmin',
    'uglify'
  ]);

  // Serve
  // ------------------------------------------
  grunt.registerTask('serve', [
    'concurrent:dev',
    'watch'
  ]);

  // Default
  // ------------------------------------------
  grunt.registerTask('default', [
    'jshint',
    'karma',
    'build'
  ]);
};
